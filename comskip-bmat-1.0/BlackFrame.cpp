#include "BlackFrame.h"

namespace CS {

bool operator < (const BlackFrame &f1, const BlackFrame &f2)
{
#if 1
	return f1.frame < f2.frame;
#else
	return f1.frame < f2.frame
			|| f1.cause < f2.cause
			|| f1.brightness < f2.brightness
			|| f1.uniform < f2.uniform
			|| f1.volume < f2.volume;
#endif
}

bool operator > (const BlackFrame &f1, const BlackFrame &f2)
{
#if 1
	return f1.frame > f2.frame;
#else
	return f1.frame > f2.frame
			|| f1.cause > f2.cause
			|| f1.brightness > f2.brightness
			|| f1.uniform > f2.uniform
			|| f1.volume > f2.volume;
#endif
}

bool operator == (const BlackFrame &f1, const BlackFrame &f2)
{
#if 1
	return f1.frame == f2.frame;
#else
	return f1.frame == f2.frame
			&& f1.cause == f2.cause
			&& f1.brightness == f2.brightness
			&& f1.uniform == f2.uniform
			&& f1.volume == f2.volume;
#endif
}

} // namespace

