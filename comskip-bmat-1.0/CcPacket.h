#pragma once

#include <stdint.h>

namespace CS {

class CcPacket
{
public:
	CcPacket()
	{
		_b[0] = 0;
		_b[1] = 0;
	}
	CcPacket(const CcPacket &o)
	{
		_b[0] = o._b[0];
		_b[1] = o._b[1];
	}
	void reset(void)
	{
		_b[0] = 0;
		_b[1] = 0;
	}

	uint8_t _b[2];
};

static inline bool operator == (const CcPacket &o1, const CcPacket &o2)
{
	return o1._b[0] == o2._b[0] && o1._b[1] == o2._b[1];
}

} // namespace CS

