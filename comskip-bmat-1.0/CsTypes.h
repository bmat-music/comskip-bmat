#pragma once

#include <vector>
#include <cstdint>

namespace CS {

typedef std::vector<uint8_t> U8Vec;

} // namespace CS

