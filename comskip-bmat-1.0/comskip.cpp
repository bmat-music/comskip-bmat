// comskip.c
// Copyright (C) 2004 Scott Michael
// Based on the work of Chris Pinkham of MythTV
// comskip is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// comskip is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS 1
#endif
#include <inttypes.h>
#include <stdio.h>
#ifdef _WIN32
#include <conio.h>
#include <windows.h> // needed for sleep command
#include <direct.h> // needed for getcwd
#include <process.h>
#include <io.h>
#else
#include <errno.h>
#endif
#include <algorithm>
#include <vector>
#include <utility>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <memory.h>
#include <time.h> // needed for play_nice routines
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
//#include "config.h"
#include "comskip.h"

// Define logo detection directions
#define HORIZ	0
#define VERT	1
#define DIAG1	2
#define DIAG2	3

// Define aspect ratio
#define FULLSCREEN true
#define WIDESCREEN false

// max_volume / DEEP_SILENCE defines deep silence
#define DEEP_SILENCE 6

#define OPEN_INPUT	1
#define OPEN_INI	2
#define SAVE_DMP	3
#define SAVE_INI	4

// max number of frames that can be marked
#define MAX_IDENTIFIERS 300000
#define MAX_COMMERCIALS 50000
bool initialized;
char cs_field_t;

#define KDOWN	1
#define KUP		2
#define KLEFT	3
#define KRIGHT	4
#define KNEXT	5
#define KPREV	6
#undef FRAME_WITH_HISTOGRAM
#undef FRAME_WITH_LOGO
#undef FRAME_WITH_AR

namespace CS {

int commDetectMethod
		= DET::BLACK_FRAME
		| DET::LOGO
		| DET::RESOLUTION_CHANGE
		| DET::AR
		| DET::SILENCE;

int max_repair_size(40);
int variable_bitrate(1);
double fps(25.0);

int skip_B_frames;
// search for a logo for up to x seconds
int giveUpOnLogoSearch(2000);
double min_show_segment_length(120.0);

bool live_tv;
bool doProcessCc;
char HomeDir[256]; // comskip home directory
bool disable_heuristics;
bool startOverAfterLogoInfoAvail(true);
bool secondLogoSearch;
bool sage_framenumber_bug;
bool sage_minute_bug;
bool cut_on_ar_change(true);
int _frameCount;
int lastFrame;
int lastFrameCommCalculated;
int CutScene::delta(10);

bool delete_show_after_last_commercial;
bool delete_show_before_first_commercial;
int delete_show_before_or_after_current;

// variables defining options with defaults
// frames per second (NTSC=29.970, PAL=25)
// border around edge of video to ignore
int border(10);
// border from bottom to ignore
int ticker_tape;
int ticker_tape_percentage;
int ignore_side;
int added_recording(14);
int volume_slip(40);

char cuttermaran_options[1024];
char mpeg2schnitt_options[1024];
char avisynth_options[1024];
char dvrcut_options[1024];

bool ccCheck;
double cc_commercial_type_modifier(4.0);
double cc_wrong_type_modifier(2.0);
double cc_correct_type_modifier(0.75);

double black_percentile(0.0076);
double uniform_percentile(0.003);
double score_percentile(0.71);
double logo_percentile(0.92);
double ar_delta(0.08);

int padding;
int remove_before;
int remove_after;

int delete_block_after_commercial;
int min_commercial_break_at_start_or_end(39);
int always_keep_first_seconds;
int always_keep_last_seconds;
bool intelligent_brightness;
double length_strict_modifier(3.0);
double length_nonstrict_modifier(1.5);
double combined_length_strict_modifier(2.0);
double combined_length_nonstrict_modifier(1.25);
double ar_wrong_modifier(2.0);
double excessive_length_modifier(0.01);
double dark_block_modifier(0.3);
double min_schange_modifier(0.5);
double max_schange_modifier(2.0);
double logo_present_modifier(0.01);
double punish_modifier(2.0);
double punish_threshold(1.3);
double reward_modifier(0.5);

bool punish_no_logo(true);
int punish;
int reward;

// If no logo is identified after x seconds into the show - give up.
int delay_logo_search;
double logo_max_percentage_of_screen(0.12);
int subtitles;
int logo_at_bottom;
#if 0
int edge_level_threshold(5);
int edge_radius(2);
int edge_weight(10);
int edge_step(1);
#endif

double logo_threshold(0.80);
int logo_filter;
bool connect_blocks_with_logo(true);
double logo_fraction(0.40);
double shrink_logo(5.0);
int shrink_logo_tail;
int before_logo;
int after_logo;
int where_logo;

unsigned int min_black_frames_for_break(1);
int standoff;
int dvrmsstandoff(120000);
// set to true to only mark breaks divisible by 5 as a commercial.
bool require_div5;
double div5_tolerance(-1);
int incommercial_frames(1000);
int videoredo_offset(2);
bool deleteLogoFile(true);
char windowtitle[1024] = "Comskip - %s";
bool enable_mencoder_pts;
int csBrightnessLast;

int min_brightness_found;

uint8_t ccData[500];
int ccDataLen;

int volumeScale(10);
int framenum_real;
double last_ar_ratio(0.0);
int min_hasBright(255000);
int min_dimCount(255000);
const int schange_threshold(90);
double logoFreq(1.0); // times fps between logo checks
int framesprocessed;

std::string secondsToString(double seconds)
{
	int hours((int)(seconds / 3600));
	seconds -= hours * 60 * 60;
	int minutes((int)(seconds / 60));
	seconds -= minutes * 60;

	char buf[32];

	::snprintf(buf, sizeof(buf)
			, "%0i:%.2i:%.2d.%.2d"
			, hours, minutes, (int)seconds
			, (int)((seconds - (int)(seconds))*100) );

	return buf;
}

std::string secondsToString(int seconds)
{
	int hours((int)(seconds / 3600));
	seconds -= hours * 60 * 60;
	int minutes((int)(seconds / 60));
	seconds -= minutes * 60;

	char buf[32];

	::snprintf(buf, sizeof(buf)
			, "%i:%.2i:%.2i"
			, hours, minutes, seconds);

	return buf;
}

#if 0
static int aaa;
#define assert(T) (aaa = ((T) ? 1 : *(int *)0))
#endif
#define F2V(X) ((X) <= 0 ? _frame[1].pts : ((X) >= _frameCount ? _frame[_frameCount - 1].pts : _frame[X].pts ))
#define F2T(X) (F2V(X) - _frame[1].pts)
#define F2L(X,Y) (F2V(X) - F2V(Y))
#define F2F(X) ((int) (F2T(X) * fps + 1.5 ))

class BlockInfo
{
public:
	int f_start;
	int f_end;
	unsigned int b_head;
	unsigned int b_tail;
	unsigned int bframe_count;
	unsigned int schange_count;
	double schange_rate; // in changes per second
	double length;
	double score;
	int combined_count;
	int cc_type;
	double ar_ratio;
	int cause;
	int more;
	int less;
	int brightness;
	int volume;
	int silence;
	int uniform;
	int stdev;
	char reffer;
	double logo;
	double correlation;
	int strict;
	bool iscommercial;
	static int count;
	static int max;
};

#define cblock cblock
#define MAX_BLOCKS 1000
BlockInfo cblock[MAX_BLOCKS];
int BlockInfo::count;
int BlockInfo::max;

class XdsBlockInfo
{
public:
	int frame;
	char name[40];
	int v_chip;
	int duration;
	int position;
	int composite1;
	int composite2;
};

XdsBlockInfo *XDS_block;
int XDS_block_count;
int max_XDS_block_count;

int commercial_count(-1);

class Commercial
{
public:
	int start_frame;
	int end_frame;
	int start_block;
	int end_block;
	double length;
};

Commercial commercial[MAX_COMMERCIALS];

int reffer_count(-1);

class Reffer
{
public:
	int start_frame;
	int end_frame;
};

Reffer reffer[MAX_COMMERCIALS];

double dominant_ar;

int curvolume(-1);
int ascr;
int scr;
char tempString[256];
int csBrightness;
int max_logo_gap;
int max_nonlogo_block_length;
double logo_overshoot;
double logo_quality;
int old_width;
int old_height;
#ifdef USE_AR_WIDTH
int ar_width;
#endif

int min_volume;
int min_uniform;

int after_start;
int before_end;
int doublCheckLogoCount;

int schange_cutlevel(15);

int avg_brightness;
int maxi_volume;
int avg_volume;
int avg_silence;
int avg_uniform;
double avg_schange(0.0);
double dictionary_modifier(1.05);
// int dummy1;
// int dummy2;
// bool frameIsBlack;
// bool lastFrameWasBlack = false;
// bool lastFrameWasSceneChange = false;
// bool sceneHasChanged;
// bool detectSceneChanges;
// bool detectBlackFrames;
int sceneChangePercent;
int histogram[256];
int lastHistogram[256];

int cutscenematch;

char debugText[20000];
#if 0
int *logoFrameNum; // Keep track of the frame numbers of each buffer
uint8_t **logoFrameBuffer; // rotating storage for frames
int logoFrameBufferSize;
int newestLogoBuffer = -1; // Which buffer is the newest? Increments prior to fill so start at -1
int oldestLogoBuffer; // Which buffer is the oldest?
bool logoBuffersFull;
#endif

int minHitsForTrend(10);
// bool hindsightLogoState = true;
double logoPercentage(0.0);
bool reverseLogoLogic;
#ifdef MAXMIN_LOGO_SEARCH
double borderIgnore = .05; // Percentage of each side to ignore for logo detection
#endif

int edge_count;
int lwidth;
int lheight;

int cc_count[5] = { 0, 0, 0, 0, 0 };
uint8_t **cc_screen;
uint8_t **cc_memory;
#ifdef _WIN32
int helpflag;
int timeflag;
#endif
int recalculate;

double currentGoodEdge(0.0);

bool LengthWithinTolerance(double test_length
				, double expected_length, double tolerance);

int GetAvgBrightness(void);
bool CheckFrameIsBlack(void);
void BuildBlackFrameCommList(void);
void BuildBlackFrameAndLogoCommList(void);
char CheckFramesForCommercial(int start, int end);
char CheckFramesForReffer(int start, int end);
const char *CcTypeToStr(int type);
int CEW_init(int argc, char *argv[]);

bool CheckOddParity(uint8_t ch);
double AverageARForBlock(int start, int end);
void SetARofBlocks(void);
int FindBlock(int frame);

char *CauseString(int i)
{
	static char cs[4][80];
	static int ii = 0;
	char *c = &(cs[ii][0]);
	char *rc = &(cs[ii][0]);

	*c++ = (i & C_H7		? '7' : ' ');
	*c++ = (i & C_H6		? '6' : ' ');
	*c++ = (i & C_H5		? '5' : ' ');
	*c++ = (i & C_H4		? '4' : ' ');
	*c++ = (i & C_H3		? '3' : ' ');
	*c++ = (i & C_H2		? '2' : ' ');
	*c++ = (i & C_H1		? '1' : ' ');

	if ( strncmp((char*)cs[ii],"       ",7) )
		*c++ = '{';
	else
		*c++ = ' ';

	*c++ = (i & C_SC		? 'F' : ' ');
	*c++ = (i & C_AR		? 'A' : ' ');
	*c++ = (i & C_EXCEEDS	? 'E' : ' ');
	*c++ = (i & C_LOGO		? 'L' : (i & C_BRIGHT ? 'B': ' '));
	*c++ = (i & C_COMBINED ? 'C' : ' ');
	*c++ = (i & C_NONSTRICT? 'N' : ' ');
	*c++ = (i & C_STRICT	? 'S' : ' ');
	*c++ = (i & 2			? 'c' : (i & C_t ? 't': ' '));
	*c++ = (i & 1			? 'l' : (i & C_v ? 'v': ' '));
	*c++ = (i & 4			? 's' : ' ');
	*c++ = (i & 32			? 'a' : ' ');
	*c++ = (i & 8			? 'u' : ' ');
	*c++ = (i & 16			? 'b' : ' ');
	*c++ = (i & C_r			? 'r' : ' ');
	*c++ = 0;

	ii = (ii + 1) % 4;

	return rc;
}

FILE *FlagFile::open(const char *filename, const char *options
		, bool throwOnError) throw(FileException)
{
	if ( f )
		::fclose(f);

	f = ::fopen(filename, options);

	if ( f || ! throwOnError )
		return f;

	throw FileException(std::string("FileException: ")
					+ std::string(filename)
					+ ": " + strerror(errno));
}

void FlagFile::printf(const char *fmt, ...)
{
	va_list args;
	::va_start(args, fmt);
	::vfprintf(f, fmt, args);
	::va_end(args);
}

FlagFile::~FlagFile()
{
	close();
}

void FlagFile::close(void)
{
	if ( f )
	{
		::fclose(f);
		f = 0;
	}
}

double CommercialSkipper::validateBlackFrames(int reason
	, double ratio, int remove)
{
	if ( ratio == 0.0 )
		return 0.0;

	const char *r(" -undefined- ");

	if ( reason == C_b )
		r = "Black Frame  ";
	else if ( reason == C_v )
		r = "Volume       ";
	else if ( reason == C_s )
		r = "Scene Change ";
	else if ( reason == C_c )
		r = "Change       ";
	else if ( reason == C_u )
		r = "Uniform Frame";
	else if ( reason == C_a )
		r = "Aspect Ratio ";
	else if ( reason == C_t )
		r = "Cut Scene    ";
	else if ( reason == C_l )
		r = "Logo         ";

#ifndef undef
	int negative_count(0);
	int positive_count(0);
	int last(0);
	double length(0.0);
	double summed_length(0.0);

	bool incommercial(false);

	const size_t blackSize(csBlack.size());

	for ( size_t i = 0; i < blackSize; )
	{
		while (i < blackSize && (csBlack[i].cause & reason) == 0)
			++i;

		size_t k(i);

		while (k < (blackSize - 1)
				&& (csBlack[k+1].cause & reason) != 0
				&& csBlack[k+1].frame == csBlack[k].frame + 1 )
			++k;

		if ( i < blackSize )
		{
			length = F2T(csBlack[(i+k)/2].frame) - F2T(csBlack[last].frame);

			if ( length > _commBreakSize._size.max )
			{
				if ( incommercial )
				{
					incommercial = false;

					if ( summed_length < _commBreakSize._break.min
							&& summed_length > 4.7
							&& csBlack[(i+k)/2].frame < _frameCount * 6 / 7
							&& csBlack[last].frame > _frameCount * 1 / 7 )
					{
						++negative_count;
						Debug (10, "Negative %s cutpoint at %6i, commercial too short\n"
								, r, csBlack[last].frame);
					}
					else
						++positive_count;

					summed_length = 0.0;
				}
				else
				{
					++positive_count;
				}

				summed_length = 0.0;
			}
			else
			{
				summed_length += length;

				if ( incommercial && summed_length > _commBreakSize._break.max )
				{
					if ( csBlack[(i+k)/2].frame < _frameCount * 6 / 7 )
					{
						++negative_count;
						Debug (10, "Negative %s cutpoint at %6i,"
								" commercial too long\n"
								, r, csBlack[(i+k)/2].frame);
					}
				}
				else
				{
					++positive_count;
					incommercial = true;
				}
			}
		}

		last = ( i + k ) / 2;
		i = k + 1;
	}

	Debug (1,"Distribution of %s cutting: %3i positive and %3i negative, ratio is %6.4f\n"
			, r, positive_count, negative_count
			, (negative_count > 0) ? (double)positive_count / (double)negative_count : 9.99);

	if ( (logoPercentage < logo_fraction || logoPercentage > logo_percentile)
			&& negative_count > 1 )
	{
		Debug (1,"Confidence of %s cutting: %3i negative"
				" without good logo is too much\n"
				, r, negative_count);

		if ( remove )
		{
			int i(0);
			std::vector<int> removeBlack;
                        
			//for ( BlackFrame &bf : csBlack )
                        for(int i=0; i < csBlack.size(); i++)
			{
				if ( csBlack[i].cause & reason )
				{
					csBlack[i].cause &= ~reason;

					if ( csBlack[i].cause == 0 )
						removeBlack.push_back(i++);
				}
			}

			while ( ! removeBlack.empty() )
			{
				csBlack.erase(csBlack.begin() + removeBlack.back());
				removeBlack.pop_back();
			}
		}

		/*
		if (negative_count > 1 && reason == C_v)
		{
			Debug(1, "Too mutch Silence Frames, disabling silence detection\n");
			commDetectMethod &= ~SILENCE;
		}

		if (negative_count > 1 && reason == C_s)
		{
			Debug(1, "Too mutch Scene Change, disabling Scene Change detection\n");
			commDetectMethod &= ~SCENE_CHANGE;
		}
		*/
	}

#endif

	int strict_count(0);
	int count(0);

	{
		const int blackSizeInt((int)csBlack.size());
		int prev_cause(0);

		for ( int i = 1; i < blackSizeInt; )
		{
			int total_cause(csBlack[i].cause);
			int k(i);

			while ( k < blackSizeInt && csBlack[k+1].frame == csBlack[k].frame + 1 )
			{
				++k;
				total_cause |= csBlack[k].cause;
			}
#if 0
			last_length = ( k - i ) / 2;
#endif
			last = ( i + k ) / 2;

			if ( (total_cause & reason) && (prev_cause & reason) )
			{
				int j(i - 1);

				while ( j > 0 && csBlack[j-1].frame == csBlack[j].frame - 1 )
					--j;

				length = F2T(csBlack[i].frame) - F2T(csBlack[(i-1+j)/2].frame);

				if ( length > 1.0 && length < _commBreakSize._size.max )
				{
					++count;

					if ( isStandardCommercialLength(length, F2T(i) - F2T(j) + 0.8 , false))
					// if (length > _commBreakSize._size.max)
					{
						++strict_count;
					}
				}
			}

			prev_cause = reason;
			++k;
			i = k;
		}
	}

	if ( strict_count < 2 || 100 * strict_count < 100 * count / ratio )
	{
		Debug (1,"Confidence of %s cutting: %3i out of %3i are strict, too low\n"
				, r, strict_count, count);

		if ( remove )
		{
			int i(0);
			std::vector<int> removeBlack;
                        for(int i=0; i < csBlack.size(); i++)
			//for ( BlackFrame &bf : csBlack )
			{
				if ( csBlack[i].cause & reason )
				{
					csBlack[i].cause &= ~reason;

					if ( csBlack[i].cause == 0 )
						removeBlack.push_back(i++);
				}
			}

			while ( ! removeBlack.empty() )
			{
				csBlack.erase(csBlack.begin() + removeBlack.back());
				removeBlack.pop_back();
			}
		}
	}
	else
		Debug (1,"Confidence of %s cutting: %3i out of %3i are strict\n"
					, r, strict_count, count);

	return ( count > 0 ) ? (double)strict_count / (double) count : 0.0;
}

bool CommercialSkipper::buildBlocks(bool recalc)
{
// Function code blocks

	// int i = 0;
	// int k = 0;
	// int j = 0;
	// int a = 0;
	int count;
	int v_count;
	int b_count;
	int black_start;
	int black_end;
	int cause = 0;
	int black_threshold;
	int uniform_threshold;
	int prev_start = 1;
	int prev_head = 0;

	int b_start;
	int b_end;
	int b_counted;
//	char *t = "";

	BlockInfo::max = MAX_BLOCKS;
	BlockInfo::count = 0;
	recalculate = recalc;
	initializeBlockArray(0);

	/*
		// If there are no black frames, nothing can be done
		if ( ! Black_frameCount && ! ar_block_count )
			return false;
	*/

	// outputHistogram(_histogram.volume, volumeScale, "Volume", true);

	if ( ! recalc )
	{
		// Eliminate frames that are too bright from black frame list
		if ( intelligent_brightness )
		{
			outputBrightHistogram();
			_maxAvgBrightness = black_threshold = findBlackThreshold(black_percentile);
			Debug(1, "Setting brightness threshold to %i\n", black_threshold);
		}

		if ( (intelligent_brightness && _nonUniformity > 0)
#if 0
				|| (commDetectMethod & DET::BLACK_FRAME
					&& _nonUniformity == 0)
#endif
			)
		{
			outputUniformHistogram ();
			_nonUniformity = uniform_threshold = findUniformThreshold(uniform_percentile);
			Debug(1, "Setting uniform threshold to %i\n", uniform_threshold);

			if ( commDetectMethod & DET::BLACK_FRAME )
			{
				for ( int i = 1; i < _frameCount; ++i )
				{
					_frame[i].isblack &= ~C_u;

					if (/* ! (_frame[i].isblack & C_b) && */
							_nonUniformity > 0
							&& _frame[i].uniform < _nonUniformity
							&& _frame[i].brightness < 180
							/* && _frame[i].volume < _maxVolume */
							)
						insertBlackFrame(i, (int)C_u);
				}
			}
		}
	}

	if ( _nonUniformity < min_uniform + 100 )
		_nonUniformity = min_uniform + 100;

	// find minumum volume around black frame
	if ( _useFrameArray )
	{
		const int blackSizeInt((int)csBlack.size());

		for ( int k = blackSizeInt - 1; k >= 0; --k )
		{
			BlackFrame &bf(csBlack[k]);

			if ( bf.cause == C_s || bf.cause == C_c || bf.cause == (C_c|C_s) )
			{
				// Find quality of silence around black frame
				int i(bf.frame - volume_slip);

				if ( i < 0 )
					i = 0;

				int j(bf.frame + volume_slip);

				if ( j > _frameCount )
					j = _frameCount;

				count = 0;

				for ( int a = i; a < j; ++a )
				{
					if ( _frame[a].volume < _maxVolume / 4 )
						count += volume_slip;
					else if ( _frame[a].volume < _maxVolume )
						++count;
				}

				bf.volume = ( count > volume_slip / 4 )
						? (_maxVolume / 2)
						: (_maxVolume * 10);
			}
			else
			{
				// Find minimum volume around black frame
				int i(bf.frame - volume_slip);

				if ( i < 0 )
					i = 0;

				int j(bf.frame + volume_slip);

				if ( j > _frameCount )
					j = _frameCount;

				for ( int a = i; a < j; ++a )
					if ( _frame[a].volume >= 0 && bf.volume > _frame[a].volume )
						bf.volume = _frame[a].volume;
			}
		}

		for ( int k = ar_block_count - 1; k >= 0; --k )
		{
			int i(ar_block[k].end - volume_slip);

			if ( i < 0 )
				i = 0;

			int j(ar_block[k].end + volume_slip);

			if ( j > _frameCount )
				j = _frameCount;

			ar_block[k].volume = _frame[i].volume;

			for ( int a = i; a < j; ++a )
			{
				if ( _frame[a].volume >= 0
						&& ar_block[k].volume > _frame[a].volume )
				{
					ar_block[k].volume = _frame[a].volume;
					ar_block[k].end = a;

					if ( k < ar_block_count - 1 )
						ar_block[k+1].start = a;
				}
			}
		}
	}

	for ( int k = 1; k < 255; ++k )
	{
		if ( _histogram.volume[k] > 10 )
		{
			min_volume = (k-1) * volumeScale;
			break;
		}
	}

	for ( int k = 1; k < 255; ++k )
	{
		if ( _histogram.uniform[k] > 10 )
		{
			min_uniform = (k-1) * UNIFORMSCALE;
			break;
		}
	}

	for ( int k = 0; k < 255; ++k )
	{
		if ( _histogram.bright[k] > 1 )
		{
			min_brightness_found = k;
			break;
		}
	}

	if ( _maxVolume > 0 )
	{
		int i(0);
		std::vector<int> removeBlack;
                for(int i=0; i < csBlack.size(); i++)
		//for ( BlackFrame &bf : csBlack )
		{
                        BlackFrame &bf = csBlack[i];
			if ( (bf.cause & C_t) )
				continue;

			if ( bf.volume > _maxVolume
					/*
					&& ( bf.frame > 10 && (int)_frame[bf.frame-2].brightness < (int)_frame[bf.frame].brightness + 50
					&& bf.frame < _frameCount - 10
					&& (int)_frame[bf.frame+2].brightness < (int) _frame[bf.frame].brightness + 50 )
					|| bf.volume >  _maxVolume * 1.5
					*/
					)
			{
				Debug(12, "%i - Removing black frame %i, from black frame list"
							" because volume %i is more than %i,"
							" brightness %i, uniform %i\n"
						, i
						, bf.frame
						, bf.volume
						, _maxVolume
						, bf.brightness
						, bf.uniform);

				removeBlack.push_back(i++);
			}
		}

		while ( ! removeBlack.empty() )
		{
			csBlack.erase(csBlack.begin() + removeBlack.back());
			removeBlack.pop_back();
		}
	}

	{
		int i(0);
		std::vector<int> removeBlack;
                for(int i=0; i < csBlack.size(); i++)
//		for ( BlackFrame &bf : csBlack )
		{
                        BlackFrame &bf = csBlack[i];
			if ( (bf.cause & C_t) || (bf.cause & C_r) )
				continue;

			if ( (bf.cause & C_b) && bf.brightness > _maxAvgBrightness )
			{
				Debug(6,"%i - Removing black frame %i, from black frame list because"
						" %i is more than %i, uniform %i\n"
						, i
						, bf.frame
						, bf.brightness
						, _maxAvgBrightness
						, bf.uniform);

				removeBlack.push_back(i++);
			}
		}

		while ( ! removeBlack.empty() )
		{
			csBlack.erase(csBlack.begin() + removeBlack.back());
			removeBlack.pop_back();
		}
	}

	if ( _nonUniformity > 0 )
	{
		int i(0);
		std::vector<int> removeBlack;

	       for(int i=0; i < csBlack.size(); i++)
	       {
		        BlackFrame &bf = csBlack[i];

			if ( (bf.cause & C_t) )
				continue;

			if ( (bf.cause & C_u) && bf.uniform > _nonUniformity )
			{
				 Debug( 6, "%i - Removing uniform frame %i, from black frame list"
					" because %i is more than %i, brightness %i\n"
					, i
					, bf.frame
					, bf.uniform
					, _nonUniformity
					, bf.brightness);

				removeBlack.push_back(i++);
			}
		}

		while ( ! removeBlack.empty() )
		{
			csBlack.erase(csBlack.begin() + removeBlack.back());
			removeBlack.pop_back();
		}
	}

	if ( cut_on_ar_change == 2 )
	{
		if ( logoPercentage > logo_fraction
				&& logoPercentage < logo_percentile )
			cut_on_ar_change = 1;
		/*
		else
			ar_wrong_modifier = 1;
		*/
	}

	if ( cut_on_ar_change == 1 )
	{
		/*
		if ( logoPercentage < logo_fraction
				|| logoPercentage > logo_percentile )
			cut_on_ar_change = 2;
		*/
	}

	if ( ((commDetectMethod & DET::LOGO) && cut_on_ar_change )
			|| cut_on_ar_change >= 2 )
	{
		// if ( cut_on_ar_change ) {
		for ( int i = 0; i < ar_block_count; ++i )
		{
			if ((cut_on_ar_change == 1 || ar_block[i].volume < _maxVolume)
					&& ar_block[i].ar_ratio != AR_UNDEF
					&& ar_block[i+1].ar_ratio != AR_UNDEF )
			{
				const int a(ar_block[i].end);
				// if (a > 20 * fps)
				insertBlackFrame(a, C_a);
			}
		}
	}

	if ( validateBlackFrames(C_b, 3.0, false) < 1 / 3.0 )
		Debug(8, "Black Frame cutting too low\n");

	if ( _validateSceneChange
		/*
		 || (logoPercentage < logo_fraction
		 || logoPercentage > logo_percentile) */
			)
		validateBlackFrames(C_s
				, ((logoPercentage < logo_fraction || logoPercentage > logo_percentile) ? 1.2 : 3.5), true);

	// validateBlackFrames(C_c, 3.0, true);

	if ( _validateUniform )
		validateBlackFrames(C_u, 3.0, true);

	if ( commDetectMethod & DET::SILENCE )
	{
		int k(0);

		for ( int i = 0; i < _frameCount; ++i )
			if ( _frame[i].volume < _maxVolume )
				++k;

		/*
		if ( k * 100 / _frameCount > 25 )
		{
			Debug(8, "Too mutch Silence Frames (%d%%), disabling silence detection\n"
					, k * 100 / _frameCount);
			validateBlackFrames(C_v, 1.0, true);
			commDetectMethod &= ~SILENCE;
			_validateSilence = false;
		}
		else
		*/
		if ( _validateSilence )
			validateBlackFrames(C_v, 3.0, true);
	}

	/*
	if ( logoPercentage < logo_fraction )
		if ( cut_on_ar_change == 2 )
			validateBlackFrames(C_a, 3.0, true);
	*/

	{
		const int blackSizeInt((int)csBlack.size());

		Debug(8, "Black Frame List\n---------------------------\n"
					"Black Frame Count = %d\nnr \tframe\tbright\t"
					"uniform\tvolume\t\tcause\tdimcount  bright\n"
				, blackSizeInt);

		for ( int k = 0; k < blackSizeInt; ++k )
		{
			const BlackFrame &bf(csBlack[k]);

			Debug(8, "%3i\t%6i\t%6i\t%6i\t%6i\t%6s\t%6i\t%6i\n"
					, k
					, bf.frame
					, bf.brightness
					, bf.uniform
					, bf.volume
					, &(CauseString(bf.cause)[10])
					, _frame[bf.frame].dimCount
					, _frame[bf.frame].hasBright);

			if ( k + 1 < blackSizeInt && bf.frame + 1 != csBlack[k + 1].frame )
				Debug(8, "-----------------------------\n");
		}
	}

	// add black frame at end to enable usage of last cblock
	insertBlackFrame(0, 0, 0, framesprocessed, C_b);

	//Create blocks

	{
		int i(0);
		int j(0);
		int a(ar_block_count); // Don't cut on AR when logo disabled

#if 0
		if ( ((commDetectMethod & LOGO) && cut_on_ar_change )
				|| cut_on_ar_change == 2 )
			a = 0;
#endif

		cause = 0;
		BlockInfo::count = 0;
		prev_start = 1;
		prev_head = 0;

//again:
		{
			const int blackSizeInt((int)csBlack.size());

			while ( i < blackSizeInt || a < ar_block_count )
			{
				if ( ! (commDetectMethod & DET::LOGO)
						&& i < blackSizeInt
						&& (csBlack[i].cause & (C_s | C_l)) )
				{
#if 0
					++i; // Skip logo cuts and brighness cuts when not enough logo detected
					goto again;
#else
					i = i;
#endif
				}

				cause = 0;
				b_start = csBlack[i].frame;
				cause |= csBlack[i].cause;
				b_end = b_start;
				j = i + 1;

				v_count = 0;
				b_count = 0;
				black_start = 0;
				black_end = 0;

				//Find end of next black cblock
				while ( j < blackSizeInt && (F2T(csBlack[j].frame) - F2T(b_end) < 1.0 ) )
				{
					//Allow for 2 missing black frames
					if (csBlack[j].frame - b_end > 2
							&& (((csBlack[j].cause & (C_v)) != 0
							&& (cause & (C_v)) == 0)
							|| ((csBlack[j].cause & (C_v)) == 0
							&& (cause & (C_v)) != 0)))
					{
						Debug(6, "At frame %i there is a gap of %i frames in the blackframe list\n"
								, csBlack[j].frame
								, csBlack[j].frame - b_end);
					}

					if ( (csBlack[j].cause & (C_b | C_s | C_u | C_r)) != 0 )
					{
						++b_count;

						if ( black_start == 0 )
							black_start = csBlack[j].frame;

						black_end = csBlack[j].frame;
					}

					if ( (csBlack[j].cause & (C_v)) )
						++v_count;

					if ( csBlack[j].cause == C_a )
					{
						cause |= csBlack[j].cause;
						++j;
					}
					else if ( cause == C_a )
					{
						cause |= csBlack[j].cause;
						b_start = b_end = csBlack[j++].frame;
					}
					else
					{
						cause |= csBlack[j].cause;
						b_end = csBlack[j++].frame;
					}
				}

				i = j;

				if ( b_count > 0 && v_count > 1.5 * b_count )
				{
					b_start = black_start;
					b_end = black_end;
					b_counted = b_count;
				}
				else if ( b_count == 0 && v_count > 5 )
				{
					b_start = b_start - 1 + v_count / 2;
					b_end = b_end + 1 - v_count / 2;
					b_counted = 3;
				}

				cblock[BlockInfo::count].cause = cause;

				// Do it this way for in roundoff problems
				b_counted = (b_end - b_start + 1)/2;

				cblock[BlockInfo::count].b_head = prev_head;
				cblock[BlockInfo::count].f_start = prev_start - cblock[BlockInfo::count].b_head;

				cblock[BlockInfo::count].f_end = ( b_end == framesprocessed )
						? framesprocessed
						: (b_start + b_counted - 1);

				// half on the tail of this cblock
				cblock[BlockInfo::count].b_tail = b_counted;
				cblock[BlockInfo::count].bframe_count
					= cblock[BlockInfo::count].b_head + cblock[BlockInfo::count].b_tail;
				cblock[BlockInfo::count].length
					= F2T(cblock[BlockInfo::count].f_end) - F2T(cblock[BlockInfo::count].f_start);

				//If first cblock is < 1 sec. throw it away
				if( BlockInfo::count > 0
						|| F2L( cblock[BlockInfo::count].f_end, cblock[BlockInfo::count].f_start) > 1.0
						|| cblock[BlockInfo::count].f_end == framesprocessed )
				{
#if 0
				Debug(6, "Creating cblock %i From %i (%i) to %i (%i) because of %s with %i head and %i tail\n"
						, BlockInfo::count
						, cblock[BlockInfo::count].f_start
						, (cblock[BlockInfo::count].f_start + cblock[BlockInfo::count].b_head)
						, cblock[BlockInfo::count].f_end
						, (cblock[BlockInfo::count].f_end - cblock[BlockInfo::count].b_tail)
						, t
						, cblock[BlockInfo::count].b_head
						, cblock[BlockInfo::count].b_tail);
#endif

					++BlockInfo::count;
					initializeBlockArray(BlockInfo::count);
					prev_start = b_end + 1; // cblock starts at end of black initially
					prev_head = b_end - b_start - b_counted + 1; //remaining black from previous cblock tail
				}
			}
		}
	}

#if 1
     //Combine blocks with less than minimum black between them
     for ( int i = BlockInfo::count - 1; i >= 1; --i )
	 {
		unsigned int bfcount(cblock[i].b_head + cblock[i-1].b_tail);

		if ( bfcount < min_black_frames_for_break
				&& cblock[i-1].cause == C_b )
		{
			Debug(10, "Combining blocks %i and %i at %i"
						" because there are only %i black frames seperating them.\n"
					, i - 1
					, i
					, cblock[i-1].f_end
					, bfcount);

			cblock[i-1].f_end = cblock[i].f_end;
			cblock[i-1].b_tail = cblock[i].b_tail;
			cblock[i-1].length = F2L(cblock[i-1].f_end, cblock[i-1].f_start);
			cblock[i-1].cause = cblock[i].cause;

			for ( int k = i; k < BlockInfo::count - 1; ++k )
				cblock[k] = cblock[k+1];

			--BlockInfo::count;
		}
	}
#endif

	return true;
}

void CommercialSkipper::cleanLogoBlocks(void)
{
#if 1
	if ((commDetectMethod & DET::LOGO
			/* || startOverAfterLogoInfoAvail==0 */ )
			&& ! reverseLogoLogic
			&& connect_blocks_with_logo
		)
	{
		//Combine blocks with both logo
		for ( int i = BlockInfo::count - 1; i >= 1; --i )
		{
			if ( checkFrameForLogo(cblock[i-1].f_end)
					&& checkFrameForLogo(cblock[i].f_start) )
			{
				Debug(6, "Joining blocks %i and %i at frame %i"
							" because they both have a logo.\n"
						, i-1, i, cblock[i-1].f_end);

				cblock[i-1].f_end = cblock[i].f_end;
				cblock[i-1].b_tail = cblock[i].b_tail;

				if ( cblock[i].length > cblock[i-1].length )
				{
					// Use AR of longest cblock
					cblock[i-1].ar_ratio = cblock[i].ar_ratio;
				}

				cblock[i-1].length = F2L(cblock[i-1].f_end, cblock[i-1].f_start);
				cblock[i-1].cause = cblock[i].cause;

				for ( int k = i; k < BlockInfo::count - 1; ++k )
					cblock[k] = cblock[k+1];

				--BlockInfo::count;
			}
		}
	}
#endif

	/*
	int k = -1;
	// Checking cblock size ratio
	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		if (F2L(cblock[i].f_end, cblock[i].f_start) > (int) min_show_segment_length )
		{
			if ( k != -1 && i > k + 1 )
			{
				a = cblock[k].f_end - cblock[k].f_start;
				j = cblock[i].f_start - cblock[k].f_end;
				Debug(1, "Long/Short cblock ratio for cblock %i till %i is %i percent\n",k, i-1 , (int)(100 * a)/(a+j));
			}

			k = i;
		}
	}
	*/

	avg_brightness = 0;
	avg_volume = 0;
	maxi_volume = 0;
	avg_silence = 0;
	avg_uniform = 0;
	avg_schange = 0.0;

	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		int sum_brightness(0);
		int sum_volume(0);
		int sum_silence(0);
		int sum_uniform(0);
		double sum_brightness2(0.0);
		int sum_delta(0);

		if ( _useFrameArray )
		{
			for ( int k = cblock[i].f_start + 1; k < cblock[i].f_end; ++k )
			{
#if 0
				int b(_frame[k].brightness);
#else
				int b(abs(_frame[k].brightness - _frame[k-1].brightness));
#endif
				int v(_frame[k].volume);

				if ( maxi_volume < v )
					maxi_volume = v;

				int s((_frame[k].volume < _maxVolume) ? 0 : 99);
				sum_brightness += b;
				sum_volume += v;
				sum_silence += s;
				sum_uniform += abs(_frame[k].uniform - _frame[k-1].uniform);
				sum_brightness2 += b * b;
				sum_delta += abs(_frame[k].brightness - _frame[k-1].brightness);
			}
		}

		int n(cblock[i].f_end - cblock[i].f_start + 1);
		cblock[i].brightness = sum_brightness * 1000 / n;
		cblock[i].volume = sum_volume / n;
		cblock[i].silence = sum_silence / n;
		cblock[i].uniform = sum_uniform / n;

		if ( (cblock[i].schange_count = countSceneChanges(cblock[i].f_start, cblock[i].f_end)))
			cblock[i].schange_rate = (double)cblock[i].schange_count / n;
		else
			cblock[i].schange_rate = 0.0;

		cblock[i].stdev =
#if 0
			sqrt( (n*sum_brightness2 - sum_brightness*sum_brightness)/ (n * (n-1)));
#else
			100* sum_delta / n;
#endif

		avg_brightness += sum_brightness * 1000;
		avg_volume += sum_volume;
		avg_silence += sum_silence;
		avg_uniform += sum_uniform;
		avg_schange += cblock[i].schange_rate*n;
	}

	int n(cblock[BlockInfo::count-1].f_end - cblock[0].f_start);
	avg_brightness /= n;
	avg_volume /= n;
	avg_silence /= n;
	avg_uniform /= n;
	avg_schange /= n;

	// Debug(1, "Average brightness is %i\n",avg_brightness);
	// Debug(1, "Average volume is %i\n",avg_volume);
}

#ifdef _WIN32

#define DEBUGFRAMES 80000

#define PIXEL(X,Y) _graph[(((oheight+30) -(Y))*owidth+(X))*3+0] = _graph[(((oheight+30) -(Y))*owidth+(X))*3+1] = _graph[(((oheight+30) -(Y))*owidth+(X))*3+2]
#define SETPIXEL(X,Y,R,G,B) { _graph[(((oheight+30) -(Y))*owidth+(X))*3+0] = (B); _graph[(((oheight+30) -(Y))*owidth+(X))*3+1] = (G); _graph[(((oheight+30) -(Y))*owidth+(X))*3+2] = (R); }

#define PLOT(S, I, X, Y, MAX, L, R,G,B) { int y, o; o = oheight - (oheight/(S))* (I); y = (Y)*(oheight/(S)-5)/(MAX); if (y < 0) y = 0; if (y > (oheight/(S)-1)) y = (oheight/(S)-1); SETPIXEL((X),(o - y),((Y) < (L) ? 255: R ) , ((Y) < (L) ? 255: G ) ,((Y) < (L) ? 255: B));}

int oheight = 0;
int owidth = 0;
int divider = 1;
int oldfrm = -1;
int zstart = 0;
int zfactor = 1;
bool show_XDS;

int gy;

const char *helptext[] =
{
	"Help: press any key to remove",
	"Key          Action",
	"Arrows	        Reposition current location",
	"PgUp/PgDn      Reposition current location",
	"n/p            Jump to next/previous cutpoint",
	"e/b            Jump to next/previous end of cblock",
	"z/u            Zoom in/out on the timeline",
	"g              Graph on/off",
	"x              XDS info on/off",
	"t              Toggle current cblock between show and commercial",
	"w              Write the new cutpoints to the ouput files",
	"c              Dump this frame as CutScene"
	"F2             Reduce the max_volume detection level",
	"F3             Reduce the non_uniformity detection level",
	"F4             Reduce the max_avg_brighness detection level",
	"F5             Toggle frame number / timecode display",
	"",
	"During commercial break review",
	"e              Set end of commercial to this position",
	"b              Set begin of commercial to this position",
	"i              Insert a new commercial",
	"d              Delete the commercial at current location",
	"s              Jump to Start of the recording",
	"f              Jump to Finish of the recording",
	0
};

void OutputDebugWindow(bool showVideo, int frm, int grf)
{
	static bool vo_init_done = false;
	int i,j,x,y,a,c,r,s,g,gc,lb=0,e,f,n,bl,xd;
	int v,w;
	int bartop = 0;
	int b,cb;
	int barh = 32;
	int vidtop = 30;
	char t[1024];
	char x1[80];
	char x2[80];
	char x3[80];
	char x4[80];
	char x5[80];
	char *tt[40];
	char frametext[80];
	bool blackframe;
	bool bothtrue;
	bool haslogo;
	bool uniformframe;
	int silence = 0;

	// ++frm;
	if ( oldfrm == frm )
		return;

	oldfrm = frm;

     if ( flagFiles.debugwindow.use && _frameCount )
	 {
          if ( ! vo_init_done )
		  {
               if (width == 0 /*|| (_isLoadingCsv && !showVideo) */)
                    _videoWidth = _width = 800;

               if ( _height == 0
						/*||  (_isLoadingCsv && !showVideo) */
						)
                    _height = 600 - barh;
               if ( _height > 600 || _width > 800 )
			   {
                    oheight = _height / 2;
                    owidth = _width / 2 ;
                    divider = 2;
               }
			   else
			   {
                    oheight = _height;
                    owidth = _width;
                    divider = 1;
               }

               oheight = (oheight + 31) & -32;
               owidth = (owidth + 31) & -32;
               sprintf(t, windowtitle, filename);
               vo_init(owidth, oheight+barh, t);
//			vo_init(owidth, oheight+barh,"Comskip");
               owidth = owidth;
               vo_init_done = true;
          }
//		bartop = oheight;

          if ( frm >= _frameCount )
               frm = _frameCount - 1;

          if ( frm < 1 )
               frm = 1;

          v = _frameCount / zfactor;

          if ( frm < zstart + v / 10) zstart = frm - v / 10;
          if ( zstart < 0 ) zstart = 0;


          if ( frm > v + zstart - v / 10)
				zstart = frm - v + v / 10;

          if ( zstart + v > _frameCount )
				zstart = _frameCount - v;
//		if ( frm > v + zstart) zstart = frm - v;

          w = ((frm - zstart)* owidth / v);



          if ( showVideo && _videoFramePointer )
		  {
               memset(graph, 0, owidth*oheight*3);
               /*
               			for (x = 0; x < border; x++) {
               				for (y = 0; y < oheight; y++) {
               					PIXEL(x,y+barh) = 0;
               					PIXEL(owidth - 1 - x,y+barh) = 0;
               				}
               			}
               */
               for (x = 0+border; x < owidth-border; x++) {
//				for (y = 0; y < border; y++) {
//					PIXEL(x,y+barh) = 0;
//					PIXEL(x,oheight - 1 - (y+barh)) = 0;
//				}
                    for (y = 0+border; y < oheight-border; y++) {
                         if (x*divider < width && y*divider < height)
                              PIXEL(x,y+barh) = _videoFramePointer[(y*divider)*(width)+(x*divider)] >> (grf?1:0);
//					PIXEL(x,y+barh) = min_br[(y*divider)*width+(x*divider)];		//MAXMIN Logo search

//					PIXEL(x,y+barh) = vert_edges[(y*divider)*width+(x*divider)];	//Edge detect

//					PIXEL(x,y+barh) = (_verEdgeCount[(y*divider)*width+(x*divider)]* 4);		// Edge count
                         /*
                         					PIXEL(x,y+barh) =(abs((_videoFramePointer[(y*divider)*width+(x*divider)] +
                         										      _videoFramePointer[(y*divider)*width+((x+1)*divider)])/2
                         											  -
                         											  (_videoFramePointer[(y*divider)*width+((x+2)*divider)]+
                         											  _videoFramePointer[(y*divider)*width+((x+3)*divider)])/2
                         										) > _edges.getThreshold() ? 200 : 0);
                         */
                         //					graph[((oheight - y)*owidth+x)*3+0] = _videoFramePointer[y*owidth+x];
                         //					graph[((oheight - y)*owidth+x)*3+1] = _videoFramePointer[y*owidth+x];
                         //					graph[((oheight - y)*owidth+x)*3+2] = _videoFramePointer[y*owidth+x];
                    }
               }
               //			memcpy(&graph[owidth*oheight * 0], _videoFramePointer, owidth*oheight);
               //			memcpy(&graph[owidth*oheight * 1], _videoFramePointer, owidth*oheight);
               //			memcpy(&graph[owidth*oheight * 2], _videoFramePointer, owidth*oheight);
               if ( _useFrameArray && grf && ((commDetectMethod & LOGO) || _logoInfoAvailable ))
			   {
                    if ( _aggressiveLogoRejection )
                         s = _edges.getRadius() / 2; // Cater of mask offset
                    else
                         s = 0;
//				w = 0;
//				v = 0;
                    // Show logo mask
                    if ( _logoInfoAvailable )
					{
                         if ( frame[frm].currentGoodEdge > logo_threshold )
						 {
                              e = (int)(frame[frm].currentGoodEdge * 250);
							  const BoolMatrix &cHor(_edgeMask.getConstCHor());
							  const BoolMatrix &cVer(_edgeMask.getConstCVer());

                              for ( y = _logoRange.y.min / divider
									; y < (_logoRange.y.max+1)/divider
									; ++y )
							  {
									const int yDiv(y*divider);

                                   for ( x = _logoRange.x.min / divider
										; x < (_logoRange.x.max+1) / divider
										; ++x )
								   {
										const int xDiv(x*divider);

                                        r = ( cHor.get(xDiv, yDiv) )
											? 255
											: 0;

                                        g = ( cVer.get(xDiv, yDiv) )
											? 255
											: 0;

                                        if ( r || g )
											SETPIXEL(x-s, y-s+barh, r, g, 0);
                                   }
                              }
                         }
                    }
					else // Show detected logo pixels only while scanning input
					{
						const int twoThirdsBufferCount(_logoBuffers.getBufferCount() * 2 / 3);

                         if ( frm + 1 == _frameCount )
						 {
                              for ( y = s; y < oheight; ++y )
							  {
									const int yDiv(y*divider);

                                   for ( x = s; x < owidth; ++x )
								   {
										const int xDiv(x*divider);

                                        r = ( _horEdgeCount[(yDiv) * width + (xDiv)] >= twoThirdsBufferCount )
											? 255
											: 0;

                                        g = ( _verEdgeCount[(yDiv) * width + (xDiv)] >= twoThirdsBufferCount )
											? 255
											: 0;

                                        if ( r || g )
											SETPIXEL(x-s, y-s+barh, r, g, 0);
                                   }
                              }

                         }

                    }

               }
          }
		  else
		  {
               ::memset(graph, 0, owidth*oheight*3);
          }


          if ( _useFrameArray && grf )
		  {
               for ( y = 0; y < oheight; ++y )
                    SETPIXEL(w,y,100,100,100);

               bl = 0;

               // debug bar
               for ( x = 0; x < owidth; ++x )
			   {
                    a = 0;
                    b = 0;
                    s = 0;
                    c = 0;
                    n = 0;

                    if ( cblock && grf == 2 )
					{
                         while (bl < BlockInfo::count && cblock[bl].f_end < zstart+(int)((double)x * v /owidth))
                              ++bl;
#define PLOTS	9

                         PLOT(PLOTS, 0, x, cblock[bl].brightness, 2550, (int)(avg_brightness*punish_threshold), 0, 255, 0); // RED
                         PLOT(PLOTS, 1, x, cblock[bl].volume/100, 100000, (int)(avg_volume*punish_threshold)/100, 255, 0, 0); // RED
                         PLOT(PLOTS, 2, x, cblock[bl].uniform, 3000, (int)(avg_uniform*punish_threshold), 255, 0,0); // RED
                         PLOT(PLOTS, 3, x, (int)(cblock[bl].schange_rate*1000), 1000, (int)(avg_schange*punish_threshold*1000), 255, 0, 0);	// PURPLE
                    }

                    for ( i = zstart+(int)((double)x * v /owidth); i < zstart+(int)((double)(x+1) * v /owidth ); ++i )
					{
                         if ( i <= _frameCount )
						 {
                              b += frame[i].brightness;
                              PLOT(PLOTS, 0, x, frame[i].brightness, 255, _maxAvgBrightness, 255, 0,0); // RED
                              s += frame[i].volume;
                              PLOT(PLOTS, 1, x, frame[i].volume, 10000, _maxVolume, 0, 255, 0);			// GREEN
                              e += frame[i].uniform;
                              PLOT(PLOTS, 2, x, frame[i].uniform, 30000, _nonUniformity, 0, 255, 255);	// LIGHT BLUE
                              c += (int)(frame[i].currentGoodEdge*100);
                              PLOT(PLOTS, 4, x, (int)(frame[i].currentGoodEdge*100), 100, 0, 255, 255, 0);  // YELLOW
                              PLOT(PLOTS, 4, x, (int)(frame[i].logo_filter*50+50), 100, 0, (frame[i].logo_filter < 0.0 ?255:0) , (frame[i].logo_filter < 0.0 ?0:255), 0);
                              PLOT(PLOTS, 5, x, (int)((frame[i].ar_ratio-0.5) * 100), 250, 0, 0, 0, 255);   // BLUE

                              if ( commDetectMethod & CUTSCENE )
							  {
                                   PLOT(PLOTS, 3, x, (int)(frame[i].cutscenematch)
										, 100, cutscenedelta, 255, 0, 255); // PURPLE
                              }
							  else
							  {
                                   PLOT(PLOTS, 3, x, (int)(frame[i].schange_percent)
										, 100, schange_cutlevel, 255, 0, 255); // PURPLE
                              }

                              a += frame[i].maxY;
                              PLOT(PLOTS, 6, x, frame[i].maxY, height, 0, 0, 128, 128);
                              b += frame[i].minY;
                              PLOT(PLOTS, 6, x, frame[i].minY, height, 0, 0, 128, 128);
                              ++n;
                              PLOT(PLOTS, 7, x, frame[i].maxX, width, 0, 0, 0, 255);
                              PLOT(PLOTS, 7, x, frame[i].minX, width, 0, 0, 0, 255);
                         }
                    }

                    if ( n > 0 )
					{
                         a /= n;
						// f /= n;
                         b /= n;
                         s /= n;
                         c /= n;
                         e /= n;
                    }
// PLOT(S, I, X, Y, MAX, L, R,G,B)
               }
          }

          if ( _videoFramePointer && _useFrameArray )
		  {
//			for (x=0; x < owidth; x++) { // Edge counter indicator
//				graph[2* owidth + x] = (x < edge_count /8 ? 255 : 0);
//			}

               x = _frame[frm].maxX / divider;

               if ( x == 0 )
					   x = owidth;

               for ( i = _frame[frm].minX / divider; i < x; ++i )
			   {				// AR lines
                    SETPIXEL(i, (frame[frm].minY/divider)+barh, 0,0,255);
                    SETPIXEL(i, (frame[frm].maxY/divider)+barh, 0,0,255);

//				graph[frame[frm].minY* owidth + i] = 255;
//				graph[frame[frm].maxY* owidth + i] = 255;
               }

               for ( i=(_frame[frm].minY/divider); i < (_frame[frm].maxY/divider); ++i )
			   {				// AR lines
                    SETPIXEL((frame[frm].minX/divider), i+barh, 0,0,255);
                    SETPIXEL((frame[frm].maxX/divider), i+barh, 0,0,255);
               }


          }

          if ( _useFrameArray /* && commDetectMethod & LOGO */ )
		  {
			   // Logo box X
               for ( x = _logoRange.x.min/divider; x < _logoRange.x.max/divider; ++x )
			   {
                    SETPIXEL(x,_logoRange.y.min/divider+barh,255,e,e);
                    SETPIXEL(x,_logoRange.y.max/divider+barh,255,e,e);
               }

			   // Logo box Y
               for ( y = _logoRange.y.min/divider; y < _logoRange.y.max/divider; ++y )
			   {
                    SETPIXEL(_logoRange.x.min/divider,y+barh,255,e,e);
                    SETPIXEL(_logoRange.x.max/divider,y+barh,255,e,e);
               }
          }

          b = 0;

          for ( i = 0; i < BlockInfo::count; ++i )
		  {
               if ( cblock[i].f_start <= frm && frm <= cblock[i].f_end )
			   {
                    b = i;
                    break;
               }
          }

          /*
			::memset(graph,20,owidth*(oheight+30)*3);
			for (i=0; i<oheight/2;i++) {
				graph[(i*(owidth+0))*3] = 255;
				graph[(i*(owidth+0))*3+1] = 0;
				graph[(i*(owidth+0))*3+2] = 0;
			}
          */
//		if (0)	// disable debug bar
          // debug bar
          for ( x = 0; x < owidth; ++x )
		  {
               blackframe = false;
               uniformframe = false;
               silence = 0;
               haslogo = false;
               bothtrue = false;
               g = 0;
               gc = 0;
               xd = 0;
				// v = std::max(_frameCount, DEBUGFRAMES);
               if ( _useFrameArray )
			   {
                    xd = XDS_block_count - 1;
                    while (xd > 0 && XDS_block[xd].frame > zstart+(int)((double)(x+1) * v /owidth) )
                         xd--;
                    if (xd > 0 && XDS_block[xd].frame >= zstart+(int)((double)x * v /owidth))
                         xd = xd;
                    else
                         xd = 0;

                    for (i = zstart+(int)((double)x * v /owidth)
									; i < zstart+(int)((double)(x+1) * v /owidth )
									; ++i )
					{
                         if ( i <= _frameCount )
						 {
                              if ((frame[i].isblack & C_b) || (frame[i].isblack & C_r))
							  {
                                   blackframe = true;

                                   for ( j = 0; j < BlockInfo::count; ++j )
								   {
                                        if (cblock[j].f_end == i)
                                             bothtrue = true;
                                   }
                              }

                              if ( frame[i].isblack & C_u )
                                   uniformframe = true;

                              if ( frame[i].volume < _maxVolume
										&& silence < 1 )
								  silence = 1;

                              if ( (frame[i].volume < 50
										|| frame[i].volume < _maxSilence)
									&& silence < 2)
								silence = 2;

                              if ( frame[i].volume < 9 )
								 silence = 3;

                              if ( (frame[i].isblack & C_v) )
									silence = 3;

                              if ( (frame[i].isblack & C_b)
									&& frame[i].volume < _maxVolume )
								bothtrue = true;

                              if ( (frame[i].isblack & C_r) )
								bothtrue = true;

                              if ( frm + 1 == _frameCount )
							  { // Show details of logo while scanning
                                   if ( frame[i].logo_present )
										   haslogo = true;
                              }
							  else
							  {
                                   // Show logo blocks when finished
                                   while ( lb < _logoBlockCount && i > _logoBlock[lb].end )
                                        ++lb;

                                   if ( lb < _logoBlockCount && i >= _logoBlock[lb].start )
                                        haslogo = true;
                              }
//					if (frame[i].currentGoodEdge > logo_threshold) haslogo = true;
                              a = (int)((frame[i].ar_ratio - 0.5 - 0.1)*6);		// Position of AR line
                              g += (int)(frame[i].currentGoodEdge * 5);
                              gc++;
                         }
                    }
               }
               if (gc > 0)
                    g /= gc;

               c = 255;

               if ( c == 255 )
			   {
                    for (i = 0; i <= commercial_count; ++i ) {	// Inside commercial?
                         if (zstart+(int)((double)x * v /owidth ) >= commercial[i].start_frame &&
                                   zstart+(int)((double)x * v /owidth ) <= commercial[i].end_frame )
						 {
                              c = 128;
                              break;
                         }
                    }
               }

               if ( c == 255 )
			   { // not in a commercial but score above threshold
                    for ( i = 0; i < BlockInfo::count; ++i )
					{
                         if (zstart+(int)((double)x * v /owidth ) >= cblock[i].f_start &&
                                   zstart+(int)((double)x * v /owidth ) <= cblock[i].f_end &&
                                   cblock[i].score > _globalThreshold )
						 {
                              c = 220;
                              break;
                         }
                    }
               }

               r = 255;

               // Inside reference?
               for ( i = 0; i <= reffer_count; ++i )
			   {
                    if ( zstart+(int)((double)x * v /owidth )
								>= reffer[i].start_frame
							&& zstart+(int)((double)x * v /owidth )
								<= reffer[i].end_frame )
					{
                         r = 0;
                         break;
                    }
               }

               a = bartop + 14 - a;

               // Commercial / AR bar
               for ( y = bartop + 5; y < bartop + 15 ; ++y )
				{
                    if ( y == a )
					{
                         SETPIXEL(x, y, 0, 0, 255);
					}
                    else
					{
                         SETPIXEL(x, y, c, c, c);
						// PIXEL(x,y) = c;
                    }
				}

               g = 5; // Disable goodEdge graph

               for ( i = 0; i < BlockInfo::count; ++i )
			   {
                    if ( zstart+(int)((double)x * v /owidth )
								>= cblock[i].f_start
							&& zstart+(int)((double)x * v /owidth )
								<= cblock[i].f_end
							&& cblock[i].correlation > 0 )
					{
                         g = 2;
                         break;
                    }
               }

			   // Logo bar
               for ( y = bartop + 15; y < bartop+20 ; ++y )
			   {
                    if ( haslogo )
						PIXEL(x,y) = ((y - (bartop + 15) == g)
										? 255
										:((commDetectMethod & LOGO)
											? 0
											: 128));
                    else
						PIXEL(x,y) = ((y - (bartop + 15) == g)
										? 0
										: 255);
					// if (y - (bartop + 15) == g) graph[y * owidth + x] = 128;
               }

               cb = 255;
               if ( cblock != 0
						&& cblock[b].f_start <= zstart+(int)((double)x * v /owidth )
						&& zstart+(int)((double)x * v /owidth ) <= cblock[b].f_end )
                    cb = 0;

               if ( bothtrue )
                    c = 0;
               else
                    c = 255;

               for ( y = bartop + 20; y < bartop+25 ; ++y ) // Blackframe bar
			   {
                    if ( blackframe )
                         SETPIXEL(x, y, c, 0, 0);
                    else if ( uniformframe )
                         SETPIXEL(x, y, 0, 0, c);
                    else
                         SETPIXEL(x, y, 255, cb, 255);
               }

               c = 255;

               for ( y = bartop + 25; y < bartop+30 ; ++y ) // Silence bar
				{
                    if ( silence == 1 )
                         SETPIXEL(x, y, 0, c, 0);
                    else if ( silence == 2 )
                         SETPIXEL(x, y, 0, 0, c);
                    else if ( silence == 3 )
                         SETPIXEL(x, y, c, 0, 0);
                    else
                         PIXEL(x,y) = 255;
               }

               if ( w < owidth ) // Progress indicator
                    for ( y = bartop; y < bartop+30 ; ++y )
							SETPIXEL(w,y,255,0,0);

               for ( y = bartop; y < bartop+(_isLoadingTxt?20:5) ; ++y )
			   {
                    // Reference bar
                    if ( xd )
                         SETPIXEL(x, y, 128, 128, 128);
                    else if ( reffer_count >= 0 )
						PIXEL(x, y) = r;
               }
          }

          vo_draw(graph);

          //		sprintf(t, "%8i %8i %1s %1s", frm, framenum_infer, (frame[frm].isblack?"B":" "), (frame[frm].volume < _maxVolume?"S":" "));
          b = 0;

          for ( i = 0; i < BlockInfo::count; ++i )
		  {
               if ( cblock[i].f_start <= frm && frm <= cblock[i].f_end )
			   {
                    b = i;
                    break;
               }
          }

          if ( timeflag && _useFrameArray )
               ::sprintf(frametext, "%s", secondsToString(F2T(frm)).c_str());
		  else
               ::sprintf(frametext, "%8i", frm);

          if ( recalculate )
		  {
               ::sprintf(t, "max_volume=%d, non_uniformity=%d"
					", max_avg_brightness=%d"
					, _maxVolume
					, _nonUniformity
					, _maxAvgBrightness);
          }
		  else
		  {
               if ( _useFrameArray )
			   {
                    if (b < BlockInfo::count)
                         sprintf(t, "%s B=%i%1s V=%i%1s U=%i%1s AR=%4.2f  Block #%i Length=%6.2fs Score=%6.2f Logo=%6.2f %s                      ", frametext, frame[frm].brightness, (frame[frm].isblack & C_b?"B":" "), frame[frm].volume, (frame[frm].volume<_maxVolume?"S":" "),frame[frm].uniform, (frame[frm].uniform < _nonUniformity?"U":" "), frame[frm].ar_ratio, b, cblock[b].length, cblock[b].score, cblock[b].logo, CauseString(cblock[b].cause)
                                );
                    else
                         sprintf(t, "%s B=%i%1s V=%i%1s U=%i%1s AR=%4.2f                                                                          ", frametext, frame[frm].brightness, (frame[frm].isblack & C_b?"B":" "), frame[frm].volume, (frame[frm].volume<_maxVolume?"S":" "),frame[frm].uniform, (frame[frm].uniform < _nonUniformity?"U":" "), frame[frm].ar_ratio);
               }
			   else
                    sprintf(t, "%s", frametext);
          }

          if ( soft_seeking )
		  {
               tt[0] = t;
               tt[1] = "WARNING: Seeking inaccurate, do not use for cutpoint review!";
               tt[2] = 0;
               ShowHelp(tt);
          }
		  else if ( helpflag )
               ShowHelp(helptext);
          else if ( show_XDS )
		  {
               tt[0] = t;
               i = (XDS_block_count > 0 ? XDS_block_count-1 : 0);
               while (i > 0 && XDS_block[i].frame > frm)
                    i--;
               sprintf(x1,"Program Name    : %s", XDS_block[i].name);
               tt[1] = x1;
               sprintf(x2,"Program V-Chip  : %4x", XDS_block[i].v_chip);
               tt[2] = x2;
               sprintf(x3,"Program duration: %2d:%02d", (XDS_block[i].duration & 0x3f00)/256, (XDS_block[i].duration & 0x3f) % 256);
               tt[3] = x3;
               sprintf(x4,"Program position: %2d:%02d", (XDS_block[i].position & 0x3f00)/256, (XDS_block[i].position & 0x3f) % 256);
               tt[4] = x4;
               sprintf(x5,"Composite Packet: %2d:%02d, %2d/%2d, ", (XDS_block[i].composite1 & 0x3f00)/256, (XDS_block[i].composite1 & 0x1f) % 256, (XDS_block[i].composite2 & 0x1f00)/256, (XDS_block[i].composite2 & 0x0f) % 256);
               tt[5] = x5;
               tt[6] = 0;
               ShowHelp(tt);
          } else
               ShowDetails(t);
     }

     if ( key == 0x20 )
	 {
          subsample_video  = 0;
          key = 0;
     }

     if ( key == 27 )
	 {
          exit(1);
     }

     if ( key == 'G' )
	 {
          subsample_video  = 0x3f;
          key = 0;
     }

     if ( subsample_video == 0 )
	 {
          //	Enable for single stepping trough the video
          if ( ! vo_init_done )
		  {
               if (width == 0 /*|| (_isLoadingCsv && !showVideo) */)
                    videowidth = width = 800;
               if (height == 0 /*||  (_isLoadingCsv && !showVideo) */)
                    height = 600 - barh;

               if ( height > 600 || width > 800 )
			   {
                    oheight = height / 2;
                    owidth = width / 2;
                    divider = 2;
               }
			   else
			   {
                    oheight = height;
                    owidth = width;
                    divider = 1;
               }

               owidth = (owidth + 31) & -32;
               sprintf(t, windowtitle, filename);
               vo_init(owidth, oheight+barh,t);
//			vo_init(owidth, oheight+barh,"Comskip");
               owidth = owidth;
               vo_init_done = true;
          }

          while ( key == 0 )
               vo_draw(graph);

          if ( key == 27 )
		  {
               exit(1);
          }

          if ( key == 'G' )
		  {
               subsample_video  = 0x3f;
          }

          key = 0;
     }

     recalculate = 0;
}

void CommercialSkipper::recalc(void)
{
	buildBlocks(true);

	if ( commDetectMethod & LOGO )
		printLogoFrameGroups();

	weighBlocks();
	outputBlocks();
}

int shift;

bool CommercialSkipper::reviewResult(void)
{
     FILE *review_file(0);
     int curframe(1);
     int lastcurframe(-1);
     int bartop(0);
     int grf(2);
     int i;
	 int j;
     int prev;
     int b = -1;
     char tsfilename[MAX_PATH];

     if ( ! _useFrameArray )
			 grf = 0;

     flagFiles.demux.use = false;
     flagFiles.data.use = false;
     flagFiles.srt.use = false;
     flagFiles.smi.use = false;

     if ( ! review_file && mpegfilename[0] )
          review_file = fopen(mpegfilename, "rb");
     if (review_file == 0 ) {
          strcpy(tsfilename, mpegfilename);
          i = strlen(tsfilename);
          while (i > 0 && tsfilename[i-1] != '.') i--;
          tsfilename[i] = 't';
          tsfilename[i+1] = 's';
          tsfilename[i+2] = 0;
          review_file = fopen(tsfilename, "rb");
          if (review_file) {
               demux_pid = 1;
               strcpy(mpegfilename, tsfilename);
          }
     }
     if (review_file == 0 ) {
          strcpy(tsfilename, mpegfilename);
          i = strlen(tsfilename);
          while (i > 0 && tsfilename[i-1] != '.') i--;
          strcpy(&tsfilename[i], "dvr-ms");
          review_file = fopen(tsfilename, "rb");
          if (review_file) {
               demux_asf = 1;
               strcpy(mpegfilename, tsfilename);
          }
     }

     while ( true )
	 {
          if ( key != 0 )
		  {
               if ( key == 27 && ! helpflag )
					   exit(0);

               if ( key == 112 )
			   {
                    helpflag = 1;     // F1 Key
                    oldfrm = -1;
               }
			   else
			   {
                    if ( helpflag == 1 )
					{
                         helpflag = 0;
                         oldfrm = -1;
                    }
               }

               if ( key == 16 )
			   {
                    shift = 1;
               }

               if (key == 37) curframe -= 1;
               if (key == 39) curframe += 1;
               if (key == 38) curframe -= (int)fps;
               if (key == 40) curframe += (int)fps;
               if (key == 33) curframe -= (int)(20*fps);
               if (key == 34) curframe += (int)(20*fps);

               if (key == 78 || (key == 39 && shift)) { // Next key
                    curframe += 5;
                    if ( _useFrameArray )
					{
                         i = 0;
                         while (i <= commercial_count && curframe > commercial[i].end_frame) i++;
                         //					if (i > 0)
                         curframe = commercial[i].end_frame+5;
//						while (curframe < _frameCount && frame[curframe].isblack) curframe++;
//						while (curframe < _frameCount && !frame[curframe].isblack) curframe++;
                         //					while (curframe < _frameCount && frame[curframe].isblack) curframe++;
                    } else {
                         i = 0;
                         while (i <= reffer_count && curframe > reffer[i].end_frame) i++;
                         //					if (i > 0)
                         curframe = reffer[i].end_frame+5;
                    }
                    curframe -= 5;
               }

               if (key == 80 || (key == 37 && shift)) {	// Prev key
                    curframe -= 5;
                    if ( _useFrameArray )
					{
                         i = commercial_count;
                         while (i >= 0 && curframe < commercial[i].start_frame) i--;
                         //					if (i > 0)
                         curframe = commercial[i].start_frame-5;
                         //					while (curframe > 1 && frame[curframe].isblack) curframe--;
                         //					while (curframe > 1 && !frame[curframe].isblack) curframe--;
                         //					while (curframe > 1 && frame[curframe].isblack) curframe--;
                    } else {
                         i = reffer_count;
                         while (i >= 0 && curframe < reffer[i].start_frame) i--;
                         //					if (i > 0)
                         curframe = reffer[i].start_frame-5;
                    }
                    curframe += 5;
               }

               if ( key == 'S' )
			   {
                    if ( _useFrameArray )
                         curframe = 0;
					else
                         curframe = 0;
               }

               if ( key == 'F' )
			   {
                    if ( _useFrameArray )
                         curframe = _frameCount;
					else
                         curframe = _frameCount;
               }

               if (key == 'E') {	// End key
                    if ( _useFrameArray )
					{
                         curframe += 10;
                         i = 0;
                         while (i < BlockInfo::count && curframe > cblock[i].f_end) i++;
                         //					if (i > 0)
                         curframe = cblock[i].f_end+5;
                         curframe -= 10;
                    } else {
                         i = reffer_count;
                         while (i >= 0 && curframe < reffer[i].start_frame) i--;
                         if (i >= 0)
                              reffer[i].end_frame = curframe;
                         oldfrm = -1;
                    }
               }
               if (key == 'B') {	// begin key
                    if ( _useFrameArray )
					{
                         curframe -= 10;
                         i = BlockInfo::count - 1;

                         while ( i > 0 && curframe < cblock[i].f_start)
								 --i;
                         //					if (i > 0)
                         curframe = cblock[i].f_start-5;
                         curframe += 10;
                    }
					else
					{
                         i = 0;

                         while ( i <= reffer_count && curframe > reffer[i].end_frame )
								 ++i;

                         if ( i <= reffer_count )
                              reffer[i].start_frame = curframe;

                         oldfrm = -1;
                    }
               }

               // Toggle key
               if ( key == 'T' )
			   {
                    if ( _useFrameArray )
					{
                         i = 0;
                         while ( i < BlockInfo::count && curframe > cblock[i].f_end )
								 ++i;

                         if ( i < BlockInfo::count )
						 {
                              if ( cblock[i].score < _globalThreshold )
                                   cblock[i].score = 99.99;
                              else
                                   cblock[i].score = 0.01;

                              cblock[i].cause |= C_F;
                              oldfrm = -1;
                              buildCommercial();
                              key = 'W'; // Trick to cause writing of the new commercial list
                         }
                    }
               }

               // Delete key
               if ( key == 68 )
			   {
                    if ( _useFrameArray )
					{
                         i = 0;

                         while ( i < BlockInfo::count && curframe > cblock[i].f_end )
								 ++i;

                         if ( i < BlockInfo::count )
						 {
                              cblock[i].score = 99.99;
                              cblock[i].cause |= C_F;
                              oldfrm = -1;
                              buildCommercial();
                         }
                    }
					else
					{
                         i = reffer_count;
                         while (i >= 0 && curframe < reffer[i].start_frame) i--;
                         if (i >= 0 && reffer[i].start_frame <= curframe && curframe <= reffer[i].end_frame ) {
                              while (i < reffer_count) {
                                   reffer[i] = reffer[i+1];
                                   i++;
                              }
                              reffer_count--;
                              oldfrm = -1;
                         }
                    }
               }
               if (key == 73) {	// Insert key
                    if ( _useFrameArray )
					{
                         i = 0;

                         while ( i < BlockInfo::count && curframe > cblock[i].f_end )
								 ++i;

                         if ( i < BlockInfo::count )
						 {
                              cblock[i].score = 0.01;
                              cblock[i].cause |= C_F;
                              oldfrm = -1;
                              buildCommercial();
                         }
                    }
					else
					{
                         i = reffer_count;

                         while ( i >= 0 && curframe < reffer[i].start_frame )
								 i--;

                         if (i == -1 || curframe > reffer[i].end_frame ) { //Insert BEFORE i
                              j = reffer_count;

                              while ( j > i )
							  {
                                   reffer[j+1] = reffer[j];
                                   j--;
                              }

                              reffer[i+1].start_frame = std::max(curframe-1000,1);
                              reffer[i+1].end_frame = std::min(curframe+1000,_frameCount);
                              reffer_count++;
                              oldfrm = -1;
                         }
                    }
               }

               if (key == 'W') { // W key
                    flagFiles._default.use = true;
                    openOutputFiles();

                    if ( _useFrameArray )
					{
                         prev = -1;

                         for ( i = 0; i <= commercial_count; ++i )
						 {
                              outputCommercialBlock(i, prev, commercial[i].start_frame, commercial[i].end_frame, (commercial[i].end_frame < _frameCount-2 ? false : true));
                              prev = commercial[i].end_frame;
                         }

                         if (commercial[commercial_count].end_frame < _frameCount-2)
                              outputCommercialBlock(commercial_count, prev, _frameCount-2, _frameCount-1, true);
                    }
					else
					{
                         prev = -1;

                         for ( i = 0; i <= reffer_count; ++i )
						 {
                              outputCommercialBlock(i, prev, reffer[i].start_frame, reffer[i].end_frame, (reffer[i].end_frame < _frameCount-2 ? false : true));
                              prev = reffer[i].end_frame;
                         }

                         if ( reffer[reffer_count].end_frame < _frameCount - 2 )
                              outputCommercialBlock(reffer_count, prev, _frameCount-2, _frameCount-1, true);
                    }

                    flagFiles._default.use = false;
                    oldfrm = -1;
               }

               if ( key == 'Z' )
			   {
                    if ( zfactor < 256 && _frameCount / zfactor > owidth )
					{
//						i = (curframe - zstart) * zfactor * owidth/ _frameCount;
                         zfactor = zfactor << 1;
//						zstart = i * _frameCount / owidth / zfactor;
                         zstart = (curframe + zstart) / 2;
                         oldfrm = -1;
                    }
               }

               if ( key == 'U' )
			   {
                    if ( zfactor > 1 )
					{
//						i = (curframe - zstart) * zfactor * owidth/ _frameCount;
                         zfactor = zfactor >> 1;
//						zstart = i * _frameCount / owidth / zfactor;
                         zstart = zstart - (curframe - zstart);
                         if (zstart < 0)
                              zstart = 0;
                         oldfrm = -1;

                    }
               }
               if ( key == 'C' )
			   {
                    recordCutScene(curframe, frame[curframe].brightness);
               }

               if (key == 'X') {
                    show_XDS = ! show_XDS;
                    oldfrm = -1;
               }

               if (key == 'G') {
                    grf++;
                    if (grf > 2)
                         grf = 0;
                    oldfrm = -1;
               }
               if (key == 113) {				// F2 key
                    _maxVolume = (int)(_maxVolume / 1.1);
                    recalc();
                    oldfrm = -1;
               }
               if (key == 114) {				// F3 key
                    _nonUniformity = (int)(_nonUniformity / 1.1);
                    recalc();
                    oldfrm = -1;
               }
               if (key == 115) {				// F4 key
                    _maxAvgBrightness = (int)(_maxAvgBrightness / 1.1);
                    recalc();
                    oldfrm = -1;
               }
               if (key == 116) {				// F5 key
                    timeflag = !timeflag;
                    oldfrm = -1;
               }
               if (key == '.') {
                    oldfrm = -1;
               }
               if (key == 82) return(true);
               if (key != 16) shift = 0;
               key = 0;
          }

          if ( lMouseDown )
		  {
               if (yPos >= bartop && yPos < bartop + 30)
                    curframe = zstart + _frameCount * xPos / owidth / zfactor + 1;

               lMouseDown = 0;
          }

          if ( curframe < 1 )
				curframe = 1;

          if ( _frameCount > 0 )
		  {
               if (curframe >= _frameCount)
					   curframe = _frameCount - 1;
          }

          if ( _frameCount > 0 && review_file )
			{
               if ( curframe != lastcurframe )
			   {
                    decodeOnePicture(review_file
							, (_useFrameArray)
								? F2T(curframe)
								: (double)curframe / fps
							, this );
                    lastcurframe = curframe;
               }
			}

#ifdef _WIN32
          OutputDebugWindow((review_file ? true : false),curframe, grf);
#if !defined(GUI)
          vo_wait();
//				vo_refresh();
//				Sleep((DWORD)100);
#endif
#endif
     }

     return false;
}

#endif /* _WIN32 */

void Init_XDS_block(void);

const int DET::BLACK_FRAME(1);
const int DET::LOGO(2);
const int DET::SCENE_CHANGE(4);
const int DET::RESOLUTION_CHANGE(8);
const int DET::CC(16);
const int DET::AR(32);
const int DET::SILENCE(64);
const int DET::CUTSCENE(128);

void CommercialSkipper::initComSkip(void)
{
	min_brightness_found = 255;
	max_logo_gap = -1;
	max_nonlogo_block_length = -1;
	logo_overshoot = 0.0;

	for ( int i = 0; i < 256; ++i )
		_histogram.bright[i] = 0;

	for ( int i = 0; i < 256; ++i )
		_histogram.uniform[i] = 0;

	for ( int i = 0; i < 256; ++i )
		_histogram.volume[i] = 0;

	if ( doProcessCc )
	{
		_ccBlock[0]._startFrame = 0;
		_ccText[0]._startFrame = 1;
		_ccText[0]._endFrame = -1;
		_ccText[0]._text = "";

		if ( ! initialized )
		{
			cc_memory = (uint8_t**)::malloc(15 * sizeof(uint8_t *));
			cc_screen = (uint8_t**)::malloc(15 * sizeof(uint8_t *));

			for ( int i = 0; i < 15; ++i )
			{
				cc_memory[i] = (uint8_t*)::malloc(32 * sizeof(uint8_t));
				cc_screen[i] = (uint8_t*)::malloc(32 * sizeof(uint8_t));
			}
		}

		for ( int i = 0; i < 15; ++i )
			for ( int j = 0; j < 32; ++j )
				cc_memory[i][j] = cc_screen[i][j] = 0;
	}

#if 0
	if ( commDetectMethod & AR )
	{
#endif
	if ( ! initialized )
	{
		max_ar_block_count = 100;
		ar_block = (ArBlockInfo*)::malloc((int)((max_ar_block_count + 1) * sizeof(ArBlockInfo)));
	}

	if ( ! ar_block )
	{
		Debug(0, "Could not allocate memory for aspect ratio cblock array\n");
		exit(31);
	}
#if 0
	}
#endif

	_cc.reset();
	_lastCc.reset();

	initXdsBlock();

	if ( _maxAvgBrightness == 0 )
	{
		if ( fps == 25.00 )
			_maxAvgBrightness = 19;
		else
			_maxAvgBrightness = 19;
	}

	_sceneChangeVec.clear();
	_frameCount = 0;
	framesprocessed = 0;
	csBlack.clear();
	BlockInfo::count = 0;
	ar_block_count = 0;
	framenum_real = 0;
	_framesWithLogo = 0;
	framenum = 0;
	// audio_framenum = 0;
	_ccBlockCount = 0;
	_ccTextCount = 0;
	_logoTrendCounter = 0;
	_logoBlockCount = 0;
	// pts = 0;
	ascr = scr = 0;
	initScanLines();
	initHasLogo();
	initialized = true;
	closeDump();
}

int CommercialSkipper::detectCommercials(int frameIndex, double pts)
{
	(void)frameIndex;

	if ( _isLoadingTxt || _isLoadingCsv )
		return 0;

	if ( ! initialized )
		initComSkip();

	_frameCount = framenum_real = framenum + 1;

     if ( framenum_real < 0 )
		return 0;

#ifdef _WIN32
     if ( _playNice.use )
		Sleep(_playNice.ms);
#endif

	if ( _useFrameArray )
		initFrame(framenum_real);

	curvolume = _volumes.get(_frameCount);

	if ( _useFrameArray )
	{
		_frame[_frameCount].volume = curvolume;
		_frame[_frameCount].goppos = headerpos;
		_frame[_frameCount].pts = pts;

		if ( _frameCount == 1 )
			_frame[0].pts = pts;
	}

	if ( curvolume > 0 )
		_histogram.volume[(curvolume/volumeScale < 255 ? curvolume/volumeScale : 255)]++;

	if ( ticker_tape_percentage > 0 )
		ticker_tape = ticker_tape_percentage * _height / 100;

	if ( ticker_tape )
		::memset(&_videoFramePointer[_width*(_height - ticker_tape)], 0, _width*ticker_tape);

	if ( ignore_side )
	{
		for ( int i = 0; i < _height; ++i )
		{
			for ( int j = 0; j < ignore_side; ++j )
			{
				_videoFramePointer[_width * i + j] = 0;
				_videoFramePointer[_width * i + (_width -1) - j] = 0;
			}
		}
	}

	size_t prevBlackCount(csBlack.size());
	checkSceneHasChanged();
	bool isBlack(prevBlackCount != csBlack.size());

	if ( (commDetectMethod & DET::LOGO) && ((_frameCount % (int)(fps * logoFreq)) == 0) )
	{
		if ( ! _logoInfoAvailable || (! _lastLogoTest && ! startOverAfterLogoInfoAvail ) )
		{
			if (delay_logo_search == 0
					|| (delay_logo_search == 1 && F2T(_frameCount) > added_recording * 60)
					|| (F2T(_frameCount) > delay_logo_search) )
			{
				fillLogoBuffer();

				if ( _logoBuffers.getIsFull() )
				{
					Debug(6, "\nLooking For Logo in frames %i to %i.\n"
							, _logoBuffers.getOldestFrameNumber()
							, _frameCount);

					if ( ! searchForLogoEdges() )
					{
						initComSkip();
						return 1;
					}
				}

#if 0
				if ( _logoInfoAvailable )
				{
					_logoTrendCounter = _logoBuffers.getBufferCount();
					_lastLogoTest = true;
					curLogoTest = true;

					logoTrendStartFrame = logoFrameNum[oldestLogoBuffer];
					curLogoTest = true;
					lastRealLogoChange = logoFrameNum[oldestLogoBuffer];

					hindsightLogoState = ( _logoBuffers.getBufferCount() >= minHitsForTrend );
				}
#endif
			}
		}

		if ( _logoInfoAvailable )
		{
			currentGoodEdge = checkStationLogoEdge(_videoFramePointer);
			_curLogoTest = (currentGoodEdge > logo_threshold);
			_lastLogoTest = processLogoTest(_frameCount, _curLogoTest
											, false);

			// Lost logo
			if ( ! _lastLogoTest
					&& ! startOverAfterLogoInfoAvail
					&& _logoBuffers.getIsFull() )
			{
				_logoBuffers.setIsFull(false);
				initLogoBuffers();
				_logoBuffers.setNewestIndex(-1);
			}

			if ( startOverAfterLogoInfoAvail
					&& ! _isLoadingCsv
					&& ! secondLogoSearch
					&& _logoBlockCount > 0
					&& ! _lastLogoTest
					&& F2L(_frameCount, _logoBlock[_logoBlockCount-1].end)
							> ( _commBreakSize._break.max * 1.2 )
					&& (double)_framesWithLogo / (double)_frameCount < 0.5 )
			{
				Debug(6, "\nNo Logo in frames %i to %i, restarting Logo search.\n"
						, _logoBlock[_logoBlockCount-1].end, _frameCount);
				// First logo found but no logo found
				// after first commercial cblock so search new logo
				_logoInfoAvailable = false;
				secondLogoSearch = true;
				_logoBuffers.setIsFull(false);
				initLogoBuffers();
				_logoBuffers.setNewestIndex(-1);
			}
		}
	}

	if ( _useFrameArray )
		_frame[_frameCount].logo_present
			= ( _logoInfoAvailable )
				? _lastLogoTest
				: 0.0;

     if ( _lastLogoTest )
          ++_framesWithLogo;

     if ( _useFrameArray )
         _frame[_frameCount].currentGoodEdge = currentGoodEdge;

#ifdef _WIN32
	if ( ! (_frameCount & subsample_video) )
		OutputDebugWindow(true, _frameCount, true);

	key = 0;
	while ( key == 0 )
		vo_wait();
#endif

	++framesprocessed;
	++scr;

	if ( live_tv && ! isBlack )
		buildCommListAsYouGo();

	return 0;
}

#define LOGO_BORDER 5

void CommercialSkipper::initScanLines(void)
{
	for ( int i = 0; i < _height; ++i )
	{
		if ( i < _logoRange.y.min - LOGO_BORDER
				|| i > _logoRange.y.max + LOGO_BORDER )
		{
			_bfRange[i].set(border, _videoWidth - 1 - border);
		}
		else
		{
			// Most pixels left of the logo
			if ( _logoRange.x.min > _videoWidth - _logoRange.x.max )
				_bfRange[i].set(border, _logoRange.x.min - LOGO_BORDER);
			else
				_bfRange[i].set(_logoRange.x.max + LOGO_BORDER
								, _videoWidth - 1 - border);
		}
	}
}

void CommercialSkipper::initHasLogo(void)
{
	Rectangle range(_logoRange);
	range.y.min += LOGO_BORDER;
	range.y.max -= LOGO_BORDER;
	range.x.min += LOGO_BORDER;
	range.x.max -= LOGO_BORDER;

	_hasLogo.initRange(range);
}

double AverageARForBlock(int start, int end)
{
	int maxSize(0);
	double Ar(0.0);

	for ( int i = 0; i < ar_block_count; ++i )
	{
		int f(std::max(ar_block[i].start, start));
		int t(std::min(ar_block[i].end, end));

		if ( maxSize < t - f + 1 )
		{
			Ar = ar_block[i].ar_ratio;
			maxSize = t - f + 1;
		}

		if ( ar_block[i].start > end )
			break;
	}

	return Ar;
}

double FindARFromHistogram(double ar_ratio)
{
	for ( int i = 0; i < MAX_ASPECT_RATIOS; ++i )
	{
		if ( ar_ratio > ar_histogram[i].ar_ratio - ar_delta
				&& ar_ratio < ar_histogram[i].ar_ratio + ar_delta )
			return ar_histogram[i].ar_ratio;
	}

	for ( int i = 0; i < MAX_ASPECT_RATIOS; ++i )
	{
		if ( ar_ratio > ar_histogram[i].ar_ratio - 2*ar_delta
				&& ar_ratio < ar_histogram[i].ar_ratio + 2*ar_delta )
			return ar_histogram[i].ar_ratio;
	}

     for ( int i = 0; i < MAX_ASPECT_RATIOS; ++i )
	 {
		if ( ar_ratio > ar_histogram[i].ar_ratio - 4*ar_delta
				&& ar_ratio < ar_histogram[i].ar_ratio + 4*ar_delta )
			return ar_histogram[i].ar_ratio;
	}

	return 0.0;
}

void CommercialSkipper::fillArHistogram(bool refill)
{
	if ( refill )
	{
		for ( int i = 0; i < MAX_ASPECT_RATIOS; ++i )
		{
			ar_histogram[i].frames = 0;
			ar_histogram[i].ar_ratio = 0.0;
		}

		for ( int i = 0; i < ar_block_count; ++i )
		{
			int hi = (int)((ar_block[i].ar_ratio - 0.5)*100);

			if ( hi >= 0 && hi < MAX_ASPECT_RATIOS )
			{
				ar_histogram[hi].frames += ar_block[i].end - ar_block[i].start + 1;
				ar_histogram[hi].ar_ratio = ar_block[i].ar_ratio;
			}
		}
	}

	{
		int counter(0);
		bool hadToSwap;

		do
		{
			++counter;
			hadToSwap = false;

			for ( int i = 0; i < MAX_ASPECT_RATIOS - 1; ++i )
			{
				if ( ar_histogram[i].frames < ar_histogram[i + 1].frames )
				{
					hadToSwap = true;
					int tempFrames = ar_histogram[i].frames;
					double tempRatio = ar_histogram[i].ar_ratio;
					ar_histogram[i] = ar_histogram[i + 1];
					ar_histogram[i+1].frames = tempFrames;
					ar_histogram[i+1].ar_ratio = tempRatio;
				}
			}
		} while ( hadToSwap );

		int totalFrames(0);

		for ( int i = 0; i < MAX_ASPECT_RATIOS; ++i )
			totalFrames += ar_histogram[i].frames;

		Debug(10, "\n\nAfter Sorting - %i\n--------------\n", counter);

		int tempCount(0);

		for ( int i = 0; i < MAX_ASPECT_RATIOS && ar_histogram[i].frames > 0; ++i )
		{
			tempCount += ar_histogram[i].frames;
			Debug(10, "Aspect Ratio  %5.2f found on %6i frames totaling \t%3.1f%c\n"
					, ar_histogram[i].ar_ratio, ar_histogram[i].frames
					, ((double)tempCount / (double)totalFrames)*100,'%');
		}
	}
}

void CommercialSkipper::insertBlackFrame(const Frame &f, int fid, int c)
{
	insertBlackFrame(f.brightness, f.uniform, f.volume, fid, c);
}

void CommercialSkipper::insertBlackFrame(int fid, int c)
{
	const Frame &f(_frame[fid]);
	insertBlackFrame(f.brightness, f.uniform, f.volume, fid, c);
}

void CommercialSkipper::insertBlackFrame(int b, int u, int v, int fid, int c)
{

   for(int i=0; i < csBlack.size(); i++)
   {
        BlackFrame &bf = csBlack[i];

		if ( bf.frame == fid )
		{
			bf.cause |= c;
			return;
		}
	}

	csBlack.push_back(BlackFrame(fid, c, b, u, v));
	std::sort(csBlack.begin(), csBlack.end());
}

bool CommercialSkipper::buildMasterCommList(void)
{
	if ( _frameCount == 0 )
	{
		Debug(1, "No video found\n");
		return false;
	}

	Debug(7, "Finished scanning file.  Starting to build Commercial List.\n");

	int count(0);
	int cp(0);

	double new_ar_ratio;
	bool foundCommercials(false);
	time_t ltime;

	double length(F2L(_frameCount - 1, 1));

	if ( abs(length - (_frameCount -1)/fps) > 0.5 )
		Debug(1, "WARNING: Timeline errors in the recording!!!!"
				" Results may be wrong, .ref input will be misaligned."
				" .txt editing will produce wrong results");

	_frame[_frameCount].pts = _frame[_frameCount - 1].pts + 1.0 / fps;

	for ( int i = 1; i < 255; ++i )
	{
		if ( _histogram.volume[i] > 10 )
		{
			min_volume = (i-1)*volumeScale;
			break;
		}
	}

	for ( int i = 1; i < 255; ++i )
	{
		if ( _histogram.uniform[i] > 10 )
		{
			min_uniform = (i-1) * UNIFORMSCALE;
			break;
		}
	}

	for ( int i = 0; i < 255; ++i )
	{
		if ( _histogram.bright[i] > 1 )
		{
			min_brightness_found = i;
			break;
		}
	}

	logoPercentage = (double) _framesWithLogo / (double) framenum_real;

	int platIdx(0);
	int mv(0);
	int ms(0);
	int a(0);

	// if ( _maxVolume == 0 )
	{
		int plataus(0);

#define VOLUME_DELTA	10
#define VOLUME_MAXIMUM	300
#define VOLUME_PLATAU_SIZE		6

		int volume_delta(VOLUME_DELTA);

try_again:
		// Find silence volume level
		if ( _useFrameArray )
		{
			for ( int i = 0; i < 255; ++i )
				_histogram.silence[i] = 0;

			plataus = 0;
			int j(1);

			{
				for ( platIdx = VOLUME_PLATAU_SIZE
						; platIdx < _frameCount - VOLUME_PLATAU_SIZE; )
				{
					if ( _frame[platIdx].volume > VOLUME_MAXIMUM
							|| _frame[platIdx].volume < 0 )
					{
						++platIdx;
						continue;
					}

					while ( platIdx + 1 < _frameCount - VOLUME_PLATAU_SIZE
							&& _frame[platIdx + 1].volume < _frame[platIdx].volume )
						++platIdx;

					int k(1);

					while ( platIdx - k - VOLUME_PLATAU_SIZE > 1
							&& (abs(_frame[platIdx-k].volume - _frame[platIdx].volume)
										< volume_delta
								// || frame[i-k].volume < 50
							))
						++k;

					if ( _frame[platIdx-k].volume < _frame[platIdx].volume )
					{
						++platIdx;
						continue;
					}

					a = 1;

					while ( platIdx + a + VOLUME_PLATAU_SIZE < _frameCount
							&& (abs(_frame[platIdx+a].volume - _frame[platIdx].volume)
										< volume_delta
								// || frame[platIdx+a].volume < 50
							))
						++a;

					if ( _frame[platIdx+a].volume < _frame[platIdx].volume )
					{
						platIdx += a;
						continue;
					}

					// i=8 k=1 a=11
					if ( a + k > VOLUME_PLATAU_SIZE
							&& platIdx - k - VOLUME_PLATAU_SIZE > 0
							&& platIdx + a + VOLUME_PLATAU_SIZE < _frameCount )
					{
						int p_vol = (_frame[platIdx-k-4].volume
								+ _frame[platIdx-k-VOLUME_PLATAU_SIZE+3].volume
								+ _frame[platIdx-k-VOLUME_PLATAU_SIZE+2].volume
								+ _frame[platIdx-k-VOLUME_PLATAU_SIZE+1].volume
								+ _frame[platIdx-k-VOLUME_PLATAU_SIZE].volume) / 5;

						int n_vol = (_frame[platIdx+a+4].volume
								+ _frame[platIdx+a+VOLUME_PLATAU_SIZE-3].volume
								+ _frame[platIdx+a+VOLUME_PLATAU_SIZE-2].volume
								+ _frame[platIdx+a+VOLUME_PLATAU_SIZE-1].volume
								+ _frame[platIdx+a+VOLUME_PLATAU_SIZE].volume) / 5;

						if ( p_vol > _frame[platIdx].volume + 220
								|| n_vol > _frame[platIdx].volume + 220 )
							// if ( abs(_frame[platIdx-k-2].volume - _frame[platIdx].volume) > VOLUME_DELTA*2
							// || abs(_frame[platIdx+a+2].volume - _frame[platIdx].volume) > VOLUME_DELTA*2)
						{
							Debug(8, "Platau@[%d] frames %d, volume %d, distance %d seconds\n"
									, platIdx
									, k + a
									, _frame[platIdx].volume
									, (int)F2L(platIdx, j));

							j = platIdx;

#if 0
							for ( j = platIdx - k; j < platIdx + a; ++j )
								frame[j].isblack |= C_v;
#endif

							++plataus;
							_histogram.silence[_frame[platIdx].volume/10]++;
						}
					}

					platIdx += a;
				}

				Debug(9, "Vol: Frames %3d: %d\n", platIdx*10, _histogram.silence[platIdx]);
			}

			a = 0;

			for ( int i = 0; i < 255; ++i )
			{
				a += _histogram.silence[i];

				if ( _histogram.silence[i] > 0 )
					Debug(9, "%3d: %d\n", i*10, _histogram.silence[i]);
			}

			a = a * 6 / 10;

			{
				int j(0);
				int i(0);

				while ( j < a )
					j += _histogram.silence[i++];

				ms = i * 10;
			}

			Debug(7, "Calculated silence level = %d\n", ms);

			if ( ms > 0 && ms < 10 )
				ms = 10;

			if ( ms < 50 )
				mv = 1.5 * ms;
			else if ( ms < 100 )
				mv = 2 * ms;
			else if ( ms < 200 )
				mv = 2 * ms;
			else
				mv = 2 * ms;
		}

		if ( mv == 0 || plataus < 5 )
		{
			volume_delta *= 2;

			if ( volume_delta < VOLUME_MAXIMUM )
				goto try_again;
		}
	}

	if ( _maxVolume == 0 )
	{
		_maxVolume = mv;
		_maxSilence = ms;
	}

#if 0
	if ( _maxSilence < min_volume + 30 )
		_maxSilence = min_volume + 30;

	if ( _maxVolume < 100 )
	{
		if ( _maxVolume < min_volume + 30 )
			_maxVolume = min_volume + 30;
	}
	else if ( _maxVolume < min_volume + 100 )
		_maxVolume = min_volume + 100;
#endif

	if ( _maxVolume == 0 )
	{
		// Find silence volume level
		if ( _useFrameArray )
		{

#define START_VOLUME	500
			count = 21;
scanagain:
			a = START_VOLUME;
			int k(0);

			int j( ( _frame[platIdx].volume > 0 ) ? _frame[platIdx].volume : 0);

			for ( int i = 1; i < _frameCount; ++i )
			{
				if ( _frame[i].volume > 0 && _frame[i].volume < a )
				{
					if ( _frame[i].volume < j )
						j = _frame[i].volume;

					++k;

					if ( k > count && a > _frame[i-count].volume + 20
							&& j > a - 250 )
					{
						i = i - count;
						a = _frame[i].volume;
						j = _frame[i].volume;
						k = 0;
					}
				}
				else
				{
					k = 0;

					if ( _frame[i].volume > 0 )
						j = _frame[i].volume;
				}
			}
		}

		if ( a > START_VOLUME-100 && count > 7 )
		{
			count = count - 7;
			goto scanagain;
		}

		_maxSilence = a + 10;
		_maxVolume = a + 150;
	}

	if ( _maxVolume == 0 )
	{
		for ( int k = 2; k < 255; ++k )
		{
			if ( _histogram.volume[k] > 10 )
			{
				_maxVolume = k * volumeScale + 200;
				_maxSilence = k * volumeScale + 20;
				break;
			}
		}

#if 0
		_maxVolume = 1000;

		for ( int k = Black_frameCount - 1; k >= 0; --k )
		{
			if ( black[k].volume >= 0 && black[k].volume <  _maxVolume )
				_maxVolume = black[k].volume;
		}

		_maxVolume *= 4;
#endif
		Debug ( 1, "Setting max_volume to %i\n", _maxVolume);
	}

     if ( commDetectMethod & DET::LOGO )
	 {
          // close out last logo cblock if one is open
          processLogoTest(_frameCount, false, true);
          /*
          		if (_isLoadingCsv) {
          			prev_logo_threshold = logo_threshold-1.0;
          			findLogoThreshold();
          			if (fabs(logo_threshold - prev_logo_threshold) > 0.4) {
          				Debug(2,"Changed logo_threshold to %.2f, recalculating logo timeline\n", logo_threshold);
          				initProcessLogoTest();
          				for ( i = 1; i < _frameCount; ++i ) {
          					_curLogoTest = (frame[i].currentGoodEdge > logo_threshold);
          					_lastLogoTest = processLogoTest(i, _curLogoTest);
          					frame[i].logo_present = _lastLogoTest;
          					if (_lastLogoTest) ++_framesWithLogo;
          				}
          				logoPercentage = (double) _framesWithLogo / (double) framenum_real;
          			}
          			else
          				logo_threshold = prev_logo_threshold;

          		}
          */

          if ( logo_quality == 0.0 )
               findLogoThreshold();

          // Clean up logo blocks
          /*
          		for (i = _logoBlockCount-2; i >= 0; i--) {
          			if (F2L(_logoBlock[i+1].start, _logoBlock[i].end) < _commBreakSize._size.min + (2*shrink_logo)) {
          				Debug(1, "Logo cblock %d and %d combined because gap (%i s) too short with previous\n", i, i+1, (int)F2L(_logoBlock[i+1].start, _logoBlock[i].end ));
          				_logoBlock[i+1].start = _logoBlock[i].start;
          				for (t = i; t+1 < _logoBlockCount; t++) {
          					_logoBlock[t] = _logoBlock[t+1];
          				}
          				--_logoBlockCount;
          			}
          		}
          */

          for ( int i = _logoBlockCount-1; i >= 0; --i )
		  {
               if ( F2L(_logoBlock[i].end, _logoBlock[i].start)
							   < _commBreakSize._size.min - 2*shrink_logo )
			   {
                    Debug(1, "Logo cblock %d deleted because too short (%i s)\n"
							, i
							, (int)F2L(_logoBlock[i].end
							, _logoBlock[i].start) );

                    for ( int t = i; t + 1 < _logoBlockCount; ++t )
                         _logoBlock[t] = _logoBlock[t+1];

                    --_logoBlockCount;
               }
          }

          if ( logoPercentage > logo_fraction
				&& after_logo > 0
				&& _useFrameArray
				&& _logoBlockCount > 0 )
		  {
               for ( int i = 0; i < _logoBlockCount; ++i )
			   {
                    // Don't do anything if too close
                    if ( i < _logoBlockCount - 1
							&& F2L(_logoBlock[i+1].start, _logoBlock[i].end)
									< _commBreakSize._break.max / 4 )
                         continue;

                    // Don't do anything if too close
                    if ( i == (_logoBlockCount - 1)
							&& F2L(_frameCount, _logoBlock[i].end)
									< _commBreakSize._break.max / 4 )
                         continue;

                    if ( after_logo == 999 )
					{
                         const int fid(_logoBlock[i].end);
                         insertBlackFrame(fid, C_l);
                         Debug(3, "Frame %6i - Cutpoint added when Logo disappears\n", fid);
                         continue;
                    }

                    int j(_logoBlock[i].end + (int)(after_logo * fps));

                    if ( j >= _frameCount )
                         j = _frameCount - 1;

                    int t(j + (int)(30 * fps));

                    if ( t >= _frameCount )
                         t = _frameCount - 1;

                    int maxsc(255);
                    cp = 0;
                    int cpf(0);

                    while ( j < t )
					{
                         int rsc(255);

                         while ( _frame[j].volume >= _maxVolume && j < t )
						 {
#if 0
							if ( rsc > _frame[j].schange_percent )
								rsc = _frame[j].schange_percent;
#endif
                              ++j;
                         }

                         int c(10);
                         j = j - 10;

                         if ( j < 1 )
						 {
                              j = j - 1;
                              c = c +j;
                              j = 1;
                         }

                         while ( c-- && j < t )
						 {
                              if ( rsc > _frame[j].schange_percent )
							  {
                                   rsc = _frame[j].schange_percent;
                                   cpf = j;
                              }

                              ++j;
                         }

                         while ( _frame[j].volume < _maxVolume && j < t )
						 {
                              if ( rsc > _frame[j].schange_percent )
							  {
                                   rsc = _frame[j].schange_percent;
                                   cpf = j;
                              }

                              ++j;
                         }

                         c = 10;

                         while ( c-- && j < t )
						 {
                              if ( rsc > _frame[j].schange_percent )
							  {
                                   rsc = _frame[j].schange_percent;
                                   cpf = j;
                              }

                              ++j;
                         }

                         if ( maxsc > rsc )
						 {
                              maxsc = rsc;
                              cp = cpf;
                         }
//					cp = j;
//					j=t;
                    }

                    if ( cp != 0 )
					{
                         insertBlackFrame(cp, C_l);
                         Debug(3,"Frame %6i - Cutpoint added %i seconds"
								" after Logo disappears at change percentage of %d\n"
								, cp, (int)F2L(cp, _logoBlock[i].end), maxsc);
                    }
               }
          }

          if ( logoPercentage > logo_fraction
				&& before_logo > 0
				&& _useFrameArray && _logoBlockCount > 0 )
		  {
               for ( int i = 0; i < _logoBlockCount; ++i )
			   {
                    if ( i > 0 && F2L(_logoBlock[i].start, _logoBlock[i-1].end) < _commBreakSize._break.max/4)
                         continue;

                    if ( i == 0 && F2T(_logoBlock[i].start) < _commBreakSize._break.max / 4 )
                         continue;

                    if ( before_logo == 999 )
					{
                         const int fid(_logoBlock[i].start);

                         insertBlackFrame(fid, C_t);
                         Debug(3, "Frame %6i - Cutpoint added when Logo appears\n", fid);

                         continue;
                    }

                    int j(_logoBlock[i].start - (int)(before_logo * fps));

                    if ( j < 1 )
                         j = 1;

                    int t(j - (int)(30 * fps));

                    if ( t < 1 )
                         t = 1;

                    int maxsc(255);
                    cp = 0;
					int cpf(0);

                    while ( j > t )
					{
                         int rsc(255);

                         while ( _frame[j].volume >= _maxVolume && j > t )
						 {
//						if ( rsc > frame[j].schange_percent )
//							rsc = frame[j].schange_percent;
                              --j;
                         }

                         int c(10);
                         j = j + 10;

                         if ( j >= _frameCount )
						 {
                              j = j - _frameCount;
                              c = c - j;
                              j = _frameCount - 1;
                         }

                         while ( c-- && j > t )
						 {
                              if ( rsc > _frame[j].schange_percent )
							  {
                                   rsc = _frame[j].schange_percent;
                                   cpf = j;
                              }

                              --j;
                         }

                         while ( _frame[j].volume < _maxVolume && j > t )
						 {
                              if ( rsc > _frame[j].schange_percent )
							  {
                                   rsc = _frame[j].schange_percent;
                                   cpf = j;
                              }

                              --j;
                         }

                         c = 10;

                         while ( c-- && j > t )
						 {
                              if ( rsc > _frame[j].schange_percent )
							  {
                                   rsc = _frame[j].schange_percent;
                                   cpf = j;
                              }

                              --j;
                         }

                         if ( maxsc > rsc )
						 {
                              maxsc = rsc;
                              cp = cpf;
                         }
//					cp = j;
//					j=t;
                    }

                    if ( cp != 0 )
					{
                         insertBlackFrame(cp, C_l);

                         Debug(3, "Frame %6i - Cutpoint added %i seconds"
								" before Logo disappears at change percentage of %d\n"
								, cp, (int)F2L(cp, _logoBlock[i].end), maxsc);
                    }
               }
          }
//		if (logoPercentage > .15 && logoPercentage < .32 ) {
//			reverseLogoLogic = true;
//			logoPercentage = 1 - logoPercentage;
//		}

          if ( logoPercentage < logo_fraction - 0.05
				|| logoPercentage > logo_percentile )
		  {
               Debug(1, "\nNot enough or too much logo's found (%.2f),"
					" disabling the use of Logo detection\n"
					, logoPercentage );
               commDetectMethod -= DET::LOGO;
          }
     }

	if ( commDetectMethod & DET::SILENCE )
	{
		int silence_count(0);
		int silence_start(0);
		int summed_volume1(0);
		int summed_volume2(0);
		bool schange_found(false);
		int schange_frame(0);
		int schange_max(100);
		int low_volume_count(0);
		int very_low_volume_count(0);

		for ( int i = 1; i < _frameCount; ++i )
		{
			if ( i == 21361 )
				i = i;

			if ( _minSilence > 0 )
			{
				if ( 0 <= _frame[i].volume
						&& _frame[i].volume < _maxSilence )
				{
					if ( silence_start == 0 )
						silence_start = i;

					++silence_count;

					if ( _frame[i].schange_percent < schange_threshold )
					{
						schange_found = true;

						if ( schange_max > _frame[i].schange_percent )
						{
							schange_frame = i;
							schange_max = _frame[i].schange_percent;
						}
					}

					if ( _frame[i].uniform < _nonUniformity )
					{
						schange_found = true;
						schange_frame = i;
					}

					if ( _frame[i].volume < 12 )
					{
						++low_volume_count;

						if ( _frame[i].volume < 9 )
							++very_low_volume_count;
					}
				}
				else
				{
					if ( silence_count > _minSilence /* * (int)fps */
							&& silence_count < 5 * fps )
					{
						if ( very_low_volume_count > (int)(silence_count * 0.7)
								|| schange_found
								|| _frame[i].schange_percent < schange_threshold )
						{
#define SILENCE_CHECK	((int)(2.5 * fps))
							summed_volume1 = 0;

							for ( int j = std::max(silence_start - SILENCE_CHECK, 1)
									; j < silence_start; ++j )
							{
								// if ( summed_volume1 < frame[j].volume )
								summed_volume1 += _frame[j].volume;
							}

							summed_volume1 /= std::min(SILENCE_CHECK, silence_start+1);
							summed_volume2 = 0;

							for ( int j = i
									; j < std::min(i+SILENCE_CHECK, _frameCount)
									; ++j )
							{
								// if ( summed_volume2 < frame[j].volume )
								summed_volume2 += _frame[j].volume;
							}

							summed_volume2 /= std::min(SILENCE_CHECK, _frameCount - i + 1);

							if ( (summed_volume1 > 0.9 * _maxVolume
										&&  summed_volume2 > 0.9 * _maxVolume
										&& low_volume_count > _minSilence )
									|| ( summed_volume1 > 2 * _maxVolume
										&& summed_volume2 > 2 * _maxVolume )
									|| ( summed_volume1 > 4 * _maxVolume
										|| summed_volume2 > 4 * _maxVolume)
									|| very_low_volume_count > _minSilence)
							{
								if ( schange_frame == 0 )
									schange_frame = i;

#if 1
								for ( int j = silence_start; j < i; ++j )
								{
									_frame[j].isblack |= C_v;
									insertBlackFrame(j, C_v);
								}
#else
								_frame[schange_frame].isblack |= C_v;
								insertBlackFrame(schange_frame, C_v);
#endif
								// for (j = silence_start /*i - _minSilence /* * (int)fps */; j <= i; j++) {
                                        //	_frame[j].isblack |= C_v;
                                        //	insertBlackFrame(j, C_v);
                                //}
							}
						}
					}

					silence_start = 0;
					silence_count = 0;
					schange_found = false;
					schange_frame = 0;
					schange_max = 100;
					low_volume_count = 0;
					very_low_volume_count = 0;
				}
			}
		}
	}

	after_start = added_recording * fps * 60;
	before_end  = _frameCount - added_recording * fps * 60;

	_frame[_frameCount].dimCount = 0;
	_frame[_frameCount].hasBright = 0;
	insertBlackFrame(0, 0, 0, _frameCount, C_b);

     // close out the last ar cblock
     if ( commDetectMethod & DET::AR )
	 {
          if ( ar_block[ar_block_count].start > 0 )
		  {
               Debug(5, "The last ar cblock wasn't closed.  Now closing.\n");
               ar_block[ar_block_count].end = _frameCount;
               ++ar_block_count;
          }

          // Print out ar cblock list
          Debug(9, "\nPrinting AR cblock list before cleaning\n"
				"-----------------------------------------\n");

          for ( int i = 0; i < ar_block_count; ++i )
		  {
               Debug(9, "Block: %i\t%s Length: %s\n"
					, i
					, ar_block[i].getString().c_str()
					, secondsToString(F2L(ar_block[i].end, ar_block[i].start)).c_str() );
          }

          // Calculate histogram with noisy aspect ratios
          fillArHistogram(false);

          // Update histogram to remove replaced ratios
          for ( int i = 0 ; i < MAX_ASPECT_RATIOS; ++i )
		  {
               for ( int j = i + 1; j < MAX_ASPECT_RATIOS; ++j )
			   {
                    if (ar_histogram[j].ar_ratio < ar_histogram[i].ar_ratio+ar_delta &&
                              ar_histogram[j].ar_ratio > ar_histogram[i].ar_ratio-ar_delta )
                         ar_histogram[j].ar_ratio = ar_histogram[i].ar_ratio;
               }

          }

          // Normalize aspect ratios
          for ( int i = 0; i < ar_block_count; ++i )
		  {
               new_ar_ratio = FindARFromHistogram (ar_block[i].ar_ratio);
               ar_block[i].ar_ratio = new_ar_ratio;
          }

          // Calculate histogram with normalized aspect ratios
          fillArHistogram(true);
          dominant_ar = ar_histogram[0].ar_ratio;

again:
          // Clean up ar cblock list

          for ( int i = ar_block_count - 1; i > 0; --i )
		  {
               length = ar_block[i].end - ar_block[i].start;

               if ( cut_on_ar_change > 2
						&& length < cut_on_ar_change*(int)fps
						&& ar_block[i].ar_ratio != AR_UNDEF )
			   {
                    Debug(6, "Undefining AR cblock %i because length of %s is too short\n"
						, i, secondsToString(length / fps).c_str());
                    ar_block[i].ar_ratio = AR_UNDEF;
                    goto again;
               }

               /*
               			if (ar_block[i].ar_ratio == AR_UNDEF && length < 5*(int)fps) {
               				ar_block[i - 1].end = ar_block[i].end;
               				ar_block_count--;
               				Debug(
               					6,
               					"Deleting AR cblock %i because it is too short\n",
               					i,
               					secondsToString(length / fps).c_str()
               				);
               				for (j = i; j < ar_block_count; j++) {
               					ar_block[j].start = ar_block[j + 1].start;
               					ar_block[j].end = ar_block[j + 1].end;
               					ar_block[j].ar_ratio = ar_block[j + 1].ar_ratio;
               				}
               				goto again;
               			}
               */
#if 1
               if (commDetectMethod & DET::LOGO
						&&	ar_block[i - 1].ar_ratio != AR_UNDEF
						&& ar_block[i].ar_ratio
								> ar_block[i - 1].ar_ratio
						&& checkFrameForLogo(ar_block[i-1].end)
						&& checkFrameForLogo(ar_block[i].start) )
			   {
                    if (ar_block[i].end - ar_block[i].start > ar_block[i-1].end - ar_block[i-1].start)
					{
                         int j(ar_block[i-1].start);
                         ar_block[i-1] = ar_block[i];
                         ar_block[i-1].start = j;
                    }
					else
                         ar_block[i - 1].end = ar_block[i].end;

                    --ar_block_count;

                    Debug(6, "Joining AR blocks %i and %i because both have logo\n"
							, i - 1, i);

                    for ( int j = i; j < ar_block_count; ++j )
                         ar_block[j] = ar_block[j + 1];

                    goto again;
               }
//
#endif
               if ( i == 1 && ar_block[i-1].ar_ratio == AR_UNDEF )
			   {
                    int j(ar_block[i - 1].start);
                    ar_block[i - 1] = ar_block[i];
                    ar_block[i - 1].start = j;
                    --ar_block_count;
                    Debug(6, "Joining AR blocks %i and %i because cblock 0"
							" has an AR ratio of %f\n"
							, i - 1
							, i
							, ar_block[i-1].ar_ratio);

                    for ( j = i; j < ar_block_count; ++j )
                         ar_block[j] = ar_block[j + 1];

                    goto again;

               }

               if (( ar_block[i].ar_ratio - ar_block[i - 1].ar_ratio < ar_delta &&
                         ar_block[i].ar_ratio - ar_block[i - 1].ar_ratio > -ar_delta ))
			   {
                    ar_block[i - 1].end = ar_block[i].end;
                    --ar_block_count;
                    Debug(6, "Joining AR blocks %i and %i because both"
							" have an AR ratio of %.2f\n"
							, i - 1, i, ar_block[i].ar_ratio);

                    for ( int j = i; j < ar_block_count; ++j )
                         ar_block[j] = ar_block[j + 1];

                    goto again;

               }
               if (  ar_block[i-1].ar_ratio == AR_UNDEF && i > 1 &&
                         ar_block[i].ar_ratio - ar_block[i - 2].ar_ratio < ar_delta &&
                         ar_block[i].ar_ratio - ar_block[i - 2].ar_ratio > -ar_delta )
			   {
                    ar_block[i - 2].end = ar_block[i].end;
                    ar_block_count -= 2;
                    Debug(6, "Joining AR blocks %i and %i because they have a dummy cblock inbetween\n", i - 2, i);

                    for ( int j = i - 1; j < ar_block_count; ++j )
                         ar_block[j] = ar_block[j + 2];

                    goto again;

               }
          }

          // Print out ar cblock list
          Debug(4, "\nPrinting AR cblock list\n-----------------------------------------\n");

          for ( int i = 0; i < ar_block_count; ++i )
		  {
               Debug(4, "Block: %i\t%s Length: %s\n"
					, i
					, ar_block[i].getString().c_str()
					, secondsToString(F2L(ar_block[i].end, ar_block[i].start)).c_str() );
          }
     }

	// close out the last cc cblock
	if ( doProcessCc )
	{
		_ccBlock[_ccBlockCount]._endFrame = _frameCount;
		++_ccBlockCount;

		_ccText[_ccTextCount]._endFrame = _frameCount;
		++_ccTextCount;

		for ( int i = _ccTextCount - 1; i > 0; --i )
		{
			if ( _ccText[i]._text.length() )
			{
				for ( int j = i; j < _ccTextCount; ++j )
					_ccText[j] = _ccText[j + 1];

				--_ccTextCount;
			}
		}

		Debug(2, "Closed caption transcript\n--------------------\n");

		for ( int i = 0; i < _ccTextCount; ++i )
		{
			Debug(2, "%i) S:%6i E:%6i L:%4zu %s\n"
					, i
					, _ccText[i]._startFrame
					, _ccText[i]._endFrame
					, _ccText[i]._text.length()
					, _ccText[i]._text.c_str());
		}
	}

	if ( flagFiles.framearray.use )
		outputFrameArray(false);

	buildBlocks(false);

	if ( commDetectMethod & DET::LOGO )
		printLogoFrameGroups();

	weighBlocks();

	foundCommercials = outputBlocks();

	if ( _verbose )
	{
		Debug(1, "\n%i Frames Processed\n", framesprocessed);
		FlagFile &ff(flagFiles.log);
		ff.open(_logFilename.c_str(), "a+");
		ff.printf("################################################################\n");
		time(&ltime);
		ff.printf("Time at end of run:\n%s", ctime(&ltime));
		ff.printf("################################################################\n");
		ff.close();
	}

	if ( ccCheck && doProcessCc )
	{
		const std::string ccyes(_workBasename + ".ccyes");
		const std::string ccno(_workBasename + ".ccno");

		if ( (_mostCcType == CC_PAINTON)
				|| (_mostCcType == CC_ROLLUP)
				|| (_mostCcType == CC_POPON) )
		{
			FILE *f(fopen(ccyes.c_str(), "w"));
			fclose(f);
			remove(ccno.c_str());
		}
		else
		{
			FILE *f(fopen(ccno.c_str(), "w"));
			fclose(f);
			remove(ccyes.c_str());
		}
	}

	if ( deleteLogoFile )
	{
		FILE *f(fopen(_logoFilename.c_str(), "r"));

		if ( f )
		{
			::fclose(f);
			::remove(_logoFilename.c_str());
		}
	}

	// free(frame);
    return foundCommercials;
}

#if 0
bool WithinDivisibleTolerance(double test_number
		, double divisor, double tolerance)
{
	double added(test_number + tolerance);
	double remainder(added - divisor * ((int)(added / (double)divisor)));
	return ((remainder >= 0.0) && (remainder <= (2.0 * tolerance)));
}
#endif

/*
void CalculateCorrelation()
{
	int i,j;
	double position;
	double length;
	double min_weight;
	double pivot;
	double correlation;
	double distance;
	double weight;

	for ( i = 0; i < BlockInfo::count; ++i ) {
		pivot = ((double)(cblock[i].f_start+cblock[i].f_end)/(2*_frameCount));
		length = ((double)(cblock[i].f_end - cblock[i].f_start)/_frameCount);
		correlation = 0;
		min_weight = length;
		for ( j = 0; j < BlockInfo::count; ++j ) {
			if (i != j) {
				distance = fabs(pivot - ((double)(cblock[j].f_start+cblock[j].f_end)/(2*_frameCount)));
				weight = ((double)(cblock[j].f_end - cblock[j].f_start)/_frameCount);
				if (min_weight > weight)
					min_weight = weight;
				correlation += (1 / (distance * distance) ) / (weight );
			}
		}
		cblock[i].correlation = correlation / length * (min_weight*min_weight*min_weight);
	}

}

void CalculateFit()
{
	int i,j;
	int start;
	double position;
	double length;
	double min_weight;
	double pivot;
	double correlation;
	double prev_correlation = 0;
	double distance;
	double weight;

	for ( i = 0; i < BlockInfo::count; ++i ) {
		start = cblock[i].f_start;
		correlation = 0;
		j = i+1;
		if (F2L(cblock[i].f_end, cblock[i].f_start) < _commBreakSize._size.max && j < BlockInfo::count) {
			while ( F2L(cblock[j].f_end, cblock[j].f_start) < _commBreakSize._size.max && j < BlockInfo::count - 1 && F2L(cblock[j].f_end, start)  < _commBreakSize._break.max)
				j++;
			if ( F2L(cblock[j].f_start, start) > _commBreakSize._break.min ) {
				correlation = j - i;
			}
		}
		if (correlation > prev_correlation)
			prev_correlation = correlation;
		cblock[i].correlation = prev_correlation;
		prev_correlation -= 1;
	}
}

*/


/*
int beforeblocks[100];
int afterblocks[100];

int MatchBlocks(int k, char *t)
{
// Match string ([*+]*[CS]+)*M([*+]*[CS]+)*
	int match = false;
	char after[80];

	int i = 0;

	while ( t[i] != 0 && t[i] != 'M' )
		++i;

	if ( t[i] == 0 )
		return 0;

	int j = 0;

	while ( t[i+j+1] != 0 )
	{
		after[j] = t[i+j+1];
		++j;
	}

	return 0;
}
*/

int length_order[2000];
bool length_sorted;
int min_val[10];
int max_val[10];
//int delta_val[10];

void BuildPunish(void)
{
	if ( ! length_sorted )
	{
		for ( int i = 0 ; i < BlockInfo::count; ++i )
			length_order [i] = i;
again:
		for ( int j = 0; j < BlockInfo::count; ++j )
		{
			for ( int i = j; i< BlockInfo::count; ++i )
			{
				if ( cblock[length_order[i]].length
							> cblock[length_order[j]].length )
				{
					int t = length_order[j];
					length_order[j] = length_order[i];
					length_order[i] = t;
					goto again;
				}
			}
		}

		length_sorted = true;
	}

	max_val[0] = min_val[0] = cblock[length_order[0]].brightness;
	max_val[1] = min_val[1] = cblock[length_order[0]].volume;
	max_val[2] = min_val[2] = cblock[length_order[0]].silence;
	max_val[3] = min_val[3] = cblock[length_order[0]].uniform;
	max_val[4] = min_val[4] = cblock[length_order[0]].ar_ratio;
	max_val[5] = min_val[5] = cblock[length_order[0]].schange_rate;

	int l = 0;

	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		l += cblock[length_order[i]].length * fps;
#define MINMAX(I,FIELD)	{ if (min_val[I] > cblock[length_order[i]].FIELD)			min_val[I] = cblock[length_order[i]].FIELD; if (max_val[I] < cblock[length_order[i]].FIELD) max_val[I] = cblock[length_order[i]].FIELD; }
		MINMAX(0, brightness)
		MINMAX(1, volume)
		MINMAX(2, silence)
		MINMAX(3, uniform)
		MINMAX(4, ar_ratio)
		MINMAX(5, schange_rate)

		if ( l > cblock[BlockInfo::count - 1].f_end* 70 / 100 )
			break;
	}
}

void CommercialSkipper::weighBlocks(void)
{
	double cl;
	double combined_length;
	double tolerance;
	double wscore(0.0);
	double lscore(0.0);
#if 0
	bool end_deleted(false);
	bool start_deleted(false);
#endif
	// int framecount(0);
	double max_score(99.99);
	int max_combined_count(25);
	bool breakforcombine(false);
	// double schange_modifier;

	if ( commDetectMethod & DET::AR )
	{
		// showAvgAR = AverageARForBlock(1, framesprocessed);
		SetARofBlocks();
	}

	for ( int i = 0; i < BlockInfo::count - 2; ++i )
	{
		if ( CUTCAUSE(cblock[i].cause) == C_a
				&& CUTCAUSE(cblock[i+1].cause) == C_a
				&& cblock[i+1].length < 3.0
				&& fabs(cblock[i].ar_ratio - cblock[i+2].ar_ratio) < ar_delta )
		{
			Debug(2, "Deleting cblock %d starting at frame %d"
					" because too short and same AR before and after\n"
					, i+1
					, cblock[i+1].f_start);
			cblock[i].b_tail = cblock[i+2].b_tail;
			cblock[i].f_end = cblock[i+2].f_end;
			cblock[i].length += cblock[i+1].length + cblock[i+2].length;

			for ( int j = i + 1; j < BlockInfo::count - 2; ++j )
				cblock[j] = cblock[j+2];

			BlockInfo::count -= 2;
		}
	}

	if ( doProcessCc )
	{
		printCcBlocks();

		for ( int i = 0; i < BlockInfo::count; ++i )
			cblock[i].cc_type = determineCcTypeForBlock(cblock[i].f_start
											, cblock[i].f_end);
	}

	if ( commDetectMethod & DET::LOGO )
	{
		if ( logoPercentage < logo_fraction - 0.05 || logoPercentage > logo_percentile )
		{
			Debug(1, "Not enough or too much logo's found,"
					" disabling the use of Logo detection\n");
			commDetectMethod -= DET::LOGO;
			max_score = 10000;
		}
	}

	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		cblock[i].logo = (commDetectMethod & DET::LOGO)
				? calculateLogoFraction(cblock[i].f_start, cblock[i].f_end)
				: 0;
	}

	// CalculateCorrelation();
	// CalculateFit();

	cleanLogoBlocks(); // Can join blocks, so recalculate logo

	if ( commDetectMethod & DET::SCENE_CHANGE )
	{
		for ( int i = 0; i < BlockInfo::count; ++i )
			Debug(5, "Block %.3i\tschange_rate - %.2f\t average - %.2f\n"
					, i
					, cblock[i].schange_rate
					, avg_schange);
	}

	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		cblock[i].logo = (commDetectMethod & DET::LOGO)
				? calculateLogoFraction(cblock[i].f_start, cblock[i].f_end)
				: 0;
	}


	if ( (commDetectMethod & DET::LOGO) && logoPercentage > 0.4 )
	{
		if ( score_percentile + logoPercentage < 1.0 )
			score_percentile = logoPercentage + score_percentile;
	}
	else if ( score_percentile < 0.5 )
		score_percentile = 0.71;

#if 0
	// TESTING!!!!!!!!!!!!!!!!!!

	if ( (commDetectMethod & DET::LOGO)
			&& logoPercentage > logo_fraction
			&& logoPercentage < logo_percentile
			&& logo_present_modifier != 1.0 )
		excessive_length_modifier = 1;
#endif

	Debug(5, "\nFuzzy scoring of the blocks\n---------------------------\n");

	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		if ( i == 0
#if 0
				|| (cblock[i-1].cause & (C_b | C_u | C_v))
				|| cut_on_ar_change == 2
				|| ( ! (commDetectMethod & BLACK_FRAME)
						&& (cblock[j].cause & C_v) )
#else
				|| true )
#endif
		{
			int j = i;
			combined_length = cblock[i].length;

#if 0
			while ( j < BlockInfo::count
					&& ((cblock[j].cause & C_a)
					&& (cut_on_ar_change == 1)
					&& ! (!(commDetectMethod & BLACK_FRAME)
						&& (cblock[j].cause & C_v)) ) )
				combined_length += cblock[++j].length;
#endif

//expand:
			int k = j;

			if ( i > 0 && ((CUTCAUSE(cblock[i-1].cause) == C_b)
					|| (CUTCAUSE(cblock[i-1].cause) == C_u)) )
				combined_length -= cblock[i].b_head / fps / 4 ;

			if ( (CUTCAUSE(cblock[i].cause) == C_b) || (CUTCAUSE(cblock[i].cause) == C_u) )
				combined_length -= cblock[j+1].b_head / fps / 4 ;

			combined_length -= (cblock[i].b_head + cblock[j + 1].b_head) / fps / 4;
			tolerance = (cblock[i].b_head + cblock[j + 1].b_head + 4) / fps;

			if ( isStandardCommercialLength(combined_length, tolerance, true) )
			{
				while ( j >= i )
				{
					cblock[j].strict = 2;
					Debug(2, "Block %i has strict standard length for a commercial.\n", j);
					Debug(3, "Block %i score:\tBefore - %.2f\t", j, cblock[j].score);
					cblock[j].score *= length_strict_modifier;
					// cblock[j].score *= length_strict_modifier;
					Debug(3, "After - %.2f\n", cblock[j].score);
					cblock[j].cause |= C_STRICT;
					cblock[j].more |= C_STRICT;
					--j;
				}
			}
			else if ( isStandardCommercialLength(combined_length, tolerance, false) )
			{
				while ( j >= i )
				{
					cblock[j].strict = 1;
					Debug(2, "Block %i has non-strict standard length for a commercial.\n", j);
					Debug(3, "Block %i score:\tBefore - %.2f\t", j, cblock[j].score);
					cblock[j].score *= length_nonstrict_modifier;
					cblock[j].score = (cblock[j].score > max_score) ? max_score : cblock[j].score;
					Debug(3, "After - %.2f\n", cblock[j].score);
					cblock[j].cause |= C_NONSTRICT;
					cblock[j].more |= C_NONSTRICT;
					--j;
				}
			}
			else
			{
				while ( j >= i )
					cblock[j--].strict = 0;
			}

			j = k;
#if 0
			if ( j + 1 < BlockInfo::count && cblock[i].strict == 0 && cblock[j+1].length < 5.0 )
			{
				combined_length += cblock[++j].length;
				goto expand;
			}
#endif
		}

#if 0
		tolerance = (cblock[i].bframe_count + cblock[i + 1].bframe_count + 6) / fps;
		if (isStandardCommercialLength(cblock[i].length, tolerance, true)) {
			cblock[i].strict = 2;
			Debug(2, "Block %i has strict standard length for a commercial.\n", i);
			Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
			cblock[i].score *= length_strict_modifier;
			cblock[i].score *= length_strict_modifier;
			Debug(3, "After - %.2f\n", cblock[i].score);
			cblock[i].cause |= C_STRICT;
		} else if (isStandardCommercialLength(cblock[i].length, tolerance, false)) {
			cblock[i].strict = 1;
			Debug(2, "Block %i has non-strict standard length for a commercial.\n", i);
			Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
			cblock[i].score *= length_nonstrict_modifier;
			cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
			Debug(3, "After - %.2f\n", cblock[i].score);
			cblock[i].cause |= C_NONSTRICT;
		} else
			cblock[i].strict = 0;
#endif

#if 1
		if ( cblock[i].combined_count < max_combined_count )
		{
			// Debug(3, "Attempting to combine cblock %i\n", i);
			combined_length = cblock[i].length;

			for ( int j = 1; j < BlockInfo::count - i; ++j )
			{
				if ( isStandardCommercialLength(cblock[i + j].length - (cblock[i+j].b_head + cblock[i + j + 1].b_head) / fps, (cblock[i+j].bframe_count + cblock[i + j + 1].bframe_count + 2) / fps, true))
				{
#if 0
					Debug(3, "Not attempting to forward combine blocks %i"
							" to %i because cblock %i is strict commercial.\n"
							, i, i + j, i + j);
					break;
#endif
				}

				if ((cblock[i + j].combined_count > max_combined_count)
						|| (cblock[i].combined_count > max_combined_count))
				{
					Debug(3, "Not attempting to forward combine blocks %i"
							" to %i because cblock %i has already been combined %i times.\n"
							, i, i + j, i + j, cblock[i + j].combined_count);
					breakforcombine = true;
					break;
				}

				tolerance = (cblock[i].bframe_count + cblock[i + j + 1].bframe_count + 2) / fps;
				combined_length += cblock[i + j].length;

				if ( combined_length > (_commBreakSize._size.max) + tolerance )
				{
					/*
					Debug(2, "Not trying to combine blocks %i thru %i"
					" due to excessive length - %f\n", i, i + j, combined_length);
					*/
					break;
				}
				else
				{
					if ( isStandardCommercialLength(combined_length - (cblock[i].b_head + cblock[i + j + 1].b_head) / fps, tolerance, true) && combined_length_strict_modifier != 1.0 )
					{
						Debug(2, "Combining Blocks %i thru %i"
								" result in strict standard commercial length of %.2f"
								" with a tolerance of %f.\n"
								, i, i + j, combined_length, tolerance);

						for ( int k = 0; k <= j; ++k )
						{
							Debug(3, "Block %i score:\tBefore - %.2f\t"
									, i + k, cblock[i + k].score);
							cblock[i + k].score *= 1 + (combined_length_strict_modifier / (j + 1) / 2);
							cblock[i + k].score = (cblock[i + k].score > max_score)
									? max_score
									: cblock[i + k].score;
							cblock[i + k].combined_count += 1;
							Debug(3, "After - %.2f\tCombined count - %i\n"
									, cblock[i + k].score
									, cblock[i + k].combined_count);
							cblock[i + k].cause |= C_COMBINED;
							cblock[i + k].more |= C_COMBINED;
						}
					}
					else if ( isStandardCommercialLength(combined_length - (cblock[i].b_head + cblock[i + j + 1].b_head) / fps, tolerance, false) && combined_length_nonstrict_modifier != 1.0 )
					{
						Debug(2,"Combining Blocks %i thru %i"
								" result in non-strict standard commercial length"
								" of %.2f with a tolerance of %f.\n"
								, i
								, i + j
								, combined_length
								, tolerance);

						for ( int k = 0; k <= j; ++k )
						{
							Debug(3, "Block %i score:\tBefore - %.2f\t"
									, i + k
									, cblock[i + k].score);
							cblock[i + k].score *= 1 + (combined_length_nonstrict_modifier / (j + 1) / 2);
							cblock[i + k].score = (cblock[i + k].score > max_score)
									? max_score
									: cblock[i + k].score;
							cblock[i + k].combined_count += 1;
							Debug(3, "After - %.2f\tCombined count - %i\n"
									, cblock[i + k].score
									, cblock[i + k].combined_count);
							cblock[i + k].cause |= C_COMBINED;
							cblock[i + k].more |= C_COMBINED;
						}
					}
				}
			}

			if ( breakforcombine )
			{
				// Debug(3, "Block %i Break for forward combined limit\n", i);
				breakforcombine = false;
			}

			combined_length = cblock[i].length;

			for ( int j = 1; j < i; ++j )
			{
				if (isStandardCommercialLength(cblock[i - j].length - (cblock[i-j].b_head + cblock[i - j + 1].b_head)/fps, (cblock[i-j].bframe_count + cblock[i - j + 1].bframe_count + 2) / fps, true))
				{
#if 0
					Debug(3, "Not attempting to forward combine blocks"
							" %i to %i because cblock %i is strict commercial.\n"
							, i - j
							, i
							, i - j);
#endif
                         break;
				}

				if ( (cblock[i - j].combined_count > max_combined_count)
						|| (cblock[i].combined_count > max_combined_count) )
				{
					Debug(3, "Not attempting to backward combine blocks"
							" %i to %i because cblock %i has already been combined"
							" %i times.\n"
							, i - j
							, i
							, i - j
							, cblock[i - j].combined_count);

					breakforcombine = true;
					break;
				}

				tolerance = (cblock[i + 1].bframe_count + cblock[i - j].bframe_count + 2) / fps;
				combined_length += cblock[i - j].length;

				if ( combined_length >= _commBreakSize._size.max )
				{
#if 0
					Debug(2, "Not trying to backward combine blocks %i thru %i"
							" due to excessive length - %f\n"
							, i - j
							, i
							, combined_length);
#endif
					break;
				}
				else
				{
					if (isStandardCommercialLength(combined_length - (cblock[i + 1].b_head + cblock[i - j].b_head) / fps, tolerance, true) && combined_length_strict_modifier != 1.0 )
					{
						Debug(2, "Combining Blocks %i thru %i results in strict standard"
								" commercial length of %.2f with a tolerance of %f.\n"
								, i - j
								, i
								, combined_length
								, tolerance);

						for ( int k = 0; k <= j; ++k )
						{
							Debug(3, "Block %i score:\tBefore - %.2f\t"
									, i - k
									, cblock[i - k].score);

							cblock[i - k].score *= 1 + (combined_length_strict_modifier / (j + 1) / 2);
							cblock[i - k].score = (cblock[i - k].score > max_score)
									? max_score
									: cblock[i - k].score;
							cblock[i - k].combined_count += 1;

							Debug(3, "After - %.2f\tCombined count - %i\n"
									, cblock[i - k].score
									, cblock[i - k].combined_count);

							cblock[i - k].cause |= C_COMBINED;
							cblock[i - k].more |= C_COMBINED;
						}
					}
					else if ( isStandardCommercialLength(combined_length - (cblock[i + 1].b_head + cblock[i - j].b_head) / fps, tolerance, false) && combined_length_nonstrict_modifier != 1.0 )
					{
						Debug(2, "Combining Blocks %i thru %i result in non-strict"
								" standard commercial length of %.2f"
								" with a tolerance of %f.\n"
								, i - j
								, i
								, combined_length
								, tolerance);

						for ( int k = 0; k <= j; ++k )
						{
							Debug(3, "Block %i score:\tBefore - %.2f\t"
									, i - k
									, cblock[i - k].score);

							cblock[i - k].score
									*= 1 + (combined_length_nonstrict_modifier / (j + 1) / 2);
							cblock[i - k].score = (cblock[i - k].score > max_score)
									? max_score
									: cblock[i - k].score;
							cblock[i - k].combined_count += 1;

							Debug(3, "After - %.2f\tCombined count - %i\n"
									, cblock[i - k].score
									, cblock[i - k].combined_count);

							cblock[i - k].cause |= C_COMBINED;
							cblock[i - k].more |= C_COMBINED;
						}
					}
				}
			}

			if ( breakforcombine )
			{
				// Debug(3, "Block %i Break for backward combined limit\n", i);
				breakforcombine = false;
			}
		}
#endif
          // if logo detected in cblock, score = 10%
          if (commDetectMethod & DET::LOGO) {
               if (cblock[i].logo > 0.20) {
                    Debug(2, "Block %i has logo.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= logo_present_modifier;
//				cblock[i].score *= (logo_present_modifier*cblock[i].logo) + (1-cblock[i].logo);
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_LOGO;
                    cblock[i].less |= C_LOGO;
               }
               /*			else if (cblock[i].logo > 0.10) {
               				Debug(2, "Block %i has logo.\n", i);
               				Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
               				cblock[i].score *= logo_present_modifier;
               //				cblock[i].score *= (logo_present_modifier*cblock[i].logo) + (1-cblock[i].logo);
               				cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
               				Debug(3, "After - %.2f\n", cblock[i].score);
               				cblock[i].cause |= C_LOGO;
               				cblock[i].less |= C_LOGO;
               			}
               */			else if (punish_no_logo && cblock[i].logo < 0.05 && logoPercentage > logo_fraction) {
                    Debug(2, "Block %i has no logo.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= 2;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_LOGO;
                    cblock[i].more |= C_LOGO;
               }
          }

          BuildPunish();

          if ( true ) {
               if ((punish & 1) && cblock[i].brightness > avg_brightness * punish_threshold) {
                    Debug(2, "Block %i is much brighter than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= punish_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_AB;
                    cblock[i].more |= C_AB;
               }
               if ((punish & 2) && cblock[i].uniform > avg_uniform * punish_threshold) {
                    Debug(2, "Block %i is less uniform than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= punish_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_AU;
                    cblock[i].more |= C_AU;
               }
               if ((punish & 4) && cblock[i].volume > avg_volume * punish_threshold) {
                    Debug(2, "Block %i is much louder than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= punish_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_AL;
                    cblock[i].more |= C_AL;
               }

               if ((punish & 8) && cblock[i].silence > avg_silence * punish_threshold) {
                    Debug(2, "Block %i has less silence than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= punish_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_AS;
                    cblock[i].more |= C_AS;
               }
               if ((punish & 16) && cblock[i].schange_count > 2 && cblock[i].schange_rate > avg_schange * punish_threshold) {
                    Debug(2, "Block %i has more scene change than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= punish_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_AC;
                    cblock[i].more |= C_AC;
               }
          }
          if (false) {
               if ((reward & 1) && cblock[i].brightness < avg_brightness / punish_threshold) {
                    Debug(2, "Block %i is much darker than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= reward_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_BRIGHT;
                    cblock[i].less |= C_BRIGHT;
               }
               if ((reward & 2) && cblock[i].uniform < avg_uniform / punish_threshold) {
                    Debug(2, "Block %i is more uniform than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= reward_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_BRIGHT;
                    cblock[i].less |= C_BRIGHT;
               }
               if ((reward & 4) && cblock[i].volume < avg_volume / punish_threshold) {
                    Debug(2, "Block %i is much quieter than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= reward_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_BRIGHT;
                    cblock[i].less |= C_BRIGHT;
               }
               if ((reward & 8) && cblock[i].silence < avg_silence / punish_threshold) {
                    Debug(2, "Block %i has more silence than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= reward_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_BRIGHT;
                    cblock[i].less |= C_BRIGHT;
               }
               if ((reward & 16) && cblock[i].schange_count > 2 && cblock[i].schange_rate < avg_schange / punish_threshold) {
                    Debug(2, "Block %i has less scene change than average.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= reward_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_BRIGHT;
                    cblock[i].less |= C_BRIGHT;
               }
          }

//		cblock[i].logo > 0.5 && F2L(cblock[i].f_end, cblock[i].f_start) > min_show_segment_length
#if 0
          // if length < min_show_segment_length, score = 150%
          if (cblock[i].length < min_show_segment_length && cblock[i].logo < 0.2 )) {
               Debug(2, "Block %i is shorter then minimum show segment.\n", i);
               Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
               cblock[i].score *= 1.5;
               cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
               Debug(3, "After - %.2f\n", cblock[i].score);
          }

#endif
#if 0
          if (_useFrameArray && cblock[i].length < _commBreakSize._break.max &&
                         cblock[i].brightness < avg_brightness) {
               Debug(2, "Block %i is short but has low brightness.\n", i);
               Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
               cblock[i].score *= dark_block_modifier;
               cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
               Debug(3, "After - %.2f\n", cblock[i].score);
          }
#endif
          // if length > _commBreakSize._size.max * fps, score = 10%
          if (cblock[i].length > 2 * min_show_segment_length) {
               Debug(2, "Block %i has twice excess length.\n", i);
               Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
               cblock[i].score *= excessive_length_modifier * excessive_length_modifier;
               cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
               Debug(3, "After - %.2f\n", cblock[i].score);
               cblock[i].cause |= C_EXCEEDS;
               cblock[i].less |= C_EXCEEDS;
          } else

               if (cblock[i].length > min_show_segment_length) {
                    Debug(2, "Block %i has excess length.\n", i);
                    Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                    cblock[i].score *= excessive_length_modifier;
                    cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    Debug(3, "After - %.2f\n", cblock[i].score);
                    cblock[i].cause |= C_EXCEEDS;
                    cblock[i].less |= C_EXCEEDS;
               }

          // Mod score based on scene change rate
          /*
          		if ( (commDetectMethod & SCENE_CHANGE) && (cblock[i].schange_count > 2) && (cblock[i].length > 3)) {
          #if 0
          			schange_modifier = (cblock[i].schange_rate / avg_schange);
          			schange_modifier = (schange_modifier > min_schange_modifier) ? schange_modifier : min_schange_modifier;
          			schange_modifier = (schange_modifier < max_schange_modifier) ? schange_modifier : max_schange_modifier;
          			Debug(3, "SC modifier - %.3f\tBlock %i score:\tBefore - %.2f\t", schange_modifier, i, cblock[i].score);
          			cblock[i].score *= schange_modifier;
          			cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
          			Debug(3, "\tSC\tAfter - %.2f\n", cblock[i].score);
          #else
          			schange_modifier = (cblock[i].schange_rate / avg_schange);
          			if (schange_modifier > 2.0 || schange_modifier < 0.5  ) {
          				schange_modifier = (schange_modifier > min_schange_modifier) ? schange_modifier : min_schange_modifier;
          				schange_modifier = (schange_modifier < max_schange_modifier) ? schange_modifier : max_schange_modifier;
          				Debug(3, "SC modifier - %.3f\tBlock %i score:\tBefore - %.2f\t", schange_modifier, i, cblock[i].score);
          				cblock[i].score *= schange_modifier;
          				cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
          				Debug(3, "\tSC\tAfter - %.2f\n", cblock[i].score);
          				cblock[i].cause |= C_SC;
          			}
          #endif

          		}
          */
          // Mod score based on CC type
          if ( doProcessCc )
		  {
          if ( _mostCcType == CC_NONE )
		  {
                    if ( cblock[i].cc_type != CC_NONE )
					{
                         Debug(3, "CC's exist in a non-CC'd show - Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                         cblock[i].score *= cc_commercial_type_modifier * 2;
                         Debug(3, "After - %.2f\n", cblock[i].score);
                         cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    }
               }
		  else
		  {
                    if (cblock[i].cc_type == _mostCcType) {
                         Debug(3, "CC's correct type - Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                         cblock[i].score *= cc_correct_type_modifier;
                         Debug(3, "After - %.2f\n", cblock[i].score);
                         cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    } else if (cblock[i].cc_type == CC_COMMERCIAL) {
                         Debug(3, "CC's commercial type - Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                         cblock[i].score *= cc_commercial_type_modifier;
                         Debug(3, "After - %.2f\n", cblock[i].score);
                         cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    }
					else if ( cblock[i].cc_type == CC_NONE )
					{
                         Debug(3, "No CC's - Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                         cblock[i].score *= (((cc_wrong_type_modifier-1.0)/2)+1.0);
                         Debug(3, "After - %.2f\n", cblock[i].score);
                         cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    } else {
                         Debug(3, "CC's wrong type - Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
                         cblock[i].score *= cc_wrong_type_modifier;
                         Debug(3, "After - %.2f\n", cblock[i].score);
                         cblock[i].score = (cblock[i].score > max_score) ? max_score : cblock[i].score;
                    }
               }
          }

          // Mod score based on AR
//		if (commDetectMethod & AR) {
          cblock[i].ar_ratio = AverageARForBlock(cblock[i].f_start, cblock[i].f_end);
          if ((dominant_ar - cblock[i].ar_ratio >= ar_delta ||
                    dominant_ar - cblock[i].ar_ratio <= - ar_delta)
//				cblock[i].length < min_show_segment_length
//				&& (cblock[i].length > 5.0 || cblock[i].ar_ratio - ar_delta < dominant_ar)
                  ) {
               Debug(2, "Block %i AR (%.2f) is different from dominant AR(%.2f).\n",i,cblock[i].ar_ratio, dominant_ar);
               Debug(3, "Block %i score:\tBefore - %.2f\t", i, cblock[i].score);
               cblock[i].score *= ar_wrong_modifier;
               Debug(3, "After - %.2f\n", cblock[i].score);
               cblock[i].cause |= C_AR;
               cblock[i].more |= C_AR;
          }
//		}
     }

	if ( doProcessCc )
	{
		if ( processCcDict() )
			Debug(4, "Dictionary processed successfully\n");
		else
			Debug(4, "Dictionary not processed successfully\n");
	}

#if 0
	for ( int i = 0; i < BlockInfo::count; ++i )
		outputStrict(cblock[i].length, (double) cblock[i].strict, 0.0);
#endif

     if (!(disable_heuristics & (1 << (2 - 1))))
	 {
          for ( int i = 0; i < BlockInfo::count - 2; ++i )
		  {
               if ( ((cblock[i].cause & C_STRICT) && (cblock[i].cause & (C_b | C_u | C_v | C_r)) )  &&
                         cblock[i+1].score > 1.05 &&  cblock[i+1].length < 4.8 &&
                         cblock[i+2].score < 1.0  &&  cblock[i+2].length > min_show_segment_length
                  )
			   {
                    cblock[i+1].score = 0.5;
                    Debug(3, "H2 Added cblock %i because short and after strict commercial.\n", i+1);
                    cblock[i+1].cause |= C_H2;
                    cblock[i+1].less |= C_H2;
               }
          }

          for ( int i = 0; i < BlockInfo::count - 2; ++i )
		  {
               if ( ((cblock[i+2].cause & C_STRICT) && (cblock[i+1].cause & (C_b | C_u | C_v | C_r)) )  &&
                         cblock[i+1].score > 1.05 &&  cblock[i+1].length < 4.8 &&
                         cblock[i].score < 1.0  &&  cblock[i].length > min_show_segment_length
                  )
			   {
                    cblock[i+1].score = 0.5;
                    Debug(3, "H2 Added cblock %i because after show, short and before strict commercial.\n", i+1);
                    cblock[i+1].cause |= C_H2;
                    cblock[i+1].less |= C_H2;
               }
          }

          for ( int i = 0; i < BlockInfo::count - 2; ++i )
		  {
               if ( (cblock[i].cause & (C_b | C_u | C_r))  && (cblock[i+1].cause & C_a)  &&
                         cblock[i+1].score > 1.0 &&  cblock[i+1].length < 4.8 &&
                         cblock[i+2].score < 1.0  &&  cblock[i+2].length > min_show_segment_length
                  )
			   {
                    cblock[i+1].score = 0.5;
                    Debug(3, "H2 Added cblock %i because short and based on aspect ratio change after commercial.\n", i+1);
                    cblock[i+1].cause |= C_H2;
                    cblock[i+1].less |= C_H2;
               }
          }

          for ( int i = 0; i < BlockInfo::count - 2; ++i )
		  {
               if ( (cblock[i+1].cause & (C_b | C_u | C_r))  && (cblock[i].cause & C_a)  &&
                         cblock[i+1].score > 1.0 &&  cblock[i+1].length < 4.8 &&
                         cblock[i].score < 1.0  &&  cblock[i].length > min_show_segment_length
                  )
			   {
                    cblock[i+1].score = 0.5;
                    Debug(3, "H2 Added cblock %i because short and based on aspect ratio change before commercial.\n", i+1);
                    cblock[i+1].cause |= C_H2;
                    cblock[i+1].less |= C_H2;
               }
          }

     }

     if (!(disable_heuristics & (1 << (1 - 1))))
	 {
          for ( int i = 0; i < BlockInfo::count - 1; ++i )
		  {
               if (cblock[i].score > 1.4 && cblock[i+1].score <= 1.05 &&  cblock[i+1].score > 0.0)
			   {
                    combined_length = 0;
                    wscore = 0;
                    lscore = 0;
                    int j(i + 1);

                    while (j < BlockInfo::count && combined_length < min_show_segment_length && cblock[j].score <= 1.05 && cblock[j].score > 0.0)
					{
                         combined_length += cblock[j].length;
                         wscore += cblock[j].length * cblock[j].score;
                         lscore += cblock[j].length * cblock[j].logo;
                         ++j;
                    }

                    wscore /= combined_length;
                    lscore /= combined_length;

                    if (//lscore < 0.36 &&
                         ((combined_length < min_show_segment_length / 2.0 && wscore > 0.9) ||
                          (combined_length < min_show_segment_length / 3.0 && wscore > 0.3) ) &&
                         cblock[j].score > 1.4 &&
                         (combined_length < min_show_segment_length / 6 ||
                          (cblock[i].f_start > after_start &&
                           cblock[j].f_end < before_end)))
					{
                         for ( int k = i + 1; k < j; ++k )
						 {
                              cblock[k].score = 99.99;
                              Debug(3, "H1 Discarding cblock %i because too short and between two strong commercial blocks.\n",
                                    k);
                              cblock[k].cause |= C_H1;
                              cblock[k].more |= C_H1;
                         }

                    }

               }
          }

          for ( int i = 0; i < BlockInfo::count-1; ++i )
		  {
               if (cblock[i].score > 1.1 && cblock[i+1].score <= 1.05 &&  cblock[i+1].score > 0.0)
			   {
                    combined_length = 0.0;
                    wscore = 0;
                    lscore = 0;
                    int j = i + 1;

                    while (j < BlockInfo::count && combined_length < min_show_segment_length && cblock[j].score <= 1.05 && cblock[j].score > 0.0)
					{
                         combined_length += cblock[j].length;
                         wscore += cblock[j].length * cblock[j].score;
                         lscore += cblock[j].length * cblock[j].logo;
                         ++j;
                    }

                    wscore /= combined_length;
                    lscore /= combined_length;

                    if (//lscore < 0.36 &&
                         ((combined_length < min_show_segment_length / 4.0 && wscore > 0.9) ||
                          (combined_length < min_show_segment_length / 6 && wscore > 0.3) ) &&
                         cblock[j].score > 1.1 &&
                         (combined_length < min_show_segment_length / 12 ||
                          (cblock[i].f_start > after_start &&
                           cblock[j].f_end < before_end)))
					{
                         for ( int k = i + 1; k < j; ++k )
						 {
                              cblock[k].score = 99.99;
                              Debug(3, "H1 Discarding cblock %i because too short and between two weak commercial blocks.\n",
                                    k);
                              cblock[k].cause |= C_H1;
                              cblock[k].more |= C_H1;
                         }

                    }

               }
          }

     }

     /*
     for ( int i = 0; i < BlockInfo::count - 2; ++i ) {
     	if (cblock[i].score < 0.9 && cblock[i+1].score == 1.0 && cblock[i+1].length < min_show_segment_length/2 && cblock[i+2].score > 1.5) {
     		cblock[i+1].score *= 1.5;
     		Debug(3, "Discarding cblock %i because short and on edge between commercial and show.\n",
     				i+1);
     	}
     	if (cblock[i].score > 1.5 && cblock[i+1].score == 1.0 && cblock[i+1].length < min_show_segment_length/2 && cblock[i+2].score < 0.9) {
     		cblock[i+1].score *= 1.5;
     		Debug(3, "Discarding cblock %i because short and on edge between commercial and show.\n",
     				i+1);
     	}
     }
     */

     /*
     	if (delete_show_before_or_after_current && logoPercentage == 0) {
     		i = 0;
     		while (i < BlockInfo::count - 1 && cblock[i].score < 1.0 && cblock[i].f_end < before_end) {
     			j = i+1;
     			cl = 0.0;
     			while (cblock[j].score > 1.05 && cl + cblock[j].length < _commBreakSize._break.min && j < BlockInfo::count-1) {
     				cl += cblock[j].length;
     				j++;
     			}
     			if (cblock[j].score < 1.0) {
     				cblock[i].score = 99.99;
     				Debug(3, "Discarding cblock %i because separated from cblock %i with small non show gap.\n",
     					i, j);
     				cblock[i].cause |= C_H2;
     				cblock[i].more |= C_H2;
     				start_deleted = true;
     				break;
     			}
     			i++;
     		}
     		if (! start_deleted) {
     			j = 0;
     			i = 0;
     			if (cblock[j].score < 1.0) {
     				cblock[i].score = 99.99;
     				Debug(3, "Discarding cblock %i because of being first block.\n",
     					i, j);
     				cblock[i].cause |= C_H2;
     				cblock[i].more |= C_H2;
     				start_deleted = true;
     			}
     		}

     		i = BlockInfo::count - 1;
     		while (i > 0 && cblock[i].score < 1.05 && cblock[i].f_start > before_end) {
     			j = i-1;
     			cl = 0;
     			while (cblock[j].score > 1.05 && cl + cblock[j].length < _commBreakSize._break.min && j >0) {
     				cl += cblock[j].length;
     				j--;
     			}
     			if (cblock[j].score < 1.0) {
     				cblock[i].score = 99.99;
     				Debug(3, "Discarding cblock %i because seprated from cblock %i with small non show gap.\n",
     					i, j);
     				cblock[i].cause |= C_H2;
     				cblock[i].more |= C_H2;
     				end_deleted = true;
     				break;
     			}
     			i++;
     		}

     		if (! end_deleted) {
     			i = BlockInfo::count - 1;
     			j = BlockInfo::count - 1;
     			if (cblock[j].score < 1.05) {
     				cblock[i].score = 99.99;
     				Debug(3, "Discarding cblock %i because being last block.\n",
     					i, j);
     				cblock[i].cause |= C_H2;
     				cblock[i].more |= C_H2;
     				end_deleted = true;
     			}
     		}
     	}
     */
     if (delete_show_before_or_after_current && _logoBlockCount >= 80)
          Debug(10, "Too many logo blocks, disabling the delete_show_before_or_after_current processing\n");
     if (delete_show_before_or_after_current &&
               (commDetectMethod & DET::LOGO) && connect_blocks_with_logo &&
               !reverseLogoLogic && logoPercentage > logo_fraction - 0.05 && _logoBlockCount < 40) {
          /*
          		for ( i = 0; i < BlockInfo::count - 1; ++i ) {
          			if (cblock[i].score < 1.0 && cblock[i].logo > 0.2 && cblock[i+1].score < 1.0 && cblock[i+1].logo > 0.2 ) {
          				if (cblock[i].f_end < after_start) {
          					cblock[i].score = 99.99;
          					Debug(3, "Discarding cblock %i because cblock %i has also logo.\n",
          						i, i+1);
          				} else if (cblock[i+1].f_start > before_end) {
          					cblock[i+1].score = 99.99;
          					Debug(3, "Discarding cblock %i because cblock %i has also logo.\n",
          						i+1, i);
          				}
          			}
          		}
          	*/
          if (!(disable_heuristics & (1 << (7 - 1)))) {
               int i = 0;
               while (i < BlockInfo::count - 1 && cblock[i].score < 1.05 &&
                         ((delete_show_before_or_after_current == 1 && cblock[i].f_end < after_start) ||
                          (delete_show_before_or_after_current > 1 && delete_show_before_or_after_current > cblock[i].length))) {
                    int j = i + 1;
                    cl = 0;
                    combined_length = 0;
                    while ( cblock[j].score > 1.05 && cl + cblock[j].length < _commBreakSize._break.min && j < BlockInfo::count - 1 ) {
                         combined_length += cblock[j].length;
                         cl += cblock[j].length;
                         ++j;
                    }
                    if (cblock[j].score < 1.0 && cblock[j].length > min_show_segment_length/2 ) {
                         cblock[i].score = 99.99;
                         Debug(3, "H7 Discarding cblock %i of %i seconds because cblock %i has also logo and small non show gap.\n",
                               i, (int)cblock[i].length, j);
                         cblock[i].cause |= C_H7;
                         cblock[i].more |= C_H7;
#if 0
                         start_deleted = true;
#endif
                         break;
                    }
                    ++i;
               }

               i = BlockInfo::count - 1;
               while (i > 0 && cblock[i].score < 1.0 &&
                         ((delete_show_before_or_after_current == 1 && cblock[i].f_start > before_end) ||
                          (delete_show_before_or_after_current > 1 && delete_show_before_or_after_current > cblock[i].length))) {
                    int j = i - 1;
                    cl = 0;
                    combined_length = 0;
                    while (cblock[j].score > 1.05 && cl + cblock[j].length < _commBreakSize._break.min && j >0) {
                         combined_length += cblock[j].length;
                         cl += cblock[j].length;
                         --j;
                    }

                    if (cblock[j].score < 1.0) {
                         cblock[i].score = 99.99;
                         Debug(3, "H7 Discarding cblock %i of %i seconds because cblock %i has also logo and small non show gap.\n",
                               i, (int)cblock[i].length, j);
                         cblock[i].cause |= C_H7;
                         cblock[i].more |= C_H7;
#if 0
                         end_deleted = true;
#endif
                         break;
                    }
                    ++i;
               }
          }
          /*
          		for (int i = 0; i < BlockInfo::count - 1; ++i ) {
          			if (cblock[i].score < 1.0

          //				&& cblock[i+1].score > 1.05 && cblock[i+1].length < 10 && cblock[i+2].score < 1.0 && cblock[i+2].logo > 0.2
          				) {
          				j = i+1;
          				cl = 0;
          				while (cblock[j].score > 1.05 && cl + cblock[j].length < _commBreakSize._break.min && j < BlockInfo::count - 1) {
          					cl += cblock[j].length;
          					j++;
          				}
          				if (cblock[j].score < 1.0) {
          					if (cblock[j].f_start < after_start) {
          						cblock[i].score = 99.99;
          						Debug(3, "Discarding cblock %i because cblock %i has also logo and small non logo gap.\n",
          							i, j);
          					} else if (cblock[i].f_end > before_end) {
          						cblock[j].score = 99.99;
          						Debug(3, "Discarding cblock %i because cblock %i has also logo and small non logo gap.\n",
          							j, i);
          					}
          				}
          			}
          		}
          */
          if (!(disable_heuristics & (1 << (3 - 1)))) {
               for (int i = 0; i < BlockInfo::count - 1; ++i ) {
                    if (cblock[i].score < 1.0 && cblock[i].logo < 0.1 && cblock[i].length > min_show_segment_length ) {
                         if (cblock[i].f_end < after_start) {
                              cblock[i].score *= 1.3;
                              Debug(3, "H3 Demoting cblock %i because cblock %i has no logo and others do.\n",
                                    i, i);
                              cblock[i].cause |= C_H3;
                              cblock[i].more |= C_H3;

                         } else if (cblock[i].f_start > before_end) {
                              cblock[i].score *= 1.3;
                              Debug(3, "Demoting cblock %i because cblock %i has no logo and others do.\n",
                                    i, i);
                              cblock[i].cause |= C_H3;
                              cblock[i].more |= C_H3;

                         }
                    }
               }

          }
//	if (!(disable_heuristics & (1 << (3 - 1)))) {
          for ( int i = 1; i < BlockInfo::count - 1; ++i ) {
               if (logoPercentage > logo_fraction &&
                         cblock[i].score > 1.0 && cblock[i].logo < 0.1 &&
                         cblock[i].length > min_show_segment_length+4 ) {
                    if (cblock[i].f_start > after_start &&
                              cblock[i].f_end   < before_end &&
                              (cblock[i-1].score < 1.0 || cblock[i+1].score < 1.0 )) {
                         cblock[i].score *= 0.5;
                         Debug(3, "Promoting cblock %i because cblock %i has no logo but long and in the middle of a show.\n",
                               i, i);
                         cblock[i].cause |= C_H3;
                         cblock[i].more |= C_H3;

                    }
               }
          }

//	}
     }
     if (!(disable_heuristics & (1 << (4 - 1)))) {

          if ((commDetectMethod & DET::LOGO) && !reverseLogoLogic && logoPercentage > logo_fraction) {
               for ( int i = 1; i < BlockInfo::count; ++i ) {
                    if (cblock[i].score < 1 && cblock[i].b_head > 7 && CUTCAUSE(cblock[i-1].cause) == C_b) {
                         int j = i - 1;
                         int k = 0;
                         while (j >= 0 && k < 5 && cblock[j].b_head > 7 && cblock[j].length < 7 && CUTCAUSE(cblock[j].cause) == C_b) {
                              cblock[j].score *= 0.1;   //  Add blocks with long black periods before show
                              Debug(3, "H4 Added cblock %i because of large black gap with cblock %i\n", j, i);
                              ++k;
                              cblock[j].cause |= C_H4;
                              cblock[j].less |= C_H4;
                              --j;
                         }
                    }
               }
          }
          if ((commDetectMethod & DET::LOGO) && !reverseLogoLogic && logoPercentage > logo_fraction) {
               for ( int i = 0; i < BlockInfo::count; ++i ) {
                    if (cblock[i].score < 1 && cblock[i].b_tail > 7 && CUTCAUSE(cblock[i].cause) == C_b) {
                         int j(i + 1);
                         int k(0);
                         while (j < BlockInfo::count && k < 5 && cblock[j].b_tail > 7 && cblock[j].length < 7 && CUTCAUSE(cblock[j-1].cause) == C_b) {
                              cblock[j].score *= 0.1;   //  Add blocks with long black periods before show
                              Debug(3, "H4 Added cblock %i because of large black gap with cblock %i\n", j, i);
                              ++k;
                              cblock[j].cause |= C_H4;
                              cblock[j].less |= C_H4;
                              ++j;
                         }
                    }
               }
          }
     }
     if (!(disable_heuristics & (1 << (2 - 1)))) {
          /*		i = 0;
          		cl = 0;
          		while (cblock[i].score < 1.05 && cl + cblock[i].length < _commBreakSize._break.min && i < BlockInfo::count-1) {
          			k += cblock[i].length;
          			i++;
          		}
          		if (i < BlockInfo::count-1 && cblock[i].score > 1.05 && cl < _commBreakSize._break.min) {
          			for ( int j = 0; j < i; ++j ) {
          				cblock[j].score = 99.99;
          				Debug(3, "H2 Discarding cblock %i because too short and before commercial.\n",
          					j);
          				cblock[j].cause |= C_H2;
          				cblock[j].more |= C_H2;
          			}
          		}
          */
     }
     /*

     	if (!(disable_heuristics & (1 << (1 - 1)))) {

     	for ( int i = 0; i < BlockInfo::count - 2; ++i ) {
     		if (cblock[i].score < 1.05 && cblock[i+1].score > 1.05 && cblock[i+2].score < 1.05 &&
     			cblock[i+1].length > min_show_segment_length && logoPercentage < 0.7) {
     			int j = i + 2;
     			for ( int k = i + 1; k < j; ++k ) {
     					cblock[k].score = 0.05;
     					Debug(3, "H1 Included cblock %i because too long and between two show blocks.\n",
     						k);
     					cblock[k].cause |= C_H1;
     					cblock[k].more |= C_H1;
     			}

     		}
     	}
     	}
     */

/*
	for ( int i = 0; i < BlockInfo::count - 2; ++i ) {
		if (cblock[i].score < 0.9 && cblock[i+1].score > 1.0 && cblock[i+2].score > 1.5 &&
			!(cblock[i].cause & (C_b | C_u | C_v | C_a)) ) {
			cblock[i+1].score = 0.5;
			Debug(3, "Eroded cblock %i because vague cut reason and on edge between commercial and show.\n",
					i+1);
		}
		if (cblock[i].score > 1.5 && cblock[i+1].score > 1.0 && cblock[i+2].score < 0.9 &&
			!(cblock[i+1].cause & (C_b | C_u | C_v | C_a)) ) {
			cblock[i+1].score = 0.5;
			Debug(3, "Eroded cblock %i because vague cut reason and on edge between commercial and show.\n",
					i+1);
		}
	}
*/
}

#ifndef _WIN32
#include <ctype.h>
char *_strupr(char *string)
{
	if ( string )
	{
		for ( char *s = string; *s; ++s )
			*s = toupper(*s);
	}

	return string;
}
#endif

void CommercialSkipper::openOutputFiles(void)
{
	if ( flagFiles._default.use )
	{
		FlagFile &ff(flagFiles.out);

#ifndef _WIN32
		ff.open(_outFilename, "w", true);
#else
		ff.open(_outFilename, "w");

		if ( ! ff.f )
		{
			Sleep(50L);
			ff.open(_outFilename, "w", true);
		}
#endif

		ff.printf("FILE PROCESSING COMPLETE %6i FRAMES"
				" AT %5i\n-------------------\n"
				, _frameCount - 1, (int)(fps*100));
		ff.close();
	}

	if ( flagFiles.chapters.use )
	{
		FlagFile &ff(flagFiles.chapters);
		const std::string fn(_outBasename + ".chap");

#ifndef _WIN32
		ff.open(fn, "w", true);
#else
		ff.open(fn, "w");

		if ( ! ff.f )
		{
			Sleep(50L);
			ff.open(fn, "w", true);
		}
#endif

		ff.printf("FILE PROCESSING COMPLETE %6i FRAMES"
					" AT %5i\n-------------------\n"
					, _frameCount - 1, (int)(fps*100));
	}

	if ( flagFiles.zoomplayer_cutlist.use )
		flagFiles.zoomplayer_cutlist.open(_outBasename + ".cut", "w", true);

	if ( flagFiles.plist_cutlist.use )
	{
		flagFiles.plist_cutlist.open(_outBasename + ".plist", "w", true);
		flagFiles.plist_cutlist.printf("<array>\n");
	}

	if ( flagFiles.incommercial.use )
	{
		FlagFile &ff(flagFiles.incommercial);

		ff.open(_workBasename + ".incommercial", "w", true);
		ff.printf("0\n");
		ff.close();
	}

	if ( flagFiles.zoomplayer_chapter.use )
		flagFiles.zoomplayer_chapter.open(_outBasename + ".chp", "w", true);

	if ( flagFiles.edl.use )
		flagFiles.edl.open(_outBasename + ".edl", "wb", true);

	if ( flagFiles.ipodchap.use )
	{
		FlagFile &ff(flagFiles.ipodchap);

		ff.open(_outBasename + ".chap", "w", true);
		ff.printf("CHAPTER01=00:00:00.000\nCHAPTER01NAME=1\n");
	}

	if ( flagFiles.edlp.use )
		flagFiles.edlp.open(_outBasename + ".edlp", "w", true);

	if ( flagFiles.bsplayer.use )
		flagFiles.bsplayer.open(_outBasename + ".bcf", "w", true);

	if ( flagFiles.edlx.use )
	{
		FlagFile &ff(flagFiles.edlx);

		ff.open(_outBasename + ".edlx", "w", true);
		ff.printf("<regionlist units=\"bytes\" mode=\"exclude\"> \n");
	}

	if ( flagFiles.videoredo.use )
	{
//<Version>2
//<Filename>G:\comskip79_46\mpg\MXC_20060518_00000030.mpg
//<Cut>4255584667:5666994667
//<Cut>8590582000:11001991000
//<SceneMarker 0>797115333
//<SceneMarker 1>1083729555
//<SceneMarker 2>4254502333
//<SceneMarker 3>4708947222

		FlagFile &ff(flagFiles.videoredo);
		ff.open(_outBasename + ".VPrj", "w", true);

		std::string mpgFn(_mpegFilename);

		if ( _mpegFilename[0] != DIRSEP
#ifdef _WIN32
				&& _mpegFilename[1] != ':'
#endif
				)
		{
			char cwd[256];
			mpgFn = std::string(::getcwd(cwd, sizeof(cwd))) + DIRSEPSTR + _mpegFilename;
		}

		ff.printf("<Version>2\n<Filename>%s\n", mpgFn.c_str());

		if ( is_h264 )
			ff.printf("<MPEG Stream Type>4\n");
	}

	if ( flagFiles.btv.use )
	{
		FlagFile &ff(flagFiles.btv);
		ff.open(_mpegFilename + ".chapters.xml", "w", true);

		ff.printf("<cutlist>\n");
	}

	if ( flagFiles.cuttermaran.use )
	{
		FlagFile &ff(flagFiles.cuttermaran);
		ff.open(_outBasename + ".cpf", "w", true);

		std::string bn(_basename);

		if ( _mpegFilename[0] != DIRSEP
#ifdef _WIN32
				&& _mpegFilename[1] != ':'
#endif
				)
		{
			char cwd[256];
			bn = std::string(getcwd(cwd, sizeof(cwd))) + DIRSEPSTR + _basename;
		}

		ff.printf("<?xml version=\"1.0\" standalone=\"yes\"?>\n");
		ff.printf("<StateData xmlns=\"http://cuttermaran.kickme.to/StateData.xsd\">\n");
		ff.printf("<usedVideoFiles FileID=\"0\" FileName=\"%s.M2V\" />\n", bn.c_str());
		ff.printf("<usedAudioFiles FileID=\"1\" FileName=\"%s.mp2\" StartDelay=\"0\" />\n"
			, bn.c_str());
	}

	if ( flagFiles.vcf.use )
	{
		FlagFile &ff(flagFiles.vcf);
		ff.open(_outBasename + ".vcf", "w", true);

		ff.printf("VirtualDub.video.SetMode(0);\nVirtualDub.subset.Clear();\n");
	}

	if ( flagFiles.vdr.use )
	{
		FlagFile &ff(flagFiles.vdr);
		ff.open(_outBasename + ".vdr", "w", true);

		// ff.printf("VirtualDub.video.SetMode(0);\nVirtualDub.subset.Clear();\n");
	}

	if ( flagFiles.projectx.use )
	{
		FlagFile &ff(flagFiles.projectx);
		ff.open(_mpegFilename + ".Xcl", "w", true);

		ff.printf("CollectionPanel.CutMode=2\n");
	}

	if ( flagFiles.avisynth.use )
	{
		FlagFile &ff(flagFiles.avisynth);
		ff.open(_mpegFilename + ".avs", "w", true);

		if ( avisynth_options[0] == 0 )
			ff.printf("LoadPlugin(\"MPEG2Dec3.dll\") \n"
					"MPEG2Source(\"%s\")\n"
					, _mpegFilename.c_str());
		else
			ff.printf(avisynth_options, _mpegFilename.c_str());
	}

	if ( flagFiles.womble.use )
		flagFiles.womble.open(_outBasename + ".wme", "w", true);

	if ( flagFiles.mls.use )
		flagFiles.mls.open(_outBasename + ".mls", "w", true);

	if ( flagFiles.mpgtx.use )
	{
		FlagFile &ff(flagFiles.mpgtx);
		ff.open(_outBasename + "_mpgtx.bat", "w", true);

		ff.printf("mpgtx.exe -j -f -o \"%s%s\" \"%s\" "
						, _mpegFilename.c_str() , ".clean"
						, _mpegFilename.c_str());
	}

	if ( flagFiles.dvrcut.use )
	{
		FlagFile &ff(flagFiles.dvrcut);
		ff.open(_outBasename + "_dvrcut.bat", "w", true);

		if ( dvrcut_options[0] == 0 )
			ff.printf("dvrcut \"%%1\" \"%%2\" ");
		else
			ff.printf(dvrcut_options
					, _basename.c_str()
					, _basename.c_str()
					, _basename.c_str() );
	}

	if ( flagFiles.dvrmstb.use )
	{
		FlagFile &ff(flagFiles.dvrmstb);
		ff.open(_outBasename + ".xml", "w", true);

		ff.printf("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<root>\n");
	}

	if ( flagFiles.mpeg2schnitt.use )
	{
		FlagFile &ff(flagFiles.mpeg2schnitt);
		ff.open(_basename + "_mpeg2schnitt.bat", "w", true);

		if ( mpeg2schnitt_options[0] == 0 )
			ff.printf("mpeg2schnitt.exe /S /E /R%5.2f  /Z \"%s\" \"%s\" "
							, fps, "%2", "%1");
		else
			ff.printf("%s ", mpeg2schnitt_options);
	}
}

#define CLOSEOUTFILE(F) { if ((F) && last) { fclose(F); F = 0; } }

void CommercialSkipper::outputCommercialBlock(int i, int prev
	, int start, int end, bool last)
{
	if ( prev > 0 )
		prev = F2F(prev);

	if ( start > 0 && start <= _frameCount )
		start = F2F(start);

	if ( end > 0 && end <= _frameCount )
		end = F2F(end);

	start = std::max(start, 0);
	end = std::max(end, 0);

	if ( flagFiles._default.use && prev < start /*&& !last */ )
	{
		FlagFile &ff(flagFiles.out);

#ifndef _WIN32
		ff.open(_outFilename, "a+", true);
#else
		ff.open(_outFilename, "a+");

		if ( ! ff.f )
		{
			Sleep(50L);
			ff.open(out_filename, "a+", true);
		}
#endif

		int s_start(start);
		int s_end(end);

		if ( sage_minute_bug )
		{
			const double minutes(F2T(_frameCount) / 60.0);

			s_start = (int)(start + (((int)(minutes + 0.5)) / minutes));
			s_end = (int)(end * (((int)(minutes + 0.5)) / minutes));
		}

		ff.printf("%i\t%i\n"
				, (sage_framenumber_bug ? s_start / 2 : s_start)
				, (sage_framenumber_bug ? s_end / 2 : s_end));
		ff.close();
	}

	if ( flagFiles.zoomplayer_cutlist.use
			&& prev < start
			&& end - start > 2 )
	{
		flagFiles.zoomplayer_cutlist.printf(
				"JumpSegment(\"From=%.4f\",\"To=%.4f\")\n"
				, (start) / fps
				, (end) / fps);
	}

	CLOSEOUTFILE(flagFiles.zoomplayer_cutlist.f);

	if ( flagFiles.plist_cutlist.use )
	{
		if ( prev < start /* &&!last */ )
		{
			// NOTE: we could possibly simplify this to just printing start and end without the math
			flagFiles.plist_cutlist.printf(
					"<integer>%ld</integer> <integer>%ld</integer>\n"
					, (unsigned long)((start) / fps * 90000)
					, (unsigned long)((end) / fps * 90000));
		}

		if ( last )
			flagFiles.plist_cutlist.printf("</array>\n");
	}

	CLOSEOUTFILE(flagFiles.plist_cutlist.f);

	if ( flagFiles.zoomplayer_chapter.use
			&& prev < start
			&& end - start > fps )
	{
		//flagFiles.zoomplayer_chapter.printf("AddChapterBySecond(%.4f,Commercial Segment)\nAddChapterBySecond(%.4f,Show Segment)\n", (start) / fps, (end) / fps);
		flagFiles.zoomplayer_chapter.printf(
				"AddChapterBySecond(%i,Commercial Segment)\nAddChapterBySecond(%i,Show Segment)\n"
				, (int)((start) / fps)
				, (int)((end) / fps));
	}

	CLOSEOUTFILE(flagFiles.zoomplayer_chapter.f);

	if ( flagFiles.vcf.use
			&& prev < start && start - prev > 5
			&& prev > 0 )
	{
		flagFiles.vcf.printf(
				"VirtualDub.subset.AddRange(%d,%d);\n"
				, prev-1
				, start - prev);
	}

	CLOSEOUTFILE(flagFiles.vcf.f);

	if ( flagFiles.vdr.use
			&& prev < start
			&& end - start > 2 )
	{
		if ( start < 5 )
			start = 0;

		flagFiles.vdr.printf("%s start\n"
					  , secondsToString(start / fps).c_str());
		flagFiles.vdr.printf("%s end\n"
					  , secondsToString(end / fps).c_str());
	}

	CLOSEOUTFILE(flagFiles.vdr.f);

	if ( flagFiles.projectx.use && prev < start )
	{
		flagFiles.projectx.printf("%d\n", prev+1);
		flagFiles.projectx.printf("%d\n", start);
	}

	CLOSEOUTFILE(flagFiles.projectx.f);

	if ( flagFiles.avisynth.use && prev < start )
	{
		flagFiles.avisynth.printf(
				"%strim(%d,"
				, (prev < 10 ? "" : " ++ ")
				, prev + 1);
		flagFiles.avisynth.printf(
				"%d)"
				, start);
	}

	if ( flagFiles.avisynth.use && last )
		flagFiles.avisynth.printf("\n");

	CLOSEOUTFILE(flagFiles.avisynth.f);

	if ( flagFiles.videoredo.use
			&& prev < start
			&& end - start > 2 )
	{
		if ( i == 0 && demux_pid )
			flagFiles.videoredo.printf(
					"<VideoStreamPID>%d\n<AudioStreamPID>%d\n<SubtitlePID1>%d\n"
					, selected_video_pid
					, selected_audio_pid
					, selected_subtitle_pid);

		double s((double)std::max(start-videoredo_offset - 1, 0) / fps * 10000000);
		double e((double)std::max(end - videoredo_offset - 1, 0) / fps * 10000000);

		flagFiles.videoredo.printf("<Cut>%.0f:%.0f\n", s, e);
	}

	CLOSEOUTFILE(flagFiles.videoredo.f);

	if ( flagFiles.btv.use && prev < start )
	{
		flagFiles.btv.printf(
				"<Region><start comment=\"%s\">%.0f</start>"
				"<end comment=\"%s\">%.0f</end></Region>\n"
				, secondsToString(start / fps).c_str()
				, start / fps * 10000000
				, secondsToString(end / fps).c_str()
				, end / fps * 10000000);

		if ( last )
			flagFiles.btv.printf("</cutlist>\n");
	}

	CLOSEOUTFILE(flagFiles.btv.f);

	if ( flagFiles.edl.use
			&& prev < start /* &&!last */
			&& end - start > 2 )
	{
		if ( start < 5 )
			start = 0;

		const double mkvOffset(_isMkv ? _mkvTimeOffset : 0.0);
		double s((double)std::max(start - _edlOffset, 0) / fps + mkvOffset);
		double e((double)std::max(end - _edlOffset, 0) / fps + mkvOffset);

		if ( demux_pid && enable_mencoder_pts )
		{
			s += _frame[1].pts;
			e += _frame[1].pts;
		}

		flagFiles.edl.printf("%.2f\t%.2f\t%d\n", s, e, _edlMode);
	}

	CLOSEOUTFILE(flagFiles.edl.f);

	if ( flagFiles.ipodchap.use
			&& prev < start /* &&!last */
			&& end - start > 2 )
	{
		// flagFiles.ipodchap.printf("CHAPTER01=00:00:00.000\nCHAPTER01NAME=1\n");
		flagFiles.ipodchap.printf(
			"CHAPTER%.2i=%s\nCHAPTER%.2iNAME=%d\n"
			, i + 2
			, secondsToString(((double)end) / fps).c_str()
			, i + 2
			, i + 2 );
	}

	CLOSEOUTFILE(flagFiles.ipodchap.f);

	if ( flagFiles.edlp.use
			&& prev < start
			/* && ! last */
			&& end - start > 2 )
	{
		if ( start < 5 )
			start = 0;

		flagFiles.edlp.printf("%.2f\t%.2f\t0\n"
				, (double)start / fps + _frame[1].pts
				, (double)end / fps  + _frame[1].pts);
	}

	CLOSEOUTFILE(flagFiles.edlp.f);

	if ( flagFiles.bcf.use
			&& prev < start
			/* && ! last */
			&& end - start > 2 )
	{
		flagFiles.bcf.printf("1,%.0f,%.0f\n"
				, (double)start * 1000.0 / fps
				, (double)end * 1000.0 / fps );
	}

	CLOSEOUTFILE(flagFiles.bcf.f);

	if ( flagFiles.edlx.use )
	{
		if ( prev < start /* &&!last */ && end - start > 2 )
		{
			flagFiles.edlx.printf(
					"<region start=\"%lld\" end=\"%lld\"/> \n"
					, (long long int)_frame[start].goppos
					, (long long int)_frame[end].goppos);
		}

		if ( last )
			flagFiles.edlx.printf("</regionlist>\n");
	}

	CLOSEOUTFILE(flagFiles.edlx.f);

	if ( flagFiles.womble.use )
	{
		// CLIPLIST: #1 show
		// CLIP: morse.mpg
		// 6 0 9963
		if ( ! last )
		{
			if ( start - prev > fps )
			{
				flagFiles.womble.printf(
						"CLIPLIST: #%i show\nCLIP: %s\n6 %i %i\n"
						, i + 1
						, _mpegFilename.c_str()
						, prev + 1
						, start - prev);
			}
			// CLIPLIST: #2 commercial
			// CLIP: morse.mpg
			// 6 9963 5196

			flagFiles.womble.printf(
					"CLIPLIST: #%i commercial\nCLIP: %s\n6 %i %i\n"
					, i + 1
					, _mpegFilename.c_str()
					, start
					, end - start);
		}
		else if ( end - prev > 0 )
		{
			flagFiles.womble.printf(
					"CLIPLIST: #%i show\nCLIP: %s\n6 %i %i\n"
					, i + 1
					, _mpegFilename.c_str()
					, prev + 1
					, end - prev);
		}
	}

	CLOSEOUTFILE(flagFiles.womble.f);

	if ( flagFiles.mls.use )
	{
		if ( i == 0 )
		{
			int count(commercial_count * 2 + 1);

			if ( commercial[commercial_count].end_frame
					< _frameCount - 2 )
				count += 2;

			if ( start < fps )
				count -= 1;

			flagFiles.mls.printf(
					"[BookmarkList]\nPathName= %s\n"
					"VideoStreamID= 0\nFormat= frame\nCount= %d\n"
					, _mpegFilename.c_str()
					, count);

			if ( start >= fps )
				flagFiles.mls.printf("%11i 1\n", 0);
		}
		else
			flagFiles.mls.printf("%11i 1\n", prev);

		if ( ! last )
			flagFiles.mls.printf("%11i 0\n", start);
	}

	CLOSEOUTFILE(flagFiles.mls.f);

	if ( flagFiles.mpgtx.use )
	{
		if ( ! last )
		{
			if ( start - prev > 0 )
			{
				flagFiles.mpgtx.printf(
						"[%s-"
						, ( prev < fps )
								? ""
								: secondsToString((int)(prev / fps)).c_str());
				flagFiles.mpgtx.printf(
						"%s] "
						, secondsToString((int)(start / fps)).c_str());
			}
		}
		else
		{
			if ( end - prev > 0 )
				flagFiles.mpgtx.printf(
						"[%s-]"
						, secondsToString((int)((prev + 1) / fps)).c_str());

			flagFiles.mpgtx.printf("\n");
		}
	}

	CLOSEOUTFILE(flagFiles.mpgtx.f);

	if ( flagFiles.dvrcut.use )
	{
		if ( start - prev > (int)fps /* && start > 2*fps */ )
		{
			flagFiles.dvrcut.printf(
					"%s "
					, secondsToString((int)(prev / fps)).c_str());
			flagFiles.dvrcut.printf(
					"%s "
					, secondsToString((int)(start / fps)).c_str());
		}

		if ( last )
			flagFiles.dvrcut.printf("\n");
	}

	CLOSEOUTFILE(flagFiles.dvrcut.f);

	if ( flagFiles.dvrmstb.use )
	{
		if ( end - start > 1 )
		{
			if ( start == 1 )
				start = 0;

			flagFiles.dvrmstb.printf(
					"  <commercial start=\"%f\" end=\"%f\" />\n"
					, start/fps, end/fps);
		}

		if ( last )
			flagFiles.dvrmstb.printf(" </root>\n");
	}

	CLOSEOUTFILE(flagFiles.dvrmstb.f);

	if ( flagFiles.mpeg2schnitt.use )
	{
		if ( end - start > 1 )
		{
			flagFiles.mpeg2schnitt.printf("/o%d ", start);
			flagFiles.mpeg2schnitt.printf("/i%d ", end);
		}

		if ( last )
			flagFiles.mpeg2schnitt.printf("\n");
	}

	CLOSEOUTFILE(flagFiles.mpeg2schnitt.f);

	if ( flagFiles.cuttermaran.use )
	{
		if ( prev + 1 < start )
		{
			flagFiles.cuttermaran.printf(
					"<CutElements refVideoFile=\"0\" StartPosition=\"%i\" EndPosition=\"%i\">\n"
					, prev + 1
					, start - 1);
			flagFiles.cuttermaran.printf(
					"<CurrentFiles refVideoFiles=\"0\" /> <cutAudioFiles refAudioFile=\"1\""
					" /></CutElements>\n");
		}

		if ( last )
		{
			if ( cuttermaran_options[0] == 0 )
				flagFiles.cuttermaran.printf(
						"<CmdArgs OutFile=\"%s_clean.m2v\" cut=\"true\" unattended=\"true\""
						" snapToCutPoints=\"true\" closeApp=\"true\" />\n</StateData>\n"
						, _basename.c_str());
			else
				flagFiles.cuttermaran.printf(
						"<CmdArgs OutFile=\"%s_clean.m2v\" %s />\n</StateData>\n"
						, _basename.c_str()
						, cuttermaran_options);
		}
	}

	CLOSEOUTFILE(flagFiles.cuttermaran.f);
}

char CompareLetter(int value, int average, int i)
{
	if ( cblock[i].reffer == '+' || cblock[i].reffer == '-' )
	{
		if ( value > 1.2 * average )
			return
				(cblock[i].reffer == '-')
					? '='
					: '!';

		if ( value < 0.8 * average )
			return
				(cblock[i].reffer == '-')
					? '!'
					: '=';
	}

	if ( value > average )
		return '+';

	if ( value < average )
		return '-';

	return '0';
}

void CommercialSkipper::buildCommercial(void)
{
	commercial_count = -1;
	int i(0);

	while ( i < BlockInfo::count )
	{
		if ( cblock[i].score > _globalThreshold
#if 0
			&& ( cblock[i].score >= 100 ||
				!((commDetectMethod & DET::LOGO)
					&& cblock[i].logo > 0.5
					&& F2L(cblock[i].f_end, cblock[i].f_start)
						> min_show_segment_length) ))
#endif
			)
		{
			++commercial_count;
			commercial[commercial_count].start_frame
				= cblock[i].f_start/*+ (cblock[i].bframe_count / 2)*/;
			commercial[commercial_count].end_frame
				= cblock[i].f_end/* + (cblock[i + 1].bframe_count / 2)*/;
			commercial[commercial_count].length
				= F2L(commercial[commercial_count].end_frame
						, commercial[commercial_count].start_frame);
			commercial[commercial_count].start_block = i;
			commercial[commercial_count].end_block = i;
			cblock[i].iscommercial = true;
			++i;

			while ( i < BlockInfo::count
					&& cblock[i].score > _globalThreshold
#if 0
				&& ( cblock[i].score >= 100 ||
					!((commDetectMethod & DET::LOGO)
							&& cblock[i].logo > 0.5
							&& F2L(cblock[i].f_end, cblock[i].f_start)
								> (min_show_segment_length) ))
#endif
					)
			{
				commercial[commercial_count].end_frame
					= cblock[i].f_end/* + (cblock[i + 1].bframe_count / 2)*/;
				commercial[commercial_count].length
					= F2L(commercial[commercial_count].end_frame
							, commercial[commercial_count].start_frame);
				commercial[commercial_count].end_block = i;
				cblock[i].iscommercial = true;
				++i;
			}
		}
		else
			cblock[i].iscommercial = false;

		++i;
	}
}

bool CommercialSkipper::outputBlocks(void)
{
	int k;
	int prev;
	double comlength;
	bool foundCommercials(false);
	bool deleted(false);

	double threshold
			= ( _globalThreshold >= 0.0 )
				? _globalThreshold
				: findScoreThreshold(score_percentile);

	openOutputFiles();

	Debug(1, "Threshold used - %.4f", threshold);
	threshold = ceil(threshold * 100) / 100.0;
	Debug(1, "\tAfter rounding - %.4f\n", threshold);

	buildCommercial();

#ifdef undef
	commercial_count = -1;
	// i = 0;

	for ( int i = 0; i < BlockInfo::count; /* i is incremented within the loop */ )
	{
		if (cblock[i].score > threshold
//			&&
//			( cblock[i].score >= 100 ||
//			!((commDetectMethod & DET::LOGO) && cblock[i].logo > 0.5 && F2L(cblock[i].f_end, cblock[i].f_start) > (min_show_segment_length) ))
             )
		{
			++commercial_count;
			commercial[commercial_count].start_frame = cblock[i].f_start/*+ (cblock[i].bframe_count / 2)*/;
			commercial[commercial_count].end_frame = cblock[i].f_end/* + (cblock[i + 1].bframe_count / 2)*/;
			commercial[commercial_count].length = F2L(commercial[commercial_count].end_frame,	commercial[commercial_count].start_frame);
			commercial[commercial_count].start_block = i;
			commercial[commercial_count].end_block = i;
			cblock[i].iscommercial = true;
			++i;

			while ( i < BlockInfo::count && cblock[i].score > threshold
//				&&
//				( cblock[i].score >= 100 ||
//				!((commDetectMethod & DET::LOGO) && cblock[i].logo > 0.5 && F2L(cblock[i].f_end, cblock[i].f_start) >  (min_show_segment_length) ))
                     )
			{
				commercial[commercial_count].end_frame = cblock[i].f_end/* + (cblock[i + 1].bframe_count / 2)*/;
				commercial[commercial_count].length = F2L(commercial[commercial_count].end_frame, commercial[commercial_count].start_frame);
				commercial[commercial_count].end_block = i;
				cblock[i].iscommercial = true;
				++i;
			}
		}
		else
			cblock[i].iscommercial = false;

		++i;
	}
#endif

     if ( !(disable_heuristics & (1 << (5 - 1))) )
	 {
          if ( delete_block_after_commercial > 0 )
		  {
               for ( k = commercial_count; k >= 0; --k )
			   {
                    int i(commercial[k].end_block + 1);

                    if ( i < BlockInfo::count && cblock[i].length
									< delete_block_after_commercial
								&& cblock[i].score < threshold )
					{
                         Debug(3, "H5 Deleting cblock %i because it is short"
									" and comes after a commercial.\n",
                               i);
                         commercial[k].end_frame = cblock[i].f_end/* + (cblock[i + 1].bframe_count / 2)*/;
                         commercial[k].length = F2L(commercial[k].end_frame, commercial[k].start_frame);
                         commercial[k].end_block = i;
                         cblock[i].iscommercial = true;
                         cblock[i].cause |= C_H5;
                         cblock[i].score = 99.99;
                         cblock[i].more |= C_H5;
                    }
               }
          }

          if (commercial_count > -1 &&
                    commercial[commercial_count].end_block < BlockInfo::count - 1 &&
                    F2L(cblock[BlockInfo::count-1].f_end, cblock[commercial[commercial_count].end_block].f_end) < min_show_segment_length / 2.0 ) {
               commercial[commercial_count].end_block = BlockInfo::count - 1;
               commercial[commercial_count].end_frame = cblock[BlockInfo::count - 1].f_end/* + (cblock[i + 1].bframe_count / 2)*/;
               commercial[commercial_count].length = F2L(commercial[commercial_count].end_frame, commercial[commercial_count].start_frame);
               Debug(3, "H5 Deleting cblock %i of %i seconds because it comes after the last commercial and its too short.\n",
                     BlockInfo::count - 1, (int)cblock[BlockInfo::count-1].length);
               cblock[BlockInfo::count-1].cause |= C_H5;
               cblock[BlockInfo::count-1].score = 99.99;
               cblock[BlockInfo::count-1].more |= C_H5;
          }

          if (commercial_count > -1 &&
                    commercial[0].start_block == 1 &&
                    F2T(cblock[0].f_end) < _commBreakSize._break.min) {
               commercial[0].start_block = 0;
               commercial[0].start_frame = cblock[0].f_start/* + (cblock[i + 1].bframe_count / 2)*/;
               commercial[0].length = F2L(commercial[0].end_frame,	commercial[0].start_frame);
               Debug(3, "H5 Deleting cblock %i of %i seconds because its too short and before first commercial.\n",
                     0, (int)cblock[0].length);
               cblock[0].score = 99.99;
               cblock[0].cause |= C_H5;
               cblock[0].more |= C_H5;

          }

     }

	Debug(2, "\n\n\t---------------------\n\tInitial Commercial List\n\t---------------------\n");

	for ( int i = 0; i <= commercial_count; ++i )
	{
		Debug(2, "%2i) %6i\t%6i\t%s\n"
				, i
				, commercial[i].start_frame
				, commercial[i].end_frame
				, secondsToString(commercial[i].length).c_str());
	}

#if 1

     if ( !(disable_heuristics & (1 << (6 - 1))) )
	 {
          // Delete too long/short commercials
          for ( k = commercial_count; k >= 0; --k )
		  {
               if ( (F2T(commercial[k].start_frame) > 1.0   || commercial[k].length < 10.2 /* Sage bug fix */ )
                         &&		// Do not delete too short first or last commercial
                         ((commercial[k].length > _commBreakSize._break.max && k != 0 && k != commercial_count) ||
                          (commercial[k].length < _commBreakSize._break.min)) &&
                         F2L(cblock[BlockInfo::count-1].f_end, commercial[k].start_frame) > min_commercial_break_at_start_or_end  &&
                         F2T(commercial[k].end_frame) > min_commercial_break_at_start_or_end )
			   {
                    for ( int i = commercial[k].start_block; i <= commercial[k].end_block; ++i )
					{
                         Debug(3, "H6 Deleting block %i because it is part of a too short or too long commercial.\n",
                               i);
                         cblock[i].score = 0;
                         cblock[i].cause |= C_H6;
                         cblock[i].less |= C_H6;
                    }

                    for ( int i = k; i < commercial_count; ++i )
					{
                         commercial[i] = commercial[i + 1];
                    }

                    --commercial_count;
                    deleted = true;
               }
          }
#ifdef NOTDEF
// keep first seconds
          if ( always_keep_first_seconds && commercial_count >= 0 )
		  {
               k = 0;

               if ( F2T(commercial[k].end_frame) < always_keep_first_seconds )
			   {
                    for ( i = commercial[k].start_block; i <= commercial[k].end_block; ++i )
					{
                         Debug(3, "H6 Deleting block %i because the first %d seconds should always be kept.\n",
                               i, always_keep_first_seconds);
                         cblock[i].score = 0;
                         cblock[i].cause |= C_H6;
                         cblock[i].less |= C_H6;
                    }

                    for ( int i = k; i < commercial_count; ++i )
                         commercial[i] = commercial[i + 1];

                    --commercial_count;
                    deleted = true;
               }
          }

          if ( always_keep_last_seconds && commercial_count >= 0 )
		  {
               k = commercial_count;

               if ( F2L(cblock[BlockInfo::count-1].f_end, commercial[k].start_frame) < always_keep_last_seconds )
			   {
                    for ( int i = commercial[k].start_block; i <= commercial[k].end_block; ++i )
					{
                         Debug(3, "H6 Deleting block %i because the last %d seconds should always be kept.\n",
                               i, always_keep_last_seconds);
                         cblock[i].score = 0;
                         cblock[i].cause |= C_H6;
                         cblock[i].less |= C_H6;
                    }

                    for ( int i = k; i < commercial_count; ++i )
                         commercial[i] = commercial[i + 1];

                    --commercial_count;
                    deleted = true;
               }
          }
#endif

          /*
          		// Delete too short first commercial
          		k = 0;
          		if (commercial_count >= 0 && commercial[k].start_frame < fps &&
          			commercial[k].length < min_commercial_break_at_start_or_end) {
          			for (i = commercial[k].start_block; i <= commercial[k].end_block; i++) {
          				Debug(3, "H6 Deleting block %i because it is part of a too short commercial at the start of the recording.\n",
          					i);
          				cblock[i].score = 0;
          				cblock[i].cause |= C_H6;
          				cblock[i].less |= C_H6;
          			}
          			for (i = k; i < commercial_count; i++) {
          				commercial[i] = commercial[i + 1];
          			}
          			commercial_count--;
          			deleted = true;
          		}
          		// Delete too short last commercial
          		k = commercial_count;
          		if (commercial_count >= 0 && (cblock[BlockInfo::count-1].f_end - commercial[k].end_frame) < fps &&
          			commercial[k].length < min_commercial_break_at_start_or_end) {
          			for (i = commercial[k].start_block; i <= commercial[k].end_block; i++) {
          				Debug(3, "H6 Deleting block %i because it is part of a too short commercial at the end of the recording.\n",
          					i);
          				cblock[i].score = 0;
          				cblock[i].cause |= C_H6;
          				cblock[i].less |= C_H6;
          			}
          			for (i = k; i < commercial_count; i++) {
          				commercial[i] = commercial[i + 1];
          			}
          			commercial_count--;
          			deleted = true;
          		}
          */
          /*
          	// Delete too short shows
          	for (k = commercial_count-1; k >= 0; k--) {
          		if ( commercial[k+1].start_frame - commercial[k].end_frame < min_show_segment_length / 2.5 * fps ||
          			 (commercial[k].end_frame > after_start &&
          			  commercial[k].end_frame < before_end &&
          			  commercial[k+1].start_frame - commercial[k].end_frame < min_show_segment_length  * fps)
          			) {
          			for (i = commercial[k].end_block+1; i < commercial[k+1].start_block; i++) {
          				cblock[i].score = 99.99;
          				cblock[i].cause |= C_H6;
          				cblock[i].less |= C_H6;
          			}
          			commercial[k].end_block = commercial[k+1].end_block;
          			commercial[k].end_frame = commercial[k+1].end_frame;
          			commercial[k].length = (commercial[k].end_frame - commercial[k].start_frame) / fps;

          			for (i = k+1; i < commercial_count; i++) {
          					commercial[i] = commercial[i + 1];
          			}
          			commercial_count--;
          			deleted = true;
          		}
          	}
          */

	}

     if (delete_show_after_last_commercial &&
               commercial_count > -1 &&
               //	( commercial[commercial_count].end_block == BlockInfo::count - 2 || commercial[commercial_count].end_block == BlockInfo::count - 3) &&
               ((delete_show_after_last_commercial == 1 && cblock[commercial[commercial_count].start_block].f_end > before_end) ||
                (delete_show_after_last_commercial > F2L(cblock[BlockInfo::count-1].f_end, cblock[commercial[commercial_count].start_block].f_start)) )

               &&
               commercial[commercial_count].end_block < BlockInfo::count - 1
        )
	{
		int i(commercial[commercial_count].end_block + 1);
		commercial[commercial_count].end_block = BlockInfo::count - 1;
		commercial[commercial_count].end_frame = cblock[BlockInfo::count-1].f_end/* + (cblock[i + 1].bframe_count / 2)*/;
		commercial[commercial_count].length = F2L(commercial[commercial_count].end_frame, commercial[commercial_count].start_frame);

		while ( i < BlockInfo::count )
		{
			Debug(3, "H5 Deleting cblock %i of %i seconds"
					" because it comes after the last commercial.\n"
					, i, (int)cblock[i].length );
			cblock[i].cause |= C_H5;
			cblock[i].score = 99.99;
			cblock[i].more |= C_H5;
			++i;
		}
	}

	if ( delete_show_before_first_commercial &&
               commercial_count > -1 &&
               commercial[0].start_block == 1 &&
               ((delete_show_before_first_commercial == 1 && cblock[commercial[0].end_block].f_end < after_start) ||
                (delete_show_before_first_commercial > F2T(cblock[commercial[0].end_block].f_end)))
        )
	{
		commercial[0].start_block = 0;
		commercial[0].start_frame = cblock[0].f_start/* + (cblock[i + 1].bframe_count / 2)*/;
		commercial[0].length = F2L(commercial[0].end_frame, commercial[0].start_frame);
		Debug(3, "H5 Deleting cblock %i of %i seconds"
				" because it comes before the first commercial.\n"
				, 0, (int)cblock[0].length);
		cblock[0].score = 99.99;
		cblock[0].cause |= C_H5;
		cblock[0].more |= C_H5;
	}

// keep first seconds
	if ( always_keep_first_seconds && commercial_count >= 0 )
	{
		k = 0;

		while ( F2T(commercial[k].end_frame) < always_keep_first_seconds )
		{
			Debug(3, "Deleting commercial block %i because the first %d seconds should always be kept.\n"
					, k, always_keep_first_seconds);

			for ( int i = k; i < commercial_count; ++i )
				commercial[i] = commercial[i + 1];

			--commercial_count;
			deleted = true;
		}

		if ( F2T(commercial[k].start_frame ) < always_keep_first_seconds )
		{
			Debug(3, "Shortening commercial block %i because the first %d seconds should always be kept.\n"
					, k, always_keep_first_seconds);

			while ( F2T(commercial[k].start_frame ) < always_keep_first_seconds
					&& commercial[k].start_frame < always_keep_first_seconds * fps )
				commercial[k].start_frame++;
		}
	}

	if ( always_keep_last_seconds && commercial_count >= 0 )
	{
		k = commercial_count;

		while ( F2L(cblock[BlockInfo::count-1].f_end, commercial[k].start_frame) < always_keep_last_seconds )
		{
			Debug(3, "Deleting commercial block %i because the last %d seconds should always be kept.\n"
					, k, always_keep_last_seconds);
			--commercial_count;
			k = commercial_count;
			deleted = true;
		}

		if ( F2L(cblock[BlockInfo::count-1].f_end, commercial[k].end_frame) < always_keep_last_seconds )
		{
			Debug(3, "Shortening commercial block %i because the last %d seconds should always be kept.\n"
					, k, always_keep_last_seconds);

			while ( F2L(cblock[BlockInfo::count-1].f_end, commercial[k].end_frame) < always_keep_last_seconds
					&& (cblock[BlockInfo::count-1].f_end - commercial[k].end_frame) < fps * always_keep_last_seconds )
				commercial[k].end_frame--;
		}
	}

	if ( deleted )
		Debug(1, "\n\n\t---------------------\n\tFinal Commercial List\n\t---------------------\n");
	else
		Debug(1, "No change\n");
#endif

	// Apply padding
	for ( int i = 0; i <= commercial_count; ++i )
	{
		commercial[i].start_frame += padding*fps - remove_before*fps;

		if ( i != commercial_count )
			commercial[i].end_frame -= padding*fps - remove_after*fps;

		commercial[i].length += -2*padding + remove_before + remove_after;
	}

	comlength = 0.;

	for ( int i = 0; i < commercial_count; ++i )
		comlength += commercial[i].length;

	// Debug(1, "Total commercial length found: %s\n", secondsToString(comlength).c_str());

	if ( flagFiles.zoomplayer_chapter.use
#if 0
			&& (commercial[0].length >= _commBreakSize._break.min)
			&& (commercial[0].length <= _commBreakSize._break.max)
#endif
			&& (commercial[0].start_frame > 5) )
	{
		flagFiles.zoomplayer_chapter.printf("AddChapter(1,Show Segment)\n");
	}

	prev = -1;

	for ( int i = 0; i <= commercial_count; ++i )
	{
		// if ((commercial[i].length >= _commBreakSize._break.min) && (commercial[i].length <= _commBreakSize._break.max))
		{
			foundCommercials = true;

			if ( deleted )
				Debug(1, "%i - start: %6i\tend: %6i\t[%6i:%6i]\t"
						"length: %s\n"
						, i + 1
						, commercial[i].start_frame
						, commercial[i].end_frame
						, commercial[i].start_block
						, commercial[i].end_block
						, secondsToString(commercial[i].length).c_str());

			outputCommercialBlock(i, prev, commercial[i].start_frame
							   , commercial[i].end_frame
							   , (commercial[i].end_frame < _frameCount - 2)
									? false
									: true);
			prev = commercial[i].end_frame;
		}
	}

	if ( commercial[commercial_count].end_frame < _frameCount - 2 )
		outputCommercialBlock(commercial_count + 1
				, prev
				, _frameCount - 2
				, _frameCount - 1, true);

	if ( flagFiles.videoredo.use )
	{
		FlagFile &ff(flagFiles.videoredo);
		ff.open(_outBasename + ".VPrj", "a+");

		if ( ff.f )
		{
			for ( int i = 0; i < BlockInfo::count; ++i )
			{
				ff.printf("<SceneMarker %d>%.0f\n"
						, i
						, F2T(std::max(cblock[i].f_end-videoredo_offset-1
									, 0)) * 10000000);
			}

			ff.close();
		}
	}

	if ( flagFiles.chapters.use )
	{
		if ( flagFiles.chapters.f )
		{
			for ( int i = 0; i < BlockInfo::count; ++i )
				flagFiles.chapters.printf("%d\n", cblock[i].f_end);

			flagFiles.chapters.close();
		}
	}

	reffer_count = commercial_count;

	for ( int i = 0; i <= commercial_count; ++i )
	{
		reffer[i].start_frame = commercial[i].start_frame;
		reffer[i].end_frame = commercial[i].end_frame;
	}

	inputReffer(".ref", false);

	if ( flagFiles.tuning.use )
	{
		FlagFile &ff(flagFiles.tuning);
		ff.open(_basename + ".tun", "w", true);
		ff.printf("max_volume=%6i\n", min_volume + 200);
		ff.printf("max_avg_brightness=%6i\n", min_brightness_found + 5);
		ff.printf("_commBreakSize._break.max=%6i\n", max_logo_gap + 10);
		ff.printf("shrink_logo=%.2f\n", logo_overshoot);
		ff.printf("min_show_segment_length=%6i\n"
					, max_nonlogo_block_length + 10);
		ff.printf("logo_threshold=%.3f\n", logo_quality);
	}

	if ( _verbose )
	{
		Debug(1, "\nLogo fraction:              %.4f      %s\n"
				, logoPercentage
				, ((commDetectMethod & DET::LOGO)
					? (reverseLogoLogic ? "(Reversed Logo Logic)": "")
					: "Logo disabled") );
		Debug(1,   "Maximum volume found:       %6i\n", maxi_volume);
		Debug(1,   "Average volume:             %6i\n", avg_volume);
		Debug(1,   "Sound threshold:            %6i\n", _maxVolume);
		Debug(1,   "Silence threshold:          %6i\n", _maxSilence);
		Debug(1,   "Minimum volume found:       %6i\n", min_volume);
		Debug(1,   "Average frames with silence:%6i\n", avg_silence);
		Debug(1,   "Black threshold:            %6i\n", _maxAvgBrightness);
		Debug(1,   "Minimum brightness found:   %6i\n", min_brightness_found);
		Debug(1,   "Minimum bright pixels found:%6i\n", min_hasBright);
		Debug(1,   "Minimum dim level found:    %6i\n", min_dimCount);
		Debug(1,   "Average brightness:         %6i\n", avg_brightness);
		Debug(1,   "Uniformity level:           %6i\n", _nonUniformity);
		Debug(1,   "Average non uniformity:     %6i\n", avg_uniform);
		Debug(1,   "Maximum gap between logo's: %6i\n", max_logo_gap);
		Debug(1,   "Suggested logo_threshold:   %.4f\n",logo_quality);
		Debug(1,   "Suggested shrink_logo:	    %.2f\n", logo_overshoot);
		Debug(1,   "Max commercial size found:  %6i\n", max_nonlogo_block_length);
		Debug(1,   "Dominant aspect ratio:      %.4f\n",dominant_ar);
		Debug(1,   "Score threshold:            %.4f\n", threshold);
		Debug(1,   "Framerate:                  %2.3f\n", fps);

		Debug(1,   "Total commercial length:    %s\n",	secondsToString(comlength).c_str());
		Debug(1,   "Block list after weighing\n----------------------------------------------------\n");
		Debug(1, "  #     sbf  bs  be     fs     fe    sc      len   scr cmb   ar"
				"                   cut bri  logo   vol  sil corr stdev        cc\n");

#if 0
		if ( flagFiles.training.f )
		{
			flagFiles.training.printf(
				TRAINING_LAYOUT
				, "0", 0, 0, 0, 0,0,0, 0, 0, 0, 0);
		}
#endif

		for ( int i = 0; i < BlockInfo::count; ++i )
		{
#if 0
				char cs[10];
				cs[5] = (cblock[i].cause & 16 ? 'b' : ' ');
				cs[4] = (cblock[i].cause & 8  ? 'u' : ' ');
				cs[3] = (cblock[i].cause & 32 ? 'a' : ' ');
				cs[2] = (cblock[i].cause & 4  ? 's' : ' ');
				cs[1] = (cblock[i].cause & 1  ? 'l' : ' ');
				cs[0] = (cblock[i].cause & 2  ? 'c' : ' ');
				cs[6] = 0;
#endif

               Debug(
                    1,
                    "%3i:%c%c %4i %3i %3i %6i %6i %6.2f %8.3f %5.2f %3i %4.2f %s %4i%c %4.2f %4i%c %2i%c %6.3f %5i %-10s",
                    i,
                    CheckFramesForCommercial(cblock[i].f_start+cblock[i].b_head,cblock[i].f_end - cblock[i].b_tail),
                    CheckFramesForReffer(cblock[i].f_start+cblock[i].b_head,cblock[i].f_end - cblock[i].b_tail),
                    cblock[i].bframe_count,
                    cblock[i].b_head,
                    cblock[i].b_tail,
                    cblock[i].f_start,
                    cblock[i].f_end,
                    cblock[i].score,
                    cblock[i].length,
//				cblock[i].schange_count,
                    cblock[i].schange_rate,
                    cblock[i].combined_count,
                    cblock[i].ar_ratio,
                    CauseString(cblock[i].cause),
                    cblock[i].brightness,
                    CompareLetter(cblock[i].brightness,avg_brightness,i),
                    cblock[i].logo,
                    cblock[i].volume,
                    CompareLetter(cblock[i].volume,avg_volume,i),
                    cblock[i].silence,
                    CompareLetter(cblock[i].silence,avg_silence,i),
                    0.0 /*cblock[i].correlation */ ,
                    cblock[i].stdev,
                    CcTypeToStr(cblock[i].cc_type)
               );

               if ( commDetectMethod & DET::LOGO )
			   {
//				if (CheckFramesForLogo(cblock[i].f_start, cblock[i].f_end)) {
//					Debug(1, "\tLogo Present\n");
//				} else {
                    Debug(1, "\n");
//				}
               }
			   else
			   {
                    Debug(1, "\n");
               }
          }

          outputAspect();
          outputTraining();

#if 0
		if ( flagFiles.training.f )
			flagFiles.training.printf(
				TRAINING_LAYOUT
				, "0", 0, 0, 100, 0,0,0, 0, 0, 0, 0);
#endif

     }

#if 0
	outputCleanMpg();
	OutputDebugWindow(false,0);
#endif

	return foundCommercials;
}

void CommercialSkipper::outputStrict(double len, double delta, double tol)
{
	if ( flagFiles.training.use )
	{
		if ( ! flagFiles.training.f )
		{
			flagFiles.training.open("strict.csv", "a+");
			// flagFiles.training.printf("// score, length, fraction, position,combined, ar error, logo, strict \n");
		}

		if ( flagFiles.training.f )
			flagFiles.training.printf(
					"%+f,%+f,%+f, %s\n"
					, len
					, delta
					, tol
					, _basename.c_str());
	}
}

void CommercialSkipper::outputTraining(void)
{
	if ( ! flagFiles.training.use )
		return;

	flagFiles.training.open("comskip.csv", "a+");

#ifdef WRITEPATTERN
	int r = (reffer[0].start_frame/fps < 30.0 ? reffer_count: reffer_count+1);
	int s = ( reffer[0].start_frame/fps < 30.0 ) ? reffer[0].end_frame : 0;

	flagFiles.training.printf(
					"\"%s\",%f,%d,"
					, _basename.c_str()
					, (reffer[reffer_count].start_frame - s)/fps
					, r);

	for ( int i = 0; i < 40; ++i )
	{
		if ( i <= reffer_count )
		{
			int e = ( i == 0 ) ? 0 : reffer[i-1].end_frame;
			int s = ( i == reffer_count )
					? 0
					: (reffer[i].end_frame - reffer[i].start_frame);

			if ( i > 0 )
				flagFiles.training.printf(
						"%f,%f,"
						, (reffer[i].start_frame-e) / fps
						, s / fps);
			else
			{
				if ( reffer[i].start_frame/fps > 30.0 )
					flagFiles.training.printf(
							"%f,%f, %f,%f,"
							, 0.0
							, 0.0
							, (reffer[i].start_frame-e) / fps
							, s/fps);
				else
					flagFiles.training.printf(
							"%f,%f,"
							, (reffer[i].start_frame-e) / fps
							, s / fps);
			}
		}
		else
			flagFiles.training.printf("%f,%f,", 0.0, 0.0);
	}

	flagFiles.training.printf("0\n", _basename.c_str());

	r = (commercial[0].start_frame/fps < 30.0)
			? commercial_count
			: commercial_count +1;
	s = (commercial[0].start_frame/fps < 30.0)
			? commercial[0].end_frame
			: 0;

	flagFiles.training.printf(
			"\"%s\",%f,%d,"
			, _basename.c_str()
			, (commercial[commercial_count].start_frame - s) / fps
			, r);

	for ( int i = 0; i < 40; ++i )
	{
		if ( i <= commercial_count )
		{
			int e = (i == 0) ? 0 : commercial[i-1].end_frame;
			int s = (i == commercial_count)
					? 0
					: (commercial[i].end_frame - commercial[i].start_frame);

			if ( i > 0 )
				flagFiles.training.printf(
						"%f,%f,"
						, (commercial[i].start_frame-e) / fps
						, s / fps);
			else
			{
				if ( commercial[i].start_frame/fps > 30.0 )
					flagFiles.training.printf(
							"%f,%f, %f,%f,"
							, 0.0
							, 0.0
							, (commercial[i].start_frame-e) / fps
							, s/fps);
				else
					flagFiles.training.printf(
							"%f,%f,"
							, (commercial[i].start_frame-e) / fps
							, s/fps);
			}
		}
		else
			flagFiles.training.printf("%f,%f,", 0.0, 0.0);
	}

#if 1
	flagFiles.training.printf("0\n");
#else
	// flagFiles.training.printf("0\n", _basename.c_str());
#endif

#else

	flagFiles.training.printf(
			"block, cm, rf, score, length, start, end, fromend ar, logo, cause, less, more\n");

	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		if ( flagFiles.training.f )
		{
#define TRAINING_LAYOUT	"%3d,%c,%c,%7.2f,%7.2f,%7.2f,%7.2f,%7.2f,%5.2f,%5.2f,\"%10s\",\"%10s\",\"%10s\",\"%s\",\"%s\"\n"
			flagFiles.training.printf(
						TRAINING_LAYOUT
						, i
						, CheckFramesForCommercial(cblock[i].f_start+cblock[i].b_head,cblock[i].f_end - cblock[i].b_tail)
						, CheckFramesForReffer(cblock[i].f_start+cblock[i].b_head,cblock[i].f_end - cblock[i].b_tail)
						, cblock[i].score
						, cblock[i].length
						, F2T(cblock[i].f_start)
						, F2T(cblock[i].f_end)
						, F2L(cblock[BlockInfo::count-1].f_end, cblock[i].f_end)
						, cblock[i].ar_ratio
						, cblock[i].logo
						, CauseString(cblock[i].cause)
						, CauseString(cblock[i].less)
						, CauseString(cblock[i].more)
						, _basename.c_str()
						, "a");

		}
	}
#endif
}

bool CommercialSkipper::outputCleanMpg(void)
{
	if ( _outputDirname.empty() )
		return true;

// 65536
#define BufSize 1<<22
	char *Buf(new char[BufSize]);

#ifdef _WIN32
	int outf(_creat(_outputDirname.c_str(), _S_IREAD | _S_IWRITE));

	if ( outf < 0 )
		return false;

	int inf(_open(_mpegFilename.c_str(), _O_RDONLY | _O_BINARY));
#else
	int outf(open(_outputDirname.c_str(), O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR));

	if ( outf < 0 )
		return false;

	FILE *infile(fopen(_mpegFilename.c_str(), "rb"));
	int inf(fileno(infile));
#endif

     /*
     	if (_lseeki64(Infile[File_Limit-1], process.leftlba*BUFFER_SIZE,SEEK_SET)!= -1L)
     	{

     		int j(_read(Infile[File_Limit-1], Buf, BufSize));
     		if (j>=BUFFER_SIZE)
     		{
     			for(i=0; i<(j-4); i++)
     			{
					const int dwPackStart(0xBA010000);
     				if(*((UNALIGNED DWORD*)(Buf+i)) == dwPackStart)
     				{
     					startpos = (process.leftlba*BUFFER_SIZE) + i;
     					endpos = process.total;

     					if (_lseeki64(Infile[File_Limit-1], process.rightlba*BUFFER_SIZE,SEEK_SET)!= -1L)
     					{
     						j = _read(Infile[File_Limit-1], Buf, BufSize);
     						if (j>=BUFFER_SIZE)
     						{
     							for(i=0; i<(j-4); i++)
     							{
     								if(*((UNALIGNED DWORD*)(Buf+i)) == dwPackStart)
     								{
     									endpos = (process.rightlba*BUFFER_SIZE) + i;
     									break;
     								}
     							}

     						}

     					}

     					*/

	int64_t startpos(_frame[1].goppos);
	bool firstbl(true);

     for ( int c = 0; c<=commercial_count; ++c )
	 {
		int64_t endpos(_frame[commercial[c].start_frame].goppos);
#ifdef _WIN32
          _lseeki64(inf, startpos,SEEK_SET);
#else
          fseeko(infile, startpos,SEEK_SET);
#endif

		int64_t begin(startpos);
		int prevperc(0);

          while ( startpos < endpos )
		  {
				int len((int)endpos-startpos);

               if ( len > BufSize )
					   len = BufSize;//sizeof(Buf);

               int i(_read(inf, Buf, (unsigned int)len));

               if ( i <= 0 )
			   {
                    // MessageBox(hWnd, "Source read error.         ", "Oops...", MB_ICONSTOP | MB_OK);
                    return false;
               }

               int j(0);

               if ( firstbl )
			   {
                    firstbl = false;
                    j = 14 + (Buf[13] & 7);

                    if (*((uint32_t *)(Buf+j)) == 0xBB010000 )
					{
						j = 0;
					}
                    else
					{
                         (void)write(outf, Buf, j);
						const uint8_t MPEG2SysHdr[] = {0x00, 0x00, 0x01, 0xBB, 00, 0x12, 0x80, 0x8E, 0xD3, 0x04, 0xE1, 0x7F, 0xB9, 0xE0, 0xE0, 0xB8, 0xC0, 0x54, 0xBD, 0xE0, 0x3A, 0xBF, 0xE0, 0x02};
                         (void)write(outf, MPEG2SysHdr, sizeof(MPEG2SysHdr));
                    }
               }

               if ( write(outf, Buf+j, i-j) <= 0 )
			   {
					// MessageBox(hWnd, "Write error.         ", "Oops...", MB_ICONSTOP | MB_OK);
					return false;
               }

               if ( i != len )
			   {
                    //				MessageBox(hWnd, "Something strange happened. Aborting.         ", "Oops...", MB_ICONSTOP | MB_OK);
                    return(false);
               }

               startpos +=len;

				int curperc = (int)(((startpos-begin)*100)/(endpos-begin));

               if ( curperc != prevperc )
			   {
                    // SendMessage(hBar, PBM_SETPOS, DWORD(curperc),0);
                    prevperc = curperc;
               }

          }
          startpos = _frame[commercial[c].end_frame].goppos;

     }

	_close(outf);

	delete[] Buf;

	return true;
}

bool LengthWithinTolerance(double test_length, double expected_length, double tolerance)
{
     return (abs((int)(test_length * fps) - (int)(expected_length * fps))
				<= (int)(tolerance * fps));
}

bool CommercialSkipper::isStandardCommercialLength(
	double length
	, double tolerance
	, bool strict)
{
     int		i;
     double	local_tolerance;
     int		length_count;
     double	delta;
     int		standard_length[] = { 10, 15, 20, 25, 30, /* 36, */ 45, 60, 90, 120, 150, 180,  5, 35, 40, 50, 70, 75};
     if (strict) {
          length_count = 11;
     } else {
          length_count = 17;
     }

     if (div5_tolerance >= 0) {
          local_tolerance = div5_tolerance;
     } else {
          local_tolerance = tolerance;
     }

     if (local_tolerance < 0.5)
          local_tolerance = 0.5;

     if (local_tolerance > 1.0)
          local_tolerance = 1.0;

//	length += 0.22;		// Correction for standard error
     length += 0.11;		// Correction for standard error

//	length -= 0.1;		// Correction for standard error

     for (i = 0; i < length_count; ++i )
	 {
          if ( standard_length[i] < min_show_segment_length - 3)
			{
               if (LengthWithinTolerance(length, standard_length[i], local_tolerance))
			   {
                    delta = length - standard_length[i];
                    outputStrict(length, delta, local_tolerance);
                    return (true);
               }
			}
     }

     return false;
}

int CommercialSkipper::matchCutScene(const CutScene &cs)
{
	const uint8_t *cutscene(cs.data);
	const int step((_width > 800) ? 8 : ((_width < 400) ? 2 : 4));
	const int maxY(_height - border);
	const int maxX(_videoWidth - border);
	int delta(0);
	int c(0);

	for ( int y = border; y < maxY; y += step )
	{
		for ( int x = border; x < maxX; x += step )
		{
			if ( c < MAXCSLENGTH )
			{
				const int d((int)_videoFramePointer[y * _width + x] - (int)(cutscene[c]));

				if ( d > _edges.getThreshold() || d < -_edges.getThreshold() )
					delta += 1;
			}

			++c;
		}
	}

	return delta;
}

void CommercialSkipper::recordCutScene(int frameCount, int brightness)
{
	const int step((_width > 800) ? 8 : ((_width < 400) ? 2 : 4));
	const int maxY(_height - border);
	const int maxX(_videoWidth - border);
	int c(0);
	char cs[MAXCSLENGTH];

	for ( int y = border; y < maxY; y += step )
		for ( int x = border; x < maxX; x += step )
			if ( c < MAXCSLENGTH )
				cs[c++] = _videoFramePointer[y * _width + x];

	std::string &csf(_cutSceneFile[0]);

	if ( osname[0] )
		csf = std::string(osname) + ".dmp";

	if ( csf == "" )
		csf = _workBasename + ".dmp";

	FILE *f( (csf != "") ? fopen(csf.c_str(), "wb") : 0);

	if ( f )
	{
		::fwrite(&brightness, sizeof(int), 1, f);
		::fwrite(cs, sizeof(char), c, f);
		Debug(7, "Saved frame %6i into cutfile \"%s\"\n", frameCount, csf.c_str());
		::fclose(f);
	}
}

bool CommercialSkipper::checkSceneHasChanged(void)
{
	if ( ! _videoWidth || ! _width || ! _height )
		return false;

	int similar(0);
	int hasBright(0);
	int dimCount(0);
	bool isDim(false);
	int pixels(0);
	double scale(1.0);

	_arRange.y.min = border;
	_arRange.y.max = _height - border;
	_arRange.x.min = border;
	_arRange.x.max = _videoWidth - border;
	const int step(( _videoWidth > 800 ) ? 8 : ((_videoWidth < 400) ? 2 : 4));

	memcpy(lastHistogram, histogram, sizeof(histogram));
	csBrightnessLast = csBrightness;
	csBrightness = 0;

	// compare current frame with last frame here
	memset(histogram, 0, sizeof(histogram));

#define NEW_AR_ALGO
#ifdef NEW_AR_ALGO
	int brightCountminX(0);
	int brightCountminY(0);
	int brightCountmaxX(0);
	int brightCountmaxY(0);

	for ( int delta = 0
			; delta <= _videoWidth / 2 && delta <= _height / 2
			; delta += step )
	{
		{
			const int y(border + delta);
			const int xx(_videoWidth - border - delta);

			for ( int x = border + delta; x <= xx; x += step )
			{
				if ( _hasLogo.get(x, y) )
					continue;

				const int index(y * _width + x);
				const int hereBright(_videoFramePointer[index]);
				histogram[hereBright]++;

				if ( hereBright > _testBrightness )
					++brightCountminY;
			}

			if ( brightCountminY < 5 )
			{
				// brightCountminY = 0;
				_arRange.y.min = y;
			}
		}

		{
			const int y(_height - border - delta);
			const int xx(_videoWidth - border - delta);

			for ( int x = border + delta; x <= xx; x += step )
			{
				if ( _hasLogo.get(x, y) )
					continue;

				const int index(y * _width + x);
				const int hereBright(_videoFramePointer[index]);
				histogram[hereBright]++;

				if ( hereBright > _testBrightness )
					++brightCountmaxY;
			}

			if ( brightCountmaxY < 5 )
			{
				// brightCountmaxY = 0;
				_arRange.y.max = y;
			}
		}

		{
			const int x(border + delta);
			const int yy(_height - border - delta);

			for ( int y = border + delta; y <= yy; y += step )
			{
				if ( _hasLogo.get(x, y) )
					continue;

				const int index(y * _width + x);
				const int hereBright(_videoFramePointer[index]);
				histogram[hereBright]++;

				if ( hereBright > _testBrightness )
					++brightCountminX;
			}

			if ( brightCountminX < 5 )
			{
				// brightCountminX = 0;
				_arRange.x.min = x;
			}
		}

		if ( brightCountmaxX < 5 )
		{
			const int x(_videoWidth - border - delta);
			const int yy(_height - border - delta);

			for ( int y = border + delta; y <= yy; y += step )
			{
				if ( _hasLogo.get(x, y) )
					continue;

				const int index(y * _width + x);
				const int hereBright(_videoFramePointer[index]);
				histogram[hereBright]++;

				if ( hereBright > _testBrightness )
					++brightCountmaxX;
			}

			if ( brightCountmaxX < 5 )
			{
				// brightCountmaxX = 0;
				_arRange.x.max = x;
			}
		}
	}

#else

	{
		int brightCount(0);

		for ( int y = border; y < (_height - border) / 2; y += step )
		{
			const MinMax &range(_bfRange[y]);

			for ( int x = range.min; x <= range.max; x += step )
			{
				const int hereBright(_videoFramePointer[y * width + x]);
				histogram[hereBright]++;

				if ( hereBright > _testBrightness )
					++brightCount;
			}

			if ( brightCount < 5 )
				minY = y;
		}
	}

	{
		int brightCount(0);

		for ( int y = _height - border; y >= (_height - border) / 2; y -= step )
		{
			const MinMax &range(_bfRange[y]);

			for ( int x = range.min; x <= range.max; x += step )
			{
				const int hereBright(_videoFramePointer[y * width + x]);
				histogram[hereBright]++;

				if ( hereBright > _testBrightness )
					++brightCount;
			}

			if ( brightCount < 5 )
				maxY = y;
		}
	}
#endif

	if ( _useFrameArray )
	{
#ifdef FRAME_WITH_HISTOGRAM
		::memcpy(frame[_frameCount].histogram
				, histogram, sizeof(histogram));
#endif
		_frame[_frameCount].range = _arRange;
	}

	if ( framenum_real <= 1 )
	{
		::memcpy(lastHistogram, histogram, sizeof(histogram));
		csBrightnessLast = csBrightness;

		if ( commDetectMethod & DET::AR )
		{
			initProcessArInfo(_arRange);

			if ( _useFrameArray )
				_frame[_frameCount].ar_ratio = last_ar_ratio;
		}

		for ( int i = _maxBrightness; i >= 0; --i )
			csBrightnessLast += histogram[i] * i;

		csBrightnessLast /= (_width - (border * 2)) * (_height - (border * 2)) / 16;

		if ( _useFrameArray )
		{
			_frame[_frameCount].brightness = csBrightnessLast;
			_frame[_frameCount].logo_present = false;
			_frame[_frameCount].schange_percent = 0;
		}

		if ( commDetectMethod & DET::SCENE_CHANGE )
			_sceneChangeVec.push_back(SceneChange(0, 0));

		return false;
	}

#if 0
	if ( 17652 < _frameCount && _frameCount < 17657 )
	{
		outputFrame(_frameCount);
	}
#endif

	processArInfo(_arRange);

	if ( _useFrameArray )
		_frame[_frameCount].ar_ratio = last_ar_ratio;

	similar *= 1;

	for ( int i = 255; i > _maxBrightness; --i )
	{
		pixels += histogram[i];
		csBrightness += histogram[i] * i;

		if ( histogram[i] )
			++hasBright;

#if 0
		if ( histogram[i] != lastHistogram[i] )
			similar += abs( histogram[i] - lastHistogram[i]);
#else
		if ( histogram[i] < lastHistogram[i] )
			similar += histogram[i];
#endif
		else
			similar += lastHistogram[i];
	}

	for ( int i = _maxBrightness; i > _testBrightness; --i )
	{
		pixels += histogram[i];
		csBrightness += histogram[i] * i;
		dimCount += histogram[i];
#if 0
		if ( histogram[i] != lastHistogram[i] )
			similar += abs( histogram[i] - lastHistogram[i]);
#else
		if ( histogram[i] < lastHistogram[i] )
			similar += histogram[i];
#endif
		else
			similar += lastHistogram[i];
	}

	for ( int i = _testBrightness; i >= 0; --i )
	{
		pixels += histogram[i];
		csBrightness += histogram[i] * i;
#if 0
		if ( histogram[i] != lastHistogram[i] )
			similar += abs( histogram[i] - lastHistogram[i]);
#else
		if ( histogram[i] < lastHistogram[i] )
			similar += histogram[i];
#endif
		else
			similar += lastHistogram[i];
	}

	csBrightness /= pixels;

	// fprintf(stderr, "brightness=%d\n", csBrightness);

	if ( _useFrameArray )
		_frame[_frameCount].hasBright = hasBright;

     scale = 720.0 * 480.0 / _width / _height;

	if ( min_hasBright > hasBright * scale )
		min_hasBright = hasBright * scale;

	if ( _useFrameArray )
		_frame[_frameCount].dimCount = dimCount;

	if ( min_dimCount > dimCount * scale )
		min_dimCount = dimCount * scale;

	if ( _cutSceneNo != 0 || _frameCount == _cutSceneNo )
		recordCutScene(_frameCount, csBrightness);

	int uniform(0);

	for ( int i = 255; i > csBrightness + _noiseLevel; --i )
		uniform += histogram[i] * (i - csBrightness);

	for ( int i = csBrightness - _noiseLevel; i >= 0; --i )
		uniform +=  histogram[i] * (csBrightness - i);

	uniform = (int)((double)uniform * 730.0 / (double)pixels);

	if ( _useFrameArray )
		_frame[_frameCount].uniform = uniform;

	{
		int x(0);

		for ( int i = 10; i < 100; ++i )
		{
			if ( histogram[i] > 10 )
			{
				if ( x < histogram[i] )
					x = histogram[i];
				else
				{
					if ( x > histogram[i+1] && histogram[i-1] > 1000 )
					{
						_histogram.black[i-1]++;
						break;
					}
				}
			}
		}
	}

	/* Not tested
	{
		int x(0);

		for ( int i = 10; i < 100; ++i )
			if ( _histogram.black[x] < _histogram.black[i] )
				x = i;

		if ( x > 10 && (_frameCount % 2) == 0 )
		{
			x = x + 5;

			if ( x > _maxAvgBrightness )
			{
				++_maxAvgBrightness;
				++_testBrightness;
				++_maxBrightness;
			}
			else if ( x < _maxAvgBrightness )
			{
				--_maxAvgBrightness;
				--_testBrightness;
				--_maxBrightness;
			}
		}
	}
	*/

	if ( _useFrameArray )
		_frame[_frameCount].brightness = csBrightness;

	_histogram.bright[csBrightness]++;
	_histogram.uniform[(uniform / UNIFORMSCALE < 255) ? uniform/UNIFORMSCALE : 255]++;

	if ( (dimCount > (int)(.05 * _width * _height))
			&& (dimCount < (int)(.35 * _width * _height)) )
		isDim = true;

	sceneChangePercent = (int)(100.0 * (double)similar / (double)pixels);
	// sceneChangePercent = (int)(100.0 * (1.0 - ((float)abs(prevsimilar - similar) / pixels)));
	// prevsimilar = similar;

	if ( _useFrameArray )
		_frame[_frameCount].schange_percent = sceneChangePercent;

#if 0
	cause = ProcessClues(_frameCount, csBrightness, hasBright
					, isDim, uniform, sceneChangePercent, curvolume,

	if ( _useFrameArray )
		_frame[_frameCount].isblack = cause;

	if ( cause != 0 )
		insertBlackFrame(csBrightness, uniform, curvolume, framenum_real, cause);
#endif

	int cause(0);

	if ( commDetectMethod & DET::BLACK_FRAME )
	{
		if ( ( csBrightness <= _maxAvgBrightness )
				&& hasBright <= _maxBright * _width * _height / 720 / 480
				&& ! isDim
				// && uniform < _nonUniformity
				// && ! _lastLogoTest // because logo disappearance is detected too late
				)
		{
			cause |= C_b;

			Debug(7, "Frame %6i - Black frame with brightness of %i,"
						" uniform of %i and volume of %i\n"
						, framenum_real
						, csBrightness
						, uniform
						, curvolume
						);
		}
		else if ( _nonUniformity > 0 )
		{
			if ( (csBrightness <= _maxAvgBrightness)
					&& uniform < _nonUniformity )
			{
				cause |= C_u;
				insertBlackFrame(csBrightness, uniform, curvolume, framenum_real, cause);
				Debug(7, "C_u: Frame %6i - Black frame with brightness of %i,"
						" uniform of %i and volume of %i\n"
						, framenum_real
						, csBrightness
						, uniform
						, curvolume
						);
			}

			if ( csBrightness > _maxAvgBrightness
					&& uniform < _nonUniformity
					&& csBrightness < 180 )
			{
				cause |= C_u;

				insertBlackFrame(csBrightness, uniform, curvolume, framenum_real, cause);
				Debug(7, "Frame %6i - Uniform frame with brightness of %i"
						" and uniform of %i\n"
						, framenum_real
						, csBrightness
						, uniform);
			}
		}
	}

	if ( commDetectMethod & DET::RESOLUTION_CHANGE )
	{
		if ( (old_width != 0 && _width != old_width )
						|| (old_height != 0 && _height != old_height) )
		{
			cause |= C_r;

			Debug(7, "Frame %6i - Resolution change from %d x %d to %d x %d \n"
						, framenum_real, old_width, old_height, _width, _height);
			old_width = _width;
			old_height = _height;
			resetLogoBuffers();
		}

		old_width = _width;
		old_height = _height;
	}


#if 0
	if ( abs(csBrightness - csBrightnessLast) > _brightnessJump )
	{
		cause |= C_s;
		Debug(7, "Frame %6i - Black frame because large brightness change"
				" from %i to %i with uniform %i\n"
				, framenum_real
				, csBrightnessLast
				, brightness
				, uniform);
	} // else
#endif

	if ( commDetectMethod & DET::SCENE_CHANGE )
	{
		if ( ! (_frame[_frameCount-1].isblack & C_b)
				&& ! (cause & C_b) )
		{
			if ( abs(csBrightness - csBrightnessLast) > _brightnessJump )
			{
				cause |= C_s;

				Debug( 7,"Frame %6i - Black frame because large brightness change"
						" from %i to %i with uniform %i\n"
						, framenum_real
						, csBrightnessLast
						, csBrightness
						, uniform);
			}
			else if ( sceneChangePercent < schange_cutlevel )
			{
				cause |= C_s;

				Debug( 7,"Frame %6i - Black frame because large scene change"
						" of %i, uniform %i\n"
						, framenum_real
						, sceneChangePercent
						, uniform);
			}
		}

          /*
          if ((sceneChangePercent < 10) && (!hasBright) && !(cause & C_b)) {
          Debug(
          7,
          "Frame %6i - BlackFrame detected because of a nonbright scene change:\tsc - %i\tavg - %i\n",
          framenum_real,
          sceneChangePercent,
          csBrightness
          );
          cause |= C_s;
          } else if ((sceneChangePercent < 20) && (!hasBright) && !(cause & C_b)) {




          		if (csBrightness < csBrightnessLast * 2) {
					_sceneChangeVec.push_back(SceneChange(framenum_real, sceneChangePercent));

          		memcpy(lastHistogram, histogram, sizeof(histogram));
          		//				Debug(7, "Frame %6i - Scene change with change percentage of %i\n", framenum_real, sceneChangePercent);
          		return (true);
          		}
          		if (0) {
          		Debug(
          		7,
          		"Frame %6i - BlackFrame detected because of scene change with brightness double:\tsc - %i\tavg - %i.................................................................\n",
          		framenum_real,
          		sceneChangePercent,
          		csBrightness
          		);
          		cause |= C_s;
          		}
          */
     }

	if ( sceneChangePercent < schange_threshold )
	{
		// Scene Change threshold: original = 91
		_sceneChangeVec.push_back(SceneChange(framenum_real, sceneChangePercent));
		// memcpy(lastHistogram, histogram, sizeof(histogram));
		// Debug(7, "Frame %6i - Scene change with change percentage of %i\n"
		//			, framenum_real, sceneChangePercent);
	}

	for ( int i = 0; i < 255; ++i )
		if ( histogram[i] > 10 )
			break;

	if ( csBrightness < min_brightness_found )
		min_brightness_found = csBrightness;

	if ( _frameCount == 100 )
		_frameCount = _frameCount;

	if ( _useFrameArray )
		_frame[_frameCount].cutscenematch = 100;

//	if ( csBrightness > _maxAvgBrightness + 10 )
	{
		if ( _useFrameArray )
			_frame[_frameCount].cutscenematch = 100;

                for(int i=0; i < _cutScenes.size(); i++)
		{
			if ( std::abs(csBrightness - _cutScenes[i].brightness) < 2 )
			{
				cutscenematch = matchCutScene(_cutScenes[i]);

				if ( _useFrameArray )
				{
					if ( _frame[_frameCount].cutscenematch
								> cutscenematch * 100 / _cutScenes[i].length )
						_frame[_frameCount].cutscenematch =
								cutscenematch * 100 / _cutScenes[i].length;
				}
			}
		}
	}

	if ( _frame[_frameCount].cutscenematch < CutScene::delta )
		cause |= C_t;

	if ( cause != 0 )
		insertBlackFrame(csBrightness, uniform, curvolume, framenum_real, cause);

	if ( _useFrameArray )
		_frame[_frameCount].isblack = cause;

	return false;
}

// Subroutines for Logo Detection
void CommercialSkipper::printLogoFrameGroups(void)
{
	Debug(2, "\nLogos detected on the following frames\n"
				"--------------------------------------\n");

	int count(0);

	for ( int i = 0; i < _logoBlockCount; ++i )
	{
		int f(FindBlock(_logoBlock[i].start));
		int t(FindBlock(_logoBlock[i].end - 2));

		if ( f < 0 )
			f = 0;

		if ( t < 0 )
			t = 0;

		if ( t < 0 )
		{
			Debug (2, "Panic\n");
			break;
		}

		if ( f < 0 )
		{
			Debug (2, "Panic\n");
			break;
		}

		Debug(2, "Logo start - %6i\tend - %6i\tlength - %s\t"
				"before:%.1f s\t after:%.1f s\n"
				, _logoBlock[i].start
				, _logoBlock[i].end
				, secondsToString(F2L(_logoBlock[i].end
								, _logoBlock[i].start)).c_str()
				, F2L(_logoBlock[i].start, cblock[f].f_start)
				, F2L(cblock[t].f_end, _logoBlock[i].end) );

		count += _logoBlock[i].end - _logoBlock[i].start + 1;

	}

	for ( int i = 0; i < _logoBlockCount-1; ++i )
	{
		int f(_logoBlock[i].end);
		int t(_logoBlock[i+1].start);

		if ( max_logo_gap < F2L(t, f) )
			max_logo_gap = F2L(t, f);

		f = FindBlock(_logoBlock[i].end);
		t = FindBlock(_logoBlock[i+1].start);

		for ( int l = f + 1; l < t; ++l )
		{
			if ( max_nonlogo_block_length < cblock[l].length )
				max_nonlogo_block_length = cblock[l].length;
		}
	}

	for ( int i = 0; i < _logoBlockCount - 1; ++i )
	{
		int f(FindBlock(_logoBlock[i].start));
		int t(FindBlock(_logoBlock[i].end));

		if ( F2L(_logoBlock[i].end, _logoBlock[i].start)
				> max_nonlogo_block_length )
		{
			double cl(F2L(cblock[f].f_end, _logoBlock[i].start));

			if ( (cl < cblock[f].length / 10)
						&& (cl > logo_overshoot) )
				logo_overshoot = cl;

			cl = F2L(_logoBlock[i].end, cblock[t].f_start);

			if ( (cl < cblock[t].length / 10)
						&& (cl > logo_overshoot) )
				logo_overshoot = cl;
		}
	}

	logo_overshoot = (logo_overshoot > 0)
			? (logo_overshoot + 1 + shrink_logo)
			: shrink_logo;
}

void CommercialSkipper::printCcBlocks(void)
{
	Debug(2, "Combining CC Blocks...\n");

	for ( int i = _ccBlockCount - 1; i > 0; --i )
	{
		if ( F2L(_ccBlock[i]._endFrame, _ccBlock[i]._startFrame) < 1.0 )
		{
			Debug(4, "Removing cc cblock %i because the length is %.2f.\n"
					, i, F2L(_ccBlock[i]._endFrame, _ccBlock[i]._startFrame));

			for ( int j = i; j < _ccBlockCount - 1; ++j )
			{
				_ccBlock[j]._startFrame = _ccBlock[j + 1]._startFrame;
				_ccBlock[j]._endFrame = _ccBlock[j + 1]._endFrame;
				_ccBlock[j]._type = _ccBlock[j + 1]._type;
			}

			--_ccBlockCount;
		}
	}

	Debug(2, "CC's detected on the following frames - %i total blocks\n"
			"--------------------------------------\n"
			, _ccBlockCount);
	const CcBlock &cc0(_ccBlock[0]);
	Debug(2, " 0 - CC start - %6i\tend - %6i\ttype - %s"
			, cc0._startFrame
			, cc0._endFrame
			, CcTypeToStr(cc0._type));
	Debug(2, "\tlength - %s\n"
			, secondsToString(F2L(cc0._endFrame
							, cc0._startFrame)).c_str());

	cc_count[cc0._type] += cc0._endFrame - cc0._startFrame + 1;

	for ( int i = 1; i < _ccBlockCount; ++i )
	{
		const CcBlock &cc(_ccBlock[i]);

		Debug(2, "%2i - CC start - %6i\tend - %6i\ttype - %s"
				, i
				, cc._startFrame
				, cc._endFrame
				, CcTypeToStr(cc._type));
		Debug(2, "\tlength - %s\n"
				, secondsToString(F2L(cc._endFrame
								, cc._startFrame)).c_str());

		cc_count[cc._type] += cc._endFrame - cc._startFrame + 1;
	}

	Debug(2, "\nCaption sums\n---------------------------\n");
	Debug(2, "Pop on captions:   %6i:%5.2f - %s\n"
			, cc_count[CC_POPON]
			, ((double)cc_count[CC_POPON] / (double)framesprocessed) * 100.0
			, secondsToString(cc_count[CC_POPON] / fps).c_str());
	Debug(2, "Roll up captions:  %6i:%5.2f - %s\n"
			, cc_count[CC_ROLLUP]
			, ((double)cc_count[CC_ROLLUP] / (double)framesprocessed) * 100.0
			, secondsToString(cc_count[CC_ROLLUP] / fps).c_str());
	Debug(2, "Paint on captions: %6i:%5.2f - %s\n"
			, cc_count[CC_PAINTON]
			, ((double)cc_count[CC_PAINTON] / (double)framesprocessed) * 100.0
			, secondsToString(cc_count[CC_PAINTON] / fps).c_str());
	Debug(2, "No captions:       %6i:%5.2f - %s\n"
			, cc_count[CC_NONE]
			, ((double)cc_count[CC_NONE] / (double)framesprocessed) * 100.0
			, secondsToString(cc_count[CC_NONE] / fps).c_str());

	for ( int i = 0; i <= 4; ++i )
	{
		if ( cc_count[i] > cc_count[_mostCcType] )
			_mostCcType = i;
	}

	Debug(2, "The %s type of closed captions were determined"
			" to be the most common.\n"
			, CcTypeToStr(_mostCcType));
}

#define AR_DIST	20

#define LOGO_Y_LOOP	for ( int y = (logo_at_bottom ? _height / 2 : _edges.getRadius() + border); y < (subtitles ? _height / 2 : (_height - _edges.getRadius() - border)); y += step )
#define LOGO_X_LOOP for ( int x = _edges.getRadius() + border; x < (_videoWidth - _edges.getRadius() - border); x += step )

void CommercialSkipper::edgeDetect(void)
{
	const U8Vec &buf(_logoBuffers.getConstNewestBuffer());
#if defined(MAXMIN_LOGO_SEARCH) || MULTI_EDGE_BUFFER
	int maskNumber(_logoBuffers.getNewestIndex());
	double borderIgnore(0.05);
#endif

#ifdef MAXMIN_LOGO_SEARCH
	{
		const int radius(_edges.getRadius());
		const int yStart(logo_at_bottom) ? height / 2 : radius + (int)(_height * borderIgnore);
		const int yMax(subtitles
						? height / 2
						: (_height - radius - (int)(_height * borderIgnore)));
		const int xStart(std::max(radius + (int)(_width * borderIgnore), _arRange.x.min + AR_DIST));
		const int xMax(std::min((_width - radius - (int)(_width * borderIgnore)), _arRange.x.max - AR_DIST));

		for ( int y = yStart; y < yMax; ++y )
		{
			for ( int x = xStart; x < xMax; ++x )
			{
				const int index(y * _width + x);
				_br[index].set(buf[index]);
			}
		}
	}
#endif

#if MULTI_EDGE_BUFFER
	{
		BoolMatrix &he(_logoBuffers.getLogoBuffer(maskNumber).getHorEdge());
		BoolMatrix &ve(_logoBuffers.getLogoBuffer(maskNumber).getVerEdge());
		he.reset();
		ve.reset();

		const int radius(_edges.getRadius());
		const int threshold(_edges.getThreshold());

		const int yStart(logo_at_bottom
						? _height / 2
						: radius + (int)(_height * borderIgnore));
		const int yMax(subtitles
						? _height / 2
						: (_height - radius - (int)(_height * borderIgnore)));
		const int xStart(std::max(radius + (int)(_width * borderIgnore), _arRange.x.min + AR_DIST));
		const int xMax(std::min((_width - radius - (int)(_width * borderIgnore)), _arRange.x.max - AR_DIST));

		for ( int y = yStart; y < yMax; ++y )
		{
			for ( int x = xStart; x < xMax; ++x )
			{
				const int offset(y * _width + x);
				const uint8_t herePixel(buf[offset]);

				if ( (std::abs(buf[offset - radius] - herePixel) >= threshold)
						|| (std::abs(buf[offset + radius] - herePixel) >= threshold) )
				{
					he.set(x, y, true);
				}

				if ( (std::abs(buf[(y - radius) * _width + x] - herePixel)
								>= threshold)
						|| (std::abs(buf[(y + radius) * _width + x] - herePixel)
								>= threshold) )
				{
					ve.set(x, y, true);
				}
			}
		}
	}
#else
	if ( _aggressiveLogoRejection == 1 )
	{
		const int step(_edges.getStep());
		const int logoBufferCount(_logoBuffers.getBufferCount());

		LOGO_X_LOOP
		{
			LOGO_Y_LOOP
			{
				HorVerType &hv(_edgeCount.get(x, y));

				if ( _edges.testHor1(buf, x, y, _width) )
				{
					if ( hv.getHor() < logoBufferCount )
						hv.incHor();
					else
						++edge_count;
				}
				else
					hv.setHor(0);

				if ( _edges.testVer1(buf, x, y, _width) )
				{
					if ( hv.getVer() < logoBufferCount )
						hv.incVer();
					else
						++edge_count;
				}
				else
					hv.setVer(0);
			}
		}
	}
	else if ( _aggressiveLogoRejection == 2 )
	{
		const int step(_edges.getStep());
		const int logoBufferCount(_logoBuffers.getBufferCount());

		LOGO_X_LOOP
		{
			LOGO_Y_LOOP
			{
				HorVerType &hv(_edgeCount.get(x, y));

				if ( _edges.testHor2(buf, x, y, _width) )
				{
					if ( hv.getHor() < logoBufferCount )
						hv.incHor();
					else
						++edge_count;
				}
				else
					hv.setHor(0);

				if ( _edges.testVer2(buf, x, y, _width) )
				{
					if ( hv.getVer() < logoBufferCount )
						hv.incVer();
					else
						++edge_count;
				}
				else
					hv.setVer(0);
			}
		}
	}
	else if ( _aggressiveLogoRejection == 3 )
	{
		const int step(_edges.getStep());
		const int logoBufferCount(_logoBuffers.getBufferCount());

		LOGO_X_LOOP
		{
			LOGO_Y_LOOP
			{
				HorVerType &hv(_edgeCount.get(x, y));

				if ( _edges.testHor3(buf, x, y, _width) )
				{
					if ( hv.getHor() < logoBufferCount )
						hv.incHor();
					else
						++edge_count;
				}
				else
					hv.setHor(0);

				if ( _edges.testVer3(buf, x, y, _width) )
				{
					if ( hv.getVer() < logoBufferCount )
						hv.incVer();
					else
						++edge_count;
				}
				else
					hv.setVer(0);
			}
		}
	}
	else if ( _aggressiveLogoRejection == 4 )
	{
		const int step(_edges.getStep());
		const int logoBufferCount(_logoBuffers.getBufferCount());

		LOGO_X_LOOP
		{
			LOGO_Y_LOOP
			{
				const int offset(y * _width + x);
				const int radius(_edges.getRadius());
				HorVerType &hv(_edgeCount.get(x, y));

				if ( (/*buf[offset - edge_radius] > 50 && */ buf[offset - radius] < 200)
						|| ( /*buf[offset + edge_radius] > 50 && */ buf[offset + radius] < 200) )
				{
					if ( _edges.testHor0(buf, x, y, _width) )
					{
						if ( hv.getHor() < logoBufferCount )
							hv.incHor();
						else
							++edge_count;
					}
					else if ( buf[offset] < 200 )
						hv.setHor(0);
				}

				const int yMinus(y - radius);
				const int yPlus(y + radius);

#if USE_RANGE_CHECKING
				if ( yMinus < 0 )
					throw std::runtime_error("yMinus is less than zero");
				else if ( yPlus >= 1080 )
					throw std::runtime_error("yPlus is greater than or equal to 1080");

				if ( x < 0 )
					throw std::runtime_error("x is less than zero");
				else if ( x >= 1920 )
					throw std::runtime_error("x is greater than or equal to 1920");
#endif

				if ( (/*buf[yMinus * width + x] > 50 && */ buf[yMinus * _width + x] < 200)
						|| ( /*buf[yPlus * _width + x] > 50 && */ buf[yPlus * _width + x] < 200) )
				{
					if ( _edges.testVer0(buf, x, y, _width) )
					{
						if ( hv.getVer() < logoBufferCount )
							hv.incVer();
						else
							++edge_count;
					}
					else if ( buf[offset] < 200 )
						hv.setVer(0);
				}
			}
		}
	}
	else
	{
		const int step(_edges.getStep());
		const int logoBufferCount(_logoBuffers.getBufferCount());

		LOGO_X_LOOP
		{
			LOGO_Y_LOOP
			{
				const int offset(y * _width + x);
				const int radius(_edges.getRadius());
				HorVerType &hv(_edgeCount.get(x, y));

				if ( (/*buf[offset - radius] > 50 && */ buf[offset - radius] < 200)
						|| ( /*buf[offset + radius] > 50 && */ buf[offset + radius] < 200) )
				{
					if ( _edges.testHor0(buf, x, y, _width) )
					{
						if ( hv.getHor() < logoBufferCount )
							hv.incHor();
						else
							++edge_count;
					}
					else
						hv.setHor(0);
				}

				const int yMinus(y - radius);
				const int yPlus(y + radius);

#if USE_RANGE_CHECKING
				if ( yMinus < 0 )
					throw std::runtime_error("yMinus is less than zero");
				else if ( yPlus >= 1080 )
					throw std::runtime_error("yPlus is greater than or equal to 1080");

				if ( x < 0 )
					throw std::runtime_error("x is less than zero");
				else if ( x >= 1920 )
					throw std::runtime_error("x is greater than or equal to 1920");
#endif

				if ( (/*buf[yMinus * _width + x] > 50 && */ buf[yMinus * _width + x] < 200)
						|| ( /*buf[yPlus * _width + x] > 50 && */ buf[yPlus * _width + x] < 200) )
				{
					if ( _edges.testVer0(buf, x, y, _width) )
					{
						if ( hv.getVer() < logoBufferCount )
							hv.incVer();
						else
							++edge_count;
					}
					else
						hv.setVer(0);
				}
			}
		}
	}
#endif
}

double CommercialSkipper::checkStationLogoEdge(const uint8_t *frame)
{
	currentGoodEdge = 0.0;

	if ( _aggressiveLogoRejection == 0 )
		return checkStationLogoEdge0(frame);
	else if ( _aggressiveLogoRejection == 1 )
		return checkStationLogoEdge1(frame);
	else if ( _aggressiveLogoRejection == 2 )
		return checkStationLogoEdge2(frame);
	else if ( _aggressiveLogoRejection == 3 )
		return checkStationLogoEdge3(frame);
	else if ( _aggressiveLogoRejection == 4 )
		return checkStationLogoEdge4(frame);

	throw std::invalid_argument("aggressive_logo_rejection should be 0-4");
}

class EdgeResults
{
public:
	EdgeResults(): _test(0), _good(0) {}

	void addResult(bool status)
	{
		++_test;

		if ( status )
			++_good;
	}

	double getResults(void)
	{
		return ( _test == 0 )
		? 0.5
		: (double)_good / (double)_test;
	}

private:
	int _test;
	int _good;
};

double CommercialSkipper::checkStationLogoEdge0(const uint8_t *frame)
{
	const BoolMatrix &hor(_edgeMask.getConstCHor());
	const BoolMatrix &ver(_edgeMask.getConstCVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; y += step )
	{
		for ( int x = _logoRange.x.min; x <= _logoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor0(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer0(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::checkStationLogoEdge1(const uint8_t *frame)
{
	const BoolMatrix &hor(_edgeMask.getConstCHor());
	const BoolMatrix &ver(_edgeMask.getConstCVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; y += step )
	{
		for ( int x = _logoRange.x.min; x <= _logoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor1(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer1(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::checkStationLogoEdge2(const uint8_t *frame)
{
	const BoolMatrix &hor(_edgeMask.getConstCHor());
	const BoolMatrix &ver(_edgeMask.getConstCVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; y += step )
	{
		for ( int x = _logoRange.x.min; x <= _logoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor2(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer2(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::checkStationLogoEdge3(const uint8_t *frame)
{
	const BoolMatrix &hor(_edgeMask.getConstCHor());
	const BoolMatrix &ver(_edgeMask.getConstCVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; y += step )
	{
		for ( int x = _logoRange.x.min; x <= _logoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor3(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer3(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::checkStationLogoEdge4(const uint8_t *frame)
{
	const BoolMatrix &hor(_edgeMask.getConstCHor());
	const BoolMatrix &ver(_edgeMask.getConstCVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; y += step )
	{
		for ( int x = _logoRange.x.min; x <= _logoRange.x.max; x += step )
		{
			if ( frame[y * _width + x] < 200 )
			{
				if ( hor.get(x, y) )
					er.addResult( _edges.testHor0(frame, x, y, _width) );

				if ( ver.get(x, y) )
					er.addResult( _edges.testVer0(frame, x, y, _width) );
			}
		}
	}

	return er.getResults();
}

double CommercialSkipper::doubleCheckStationLogoEdge(const U8Vec &frame)
{
	currentGoodEdge = 0.0;

	if ( _aggressiveLogoRejection == 0 )
		return doubleCheckStationLogoEdge0(frame);
	else if ( _aggressiveLogoRejection == 1 )
		return doubleCheckStationLogoEdge1(frame);
	else if ( _aggressiveLogoRejection == 2 )
		return doubleCheckStationLogoEdge2(frame);
	else if ( _aggressiveLogoRejection == 3 )
		return doubleCheckStationLogoEdge3(frame);
	else if ( _aggressiveLogoRejection == 4 )
		return doubleCheckStationLogoEdge4(frame);

	throw std::invalid_argument("aggressive_logo_rejection should be 0-4");
}

double CommercialSkipper::doubleCheckStationLogoEdge0(const U8Vec &frame)
{
	const BoolMatrix &hor(_edgeMask.getConstTHor());
	const BoolMatrix &ver(_edgeMask.getConstTVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _tLogoRange.y.min; y <= _tLogoRange.y.max; y += step )
	{
		for ( int x = _tLogoRange.x.min; x <= _tLogoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor0(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer0(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::doubleCheckStationLogoEdge1(const U8Vec &frame)
{
	const BoolMatrix &hor(_edgeMask.getConstTHor());
	const BoolMatrix &ver(_edgeMask.getConstTVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _tLogoRange.y.min; y <= _tLogoRange.y.max; y += step )
	{
		for ( int x = _tLogoRange.x.min; x <= _tLogoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor1(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer1(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::doubleCheckStationLogoEdge2(const U8Vec &frame)
{
	const BoolMatrix &hor(_edgeMask.getConstTHor());
	const BoolMatrix &ver(_edgeMask.getConstTVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _tLogoRange.y.min; y <= _tLogoRange.y.max; y += step )
	{
		for ( int x = _tLogoRange.x.min; x <= _tLogoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor2(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer2(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::doubleCheckStationLogoEdge3(const U8Vec &frame)
{
	const BoolMatrix &hor(_edgeMask.getConstTHor());
	const BoolMatrix &ver(_edgeMask.getConstTVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _tLogoRange.y.min; y <= _tLogoRange.y.max; y += step )
	{
		for ( int x = _tLogoRange.x.min; x <= _tLogoRange.x.max; x += step )
		{
			if ( hor.get(x, y) )
				er.addResult( _edges.testHor3(frame, x, y, _width) );

			if ( ver.get(x, y) )
				er.addResult( _edges.testVer3(frame, x, y, _width) );
		}
	}

	return er.getResults();
}

double CommercialSkipper::doubleCheckStationLogoEdge4(const U8Vec &frame)
{
	const BoolMatrix &hor(_edgeMask.getConstTHor());
	const BoolMatrix &ver(_edgeMask.getConstTVer());
	const int step(_edges.getStep());
	EdgeResults er;

	for ( int y = _tLogoRange.y.min; y <= _tLogoRange.y.max; y += step )
	{
		for ( int x = _tLogoRange.x.min; x <= _tLogoRange.x.max; x += step )
		{
			if ( frame[y * _width + x] < 200 )
			{
				if ( hor.get(x, y) )
					er.addResult( _edges.testHor0(frame, x, y, _width) );

				if ( ver.get(x, y) )
					er.addResult( _edges.testVer0(frame, x, y, _width) );
			}
		}
	}

	return er.getResults();
}

void CommercialSkipper::resetLogoBuffers(void)
{
	_logoBuffers.setNewestIndex(0);
	_logoBuffers.setOldestIndex(0);
	_logoBuffers.setNewestFrameNumber(framenum_real);
}

void CommercialSkipper::fillLogoBuffer(void)
{
	_logoBuffers.rotate();
	_logoBuffers.setNewestFrameNumber(framenum_real);
	_logoBuffers.setOldestIndex(0);

	for ( size_t i = 0; i < _logoBuffers.getBufferCount(); ++i )
	{
		const int fn(_logoBuffers.getFrameNumber(i));

		if ( fn && fn < _logoBuffers.getOldestFrameNumber() )
			_logoBuffers.setOldestIndex(i);
	}

	int count(std::min((int)_logoBuffers.getMaxBufSize()
				, _width * _height * (int)sizeof(_videoFramePointer[0])));

	std::copy(_videoFramePointer, _videoFramePointer + count, _logoBuffers.getNewestBuffer().begin());
	edgeDetect();
	_logoBuffers.checkIsFull();
}

bool CommercialSkipper::searchForLogoEdges(void)
{
	int i;
	double scale( ((double)_height / 572) * ((double)_videoWidth / 720) );
	double logoPercentageOfScreen;
	int sum;
	// int excludeUnderX = videowidth / 2;
	// int index;
	int last_non_logo_frame;
	int logoFound = false;
	const int logoBufferCount(_logoBuffers.getBufferCount());
	const int radius(_edges.getRadius());

	_tLogoRange.x.min = radius + border;
	_tLogoRange.x.max = _videoWidth - radius - border;
	_tLogoRange.y.min = radius + border;
	_tLogoRange.y.max = _height - radius - border;

#if MULTI_EDGE_BUFFER

	BoolMatrix &tHor(_edgeMask.getTHor());
	BoolMatrix &tVer(_edgeMask.getTVer());
	tHor.reset(true);
	tVer.reset(true);

	for ( int i = 0; i < 1; ++i )
	{
		const BoolMatrix &he(_logoBuffers.getConstLogoBuffer(i).getConstHorEdge());
		const BoolMatrix &ve(_logoBuffers.getConstLogoBuffer(i).getConstVerEdge());

		for ( int y = border; y < _height - border; ++y )
		{
			for ( int x = border; x < _videoWidth - border; ++x )
			{
				if ( ! he.get(x, y) )
					tHor.set(x, y, false);

				if ( ! ve.get(x, y) )
					tVer.set(x, y, false);
			}
		}
	}

	for ( int i = 1; i < logoBufferCount; ++i )
	{
		const BoolMatrix &he(_logoBuffers.getConstLogoBuffer(i).getConstHorEdge());
		const BoolMatrix &ve(_logoBuffers.getConstLogoBuffer(i).getConstVerEdge());

		for ( int y = border; y < _height - border; ++y )
		{
			for ( int x = border; x < _videoWidth - border; ++x )
			{
				if ( ! he.get(x, y) )
					tHor.set(x, y, false);

				if ( ! ve.get(x, y) )
					tVer.set(x, y, false);
			}
		}
	}
#else

	BoolMatrix &tHor(_edgeMask.getTHor());
	BoolMatrix &tVer(_edgeMask.getTVer());

	tHor.reset();
	tVer.reset();

	const int step(_edges.getStep());

	LOGO_X_LOOP
	{
		LOGO_Y_LOOP
		{
			const HorVerType &hv(_edgeCount.getConst(x, y));

			if ( hv.getHor() >= logoBufferCount )
				tHor.set(x, y, true);

			if ( hv.getVer() >= logoBufferCount )
				tVer.set(x, y, true);
		}
	}
#endif

	clearEdgeMaskArea(tHor, tVer);
	clearEdgeMaskArea(tVer, tHor);

	setEdgeMaskArea(tHor);

	Rectangle tempLogoRange(_tLogoRange);

	_tLogoRange.x.min = 2 + border;
	_tLogoRange.x.max = _videoWidth - 2 - border;
	_tLogoRange.y.min = 2 + border;
	_tLogoRange.y.max = _height - 2 - border;

	setEdgeMaskArea(tVer);

	if ( tempLogoRange.x.min < _tLogoRange.x.min )
		_tLogoRange.x.min = tempLogoRange.x.min;

	if ( tempLogoRange.x.max > _tLogoRange.x.max )
		_tLogoRange.x.max = tempLogoRange.x.max;

	if ( tempLogoRange.y.min < _tLogoRange.y.min )
		_tLogoRange.y.min = tempLogoRange.y.min;

	if ( tempLogoRange.y.max > _tLogoRange.y.max )
		_tLogoRange.y.max = tempLogoRange.y.max;

	logoPercentageOfScreen = (double)((_tLogoRange.y.max - _tLogoRange.y.min) * (_tLogoRange.x.max - _tLogoRange.x.min)) / (double)(_height * _width);

	if ( logoPercentageOfScreen > logo_max_percentage_of_screen )
	{
#if 0
		Debug(3, "Reducing logo search area!\tPercentage of screen - %.2f%% TOO BIG.\n"
				, logoPercentageOfScreen * 100 );
#endif

		if ( tempLogoRange.x.min > _tLogoRange.x.min + 50 )
			_tLogoRange.x.min = tempLogoRange.x.min;

		if ( tempLogoRange.x.max < _tLogoRange.x.max - 50 )
			_tLogoRange.x.max = tempLogoRange.x.max;

		if ( tempLogoRange.y.min > _tLogoRange.y.min + 50 )
			_tLogoRange.y.min = tempLogoRange.y.min;

		if ( tempLogoRange.y.max < _tLogoRange.y.max - 50 )
			_tLogoRange.y.max = tempLogoRange.y.max;
	}

	i = countEdgePixels();
#if 0
	::printf("Edges=%d\n",i);
	if ( i > 350 / (lowres+1) / (edge_step))
#else
	if ( i > 180 * scale / _edges.getStep() )
#endif
	{
		logoPercentageOfScreen = (double)((_tLogoRange.y.max - _tLogoRange.y.min)
								* (_tLogoRange.x.max - _tLogoRange.x.min))
								/ (double)(_height * _width);

		if ( i > 40000
				|| logoPercentageOfScreen > logo_max_percentage_of_screen )
		{
			Debug(3, "Edge count - %i\tPercentage of screen"
					" - %.2f%% TOO BIG, CAN'T BE A LOGO.\n"
					, i
					, logoPercentageOfScreen * 100);
			// _logoInfoAvailable = false;
		}
		else
		{
			Debug(3, "Edge count - %i\tPercentage of screen"
					" - %.2f%%, Check: %i\n"
					, i
					, logoPercentageOfScreen * 100
					, doublCheckLogoCount);
			// _logoInfoAvailable = true;
			logoFound = true;
		}
	}
	else
		Debug(3, "Not enough edge count - %i\n", i);

	if ( logoFound )
	{
		++doublCheckLogoCount;
		Debug(3, "Double checking - %i\n", doublCheckLogoCount );

		if ( doublCheckLogoCount > 1 )
		{
			// Final check done, found
		}
		else
			logoFound = false;
	}
	else
	{
		doublCheckLogoCount = 0;
	}

	sum = 0;
	_logoBuffers.setOldestIndex(0);

	for ( size_t i = 0; i < _logoBuffers.getBufferCount(); ++i )
	{
		const int fn(_logoBuffers.getFrameNumber(i));

		if ( fn && fn < _logoBuffers.getOldestFrameNumber() )
			_logoBuffers.setOldestIndex(i);
	}

	last_non_logo_frame = _logoBuffers.getOldestFrameNumber();

	if ( logoFound )
	{
		Debug(3, "Doublechecking frames %i to %i for logo.\n"
				, _logoBuffers.getOldestFrameNumber()
				, _logoBuffers.getNewestFrameNumber());

		for ( size_t i = 0; i < _logoBuffers.getBufferCount(); ++i )
		{
			currentGoodEdge = doubleCheckStationLogoEdge(_logoBuffers.getConstBuffer(i));
			bool logoPresent(currentGoodEdge > logo_threshold);
			const int fn(_logoBuffers.getFrameNumber(i));

			for ( int x = fn; x < fn + (int)( logoFreq * fps ); ++x )
			{
				_frame[x].currentGoodEdge = currentGoodEdge;
				_frame[x].logo_present = logoPresent;

				if ( ! logoPresent && (x > last_non_logo_frame) )
					last_non_logo_frame = x;
			}

			if ( logoPresent )
				++sum;
			else
				Debug(7, "Logo not present in frame %i.\n", fn);
		}
	}

	if ( logoFound && (sum >= logoBufferCount * 9 / 10) )
	{
		_logoRange = _tLogoRange;
		_edgeMask.getCHor() = _edgeMask.getConstTHor();
		_edgeMask.getCVer() = _edgeMask.getConstTVer();

		_logoTrendCounter = logoBufferCount;
		_lastLogoTest = true;
		_curLogoTest = true;

		_logoBlock[_logoBlockCount].start = last_non_logo_frame + 1;
		dumpEdgeMasks();
		// dumpEdgeMask(_edgeMask.cHor, HORIZ);
		// dumpEdgeMask(_edgeMask.cVer, VERT);
		initScanLines();
		initHasLogo();

		_logoInfoAvailable = true; //xxxxxxx
	}
	else
	{
		// _logoInfoAvailable = false; //xxxxxxx
		currentGoodEdge = 0.0;
	}

	if ( ! _logoInfoAvailable && startOverAfterLogoInfoAvail
			&& (framenum_real > (int)(giveUpOnLogoSearch * fps)) )
	{
		Debug(1, "No logo was found after %i frames.\nGiving up"
				, framenum_real);
		commDetectMethod -= DET::LOGO;
	}

	if ( added_recording > 0 )
		giveUpOnLogoSearch += added_recording * 60;

	if ( _logoInfoAvailable && startOverAfterLogoInfoAvail )
	{
		Debug(3, "_logoRange.x.min=%i\t_logoRange.x.max=%i\t"
				"_logoRange.y.min=%i\t_logoRange.y.max=%i\n"
				, _logoRange.x.min, _logoRange.x.max
				, _logoRange.y.min, _logoRange.y.max);
		saveLogoMaskData();
		Debug(3, "******************* End of Logo Processing ***************\n");
		return false;
	}

	return true;
}


#define MAX_SEARCH 25

int CommercialSkipper::clearEdgeMaskArea(BoolMatrix &temp
		, const BoolMatrix &test)
{
	int valid(0);

	const int yMax((subtitles) ? _height / 2 : _height - border);
	const int xMax(_videoWidth - border);
	const int step(_edges.getStep());

	for ( int y = (logo_at_bottom) ? _height / 2 : border
			; y < yMax
			; ++y )
	{
		for ( int x = border; x < xMax; ++x )
		{
			int count(0);

			if ( temp.get(x, y) )
			{
				if ( test.get(x, y) )
					++count;

				for ( int offset2 = step
						; offset2 < MAX_SEARCH
						; offset2 += step )
				{
					int ix;
					int iy(offset2);

					for ( ix= -offset2; ix <= offset2; ix += step )
					{
						if ( y + iy > 0
								&& y + iy < _height
								&& x + ix > 0
								&& x + ix < _videoWidth
								&& test.get(x+ix, y+iy) )
						{
							++count;
						}
					}

					iy = -offset2;

					for ( ix= -offset2; ix <= offset2; ix += step )
					{
						if ( y + iy > 0
								&& y + iy < _height
								&& x + ix > 0
								&& x + ix < _videoWidth
								&& test.get(x+ix, y+iy) )
						{
							++count;
						}
					}

					ix = offset2;

					for ( iy= -offset2 + step
							; iy <= offset2 - step
							; iy += step )
					{
						if ( y + iy > 0
								&& y + iy < _height
								&& x + ix > 0
								&& x + ix < _videoWidth
								&& test.get(x+ix, y+iy) )
						{
							++count;
						}
					}

					ix = -offset2;

					for ( iy= -offset2 + step
							; iy <= offset2 - step
							; iy += step )
					{
						if ( y + iy > 0
								&& y + iy < _height
								&& x + ix > 0
								&& x + ix < _videoWidth
								&& test.get(x+ix, y+iy) )
						{
							++count;
						}
					}

					if ( count >= _edges.getWeight() )
						goto found;
				}

				temp.set(x, y, false);
				continue;
found:
				++valid;
			}
		}
	}

	return valid;
}

#define LOGOBORDER	4

void CommercialSkipper::setEdgeMaskArea(const BoolMatrix &temp)
{
	_tLogoRange.x.min = _videoWidth - 1;
	_tLogoRange.x.max = 0;
	_tLogoRange.y.min = _height - 1;
	_tLogoRange.y.max = 0;

	const int yMax((subtitles) ? _height / 2 : _height - border);
	const int xMax(_videoWidth - border);

	for ( int y = (logo_at_bottom) ? _height / 2 : border
			; y < yMax
			; ++y )
	{
		for ( int x = border; x < xMax; ++x )
		{
			if ( temp.get(x, y) )
			{
				if ( x - LOGOBORDER < _tLogoRange.x.min )
					_tLogoRange.x.min = x - LOGOBORDER;

				if ( y - LOGOBORDER < _tLogoRange.y.min )
					_tLogoRange.y.min = y - LOGOBORDER;

				if ( x + LOGOBORDER > _tLogoRange.x.max )
					_tLogoRange.x.max = x + LOGOBORDER;

				if ( y + LOGOBORDER > _tLogoRange.y.max )
					_tLogoRange.y.max = y + LOGOBORDER;
			}
		}
	}

	const int radius(_edges.getRadius());

	if ( _tLogoRange.x.min < radius )
		_tLogoRange.x.min = radius;

	if ( _tLogoRange.x.max > (_videoWidth - radius) )
		_tLogoRange.x.max = (_videoWidth - radius);

	if ( _tLogoRange.y.min < radius )
		_tLogoRange.y.min = radius;

	if ( _tLogoRange.y.max > (_height - radius) )
		_tLogoRange.y.max = (_height - radius);
}

int CommercialSkipper::countEdgePixels(void)
{
	const double scale(((double)_height / 572.0) * ( (double) _videoWidth / 720.0 ));
	const BoolMatrix &hor(_edgeMask.getConstTHor());
	const BoolMatrix &ver(_edgeMask.getConstTVer());
	int hcount(0);
	int vcount(0);

	for ( int y = _tLogoRange.y.min; y <= _tLogoRange.y.max; ++y )
	{
		for ( int x = _tLogoRange.x.min; x <= _tLogoRange.x.max; ++x )
		{
			if ( hor.get(x, y) )
				++hcount;

			if ( ver.get(x, y) )
				++vcount;
		}
	}

	// ::printf("%6d %6d\n",hcount, vcount);
	const int step(_edges.getStep());
	if ( (hcount < 50 * scale / step) || (vcount < 50 * scale / step) )
		return 0;

    return hcount + vcount;
}

void CommercialSkipper::dumpEdgeMask(uint8_t *buffer, int direction)
{
	switch ( direction )
	{
	case HORIZ:
		Debug(1, "\nHorizontal Logo Mask \n     ");
		break;

	case VERT:
		Debug(1, "\nVertical Logo Mask \n     ");
		break;

	case DIAG1:
		Debug(1, "\nDiagonal 1 Logo Mask \n     ");
		break;

	case DIAG2:
		Debug(1, "\nDiagonal 2 Logo Mask \n     ");
		break;
	}

	std::vector<char> outbuf(_width + 1);

	{
		int x;

		for ( x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
			outbuf[x-_logoRange.x.min] = '0'+ (x % 10);

		outbuf[x-_logoRange.x.min] = 0;
	}

	Debug(1, "%s\n", (char*)outbuf.data());
	Debug(1, "\n");

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
	{
		Debug(1, "%3d: ", y);

		int x;

		for ( x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
		{
			switch ( buffer[y * _width + x] )
			{
			case 0:
				outbuf[x - _logoRange.x.min] = ' ';
				break;

			case 1:
				outbuf[x - _logoRange.x.min] = '*';
				break;
			}
		}

		outbuf[x - _logoRange.x.min] = 0;
		Debug(1, "%s\n", (char*)outbuf.data());
	}
}

bool CommercialSkipper::checkFramesForLogo(int start, int end)
{
	for ( int i = start; i <= end; ++i )
		for ( int j = 0; j < _logoBlockCount; ++j )
			if ( i > _logoBlock[j].start && i < _logoBlock[j].end )
				return ! reverseLogoLogic;

	return reverseLogoLogic;
}

double CommercialSkipper::calculateLogoFraction(int start, int end)
{
	int j(0);
	int count(0);

	for ( int i = start; i <= end; ++i )
	{
		while ( j < _logoBlockCount && i > _logoBlock[j].end )
			++j;

		if ( j < _logoBlockCount && i >= _logoBlock[j].start
				&& i <= _logoBlock[j].end )
			++count;
	}

	if ( reverseLogoLogic )
		return (1.0 - (double) count / (double)(end - start + 1));

	return ((double) count / (double)(end - start + 1));
}

bool CommercialSkipper::checkFrameForLogo(int i)
{
	int j(0);

	while ( j < _logoBlockCount && i > _logoBlock[j].end )
		++j;

	if ( j < _logoBlockCount
			&& i <= _logoBlock[j].end
			&& i >= _logoBlock[j].start )
		return ! reverseLogoLogic;

	return reverseLogoLogic;
}

char CheckFramesForCommercial(int start, int end)
{
	// Too short to decide
	if ( start >= end )
		return '0';

	int i(0);

	while ( i <= commercial_count && start > commercial[i].end_frame )
		++i;

	// Now start <= commercial[i].end_frame
	if ( i <= commercial_count )
	{
		if ( end < commercial[i].start_frame )
			return '+';

		if ( start < commercial[i].start_frame )
			return '0';

		return '-';
	}

	return '+';
}

char CheckFramesForReffer(int start, int end)
{
	if ( reffer_count < 0 )
		return ' ';

	// Too short to decide
	if ( start >= end )
		return '0';

	int i(0);

	while ( i <= reffer_count &&  reffer[i].end_frame < start + fps )
		++i;

	// Now start <= reffer[i].end_frame
	if ( i <= reffer_count )
	{
		if ( reffer[i].start_frame < start + fps )
			return '-';

		if ( reffer[i].start_frame > end - fps )
			return '+';

		if ( reffer[i].start_frame < end + fps )
			return '0';

		return '-';
	}

	return '+';
}

void CommercialSkipper::saveLogoMaskData(void)
{
	FlagFile ff;

	ff.open(_logoFilename, "w", true);

	ff.printf("logoMinX=%i\n", _logoRange.x.min);
	ff.printf("logoMaxX=%i\n", _logoRange.x.max);
	ff.printf("logoMinY=%i\n", _logoRange.y.min);
	ff.printf("logoMaxY=%i\n", _logoRange.y.max);
	ff.printf("picWidth=%i\n", _width);
	ff.printf("picHeight=%i\n", _height);

	if (1)
	{
		ff.printf("\nCombined Logo Mask\n");
		ff.printf("\202\n");

		for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
		{
			for ( int x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
			{
				if ( ! _edgeMask.getConstCHor().get(x, y) )
					ff.printf(_edgeMask.getConstCVer().get(x, y) ? "-" : " ");
				else
					ff.printf(_edgeMask.getConstCVer().get(x, y) ? "+" : "|");
			}

			ff.printf("\n");
		}
	}
	else
	{
		ff.printf("\nHorizonatal Logo Mask\n");
		ff.printf("\200\n");

		for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
		{
			for ( int x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
				ff.printf(_edgeMask.getConstCHor().get(x, y) ? "|" : " ");

			ff.printf("\n");
		}

		ff.printf("\nVertical Logo Mask\n");
		ff.printf("\201\n");

		for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
		{
			for ( int x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
				ff.printf( _edgeMask.getConstCVer().get(x, y) ? "-" : " ");

			ff.printf("\n");
		}
	}
}

int CommercialSkipper::countSceneChanges(int StartFrame, int EndFrame)
{
	double p(0.0);

        for(int i=0; i < _sceneChangeVec.size(); i++)
	    if ( (_sceneChangeVec[i].first > StartFrame) && (_sceneChangeVec[i].first < EndFrame) )
		p += (double)(100 - _sceneChangeVec[i].second)  / (100 - schange_threshold);

	return (int)p;
}

void CommercialSkipper::initLogoBuffers(void)
{
}

double CommercialSkipper::findScoreThreshold(double percentile)
{
	class ScoreContainer
	{
	public:
		double score;
		int count;
		int start;
		int blocknr;
	};

	ScoreContainer *scores(new ScoreContainer[BlockInfo::count]);

	for ( int i = 0; i < BlockInfo::count; ++i )
	{
		scores[i].blocknr = i;
		scores[i].score = cblock[i].score;
		scores[i].count = cblock[i].f_end - cblock[i].f_start + 1;
		scores[i].start = cblock[i].f_start;
	}

	bool hadToSwap(false);
	int counter(0);

	do
	{
		hadToSwap = false;
		++counter;

		for ( int i = 0; i < BlockInfo::count - 1; ++i )
		{
			if ( scores[i].score > scores[i + 1].score )
			{
				hadToSwap = true;
				double sc = scores[i].score;
				int c = scores[i].count;
				int s = scores[i].start;
				int bnr = scores[i].blocknr;
				scores[i].score = scores[i + 1].score;
				scores[i].count = scores[i + 1].count;
				scores[i].start = scores[i + 1].start;
				scores[i].blocknr = scores[i + 1].blocknr;
				scores[i + 1].score = sc;
				scores[i + 1].count = c;
				scores[i + 1].start = s;
				scores[i + 1].blocknr = bnr;
			}
		}
	} while ( hadToSwap );

	int totalframes(0);

	for ( int i = 0; i < BlockInfo::count; ++i )
		totalframes += scores[i].count;

	Debug(10, "\n\nAfter Sorting - %i\n--------------\n", counter);

	{
		int c(0);

		for ( int i = 0; i < BlockInfo::count; ++i )
		{
			c += scores[i].count;
			Debug(10, "Block %3i - %.3f\t%6i\t%6i\t%6i\t%3.1f%c\n"
					, scores[i].blocknr, scores[i].score, scores[i].start
					, cblock[scores[i].blocknr].f_end, scores[i].count
					, ((double)c / (double)totalframes)*100,'%');
		}
	}

	int targetCount((int)(totalframes * percentile));
	double tempScore(0.0);

	{
		int i(-1);
		int c(0);

		do
		{
			c += scores[++i].count;
		} while ( c < targetCount );

		tempScore = scores[i].score;
	}

	delete[] scores;

	Debug(6, "The %.2f percentile of %i frames is %.2f\n"
			, (percentile * 100.0), totalframes, tempScore);

	return tempScore;
}

void CommercialSkipper::outputLogoHistogram(int buckets)
{
	int max(0);

	for ( int i = 0; i < buckets; ++i )
		if ( max < _histogram.logo[i] )
			max = _histogram.logo[i];

	const int columns(200);
	double divisor((double)columns / (double)max);

	Debug(8, "Logo Histogram - %.5f\n", divisor);

	int counter(0);

	for ( int i = 0; i < buckets; ++i )
	{
		char stars[256];

		counter += _histogram.logo[i];
		stars[0] = 0;

		if ( _histogram.logo[i] > 0 )
		{
			int j;

			for ( j = 0; j <= (int)(_histogram.logo[i] * divisor); ++j )
				stars[j] = '*';

			stars[j] = 0;
		}

		Debug(8, "%.3f - %6i - %.5f %s\n"
				, (double)i/buckets
				, _histogram.logo[i]
				, (double)counter / (double)_frameCount
				, stars);
	}
}

void CommercialSkipper::outputBrightHistogram(void)
{
	int max(0);

	for ( int i = 0; i < 256; ++i )
		if ( max < _histogram.bright[i] )
			max = _histogram.bright[i];

	const int columns(200);
	double divisor((double)columns / (double)max);

	Debug(1, "Show Histogram - %.5f\n", divisor);

	int counter(0);

	for ( int i = 0; i < 30; ++i )
	{
		char stars[256];

		counter += _histogram.bright[i];
		stars[0] = 0;

		if ( _histogram.bright[i] > 0 )
		{
			int j;

			for ( j = 0; j <= (int)(_histogram.bright[i] * divisor); ++j )
				stars[j] = '*';

			stars[j] = 0;
		}

		Debug(1, "%3i - %6i - %.5f %s\n"
				, i
				, _histogram.bright[i]
				, (double)counter / (double)framesprocessed
				, stars);
	}
}

void CommercialSkipper::outputUniformHistogram(void)
{
	int max(0);

	for ( int i = 0; i < 30; ++i )
		if ( max < _histogram.uniform[i] )
			max = _histogram.uniform[i];

	const int columns(200);
	double divisor((double)columns / (double)max);

	Debug(1, "Show Uniform - %.5f\n", divisor);

	int counter(0);

	for ( int i = 0; i < 30; ++i )
	{
		char stars[256];

		counter += _histogram.uniform[i];
		stars[0] = 0;

		if ( _histogram.uniform[i] > 0 )
		{
			int j;

			for ( j = 0; j <= (int)(_histogram.uniform[i] * divisor); ++j )
				stars[j] = '*';

			stars[j] = 0;
		}

		Debug(1, "%3i - %6i - %.5f %s\n"
				, i * UNIFORMSCALE
				, _histogram.uniform[i]
				, (double)counter / (double)framesprocessed
				, stars);
	}
}

void CommercialSkipper::outputHistogram(int *histogram, int scale
		, const char *title, bool truncate)
{
	const int maxRange(truncate ? 255 : 256);
	int maxHistogram(0);

	for ( int i = 0; i < maxRange; ++i )
	{
		if ( maxHistogram < histogram[i] )
			maxHistogram = histogram[i];
	}

	const int columns(70);
	double divisor = (double)columns / (double)maxHistogram;

	Debug(8, "Show %s Histogram\n", title);

	int counter(0);

	for ( int i = 0; i < 256; ++i )
	{
		char stars[256];

		counter += histogram[i];
		stars[0] = 0;

		if ( histogram[i] > 0 )
		{
			int j;

			for ( j = 0 ; j <= (int)(histogram[i] * divisor) && j <= columns ; ++j )
				stars[j] = '*';

			stars[j] = 0;
		}

		Debug(8, "%3i - %6i - %.5f %s\n", i*scale, histogram[i]
				, (double)counter / (double)framesprocessed, stars);
	}
}

int CommercialSkipper::findBlackThreshold(double percentile)
{
	FILE *raw((flagFiles.training.use) ? fopen("black.csv", "a+") : 0);
	int totalframes(0);

	for ( int i = 0; i < 256; ++i )
		totalframes += _histogram.bright[i];

	if ( raw )
	{
		::fprintf(raw, "\"%s\"", _basename.c_str());

		for ( int i = 0; i < 35; ++i )
			::fprintf(raw
				, ",%6.2f"
				, (1000.0*(double)_histogram.bright[i])/totalframes);

		::fprintf(raw, "\n");
		::fclose(raw);
	}

	const int targetCount((int)(totalframes * percentile));
	int i(-1);
	int tempCount(0);

	do
	{
		tempCount += _histogram.bright[++i];
	} while ( tempCount < targetCount );

	return i;
}

int CommercialSkipper::findUniformThreshold(double percentile)
{
	FILE *raw((flagFiles.training.use) ? fopen("uniform.csv", "a+") : 0);
	int totalframes(0);

	for ( int i = 0; i < 256; ++i )
		totalframes += _histogram.uniform[i];

	if ( raw )
	{
		::fprintf(raw, "\"%s\"", _basename.c_str());

		for ( int i = 0; i < 35; ++i )
			::fprintf(raw, ",%6.2f"
				, (1000.0*(double)_histogram.uniform[i])/totalframes);

		::fprintf(raw, "\n");
		::fclose(raw);
	}

	const int targetCount((int)(totalframes * percentile));
	int i(-1);
	int tempCount(0);

	do
	{
		tempCount += _histogram.uniform[++i];
	} while ( tempCount < targetCount );

	if ( i == 0 )
		i = 1;

#if 0
	while ( _histogram.uniform[i+1] < _histogram.uniform[i] )
		++i;
#endif

	return (i+1) * UNIFORMSCALE;
}

void CommercialSkipper::outputFrame(int frame_number)
{
	Debug(5, "Sending frame to file\n");

	char array[MAX_PATH];

	::sprintf(array
			, "%.*s%i.frm"
			, (int)(_logFilename.length() - 4)
			, _logFilename.c_str()
			, frame_number);

	FlagFile ff;
	ff.open(array, "w");

	if ( ! ff.f )
	{
		Debug(1, "Could not open frame output file.\n");
		return;
	}

	ff.printf("0;");

	for ( int x = 0; x < _videoWidth; ++x )
		ff.printf(";%3i", x);

	ff.printf("\n");

	for ( int y = 0; y < _height; ++y )
	{
		ff.printf("%3i", y);

		for ( int x = 0; x < _videoWidth; ++x )
		{
			const int offset(y * _width + x);

			if ( _videoFramePointer[offset] < 30 )
				ff.printf(";   ");
			else
				ff.printf(";%3i", _videoFramePointer[offset]);
		}

		ff.printf("\n");
	}
}

void CommercialSkipper::outputAspect(void)
{
	if ( flagFiles.aspect.use )
	{
		char array[MAX_PATH];

		sprintf(array, "%.*s.aspects"
			, (int)_logFilename.length() - 4
			, _logFilename.c_str());

		FlagFile ff;
		ff.open(array, "w");

		if ( ! ff.f )
		{
			Debug(1, "Could not open aspect output file.\n");
			return;
		}

		// Print out ar cblock list
		for ( int i = 0; i < ar_block_count; ++i )
		{
			ff.printf("%s %4dx%4d %.2f minX=%4d, minY=%4d, maxX=%4d, maxY=%4d\n"
					, secondsToString(F2T(ar_block[i].start)).c_str()
					, ar_block[i].width
					, ar_block[i].height
					, ar_block[i].ar_ratio
					, ar_block[i].range.x.min
					, ar_block[i].range.y.min
					, ar_block[i].range.x.max
					, ar_block[i].range.y.max);
		}
	}
}

void CommercialSkipper::outputFrameArray(bool screenOnly)
{
	char array[MAX_PATH];
	sprintf(array
			, "%.*s.csv"
			, (int)(_logFilename.length() - 4)
			, _logFilename.c_str());

#if 0
	Debug(5, "Expanding logo blocks into frame array\n");

	for ( int i = 0; i < _logoBlockCount; ++i )
		for ( int j = _logoBlock[i].start; j <= _logoBlock[i].end; ++j )
			frame[j].logo_present = true;

	Debug(5, "Expanded logo blocks into frame array\n");
#endif

	FlagFile ff;

	ff.open(array, "w");

	if ( ! ff.f )
	{
		Debug(1, "Could not open raw output file.\n");
		return;
	}

#if 1
	ff.printf("frame,brightness,scene_change,logo,uniform,sound"
			",minY,MaxY,ar_ratio,goodEdge,isblack"
			",cutscene,minX,maxX,hasBright,dimCount,pts");
#else
	ff.printf("frame,brightness,scene_change,logo,uniform,sound"
			",minY,MaxY,ar_ratio,goodEdge,isblack,cutscene,%d"
			, (int)(fps*100));
#endif

#if 0
	for ( int k = 0; k < 32; ++k )
		ff.printf(",b%3i", k);
#endif

	ff.printf("\n");

	if ( screenOnly )
		Debug(1, "Frame\tBlack\tBrightness\tS_Change\tS_Change Perc\tLogo Present\t%i\n"
				, _frameCount);

	for ( int i = 1; i < _frameCount; ++i )
	{
		if ( screenOnly )
		{
			::printf("%i\t%i\t%i\t\tHistogram\n"
					, i
					, _frame[i].brightness
					, _frame[i].schange_percent);
		}
		else
		{
			const Frame &f(_frame[i]);

			ff.printf("%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%f"
					, i
					, f.brightness
					, f.schange_percent * 5
					, f.logo_present
					, f.uniform
					, f.volume
					, f.range.y.min
					, f.range.y.max
					, (int)((f.ar_ratio) * 100)
					, (int)(f.currentGoodEdge * 500)
					, f.isblack
					, (int)f.cutscenematch
					, f.range.x.min
					, f.range.x.max
					, f.hasBright
					, f.dimCount
					, f.pts);

#ifdef FRAME_WITH_HISTOGRAM
			for ( int k = 0; k < 32; ++k )
				ff.printf(",%i", _frame[i].histogram[k]);
#endif
			ff.printf("\n");
		}
	}
}

void CommercialSkipper::initializeBlockArray(int i)
{
	if ( BlockInfo::count >= BlockInfo::max )
	{
		Debug(0, "Panic, too many blocks\n");
		exit(102);
	}

	cblock[i].f_start = 0;
	cblock[i].f_end = 0;
	cblock[i].b_head = 0;
	cblock[i].b_tail = 0;
	cblock[i].bframe_count = 0;
	cblock[i].schange_count = 0;
	cblock[i].schange_rate = 0;
	cblock[i].length = 0;
	cblock[i].score = 1.0;
	cblock[i].combined_count = 0;
	cblock[i].ar_ratio = AR_UNDEF;
	cblock[i].brightness = 0;
	cblock[i].volume = 0;
	cblock[i].silence = 0;
	cblock[i].stdev = 0;
	cblock[i].cause = 0;
	cblock[i].less = 0;
	cblock[i].more = 0;
	cblock[i].uniform = 0;
}

void CommercialSkipper::outputCcBlock(int i)
{
	if ( i > 1 )
	{
		Debug(11, "%i\tStart - %6i\tEnd - %6i\tType - %s\n"
				, i - 2
				, _ccBlock[i - 2]._startFrame
				, _ccBlock[i - 2]._endFrame
				, CcTypeToStr(_ccBlock[i - 2]._type));
	}

	if ( i > 0 )
	{
		Debug(11, "%i\tStart - %6i\tEnd - %6i\tType - %s\n"
				, i - 1
				, _ccBlock[i - 1]._startFrame
				, _ccBlock[i - 1]._endFrame
				, CcTypeToStr(_ccBlock[i - 1]._type));
	}

	if ( i <= 0 )
	{
		Debug(11, "%i\tStart - %6i\tEnd - %6i\tType - %s\n"
				, i
				, _ccBlock[i]._startFrame
				, _ccBlock[i]._endFrame
				, CcTypeToStr(_ccBlock[i - 1]._type));
	}
}

/*
typedef struct
{
	int	frame;
	char	name[40];
	int		v_chip;
	int		duration;
	int		current;
} XdsBlockInfo;

XdsBlockInfo*			XDS_block = NULL;
int					XDS_block_count = 0;
int					max_XDS_block_count;
*/

void CommercialSkipper::initXdsBlock(void)
{
	if ( ! XDS_block )
	{
		max_XDS_block_count = 2048;
		XDS_block = (XdsBlockInfo*)::malloc((max_XDS_block_count + 1) * sizeof(XdsBlockInfo));

		if ( ! XDS_block )
		{
			Debug(0, "Could not allocate memory for XDS blocks\n");
			exit(22);
		}

		XDS_block_count = 0;
		XDS_block[XDS_block_count].frame = 0;
		XDS_block[XDS_block_count].name[0] = 0;
		XDS_block[XDS_block_count].v_chip = 0;
		XDS_block[XDS_block_count].duration = 0;
		XDS_block[XDS_block_count].position = 0;
		XDS_block[XDS_block_count].composite1 = 0;
		XDS_block[XDS_block_count].composite2 = 0;
	}
}

void CommercialSkipper::addXdsBlock(void)
{
	if ( XDS_block_count < max_XDS_block_count )
	{
		++XDS_block_count;
		XDS_block[XDS_block_count] = XDS_block[XDS_block_count-1];
		XDS_block[XDS_block_count].frame = framenum;
		_frame[framenum].xds = XDS_block_count;
	}
	else
		Debug(0, "Too much XDS data, discarded\n");
}

uint8_t XDSbuffer[40][100];
int lastXDS = 0;
int firstXDS = 1;
int startXDS = 1;
int baseXDS = 0;
//const char *ratingSystem[4] = { "MPAA", "TPG", "CE", "CF" };

#define MAXXDSBUFFER 1024
void CommercialSkipper::addXds(uint8_t hi, uint8_t lo)
{
     static uint8_t XDSbuf[MAXXDSBUFFER];
     static int c = 0;
     int i,j;
     int newXDS = 0;
     initXdsBlock();

     if ( startXDS )
	 {
          if ( (hi & 0x70) == 0 && hi != 0x8f )
		  {
               startXDS = 0;
               c = 0;
               baseXDS = hi & 0x0f;;
          }
		  else
               return;
     }
	 else
	 {
          if ((hi & 0x7f) == baseXDS + 1)
               return; // COntinueation code
          if ((hi & 0x70) == 0 && hi != 0x8f)
               return;
     }

     if ((hi & 0x01) == 0 && (hi & 0x70) == 0x00 && hi != 0x8f)
          return;

     if (hi == 0x86 && (lo == 0x02 || lo == 1))
          return;

     if ( c >= MAXXDSBUFFER - 4 )
	 {
          for ( i = 0; i < 256; ++i )
               XDSbuf[i]=0;

          c = 0;
          startXDS = 1;
          return;
     }

     XDSbuf[c++] = hi;
     XDSbuf[c++] = lo;

     if ( hi == 0x8f )
	 {
          startXDS = 1;
          j = 0;

          for ( i = 0; i < c; ++i )
               j += XDSbuf[i];

          if ( (j & 0x7f) != 0 )
		  {
               c = 0;
               return;
          }

          for ( i = 0; i < lastXDS; ++i )
		  {
               if ( XDSbuffer[i][0] == XDSbuf[0] && XDSbuffer[i][1] == XDSbuf[1] )
			   {
                    j = 0;

                    while ( j < c )
					{
                         if ( XDSbuffer[i][j] != XDSbuf[j] )
						 {
                              while ( j < 100 )
							  {
                                   XDSbuffer[i][j] = XDSbuf[j];
                                   ++j;
                              }

                              newXDS = 1;
                              break;
                         }
                         j++;
                    }
                    break;
               }
          }

          if ( i == lastXDS && ! firstXDS )
		  {
               j = 0;

               while ( j < 100 )
			   {
                    XDSbuffer[i][j] = XDSbuf[j];
                    ++j;
               }

               newXDS = 1;
               lastXDS++;
               i++;
          }

          firstXDS = 0;

          if ( newXDS )
		  {
               Debug(10, "XDS[%i]: %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x "
					   , framenum, XDSbuf[0], XDSbuf[1], XDSbuf[2]
					   , XDSbuf[3], XDSbuf[4], XDSbuf[5], XDSbuf[6]
					   , XDSbuf[7], XDSbuf[8], XDSbuf[9], XDSbuf[10]
					   , XDSbuf[11]);

               XDSbuf[c-2] = 0;

               for ( i = 2; i < c - 2; ++i )
                    XDSbuf[i] &= 0x7f;

               if ( XDSbuf[0] == 1 )
			   {
                    if ( XDSbuf[1] == 0x01 )
					{
                         Debug(10, "XDS[%i]: Program Start Time %02d:%02d %d/%d\n", framenum, XDSbuf[3] & 0x3f, XDSbuf[2] & 0x3f ,  XDSbuf[5] & 0x1f,  XDSbuf[4] & 0x0f);
                    }
					else if ( XDSbuf[1] == 0x02 )
					{
//					Debug(10, "XDS[%i]: Program Length\n", XDSbuf[2] & 0x38, XDSbuf[2] & 0x4f ,  XDSbuf[3] & 0x4f,  XDSbuf[3] & 0xb0);
                         Debug(10, "XDS[%i]: Program length %d:%d, elapsed %d:%d:%d\n"
								, framenum
								, XDSbuf[3] & 0x3f
								, XDSbuf[2] & 0x3f
								, XDSbuf[5] & 0x3f
								, XDSbuf[4] & 0x3f
								, XDSbuf[6] & 0x3f);
                         if ( (XDSbuf[2] << 8) + XDSbuf[3] != XDS_block[XDS_block_count].duration)
						 {
                              addXdsBlock();
                              XDS_block[XDS_block_count].duration = (XDSbuf[3] << 8) + XDSbuf[2];
                         }

                         if ( (XDSbuf[4] << 8) + XDSbuf[5] != XDS_block[XDS_block_count].position)
						 {
                              addXdsBlock();
                              XDS_block[XDS_block_count].position = (XDSbuf[5] << 8) + XDSbuf[4];
                         }

                         /*
                         01155         uint length_min  = xds_buf[2] & 0x3f;
                         01156         uint length_hour = xds_buf[3] & 0x3f;
                         01157         uint length_elapsed_min  = 0;
                         01158         uint length_elapsed_hour = 0;
                         01159         uint length_elapsed_secs = 0;
                         01160         if (xds_buf.size() > 6)
                         01161         {
                         01162             length_elapsed_min  = xds_buf[4] & 0x3f;
                         01163             length_elapsed_hour = xds_buf[5] & 0x3f;
                         01164         }
                         01165         if (xds_buf.size() > 8 && xds_buf[7] == 0x40)
                         01166             length_elapsed_secs = xds_buf[6] & 0x3f;
                         01167
                         01168         QString msg = QString("Program Length %1:%2%3 "
                         01169                               "Time in Show %4:%5%6.%7%8")
                         01170             .arg(length_hour).arg(length_min / 10).arg(length_min % 10)
                         01171             .arg(length_elapsed_hour)
                         01172             .arg(length_elapsed_min / 10).arg(length_elapsed_min % 10)
                         01173             .arg(length_elapsed_secs / 10).arg(length_elapsed_secs % 10);
                         01174
                         */

                    }
					else if ( XDSbuf[1] == 0x83 )
					{
                         if ( ::strcmp((const char*) XDS_block[XDS_block_count].name
								, (const char*)&XDSbuf[2]) )
						 {
                              addXdsBlock();
                              ::strcpy(XDS_block[XDS_block_count].name, (const char*) &XDSbuf[2]);
                         }

                         Debug(10, "XDS[%i]: Program Name: %s\n", framenum, &XDSbuf[2]);
						// XDS_block[XDS_block_count].name[0] = 0;
                    }
					else if ( XDSbuf[1] == 0x04 )
					{
                         Debug(10, "XDS[%i]: Program Type: %0x\n", framenum, XDSbuf[2]);
                    }
					else if ( XDSbuf[1] == 0x85 )
					{
                         Debug(10, "XDS[%i]: V-Chip: %2x %2x %2x %2x\n"
								, framenum
								, XDSbuf[2] & 0x38
								, XDSbuf[2] & 0x4f
								, XDSbuf[3] & 0x4f
								, XDSbuf[3] & 0xb0);

                         if ( (XDSbuf[2] << 8) + XDSbuf[3] != XDS_block[XDS_block_count].v_chip)
						 {
                              addXdsBlock();
                              XDS_block[XDS_block_count].v_chip = (XDSbuf[2] << 8) + XDSbuf[3];
                         }

//							XDS_block[XDS_block_count].v_chip = 0;

                    }
					else if ( XDSbuf[1] == 0x86 )
					{
                         Debug(10, "XDS[%i]: Audio Streams \n", framenum);
                    }
					else if ( XDSbuf[1] == 0x07 )
					{
                         Debug(10, "XDS[%i]: Caption Stream\n", framenum);
                    }
					else if ( XDSbuf[1] == 0x08 )
					{
                         Debug(10, "XDS[%i]: Copy Management\n", framenum);
                    }
					else if ( XDSbuf[1] == 0x89 )
					{
                         Debug(10, "XDS[%i]: Aspect Ratio\n", framenum);
                    }
					else if ( XDSbuf[1] == 0x8c )
					{
                         Debug(10, "XDS[%i]: Program Data, Name: %s\n", framenum, &XDSbuf[2]);
                    }
					else if ( XDSbuf[1] == 0x0d )
					{
                         Debug(10, "XDS[%i]: Miscellaneous Data: %s\n", framenum, &XDSbuf[2]);
                    }
					else if ( XDSbuf[1] == 0x010 )
					{
                         Debug(10, "XDS[%i]: Program Description: %s\n", framenum, &XDSbuf[2]);
                    }
					else
                         Debug(10, "XDS[%i]: Unknown\n", framenum);

               }
			   else if ( XDSbuf[0] == 0x85 )
			   {
                    if ( XDSbuf[1] == 0x01 )
					{
                         Debug(10, "XDS[%i]: Network Name: %s\n", framenum, &XDSbuf[2]);
                    }
					else if ( XDSbuf[1] == 0x02 )
					{
                         Debug(10, "XDS[%i]: Network Call Name: %s\n", framenum, &XDSbuf[2]);
                    }
					else
                         Debug(10, "XDS[%i]: Unknown\n", framenum);
               }
			   else if ( XDSbuf[0] == 0x0d )
			   {
                    Debug(10, "XDS[%i]: Private Data: %s\n", framenum, &XDSbuf[2]);
               }
			   else
			   {
//dumpname:
                    for ( i = 0; i < 256; ++i )
					{
                         XDSbuf[i] &= 0x7f;

                         if ( XDSbuf[i] < 0x20 )
                              XDSbuf[i] = ' ';
                         else if ( XDSbuf[i] == 0x20 )
                              XDSbuf[i] = '_';
                    }

                    Debug(10, "XDS[%i]: %s\n", framenum, XDSbuf);
               }
          }

          for ( i = 0; i < 256; ++i )
               XDSbuf[i]=0;

          c = 0;
     }
}

void CommercialSkipper::addCc(int i)
{
	_cc._b[0] &= 0x7f;
	_cc._b[1] &= 0x7f;

	if ( _cc._b[0] == 0 && _cc._b[1] == 0 )
		return;

	int current_frame(framenum);

	++current_frame;

	/*
	if ( (_cc._b[0] != 0x14 && _cc._b[0] < 0x20) )
	{
		_cc._b[0] = ' ';
		_cc._b[1] = 0;
	}
	*/

	int hi(_cc._b[0]);
	int lo(_cc._b[1]);

	/*
	if ( hi == ' ' && lo == 'B' )
		hi = hi;
	*/


	if ( hi >= 0x18 && hi <= 0x1f )
		hi = hi - 8;

	switch ( hi )
	{
	case 0x10:
		/*
		if ( lo >= 0x40 && lo <= 0x5f )
			handle_pac (hi,lo,wb);
		*/
		break;

	case 0x11:
		if ( lo >= 0x20 && lo <= 0x2f )
		{
			_cc._b[0] = 0x20;
			_cc._b[1] = 0x00;
		}
		// handle_text_attr (hi,lo,wb);

		if ( lo >= 0x30 && lo <= 0x3f )
		{
			_cc._b[0] = 0x20;
			_cc._b[1] = 0x00;
			// wrote_to_screen=1;
			// handle_double (hi,lo,wb);
		}

		if ( lo >= 0x40 && lo <= 0x7f )
		{
			// handle_pac (hi,lo,wb);
			_cc._b[0] = 0x20;
			_cc._b[1] = 0x00;
		}
		break;

	case 0x12:
	case 0x13:
		if ( lo >= 0x20 && lo <= 0x3f )
		{
			_cc._b[0] = 0x20;
			_cc._b[1] = 0x00;
#if 0
			handle_extended (hi, lo, wb);
			wrote_to_screen = 1;
#endif
		}
#if 0
		if ( lo >= 0x40 && lo <= 0x7f)
			handle_pac (hi, lo, wb);
#endif
		break;
     case 0x14:
     case 0x15:
#if 0
		if ( lo >= 0x20 && lo <= 0x2f )
			handle_command (hi, lo, wb);
		if ( lo >= 0x40 && lo <= 0x7f )
			handle_pac (hi,lo,wb);
#endif
          break;
     case 0x16:
//        if (lo>=0x40 && lo<=0x7f)
//          handle_pac (hi,lo,wb);
          break;
     case 0x17:
//        if (lo>=0x21 && lo<=0x22)
//           handle_command (hi,lo,wb);
//        if (lo>=0x2e && lo<=0x2f)
//            handle_text_attr (hi,lo,wb);
//        if (lo>=0x40 && lo<=0x7f)
//            handle_pac (hi,lo,wb);
          break;
     }

	if ( (_cc._b[0] >= 0x20) && (_cc._b[0] < 0x80) )
	{
		if ( (_currentCcType == CC_ROLLUP) || (_currentCcType == CC_PAINTON) )
			_isCcOnScreen = true;
		else if ( _currentCcType == CC_POPON )
			_isCcInMemory = true;

		static const char charmap[0x60] =
		{
			' ', '!', '"', '#', '$', '%', '&', '\'',
			'(', ')', '�', '+', ',', '-', '.', '/',
			'0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', ':', ';', '<', '=', '>', '?',
			'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
			'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
			'X', 'Y', 'Z', '[', '�', ']', '�', '�',
			'�', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
			'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
			'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', '�', '�', 'N', 'n', '?'
		};

		Debug(11, "%i:%zu) %i:'%c':%x\t"
				, _ccTextCount
				, _ccText[_ccTextCount]._text.length()
				, i
				, charmap[_cc._b[0] - 0x20]
				, _cc._b[0]);

		_ccText[_ccTextCount]._text += charmap[_cc._b[0] - 0x20];

		if ( (_cc._b[1] >= 0x20) && (_cc._b[1] < 0x80) )
		{
			Debug(11, "%i:%zu) %i:'%c':%x\t"
					, _ccTextCount
					, _ccText[_ccTextCount]._text.length()
					, i
					, charmap[_cc._b[1] - 0x20]
					, _cc._b[1]);

			_ccText[_ccTextCount]._text += charmap[_cc._b[1] - 0x20];

			if ( (_lastCcType == CC_ROLLUP) || (_lastCcType == CC_PAINTON) )
				_isCcOnScreen = true;
			else if ( _lastCcType == CC_POPON )
				_isCcInMemory = true;
		}
	}

     if ( (( ! isalpha(_ccText[_ccTextCount]._text[_ccText[_ccTextCount]._text.length() - 1]))
			&& (_ccText[_ccTextCount]._text.length() > 200))
					 || (_ccText[_ccTextCount]._text.length() > 245) )
	 {
          _ccText[_ccTextCount]._endFrame = current_frame - 1;
          initCcText(++_ccTextCount);
          _ccText[_ccTextCount]._startFrame = current_frame;
          _ccText[_ccTextCount]._text = "";
     }

     if ( _cc._b[0] == 0x14 )
	 {
          if ( _cc == _lastCc )
		  {
               Debug(11, "Double code found\n");
               return;
          }

          switch ( _cc._b[1] )
		  {
          case 0x20:
               Debug(11, "Frame - %6i Control Code Found:\t"
						"Resume Caption Loading\n"
						, current_frame);
               _ccText[_ccTextCount]._endFrame = current_frame - 1;
               initCcText(++_ccTextCount);
               _ccText[_ccTextCount]._startFrame = current_frame;
               _ccText[_ccTextCount]._text = "";
               _lastCcType = CC_POPON;
               _currentCcType = CC_POPON;
               addNewCcBlock(current_frame, _currentCcType);
               break;

          case 0x21:
               // Debug(11, "Frame - %6i Control Code
               // Found:\tBackSpace\n", current_frame);
               break;

          case 0x22:
               // Debug(11, "Frame - %6i Control Code
               // Found:\tAlarm Off\n", current_frame);
               break;

          case 0x23:
               // Debug(11, "Frame - %6i Control Code
               // Found:\tAlarm On\n", current_frame);
               break;

          case 0x24:
               // Debug(11, "Frame - %6i Control Code
               // Found:\tDelete to end of row\n", current_frame);
               break;

          case 0x25:
               Debug(11, "Frame - %6i Control Code Found:\tRoll Up Captions 2 row\n"
							   , current_frame);
               _ccText[_ccTextCount]._endFrame = current_frame - 1;
               initCcText(++_ccTextCount);
               _ccText[_ccTextCount]._startFrame = current_frame;
               _ccText[_ccTextCount]._text = "";
               _lastCcType = CC_ROLLUP;
               _currentCcType = CC_ROLLUP;
               addNewCcBlock(current_frame, _currentCcType);
               break;

          case 0x26:
               Debug(11, "Frame - %6i Control Code Found:\tRoll Up Captions 3 row\n"
							   , current_frame);
               _ccText[_ccTextCount]._endFrame = current_frame - 1;
               initCcText(++_ccTextCount);
               _ccText[_ccTextCount]._startFrame = current_frame;
               _ccText[_ccTextCount]._text = "";
               _lastCcType = CC_ROLLUP;
               _currentCcType = CC_ROLLUP;
               addNewCcBlock(current_frame, _currentCcType);
               break;

          case 0x27:
               Debug(11, "Frame - %6i Control Code Found:\tRoll Up Captions 4 row\n"
							   , current_frame);
               _ccText[_ccTextCount]._endFrame = current_frame - 1;
               initCcText(++_ccTextCount);
               _ccText[_ccTextCount]._startFrame = current_frame;
               _ccText[_ccTextCount]._text = "";
               _lastCcType = CC_ROLLUP;
               _currentCcType = CC_ROLLUP;
               addNewCcBlock(current_frame, _currentCcType);
               break;

          case 0x28:
               // Debug(11, "Frame - %6i Control Code
               // Found:\tFlash On\n", current_frame);
               break;

          case 0x29:
               Debug(11, "Frame - %6i Control Code Found:\tResume Direct Captioning\n"
							   , current_frame);
               _ccText[_ccTextCount]._endFrame = current_frame - 1;
               initCcText(++_ccTextCount);
               _ccText[_ccTextCount]._startFrame = current_frame;
               _ccText[_ccTextCount]._text = "";
               _lastCcType = CC_PAINTON;
               _currentCcType = CC_PAINTON;
               addNewCcBlock(current_frame, _currentCcType);
               break;

          case 0x2A:
               // Debug(11, "Frame - %6i Control Code
               // Found:\tText Restart\n", current_frame);
               break;

          case 0x2B:
               // Debug(11, "Frame - %6i Control Code
               // Found:\tResume Text Display\n", current_frame);
               break;

          case 0x2C:
               Debug(11, "Frame - %6i Control Code Found:\t"
							   "Erase Displayed Memory\n"
							   , current_frame);
               _ccText[_ccTextCount]._endFrame = current_frame - 1;
               initCcText(++_ccTextCount);
               _ccText[_ccTextCount]._startFrame = current_frame;
               _ccText[_ccTextCount]._text = "";
               _isCcOnScreen = false;
               _currentCcType = CC_NONE;
               addNewCcBlock(current_frame, _currentCcType);
               break;

          case 0x2D:
               _ccText[_ccTextCount]._text += ' ';
               Debug(11, "\n");
               break;

          case 0x2E:
               Debug(11, "Frame - %6i Control Code Found:\tErase Non-Displayed Memory\n"
						, current_frame);
               _isCcInMemory = false;
               break;

          case 0x2F:
               Debug(11, "Frame - %6i Control Code Found:\tEnd of Caption\t"
						"On Screen - %i\tOff Screen - %i\n"
						, current_frame, _isCcInMemory, _isCcOnScreen);

               _ccText[_ccTextCount]._endFrame = current_frame - 1;
               initCcText(++_ccTextCount);
               _ccText[_ccTextCount]._startFrame = current_frame;
               _ccText[_ccTextCount]._text = "";

			   {
				bool b(_isCcInMemory);
               _isCcInMemory = _isCcOnScreen;
               _isCcOnScreen = b;
			   }

               if ( ! _isCcOnScreen )
			   {
                    _currentCcType = CC_NONE;
               }
			   else
			   {
                    if ( (_ccBlockCount > 0)
							&& (_ccBlock[_ccBlockCount]._type == CC_NONE) )
                         _currentCcType = _lastCcType;
               }

               addNewCcBlock(current_frame, _currentCcType);
               break;

          default:
               Debug(11, "\nFrame - %6i Control Code Found:\tUnknown code!! - %2X\n"
							   , current_frame, _cc._b[1]);

               _ccText[_ccTextCount]._text += ' ';
               break;
          }
     }

     _lastCc = _cc;
}

#ifdef PROCESS_CC

void CommercialSkipper::processCcData(void)
{
	static uint8_t prevccData[500];
	static int prevccDataLen;

     int i;
     int proceed = 0;
     int is_CC = 0;
     int is_dish = 0;
     int is_GA = 0;
     int cctype = 0;
     int offset;
     uint8_t temp[2000];
     char hex[10];
     uint8_t t;
     uint8_t *p;
     bool cc1First = 0;
     uint8_t packetCount;

	if ( ! initialized )
		return;

     if ( _verbose >= 12 )
	 {
          p = temp;

          for ( i = 0; i < ccDataLen; ++i )
		  {
               t = ccData[i] & 0x7f;

               if (t == 0x20)
                    *p++ = '_';
               else if (t > 0x20 && t < 0x7f)
                    *p++ = t;
               else
                    *p++ = ' ';
               *p++ = ' ';
               *p++ = ' ';
          }

          *p++ = 0;

          if ( ccData[0] == 'G' )
               temp[7*3] = '0' + (temp[7*3] & 0x03);

          Debug(10, "CCData for framenum %4i%c, length:%4i: %s\n", framenum, cs_field_t, ccDataLen, temp);

          p = temp;

          for ( i = 0; i < ccDataLen; ++i )
		  {
               sprintf(hex, "%2x ",ccData[i]);
               *p++ = hex[0];
               *p++ = hex[1];
               *p++ = ' ';
          }

          *p++ = 0;

          Debug(10, "CCData for framenum %4i%c, length:%4i: %s\n", framenum, cs_field_t, ccDataLen, temp);

     }

     if ((char)ccData[0] == 'C' && (char)ccData[1] == 'C' && ccData[2] == 0x01 && ccData[3] == 0xf8)
	 {
          reorderCC = false;
          packetCount = ccData[4];
          cc1First = ( packetCount & 0x80 );
          offset = 5;
          packetCount = (packetCount & 0x1E) / 2;

          if ( (! cc1First) || (packetCount != 15) )
		  {
               Debug(11, "CC Field Order: %i.  There appear to be %i packets.\n", cc1First, packetCount);
          }

          proceed = 1;
          is_CC = 1;
     } else if ((char)ccData[0] == 'G' && (char)ccData[1] == 'A' && ccData[2] == '9' && ccData[3] == '4'&& ccData[4] == 0x03) {
          reorderCC = true;
          packetCount = ccData[5] & 0x1F;
          proceed = ( ccData[5] & 0x40) >> 6;
          offset = 7;
          is_GA = 1;
     } else if (ccData[0] == 0x05 && ccData[1] == 0x02) {
          reorderCC = false;
          proceed = 0;
          offset = 7;
          cctype = ccData[offset++];

          if ( cctype == 2 )
		  {
               ++offset;

               if ( ccData[offset] & 0x7f )
                    ccDataLen = ccDataLen;

               _cc._b[0] = ccData[offset++];
               _cc._b[1] = ccData[offset++];
               addCc(1);
               cctype = ccData[offset++];

               if ( cctype == 4 && ( ccData[offset] & 0x7f) < 32 )
			   {
                    if ( ccData[offset] & 0x7f )
                         ccDataLen = ccDataLen;

                    _cc._b[0] = ccData[offset++];
                    _cc._b[1] = ccData[offset++];
                    addCc(1);
               }

               offset += 3;
          }
		  else if ( cctype == 4 )
		  {
               ++offset;

               if ( ccData[offset] & 0x7f )
                    ccDataLen = ccDataLen;

               _cc._b[0] = ccData[offset++];
               _cc._b[1] = ccData[offset++];
               addCc(1);

               if ( ccData[offset] & 0x7f )
                    ccDataLen = ccDataLen;

               _cc._b[0] = ccData[offset++];
               _cc._b[1] = ccData[offset++];
               addCc(1);
               offset += 3;
          }
		  else if ( cctype == 5 )
		  {
               for ( i = 0; i < prevccDataLen; i +=2 )
			   {
                    if ( prevccData[i] & 0x7f )
                         prevccDataLen = prevccDataLen;
                    _cc._b[0] = prevccData[i];
                    _cc._b[1] = prevccData[i+1];
                    addCc(i/2);
               }

               prevccDataLen = 0;
//			offset += 6;
               cctype = ccData[offset++] & 0x7f;
               cctype = ccData[offset++] & 0x7f;

               if ( ccData[offset] & 0x7f )
                    ccDataLen = ccDataLen;

               cctype = ccData[offset++] & 0x7f;
               cctype = ccData[offset++] & 0x7f;

               if ( ccData[offset] & 0x7f )
                    ccDataLen = ccDataLen;

               cctype = ccData[offset++] & 0x7f;
               cctype = ccData[offset++] & 0x7f;
//
               cctype = ccData[offset++];
               ++offset;
               prevccDataLen = 0;

               if ( ccData[offset] & 0x7f )
                    ccDataLen = ccDataLen;

               prevccData[prevccDataLen++] = ccData[offset++];
               prevccData[prevccDataLen++] = ccData[offset++];

               if ( cctype == 2 )
			   {
                    cctype = ccData[offset++];

                    if ( cctype == 4 && ( ccData[offset] & 0x7f) < 32 )
					{
                         if ( ccData[offset] & 0x7f )
                              ccDataLen = ccDataLen;

                         prevccData[prevccDataLen++] = ccData[offset++];
                         prevccData[prevccDataLen++] = ccData[offset++];
                    }
               }
			   else
			   {
                    if ( ccData[offset] & 0x7f )
                         ccDataLen = ccDataLen;

                    prevccData[prevccDataLen++] = ccData[offset++];
                    prevccData[prevccDataLen++] = ccData[offset++];
               }

               offset += 3;
          }

          packetCount = cctype / 2;
          is_dish = 1;
     }

     if ( proceed )
	 {
          for ( i = 0; i < packetCount; ++i )
		  {
               if ( is_CC )
			   {
                    if ( cc1First )
					{
                         _cc._b[0] = CheckOddParity(ccData[(i * 6) + offset + 1]) ? ccData[(i * 6) + offset + 1] & 0x7f : 0x00;
                         _cc._b[1] = CheckOddParity(ccData[(i * 6) + offset + 2]) ? ccData[(i * 6) + offset + 2] & 0x7f : 0x00;
                    } else {
                         _cc._b[0] = CheckOddParity(ccData[(i * 6) + offset + 4]) ? ccData[(i * 6) + offset + 4] & 0x7f : 0x00;
                         _cc._b[1] = CheckOddParity(ccData[(i * 6) + offset + 5]) ? ccData[(i * 6) + offset + 5] & 0x7f : 0x00;
                    }

                    addCc(i);
               }

               if ( is_GA )
			   {
                    if (!(ccData[(i * 3) + offset] & 4) >>2 )
                         continue;

                    if (ccData[(i * 3) + offset] == 0xfa)
                         continue;

                    if (ccData[(i * 3) + offset + 1]  == 0x80
									&& ccData[(i * 3) + offset + 2] == 0x80)
                         continue;

                    if (ccData[(i * 3) + offset + 1]  == 0x00
									&& ccData[(i * 3) + offset + 2] == 0x00)
                         continue;

                    cctype = (ccData[(i * 3) + offset] & 3);
//				_cc._b[0] = CheckOddParity(ccData[(i * 3) + offset + 1]) ? ccData[(i * 3) + offset + 1] & 0x7f : 0x00;
//				_cc._b[1] = CheckOddParity(ccData[(i * 3) + offset + 2]) ? ccData[(i * 3) + offset + 2] & 0x7f : 0x00;
                    _cc._b[0] = ccData[(i * 3) + offset + 1] & 0x7f;
                    _cc._b[1] = ccData[(i * 3) + offset + 2] & 0x7f;

#if 0
                    if ( cctype == 0 )
                         cctype = cctype;
#endif

                    if ( cctype == 1 )
                         addXds(ccData[(i * 3) + offset + 1], ccData[(i * 3) + offset + 2]);

#if 0
                    if ( cctype == 2 )
                         cctype = cctype;
                    if ( cctype == 3 )
                         cctype = cctype;
#endif

                    if ( cctype != 0 && cctype != 1 )
                         continue;

                    if ( cctype == 0 /* || cctype == 1 */ )
					{
						// _cc._b[0] = ccData[(i * 3) + offset + 1] & 0x7f;
						// _cc._b[1] = ccData[(i * 3) + offset + 2] & 0x7f;
                         addCc(i);

                    }
					else
						_cc.reset();
               }
               /*
               			if (is_dish) {

               				if (cctype == 2 || cctype == 4) {
               					_cc._b[0] = ccData[(i * 3) + offset + 1] & 0x7f;
               					_cc._b[1] = ccData[(i * 3) + offset + 2] & 0x7f;
               					offset = offset - 1;
               					addCc(i);

               				} else
               					continue;

               			}
               */
          }
     }
}

#endif // PROCESS_CC

bool CheckOddParity(uint8_t ch)
{
	uint8_t test(1);
	int count(0);

	for ( int i = 1; i <= 8; ++i )
	{
		if ( ch & test )
			++count;

		test *= 2;
	}

	return count % 2;
}

void CommercialSkipper::addNewCcBlock(int current_frame, int type)
{
	if ( _ccBlock[_ccBlockCount]._type == type )
	{
		_ccBlock[_ccBlockCount]._endFrame = current_frame;
	}
	else
	{
		Debug(11, "\nFrame - %6i\t%s captions start\n"
				, current_frame, CcTypeToStr(type));

		if ( _ccBlock[_ccBlockCount]._endFrame == -1 )
		{
			Debug(11, "New cblock found\n");
			_ccBlock[_ccBlockCount]._endFrame = current_frame - 1;
			initCcBlock(++_ccBlockCount);
			_ccBlock[_ccBlockCount]._startFrame = current_frame;
			_ccBlock[_ccBlockCount]._type = type;

			if ( _ccBlockCount > 1 )
			{
				if ( (F2L(_ccBlock[_ccBlockCount - 1]._endFrame
						, _ccBlock[_ccBlockCount - 1]._startFrame) < 1.0 )
						&& (_ccBlock[_ccBlockCount]._type
							== _ccBlock[_ccBlockCount - 2]._type)
						&& (_ccBlock[_ccBlockCount]._type != CC_NONE) )
				{
					_ccBlockCount -= 2;
					_ccBlock[_ccBlockCount]._endFrame = -1;
				}
			}
		}
		else
		{
			initCcBlock(++_ccBlockCount);
			_ccBlock[_ccBlockCount]._startFrame = current_frame;
			_ccBlock[_ccBlockCount]._type = type;
		}

		outputCcBlock(_ccBlockCount);
	}
}

const char *CcTypeToStr(int type)
{
	static char buf[8];

	if ( doProcessCc )
	{
		switch ( type )
		{
		case CC_NONE:
			return "NONE";
		case CC_ROLLUP:
			return "ROLLUP";
		case CC_PAINTON:
			return "PAINTON";
		case CC_POPON:
			return "POPON";
		case CC_COMMERCIAL:
			return "COMMERCIAL";
		default:
			::sprintf(buf, "%d", type);
			return buf;
		}
	}

	return "";
}

int CommercialSkipper::determineCcTypeForBlock(int start, int end)
{
	int ccBlockFirst(_ccBlockCount);

	while ( _ccBlock[ccBlockFirst]._startFrame > start )
		--ccBlockFirst;

	int ccBlockLast(0);

	while ( _ccBlock[ccBlockLast]._endFrame < end )
		++ccBlockLast;

	int type(CC_NONE);

	// Look for the CC_PAINTON then CC_POPON pattern that is common in commercials
	for ( int i = ccBlockFirst; i <= ccBlockLast; ++i )
	{
		if ( _ccBlock[i]._type != CC_NONE )
		{
			if ( i > 0 )
			{
				if ( (_ccBlock[i - 1]._type == CC_PAINTON)
						&& (_ccBlock[i]._type == CC_POPON) )
				{
					type = CC_COMMERCIAL;
					break;
				}
			}

			if ( i > 1 )
			{
				if ( (_ccBlock[i - 2]._type == CC_PAINTON)
						&& (_ccBlock[i - 1]._type == CC_NONE)
						&& (F2L(_ccBlock[i - 1]._endFrame, _ccBlock[i - 1]._startFrame)
								<= 1.5)
						&& (_ccBlock[i]._type == CC_POPON) )
				{
					type = CC_COMMERCIAL;
					break;
				}
			}
		}
	}

	/*
	 * If no commercial pattern found,
	 * find the most common type of CC
	 */
	if ( type != CC_COMMERCIAL )
	{
		int ccTypeCount[5] = { 0, 0, 0, 0, 0 };

		for ( int i = start; i <= end; ++i )
		{
			for ( int j = 0; j < _ccBlockCount; ++j )
			{
				if ( (i > _ccBlock[j]._startFrame) && (i < _ccBlock[j]._endFrame) )
				{
					ccTypeCount[_ccBlock[j]._type]++;
					break;
				}
			}
		}

		type = CC_NONE;

		for ( int i = 0; i < 5; ++i )
			if ( ccTypeCount[i] > ccTypeCount[type] )
				type = i;
	}

	Debug(4, "Start - %6i\tEnd - %6i\tCCF - %2i\tCCL - %2i\tType - %s\n"
			, start, end, ccBlockFirst, ccBlockLast, CcTypeToStr(type));

	return type;
}

void SetARofBlocks(void)
{
	if ( (commDetectMethod & DET::AR) )
	{
		int k(0);

		for ( int i = 0; i < BlockInfo::count; ++i )
		{
			double sumAR(0.0);
			int frameCount(0); // To prevent divide by zero error

			for ( int j = cblock[i].f_start + cblock[i].b_head
						; j < cblock[i].f_end - (int) cblock[i].b_tail
						; ++j )
			{
				if ( k < ar_block_count && j >= ar_block[k].end )
					++k;

				if ( ar_block[k].ar_ratio > 1 )
				{
					sumAR += ar_block[k].ar_ratio;
					++frameCount;
				}
			}

			cblock[i].ar_ratio
					= ( frameCount == 0 )
						? 1.0
						: sumAR / (frameCount);
		}
	}
}

bool CommercialSkipper::processCcDict(void)
{
	FlagFile ff;

	ff.open(_dictFilename, "r");

	if ( ! ff.f )
		return false;

	Debug(2, "\n\nStarting to process dictionary\n-------------------------------------\n");

	char phrase[1024];
	bool goodPhrase(true);

	while ( fgets(phrase, sizeof(phrase), ff.f) )
	{
		char *ptr(strchr(phrase, '\n'));

		if ( ptr )
			*ptr = '\0';

		if ( ::strstr(phrase, "-----") )
		{
			goodPhrase = false;
			Debug(3, "Finished with good phrases.  Now starting bad phrases.\n");
			continue;
		}

		// just in case the line is empty
		if ( strlen(phrase) < 1 )
			continue;

		Debug(3, "Searching for: %s\n", phrase);

		for ( int i = 0; i < _ccTextCount; ++i )
		{
			const CcText &cc(_ccText[i]);

			if ( cc.contains(phrase) )
			{
				Debug(2, "%s found in cc_text_block %i\n", phrase, i);

				if ( goodPhrase )
				{
					int j(FindBlock((cc._startFrame + cc._endFrame) / 2));

					if ( j == -1 )
						Debug(1, "There was an error finding the correct cblock"
								" for cc text cblock %i.\n", i);
					else
					{
						Debug(3, "Block %i score:\tBefore - %.2f\t", j, cblock[j].score);
						cblock[j].score /= dictionary_modifier;
						Debug(3, "After - %.2f\n", cblock[j].score);
					}
				}
				else
				{
					int j(FindBlock((cc._startFrame + cc._endFrame) / 2));

					if ( j == -1 )
						Debug(1, "There was an error finding the correct cblock"
								" for cc text cblock %i.\n", i);
					else
					{
						Debug(3, "Block %i score:\tBefore - %.2f\t", j, cblock[j].score);
						cblock[j].score *= dictionary_modifier;
						Debug(3, "After - %.2f\n", cblock[j].score);
					}
				}
			}
		}
	}

	return true;
}

int FindBlock(int frame)
{
	for ( int i = 0; i < BlockInfo::count; ++i )
		if ( (frame >= cblock[i].f_start) && (frame < cblock[i].f_end) )
			return i;

	return -1;
}

void CommercialSkipper::buildCommListAsYouGo(void)
{
	if ( framenum_real - lastFrameCommCalculated <= 15 * fps )
		return;

	const int brightness_buffer(5);
	int local_blacklevel(min_brightness_found + brightness_buffer);

	if ( local_blacklevel < _maxAvgBrightness )
		local_blacklevel = _maxAvgBrightness;

	if ( ! csBlack.empty()
			&& (csBlack.back().brightness <= local_blacklevel)
			// && (csBlack.back().frame == framenum_real)
			&& (framenum_real > lastFrame) )
	{
		int c_start[MAX_COMMERCIALS];
		int c_end[MAX_COMMERCIALS];
		int ic_start[MAX_COMMERCIALS];
		int ic_end[MAX_COMMERCIALS];
		int commercials(0);
		int onTheFlyBlackCount(0);

		lastFrameCommCalculated = framenum_real;
		const size_t blackSize(csBlack.size());
		int *onTheFlyBlackFrame = new int[blackSize];
		memset(onTheFlyBlackFrame, 0, blackSize * sizeof(int));

		Debug(7, "Building list of all frames with a brightness less than %i.\n"
				, local_blacklevel);

		for(int i=0; i < csBlack.size(); i++)
		{
     	                BlackFrame &bf = csBlack[i];
			if ( bf.brightness <= local_blacklevel )
			{
				onTheFlyBlackFrame[onTheFlyBlackCount] = bf.frame;
				++onTheFlyBlackCount;
			}
		}

		bool useLogo(commDetectMethod & DET::LOGO);

		if ( (_logoBlockCount == -1) || ! _logoInfoAvailable )
			useLogo = false;

		// detect individual commercials from black frames
		for ( int i = 0; i < onTheFlyBlackCount; ++i )
		{
			int x;

			for ( x = i + 1; x < onTheFlyBlackCount; ++x )
			{
				int gap_length = onTheFlyBlackFrame[x] - onTheFlyBlackFrame[i];

				if ( gap_length < _commBreakSize._size.min * fps )
					continue;

				bool oldbreak = ( commercials > 0 )
						&& ((onTheFlyBlackFrame[i] - c_end[commercials - 1]) < 10 * fps);

				if ( gap_length > _commBreakSize._break.max * fps
						|| (!oldbreak
							&& gap_length > _commBreakSize._size.max * fps)
						|| (oldbreak
							&& (onTheFlyBlackFrame[x] - c_end[commercials - 1]
								> _commBreakSize._size.max * fps)) )
				{
					break;
				}

				double added((gap_length / fps) + div5_tolerance);
				double remainder(added - 5 * ((int)(added / 5.0)));

				if ( (require_div5 != 1)
						|| (remainder >= 0
							&& remainder <= 2 * div5_tolerance) )
				{
					// look for segments in multiples of 5 seconds
					if ( oldbreak )
					{
						if ( checkFramesForLogo(onTheFlyBlackFrame[x - 1], onTheFlyBlackFrame[x])
								&& useLogo )
						{
							c_end[commercials - 1] = onTheFlyBlackFrame[x - 1];
							ic_end[commercials - 1] = x - 1;
							Debug(10, "Logo detected between frames %i and %i."
										"  Setting commercial to %i to %i.\n"
								, onTheFlyBlackFrame[x - 1]
								, onTheFlyBlackFrame[x]
								, c_start[commercials - 1]
								, c_end[commercials - 1]);
						}
						else if ( onTheFlyBlackFrame[x] > c_end[commercials - 1] + fps )
						{
							c_end[commercials - 1] = onTheFlyBlackFrame[x];
							ic_end[commercials - 1] = x;
							Debug(5, "--start: %i, end: %i, len: %.2fs\t%.2fs\n"
								, onTheFlyBlackFrame[i]
								, onTheFlyBlackFrame[x]
								, (onTheFlyBlackFrame[x] - onTheFlyBlackFrame[i]) / fps
								, (c_end[commercials - 1] - c_start[commercials - 1]) / fps);
						}
					}
					else
					{
						if ( checkFramesForLogo(onTheFlyBlackFrame[i], onTheFlyBlackFrame[x]) && useLogo )
						{
							Debug(6, "Logo detected between frames %i and %i.  Skipping to next i.\n"
								, onTheFlyBlackFrame[i]
								, onTheFlyBlackFrame[x]);
							i = x - 1; /*Gil*/
							break;
						}
						else
						{
							Debug(1, "\n  start: %i, end: %i, len: %.2f\n"
									, onTheFlyBlackFrame[i]
									, onTheFlyBlackFrame[x]
									, ((onTheFlyBlackFrame[x] - onTheFlyBlackFrame[i]) / fps));
							ic_start[commercials] = i;
							ic_end[commercials] = x;
							c_start[commercials] = onTheFlyBlackFrame[i];
							c_end[commercials++] = onTheFlyBlackFrame[x];

							Debug(1, "\n  start: %i, end: %i, len: %i\n"
									, c_start[commercials - 1]
									, c_end[commercials - 1]
									, (int)((c_end[commercials - 1] - c_start[commercials - 1]) / fps));
						}
					}

					i = x - 1;
					x = onTheFlyBlackCount;
				}
			}
		}

		Debug(1, "\n");

		// print out commercial breaks
		// skipping those that are too small or too large
		if ( flagFiles._default.use
				|| flagFiles.edl.use
				|| flagFiles.dvrmstb.use )
		{
			if ( flagFiles._default.use )
			{
				FlagFile &ff(flagFiles.out);

				ff.use = true;

#ifndef _WIN32
				ff.open(_outFilename, "w", true);
#else
				ff.open(_outFilename, "w");

				if ( ! ff.f )
				{
					Sleep(50L);
					ff.open(_outFilename, "w", true);
				}
#endif

#if 0
				ff.printf(
						"FILE PROCESSING COMPLETE %6li FRAMES AT %4i\n"
						"-------------------\n"
						, frame_count-1, (int)(fps*100));
#endif
			}

			if ( flagFiles.edl.use )
			{
				std::string fn(_outBasename + ".edl");
				FlagFile &ff(flagFiles.edl);
#ifndef _WIN32
				ff.open(fn, "wb", true);
#else
				ff.open(fn, "wb");

				if ( ! ff.f )
				{
					Sleep(50L);
					ff.open(fn, "wb", true);
				}
#endif
			}

			if ( flagFiles.dvrmstb.use )
			{
				std::string fn(_outBasename + ".xml");
				FlagFile &ff(flagFiles.dvrmstb);
				ff.open(fn, "w", true);

				ff.printf(
					"<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"
					"<root>\n");
			}

			for ( int i = 0; i < commercials; ++i )
			{
				int len(c_end[i] - c_start[i]);

				if ((len >= (int)_commBreakSize._break.min * fps)
						&& (len <= (int)_commBreakSize._break.max * fps))
				{
					// find the middle of the scene change, max 3 seconds.
					int j(ic_start[i]);

					// find beginning
					while ( (j > 0) && ((onTheFlyBlackFrame[j] - onTheFlyBlackFrame[j - 1]) == 1))
						--j;

					int x;

					// find end
					{
						int k;

						for ( k = j; k < onTheFlyBlackCount; ++k )
							if ((onTheFlyBlackFrame[k] - onTheFlyBlackFrame[j]) > (int)(3 * fps))
								break;

						x = j + (int)((k - j) / 2);
					}

					c_start[i] = onTheFlyBlackFrame[x];
					j = ic_end[i];

					if ( j < onTheFlyBlackCount - 1 )
					{
						while ((j < onTheFlyBlackCount)
								&& ((onTheFlyBlackFrame[j + 1] - onTheFlyBlackFrame[j]) == 1))
						{
							// find end
							if ( ++j >= (onTheFlyBlackCount - 1) )
								break;
						}
					}

					{
						int k;

						// find start
						for ( k = j; k > 0; --k )
							if ( onTheFlyBlackFrame[j] - (onTheFlyBlackFrame[k]) > (int)(3 * fps) )
								break;

						x = k + (int)((j - k) / 2);
					}

					c_end[i] = onTheFlyBlackFrame[x] - 1;
					Debug(2, "Output: %i - start: %i   end: %i\n", i, c_start[i], c_end[i]);

					if ( ++commercial_count >= MAX_COMMERCIALS )
					{
						Debug(0, "Insufficient memory to manage live_tv commercials\n");
						exit(8);
					}

					commercial[commercial_count].start_frame
							= c_start[i] + padding*fps - remove_before*fps;
					commercial[commercial_count].end_frame
							= c_end[i] - padding*fps + remove_after*fps;
					commercial[commercial_count].length
							= c_end[i]-2*padding - c_start[i] + remove_before + remove_after;

					if ( flagFiles.out.use )
						flagFiles.out.printf("%i\t%i\n"
								, c_start[i] + padding, c_end[i] - padding);

					if ( flagFiles.edl.use )
						flagFiles.edl.printf("%.2f\t%.2f\t0\n"
							, (double) std::max(c_start[i] + padding - _edlOffset, 0) / fps
							, (double) std::max(c_end[i] - padding - _edlOffset, 0) / fps);

					if ( flagFiles.dvrmstb.use )
						flagFiles.dvrmstb.printf(
							"  <commercial start=\"%f\" end=\"%f\" />\n"
							, (double) (c_start[i] + padding) / fps
							, (double) (c_end[i] - padding) / fps);
				}
			}

			flagFiles.out.close();
			flagFiles.edl.close();

			if ( flagFiles.dvrmstb.f )
			{
				flagFiles.dvrmstb.printf(" </root>\n");
				flagFiles.dvrmstb.close();
			}

			if ( flagFiles.incommercial.use )
			{
				FlagFile &ff(flagFiles.incommercial);
				ff.open(_workBasename + ".incommercial", "w", true);

				ff.printf(
						( commercial[commercial_count].end_frame
							> framenum_real - incommercial_frames )
								? "1\n"
								: "0\n");

				ff.close();
			}
		}

		delete[] onTheFlyBlackFrame;
	}
}

double CommercialSkipper::getFps(void)
{
	return fps;
}

void CommercialSkipper::setFps(unsigned int fp)
{
	static int showed_fps(0);
	fps = (double)900000* 30 / (double)fp;

	if ( /* old_fps != fps && */ showed_fps++ < 4 )
		Debug(1, "Frame Rate set to %5.3f f/s\n", fps);
}

void CommercialSkipper::setFrameVolume(int f, int volume)
{
	if ( ! initialized )
		return;

	// ascr += 1;
	const int frameId(f);

	if ( frameId > 0 )
	{
		if ( _useFrameArray )
		{
			if ( frameId <= _frameCount )
			{
				if ( _frame[frameId].brightness > 5 )
					_frame[frameId].volume = volume;

				if ( volume > 0 )
					_histogram.volume[(volume/volumeScale < 255 ? volume/volumeScale : 255)]++;
			}
		}

		if ( frameId > _frameCount )
			_volumes.set(frameId, volume);

		{
			int i(csBlack.size() - 1);

			while ( i > 0 && csBlack[i].frame > frameId )
				--i;

			/*
			 * Set the zero below to 5 if you do not want the volume
			 * to be updated for uniform frames etc.
			 */
			if ( i >= 0 && csBlack[i].frame == frameId && csBlack[i].brightness > 0 )
				csBlack[i].volume = volume;
		}
	}

	// ++audio_framenum;
	// ascr += 1;
}

void CommercialSkipper::dumpAudio(const char *start, const char *end)
{
	if ( flagFiles.demux.use && ! flagFiles.dumpAudio.f )
	{
		FlagFile &ff(flagFiles.dumpAudio);
		ff.use = true;
		ff.open(_workBasename + ".mp2", "wb");
	}

	if ( flagFiles.dumpAudio.f )
		::fwrite(start, end-start, 1, flagFiles.dumpAudio.f);
}

void CommercialSkipper::dumpVideo(const char *start, const char *end)
{
	if ( flagFiles.demux.use && ! flagFiles.dumpVideo.f )
	{
		FlagFile &ff(flagFiles.dumpVideo);
		ff.use = true;
		ff.open(_workBasename + ".m2v", "wb");
	}

	if ( flagFiles.dumpVideo.f )
		fwrite(start, end-start, 1, flagFiles.dumpVideo.f);
}

int CommercialSkipper::inputReffer(const char *extension, bool setfps)
{
	int i;
	int j;
	int k;
	int pk;
	double fpos(0.0);
	double fneg(0.0);
	double total(0.0);
	double t = 0.0;
	enum {both_show,both_commercial, only_reffer, only_commercial} state;
	char line[2048];
	char split[256];
	char array[MAX_PATH];
	int x;
	int col;
	bool lineProcessed;
	char co, re;
	FILE *raw2(0);

	sprintf(array
			, "%.*s%s"
			, (int)_logFilename.length() - 4
			, _logFilename.c_str()
			, extension);

	FILE *raw(fopen(array, "r"));

	if ( ! raw )
		return 0;

	(void)fgets(line, sizeof(line), raw); // Read first line

	int frames((strlen(line) > 27) ? strtol(&line[25], NULL, 10) : 0);

	if ( setfps )
	{
		if ( strlen(line) > 42 )
			t = ((double)strtol(&line[42], NULL, 10))/100;

		if ( t > 99 )
			t = t / 10.0;

		if ( t > 0 )
			fps = t * 1.00000000000001;

		if ( t != 59.94 )
			sage_framenumber_bug = false;
	}

	reffer_count = -1;
	(void)fgets(line, sizeof(line), raw); // Skip second line


	while ( fgets(line, sizeof(line), raw) && strlen(line) > 1 )
	{
		if ( line[strlen(line)-1] != '\n' )
			strcat(&line[strlen(line)], "\n");

		i = 0;
		x = 0;
		col = 0;
		lineProcessed = false;
		++reffer_count;

		// Split Line Apart
		while ( line[i] != '\0'
				&& i < (int)sizeof(line)
				&& ! lineProcessed )
		{
			if ( line[i] == ' ' || line[i] == '\t' || line[i] == '\n' )
			{
				split[x] = '\0';

				switch ( col )
				{
				case 0:
					reffer[reffer_count].start_frame
						= strtol(split, NULL, 10);

					if ( sage_framenumber_bug )
						reffer[reffer_count].start_frame *= 2;

					break;

				case 1:
					reffer[reffer_count].end_frame = strtol(split, NULL, 10);

					if ( reffer[reffer_count].end_frame
							< reffer[reffer_count].start_frame )
					{
						Debug(0, "Error in .ref file, end < start frame\n");
						reffer[reffer_count].end_frame
							= reffer[reffer_count].start_frame + 10;
					}

					if ( sage_framenumber_bug )
						reffer[reffer_count].end_frame *= 2;

					lineProcessed = true;
					break;
				}

				++col;
				x = 0;
				split[0] = '\0';
			}
			else
			{
				split[x] = line[i];
				++x;
			}

			++i;
		}
	}

	fclose(raw);

	if ( reffer_count >= 0 )
	{
		if ( frames == 0 )
			frames = reffer[reffer_count].end_frame;

		if ( reffer[reffer_count].end_frame
				== reffer[reffer_count].start_frame + 1
					&& reffer[reffer_count].end_frame == frames )
			--reffer_count;
	}

	if ( extension[1] == 't' )
		return frames;

	::sprintf(array
			, "%.*s.dif"
			, (int)_logFilename.length() - 4
			, _logFilename.c_str());

	raw = ::fopen(array, "w");

	if ( ! raw )
		return 0;

//#ifdef faslpositive_negative
     j = 0;
     i = 0;
     state = both_show;
     k = 0;
     commercial[commercial_count+1].end_frame
		= commercial[commercial_count].end_frame + 2 ;
     commercial[commercial_count+1].start_frame
		= commercial[commercial_count].end_frame + 1;
     reffer[reffer_count+1].end_frame
		= commercial[commercial_count].end_frame + 2 ;
     reffer[reffer_count+1].start_frame
		= commercial[commercial_count].end_frame + 1;

     if ( reffer[i].end_frame - reffer[i].start_frame > 2 )
	 {
          if ( flagFiles.training.use )
				raw2 = fopen("quality.csv", "a+");

          if ( raw2 )
				::fprintf(raw2, "\"%s\", %6d, %6.1f, %6.1f, %6.1f\n"
					, _basename.c_str()
					, reffer[i].start_frame
					, 0.0
					, 0.0
					, F2L(reffer[i].end_frame
					, reffer[i].start_frame));

          total += F2L(reffer[i].end_frame, reffer[i].start_frame);
          if ( raw2 )
				::fclose(raw2);
     }

	while ( k < commercial[commercial_count].end_frame
			&& (i <= reffer_count || j <= commercial_count) )
	{
          pk = k;

          switch ( state )
		  {
          case both_show:
               if (i <= reffer_count && j <= commercial_count && abs(reffer[i].start_frame-commercial[j].start_frame) < 40)
			   {
                    state = both_commercial;
                    k = commercial[j].start_frame;
               }
			   else if (i > reffer_count || (j <= commercial_count && commercial[j].start_frame < reffer[i].start_frame) )
			   {
                    state = only_commercial;
                    k = commercial[j].start_frame;
               }
			   else
			   {
                    state = only_reffer;
                    k = reffer[i].start_frame;
               }
               break;
          case both_commercial:
               if (i <= reffer_count && j <= commercial_count && abs(reffer[i].end_frame-commercial[j].end_frame) < 40)
			   {
                    state = both_show;
                    k = commercial[j].end_frame;

                    if ( i <= reffer_count )
					{
                         ++i;

                         if (reffer[i].end_frame - reffer[i].start_frame > 2)
						 {
                              if ( flagFiles.training.use )
									raw2 = fopen("quality.csv", "a+");

                              if ( raw2 )
								::fprintf(raw2
									, "\"%s\", %6d, %6.1f, %6.1f, %6.1f\n"
									, _basename.c_str()
									, reffer[i].start_frame
									, 0.0
									, 0.0
									, F2L(reffer[i].end_frame
									, reffer[i].start_frame));

                              total += F2L(reffer[i].end_frame, reffer[i].start_frame);
                              if ( raw2 )
								fclose(raw2);
                         }
                    }

                    if ( j <= commercial_count )
						++j;
               }
			   else if ( i > reffer_count || (j <= commercial_count && commercial[j].end_frame < reffer[i].end_frame ))
			   {
                    state = only_reffer;
                    k = commercial[j].end_frame;
                    if ( j <= commercial_count )
						++j;
               }
			   else
			   {
                    state = only_commercial;
                    k = reffer[i].end_frame;

                    if ( i <= reffer_count )
					{
                         ++i;

                         if ( reffer[i].end_frame - reffer[i].start_frame > 2 )
						 {
                              if ( flagFiles.training.use )
								raw2 = fopen("quality.csv", "a+");

                              if ( raw2 )
								::fprintf(raw2
									, "\"%s\", %6d, %6.1f, %6.1f, %6.1f\n"
									, _basename.c_str()
									, reffer[i].start_frame
									, 0.0
									, 0.0
									, F2L(reffer[i].end_frame
									, reffer[i].start_frame));

                              total += F2L(reffer[i].end_frame, reffer[i].start_frame);
                              if ( raw2 )
								fclose(raw2);
                         }
                    }
               }
               break;
          case only_reffer:
               if (j > commercial_count || reffer[i].end_frame < commercial[j].start_frame) {
                    state = both_show;
                    if (i <= reffer_count)
                         k = reffer[i].end_frame;
                    else
                         k = commercial[commercial_count].end_frame;
                    if (i <= reffer_count) {
                         i++;
                         if (reffer[i].end_frame - reffer[i].start_frame > 2)
						 {
                              if ( flagFiles.training.use )
									raw2 = fopen("quality.csv", "a+");

                              if ( raw2 )
									::fprintf(raw2, "\"%s\", %6d, %6.1f, %6.1f, %6.1f\n", _basename.c_str(), reffer[i].start_frame, 0.0, 0.0, F2L(reffer[i].end_frame, reffer[i].start_frame));
                              total += F2L(reffer[i].end_frame, reffer[i].start_frame);
                              if ( raw2 )
									::fclose(raw2);
                         }
                    }
               }
			   else
			   {
                    state = both_commercial;
                    k = commercial[j].start_frame;
               }

//			::fprintf(raw, "False negative at frame %6ld of %6.1f seconds\n", pk , (k - pk)/fps );
               if ( flagFiles.training.use )
					raw2 = fopen("quality.csv", "a+");

               if ( raw2 )
					::fprintf(raw2, "\"%s\", %6d, %6.1f, %6.1f, %6.1f\n", _basename.c_str(), pk, F2L(k, pk), 0.0, 0.0);
               fneg += F2L(k,pk);
               if ( raw2 )
					fclose(raw2);
               raw2 = NULL;
               break;
          case only_commercial:
               if (i > reffer_count || commercial[j].end_frame < reffer[i].start_frame)
			   {
                    state = both_show;
                    k = commercial[j].end_frame;
                    if ( j <= commercial_count )
						++j;
               }
			   else
			   {
                    state = both_commercial;
                    k = reffer[i].start_frame;
               }
//			::fprintf(raw, "False positive at frame %6ld of %6.1f seconds\n", pk , (k - pk)/fps );

               if ( flagFiles.training.use )
					raw2 = fopen("quality.csv", "a+");

               if ( raw2 )
					::fprintf(raw2
							, "\"%s\", %6d, %6.1f, %6.1f, %6.1f\n"
							, _basename.c_str(), pk, 0.0, F2L(k, pk), 0.0);

               fpos += F2L(k, pk);

               if ( raw2 )
					fclose(raw2);

               raw2 = NULL;
               break;
          }
     }

     if ( flagFiles.training.use )
		raw2 = fopen("quality.csv", "a+");

     if ( raw2 )
	{
		::fprintf(raw2
				, "\"%s\", %6d, %6.1f, %6.1f, %6.1f\n"
				, _basename.c_str(), -1, fneg, fpos, total);

		::fclose(raw2);
	}

//#else
     j = 0;
     i = 0;
     while ( i <= reffer_count && j <= commercial_count )
	 {
          k = std::min(reffer[i].start_frame, commercial[j].start_frame);

          if ( commercial[j].end_frame < reffer[i].start_frame )
		  {
               ::fprintf(raw, "Found %6d %6d    Reference %6d %6d    Difference %+6.1f    %+6.1f\n", commercial[j].start_frame, commercial[j].end_frame, 0,0, F2L(commercial[j].end_frame, commercial[j].start_frame) , F2L(commercial[j].end_frame, commercial[j].start_frame));
//			::fprintf(raw, "Found %6ld %6ld    Not in reference\n", commercial[j].start_frame, commercial[j].end_frame);
               ++j;
          }
		  else if ( commercial[j].start_frame > reffer[i].end_frame )
		  {
               ::fprintf(raw, "Found %6d %6d    Reference %6d %6d    Difference %+6.1f    %+6.1f\n", 0, 0, reffer[i].start_frame, reffer[i].end_frame, -F2L(reffer[i].end_frame, reffer[i].start_frame) , -F2L(reffer[i].end_frame, reffer[i].start_frame));
//			::fprintf(raw, "Not found %6ld %6ld\n", reffer[i].start_frame, reffer[i].end_frame);
               ++i;
          }
		  else
		  {
               if (abs(reffer[i].start_frame-commercial[j].start_frame) > 40 ||
               abs(reffer[i].end_frame-commercial[j].end_frame) > 40 ) {
                    ::fprintf(raw, "Found %6d %6d    Reference %6d %6d    Difference %+6.1f    %+6.1f\n", commercial[j].start_frame, commercial[j].end_frame, reffer[i].start_frame, reffer[i].end_frame, F2L(reffer[i].start_frame, commercial[j].start_frame) , F2L(commercial[j].end_frame , reffer[i].end_frame));
               }
               /*
               			if (abs(reffer[i].start_frame-commercial[j].start_frame) > 40 ) {
               				::fprintf(raw, "Found %5ld %5ld    Reference %5ld %5ld    ", commercial[j].start_frame, commercial[j].end_frame, reffer[i].start_frame, reffer[i].end_frame);
               				::fprintf(raw, "starts at %5ld instead of %5ld\n", commercial[j].start_frame, reffer[i].start_frame);
               			}
               			if (abs(reffer[i].end_frame-commercial[j].end_frame) > 40 ) {
               				::fprintf(raw, "Found %5ld %5ld    Reference %5ld %5ld    ", commercial[j].start_frame, commercial[j].end_frame, reffer[i].start_frame, reffer[i].end_frame);
               				::fprintf(raw, "ends   at %5ld instead of %5ld\n", commercial[j].end_frame, reffer[i].end_frame);
               			}
               */
               i++;
               j++;
          }
     }
     while (j <= commercial_count) {
          ::fprintf(raw, "Found %6d %6d    Reference %6d %6d    Difference %+6.1f    %+6.1f\n", commercial[j].start_frame, commercial[j].end_frame, 0,0, F2L(commercial[j].end_frame, commercial[j].start_frame) , F2L(commercial[j].end_frame, commercial[j].start_frame));
//		::fprintf(raw, "Found %6ld %6ld    Not in reference\n", commercial[j].start_frame, commercial[j].end_frame);
          j++;
     }
     while (i <= reffer_count) {
          ::fprintf(raw, "Found %6d %6d    Reference %6d %6d    Difference %+6.1f    %+6.1f\n", 0, 0, reffer[i].start_frame, reffer[i].end_frame, -F2L(reffer[i].end_frame, reffer[i].start_frame) , -F2L(reffer[i].end_frame, reffer[i].start_frame));
//		::fprintf(raw, "Not found %6ld %6ld\n", reffer[i].start_frame, reffer[i].end_frame);
          i++;
     }
//#endif
     for ( i = 0; i < BlockInfo::count; ++i ) {
          co = CheckFramesForCommercial(cblock[i].f_start+cblock[i].b_head,cblock[i].f_end - cblock[i].b_tail);
          re = CheckFramesForReffer(cblock[i].f_start+cblock[i].b_head,cblock[i].f_end - cblock[i].b_tail);
          if ( co != re )
		  {
               ::fprintf(raw, "Block %6d has mismatch %c%c with cause %s\n"
					, i, co, re, CauseString(cblock[i].cause));
          }
          cblock[i].reffer = re;
     }

	fclose(raw);
	return frames;
}

void CommercialSkipper::closeDump(void)
{
	flagFiles.dumpAudio.close();
	flagFiles.dumpVideo.close();
}

void CommercialSkipper::dumpData(const char *start, int length)
{
	FlagFile &ff(flagFiles.dumpData);

	if ( ff.use && (length != 0) )
	{
		if ( ! ff.f )
			ff.open((_workBasename + ".data").c_str(), "wb");

		if ( length <= 1900 )
		{
			char temp[2000];

			::snprintf(temp, sizeof(temp), "%7d:%4d",framenum_real, length);

			for ( int i = 0; i < length; ++i )
				temp[i + 12] = start[i] & 0xff;

			::fwrite(temp, length + 12, 1, ff.f);
		}
	}
}

void CommercialSkipper::Debug(int level, const char *fmt, ...)
{
	if ( _verbose >= level )
	{
		va_list ap;
		va_start(ap, fmt);
		vsprintf(debugText, fmt, ap);
		va_end(ap);

		if ( flagFiles.console.use )
			_cprintf("%s", debugText);

		FILE *f(fopen(_logFilename.c_str(), "a+"));

		if ( f )
		{
			::fprintf(f, "%s", debugText);
			::fclose(f);
		}

		debugText[0] = '\0';
	}
}

void CommercialSkipper::findLogoThreshold(void)
{
	if ( _useFrameArray )
	{
		const int buckets(20);
		int i;

		for ( i = 1; i < _frameCount; ++i /*(int) fps */ )
			_histogram.logo[(int)(_frame[i].currentGoodEdge * (buckets - 1))]++;

		outputLogoHistogram(buckets);

		int counter(0);

		for ( i = 0; i < buckets; ++i )
		{
			counter += _histogram.logo[i];

			if ( 100 * counter / _frameCount > 40 )
				break;
		}

		if ( i < buckets / 2 )
			i = buckets * 3 / 4;
		else
		{
			if ( _histogram.logo[i - 2] < _histogram.logo[i] )
				i -= 2;
			if ( _histogram.logo[i - 1] < _histogram.logo[i] )
				i -= 1;
			if ( _histogram.logo[i - 1] < _histogram.logo[i] )
				i -= 1;
			if ( _histogram.logo[i - 1] < _histogram.logo[i] )
				i -= 1;
		}

		logo_quality = ((double) i + 0.5) / (double) buckets;
		Debug(8, "Set Logo Quality = %.5f\n", logo_quality);

		/*
		int j = 0;

		for ( i = 0; i < buckets / 2; ++i )
			j += _histogram.logo[i];

		int k = 0;

		for ( i = buckets / 2; i < buckets; ++i )
			k += _histogram.logo[i];

		if ( k < j * 1.3 )
		{
			logo_quality = 0.9;
		}
		else
		{
			k = _histogram.logo[0];

			for ( i = 0; i < buckets; ++i )
			{
				if ( _histogram.logo[k] < _histogram.logo[i] )
					k = i;
			}

			if ( k < buckets * 2 / 3 )
			{
				logo_quality = 0.9;
			}
			else
			{
				i = 0;
				j = _histogram.logo[0];
				for ( i = buckets / 2; i < k; ++i )
				{
					if ( j * 10 / 8 >= _histogram.logo[i] )
					{
						j = _histogram.logo[i];
						logo_quality = ((double) i + 0.5) / (double) buckets;
					}
				}
			}
		}
		*/
     }

	if ( logo_threshold == 0 )
		logo_threshold = logo_quality;
}

void CommercialSkipper::initProcessLogoTest(void)
{
	_logoBlockCount = 0;
	_logoTrendCounter = 0;
	_framesWithLogo = 0;
	_lastLogoTest = false;
	_curLogoTest = false;
}

#define LOGO_SAMPLE (int)(fps * logoFreq)

bool CommercialSkipper::processLogoTest(int realFrameNum
		, bool curLogoTest
		, int close)
{
	int i;
	double s1;
	double s2;

	if ( logo_filter > 0 )
	{
		if ( ! close )
		{
			if ( realFrameNum > logo_filter * 2 * LOGO_SAMPLE )
			{
				s1 = s2 = 0.0;

				for ( i = 0; i < logo_filter; ++i )
				{
					s1 += (_frame[realFrameNum - i * LOGO_SAMPLE - logo_filter * LOGO_SAMPLE].currentGoodEdge - logo_threshold > 0 ? 1 : -1);
					s2 += (_frame[realFrameNum - i * LOGO_SAMPLE].currentGoodEdge - logo_threshold > 0 ? 1 : -1);
				}

				s1 /= logo_filter;
				s2 /= logo_filter;

				for ( i = 0; i < LOGO_SAMPLE; ++i )
				{
					_frame[realFrameNum - logo_filter * LOGO_SAMPLE - i].logo_filter = (s1 + s2);
				}
			}

			for ( i = 0; i < LOGO_SAMPLE; ++i )
				_frame[realFrameNum - i].logo_filter = 0.0;

			realFrameNum -= logo_filter*LOGO_SAMPLE;

			if ( realFrameNum < 0 )
				realFrameNum = 1;

			curLogoTest = (_frame[realFrameNum].logo_filter > 0.0 ? 1 : 0);
		}
		else
			curLogoTest = false;
	}

	if ( curLogoTest != _lastLogoTest )
	{
		if ( ! curLogoTest )
		{
			// Logo disappeared
			_lastLogoTest = false;
			_logoTrendCounter = 0;
			_logoBlock[_logoBlockCount].end = realFrameNum - 1 * (int)(fps * logoFreq);

			if ( _logoBlock[_logoBlockCount].end
						- _logoBlock[_logoBlockCount].start
					> 2*(int)(shrink_logo*fps) + (shrink_logo_tail*fps) )
			{
				_logoBlock[_logoBlockCount].end -= (int)(shrink_logo*fps) + (int)(shrink_logo_tail*fps);
				_logoBlock[_logoBlockCount].start += (int)(shrink_logo*fps);
				_framesWithLogo -= 2 * (int)(fps * logoFreq) + 2*(int)(shrink_logo*fps) + (int)(shrink_logo_tail*fps);

				if ( _useFrameArray )
				{
					i = _logoBlock[_logoBlockCount].end;

					if ( i < 0 )
						i = 0;

					for ( ; i < realFrameNum; ++i )
						_frame[i].logo_present = false;
				}

				Debug(3, "\nEnd logo block %i\tframe %i\tLength - %s\n"
						, _logoBlockCount
						, _logoBlock[_logoBlockCount].end
						, secondsToString(F2L(_logoBlock[_logoBlockCount].end
						, _logoBlock[_logoBlockCount].start)).c_str() );

				++_logoBlockCount;
				ensureLogoBlockSize(_logoBlockCount);
			}
			else // else discard logo cblock
			{
				_logoBlock[_logoBlockCount].start = -1;
			}
		}
		else
		{
			// real change or false change?
			++_logoTrendCounter;

			if ( _logoTrendCounter == minHitsForTrend )
			{
				_lastLogoTest = true;
				_logoTrendCounter = 0;
				ensureLogoBlockSize(_logoBlockCount + 2);
				_logoBlock[_logoBlockCount + 1].start = -1;

				_logoBlock[_logoBlockCount].start
					= realFrameNum - ((int)(fps * logoFreq)
							* (minHitsForTrend - 1));

				_framesWithLogo += ((int)(fps * logoFreq)
										* (minHitsForTrend - 1));

				if ( _useFrameArray )
				{
					for ( i = _logoBlock[_logoBlockCount].start
							; i < realFrameNum
							; ++i )
						_frame[i].logo_present = true;
				}

				if ( ! _logoBlockCount )
				{
					Debug(3, "\t\t\t\tStart logo cblock %i\tframe %i\n"
							, _logoBlockCount
							, _logoBlock[_logoBlockCount].start);
				}
				else
				{
					Debug(3, "\n\t\t\t\tNonlogo Length - %s\n"
							"Start logo cblock %i\tframe %i\n"
							, secondsToString(F2L(_logoBlock[_logoBlockCount].start
							, _logoBlock[_logoBlockCount - 1].end)).c_str()
							, _logoBlockCount
							, _logoBlock[_logoBlockCount].start);
				}
			}
		}
	}
	else
	{
		_logoTrendCounter = 0;
	}

	return _lastLogoTest;
}

} // namespace CS

