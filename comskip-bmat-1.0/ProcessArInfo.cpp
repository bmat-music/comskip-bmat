#include "comskip.h"

#include <math.h>
#include <string.h>

namespace CS {

ArHistogram ar_histogram[MAX_ASPECT_RATIOS];

ArBlockInfo *ar_block;
// How many groups have already been identified. Increment after fill.
int ar_block_count;
int max_ar_block_count;

double ar_ratio_trend(0.0);
int ar_ratio_trend_counter(0);
int ar_ratio_start(0);
int ar_misratio_trend_counter(0);
int ar_misratio_start(0);
double ar_rounding(100);

void CommercialSkipper::initProcessArInfo(Rectangle r)
{
	double pictureHeight(r.getHeight());
	double pictureWidth(r.getWidth());

	if ( r.x.min <= border )
		r.x.min = 1;

	if ( r.x.max >= _videoWidth - border )
		r.x.max = _videoWidth;

	if ( r.y.min <= border )
		r.y.min = 1;

	if ( r.y.max >= _height - border )
		r.y.max = _height;

#ifdef USE_AR_WIDTH
	ar_width = width;

	if ( ar_width < r.y.max + r.y.min )
		ar_width = (int)((r.y.max + r.y.min) * 1.3);
#endif

	last_ar_ratio = pictureWidth / pictureHeight;
	last_ar_ratio = ceil(last_ar_ratio * ar_rounding) / ar_rounding;
	ar_ratio_trend = last_ar_ratio;

	if ( last_ar_ratio < 0.5 || last_ar_ratio > 3.0 )
		last_ar_ratio = AR_UNDEF;

	// lastAR = (last_ar_ratio <= ar_split);
	ar_ratio_start = framenum_real;
	ar_block[ar_block_count].start = framenum_real;
	ar_block[ar_block_count].width = _videoWidth;
	ar_block[ar_block_count].height = _height;
	ar_block[ar_block_count].range = r;
	// ar_block[ar_block_count].ar = lastAR;
	ar_block[ar_block_count].ar_ratio = last_ar_ratio;

#if 0
	if ( _useFrameArray )
		frame[FrameInfo::count].ar_ratio = last_ar_ratio;;
#endif

	Debug(4, "Frame: %i\tRatio: %.2f\tMinY: %i MaxY: %i"
			" MinX: %i MaxX: %i\n"
			, ar_ratio_start
			, ar_ratio_trend
			, r.y.min
			, r.y.max
			, r.x.min
			, r.x.max);

#if 0
	Debug(4, "\nFirst Frame\nFrame: %i\tMinY: %i\tMaxY: %i\t"
			"Ratio: %.2f\n"
			, framenum_real
			, minY, maxY
			, last_ar_ratio);
#endif
}

void CommercialSkipper::processArInfo(Rectangle r)
{
	if ( r.x.min <= border )
		r.x.min = 1;

	if ( r.x.max >= _videoWidth - border )
		r.x.max = _videoWidth;

	if ( r.y.min <= border )
		r.y.min = 1;

	if ( r.y.max >= _height - border )
		r.y.max = _height;

	if ( ticker_tape_percentage > 0 )
		ticker_tape = ticker_tape_percentage * _height / 100;

	if ( ticker_tape > 0 || (
			abs((_height - r.y.max) - (r.y.min)) < 13 + (r.y.min) / 15
			// discard for no symetrical check
			&& abs((_videoWidth  - r.x.max) - (r.x.min)) < 13 + (r.x.min) / 15 // discard for no symetrical check
			&& r.y.min < _height / 4
			&& r.x.min < _videoWidth / 4)
	)
	{ // check if symetrical
		int pictureHeight(r.getHeight());
		int pictureWidth(r.getWidth());
		double cur_ar_ratio((double)(pictureWidth) / (double)pictureHeight);
		cur_ar_ratio = ceil(cur_ar_ratio * ar_rounding) / ar_rounding;

		if ( cur_ar_ratio > 3.0 || cur_ar_ratio < 0.5 )
			cur_ar_ratio = AR_UNDEF;

		int hi((int)((cur_ar_ratio - 0.5)*100));

		if ( hi >= 0 && hi < MAX_ASPECT_RATIOS )
		{
			ar_histogram[hi].frames += 1;
			ar_histogram[hi].ar_ratio = cur_ar_ratio;
		}

		if ( cur_ar_ratio - last_ar_ratio > ar_delta
				|| cur_ar_ratio - last_ar_ratio < -ar_delta )
		{
			if ( cur_ar_ratio - ar_ratio_trend < ar_delta
					&& cur_ar_ratio - ar_ratio_trend > -ar_delta )
			{
				// Same ratio as previous trend
				++ar_ratio_trend_counter;

				if ( ar_ratio_trend_counter / fps > AR_TREND )
				{
					last_ar_ratio = ar_ratio_trend;
					ar_ratio_trend_counter = 0;
					ar_misratio_trend_counter = 0;

					if ( commDetectMethod & DET::AR )
					{
						ar_block[ar_block_count].end = ar_ratio_start-1;
						++ar_block_count;
						initializeArBlockArray(ar_block_count);
						ar_block[ar_block_count].start = ar_ratio_start;
						ar_block[ar_block_count].ar_ratio = ar_ratio_trend;
						ar_block[ar_block_count].volume = 0;
						ar_block[ar_block_count].width = _videoWidth;
						ar_block[ar_block_count].height = _height;
						ar_block[ar_block_count].range = r;
					}

					Debug(9, "Frame: %i\tRatio: %.2f\tMinY: %i\t"
							"MaxY: %i\tMinX: %i\tMaxX: %i\n"
							, ar_ratio_start
							, ar_ratio_trend
							, r.y.min, r.y.max
							, r.x.min, r.x.max);

					last_ar_ratio = ar_ratio_trend;

					if ( _useFrameArray )
					{
						for ( int i = ar_ratio_start; i < framenum_real; ++i )
							_frame[i].ar_ratio = last_ar_ratio;
					}
				}
			}
			else // Other ratio as previous trend
			{
				ar_ratio_trend = cur_ar_ratio;
				ar_ratio_trend_counter = 0;
				ar_ratio_start = framenum_real;
			}

			++ar_misratio_trend_counter;
		}
		else // Same ratio as previous frame
		{
			ar_ratio_trend_counter = 0;
			ar_ratio_start = framenum_real;
			ar_misratio_trend_counter = 0;
			ar_misratio_start = framenum_real;
		}
	}
	else
	{
		// Unreliable ratio
		// ar_ratio_trend = cur_ar_ratio;
		ar_ratio_trend_counter = 0;
		ar_ratio_start = framenum_real;
		/*
            //	if (_useFrameArray) frame[FrameInfo::count].minY = 0;
			//	if (_useFrameArray) frame[FrameInfo::count].maxY = height;
			//	Debug(9, "Frame: %i\tAsimetrical\tMinY: %i\tMaxY: %i\n", framenum_real, minY, maxY);
				ar_ratio_trend_counter = 0;
				ar_ratio_start = framenum_real;
				++ar_misratio_trend_counter;

				if ( _useFrameArray )
					frame[FrameInfo::count].ar_ratio = 0.0;
		*/
	}

#if 0
	if ( last_ar_ratio == 0 )
	{
		ar_misratio_trend_counter = 0;
	}
	else
	{
#endif
	if ( ar_misratio_trend_counter > 3 * fps && last_ar_ratio != AR_UNDEF )
	{
		last_ar_ratio = ar_ratio_trend = AR_UNDEF;

		if ( commDetectMethod & DET::AR )
		{
			ar_block[ar_block_count].end = framenum_real - 3 * (int)fps - 1;
			++ar_block_count;
			initializeArBlockArray(ar_block_count);
			ar_block[ar_block_count].start = framenum_real - 3 * (int)fps;
			ar_block[ar_block_count].ar_ratio = ar_ratio_trend;
			ar_block[ar_block_count].volume = 0;
			ar_block[ar_block_count].width = _videoWidth;
			ar_block[ar_block_count].height = _height;
			ar_block[ar_block_count].range = r;
		}

		Debug(9, "Frame: %i\tRatio: %.2f\tMinY: %i\tMaxY: %i\n"
				, ar_ratio_start, ar_ratio_trend , r.y.min, r.y.max);

		last_ar_ratio = ar_ratio_trend;
	}
#if 0
	}
#endif

#if 0
	if ( _useFrameArray )
		frame[FrameInfo::count].ar_ratio = last_ar_ratio;
#endif
}

void CommercialSkipper::initializeArBlockArray(int i)
{
	int increase(0);

	if ( i >= max_ar_block_count )
		increase = std::max(20, max_ar_block_count - i);
	else if ( ar_block_count >= max_ar_block_count )
		increase = 20;

	if ( increase != 0 )
	{
		max_ar_block_count += increase;
		ar_block = (ArBlockInfo*)realloc(ar_block, (max_ar_block_count + 2) * sizeof(ArBlockInfo));
		Debug(9, "Resizing aspect ratio cblock array to accomodate %i AR groups.\n"
				, max_ar_block_count);
	}
}

} // namespace CS

