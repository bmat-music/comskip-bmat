#include <string.h>

#include "comskip.h"

namespace CS {

void CommercialSkipper::processCsv(std::string filename)
{
	bool lineProcessed(false);
	bool lastLogoTest(false);
	bool curLogoTest(false);
	char line[2048];
	char split[256];
	int cont = 0;
	int minminY(10000);
	int maxmaxY(0);
	int minminX(10000);
	int maxmaxX(0);
	int cutscene_nonzero_count(0);
	int old_format = true;
	int use_bright(0);
	int i;
	int x;
	//int f;
	int col;
	int ccDataFrame;

//again:
	_logoInfoAvailable = true;

	FlagFile ff;
	ff.open(filename, "r", true);
	FILE *in_file(ff.f);

	::fgets(line, sizeof(line), in_file); // Skip first line

	if ( line[85] == ';' )
		line [85] = '+';

	if ( ::strlen(line) > 85 )
	{
		double t(((double)strtol(&line[85], NULL, 10)) / 100.0);

		if ( t > 99.0 )
			t = t / 10.0;

		// Debug(1, "T = %f\n",t);

		if ( t > 0.0 )
			fps = t * 1.00000000000001;
	}

	if ( ::strlen(line) > 94 )
	{
		double t(((double)strtol(&line[94], NULL, 10)) / 100.0);

		if ( t > 99.0 )
               t = t / 10.0;

		if ( t > 0.0 )
			fps = t * 1.00000000000001;
	}

	initComSkip();
	_frameCount = 1;

	while ( ::fgets(line, sizeof(line), in_file) )
	{
		i = 0;
		x = 0;
		col = 0;
		lineProcessed = false;
		initFrame(_frameCount);

		Frame &f(_frame[_frameCount]);

#if 0
		i, f.brightness, f.schange_percent*5, f.logo_present,
			f.uniform, f.volume,  f.minY,f.maxY,(int)((frame[i].ar_ratio)*100),
			(int)(f.currentGoodEdge * 500), f.isblack
#endif

		f.range.x.min = 0;
		f.range.x.max = 0;
		f.hasBright = 0;
		f.dimCount = 0;
		f.pts = (_frameCount - 1) / fps;

		// Split Line Apart
		while ( line[i] != '\0'
				&& i < (int)sizeof(line)
				&& ! lineProcessed )
		{
			if ( line[i] == ';' || line[i] == ',' || line[i] == '\n' )
			{
				split[x] = '\0';

				// printf("col = %i\t", col);
				switch ( col )
				{
				case 0:
					{
						int count(strtol(split, NULL, 10));

						if ( count != _frameCount )
						{
							Debug(0, "Shit!!!!\n");
							exit(23);
						}
					}
					break;

				case 1:
					f.brightness = strtol(split, NULL, 10);
					break;

				case 2:
					f.schange_percent = strtol(split, NULL, 10)/5;
					break;

				case 3:
					f.logo_present = strtol(split, NULL, 10);
					break;

				case 4:
					f.uniform = strtol(split, NULL, 10);
					break;

				case 5:
					f.volume = strtol(split, NULL, 10);
					break;

				case 6:
					f.range.y.min = strtol(split, NULL, 10);

					if ( minminY > f.range.y.min )
						minminY = f.range.y.min;

					break;

				case 7:
					f.range.y.max = strtol(split, NULL, 10);

					if ( maxmaxY < f.range.y.max )
						maxmaxY = f.range.y.max;

					break;

				case 8:
					f.ar_ratio = ((double)strtol(split, NULL, 10))/100;
					break;

				case 9:
					f.currentGoodEdge = ((double)strtol(split, NULL, 10))/500;
					break;

				case 10:
					f.isblack = strtol(split, NULL, 10);

					if ( ! (f.isblack == 0 || f.isblack == 1) )
						old_format = false;

					break;

				case 11:
					f.cutscenematch = strtol(split, NULL, 10);

					if ( f.cutscenematch > 0 )
						++cutscene_nonzero_count;

					break;

				case 12:
					f.range.x.min = strtol(split, NULL, 10);

					if ( minminX > f.range.x.min )
						minminX = f.range.x.min;

					break;

				case 13:
					f.range.x.max = strtol(split, NULL, 10);

					if ( maxmaxX < f.range.x.max )
						maxmaxX = f.range.x.max;

					break;

				case 14:
					f.hasBright = strtol(split, NULL, 10);
					break;

				case 15:
					f.dimCount = strtol(split, NULL, 10);
					break;

				case 16:
					f.pts = strtod(split, NULL);
					break;

				default:
#ifdef FRAME_WITH_HISTOGRAM
					f.histogram[col - 9] = strtol(split, NULL, 10);
#endif
					break;
				}

				++col;
				x = 0;
				split[0] = '\0';

				if ( col > 256 + 9 )
					lineProcessed = true;
			}
			else
			{
				split[x] = line[i];
				++x;
			}

			++i;
		}

		if ( _frameCount == 1 )
			_frame[0].pts = _frame[1].pts;

		++_frameCount;
	}

	_frame[_frameCount].pts = (_frameCount - 1) * fps;

	if ( ! flagFiles.dumpData.f )
		flagFiles.dumpData.open(_workBasename + ".data", "rb");

	ccDataFrame = 0;

	for ( i = 0; i < 1000 && i < _frameCount; ++i )
		if ( _frame[i].hasBright > 0 )
			use_bright = 1;

	findLogoThreshold();

	setHeight(maxmaxY + minminY);
	setVideoWidth(maxmaxX + minminX);

	csBrightnessLast = _frame[1].brightness;
	Debug(8, "CSV file loaded into memory.\n");
	fclose(in_file);
	in_file = 0;
	csBlack.clear();
	_logoBlockCount = 0;
	_sceneChangeVec.clear();
	min_brightness_found = 255;

	for ( i = 1; i < _frameCount; ++i )
	{
		if ( i == 33368 )
			i = i;
ccagain:
		if ( flagFiles.dumpData.f )
		{

			if ( ccDataFrame == 0 )
			{
				cont = fread(line, 8, 1, flagFiles.dumpData.f);
				line[8] = 0;
				sscanf(line,"%7d:",&ccDataFrame);
				// ccDataFrame = strtol(line,NULL,7);
			}

			while ( cont && ccDataFrame <=i )
			{
				cont = fread(line, 4, 1, flagFiles.dumpData.f);

				if ( ! cont )
					break;

				line[4] = 0;
				sscanf(line,"%4d",&ccDataLen);
				// ccDataLen = strtol(line,NULL,4);

				cont = fread(ccData, ccDataLen, 1, flagFiles.dumpData.f);

				if ( ! cont )
					break;

				framenum = ccDataFrame-2;
#ifdef PROCESS_CC
				if ( doProcessCc )
					ProcessCCData();

				if ( outFlag.srt || outFlag.smi )
					process_block(ccData, (int)ccDataLen);
#endif
				ccDataFrame = 0;
				goto ccagain;
			}
		}

		if ( old_format )
		{
			Frame &f(_frame[i]);

			if ( f.isblack )
			{
				if ( f.brightness <= 5 )
				{
					if ( f.brightness == 5 )
						f.isblack = C_a;
					else if ( f.brightness == 4 )
						f.isblack = C_u; // Checked
					else if ( f.brightness == 3 )
						f.isblack = C_s;
					else if ( f.brightness == 2 )
						f.isblack = C_s; // Checked
					else if ( f.brightness == 1 )
						f.isblack = C_u;

					f.brightness = _maxAvgBrightness + 1;
				}
				else
					f.isblack = C_b;
			}
			else
				f.isblack = 0;
		}
		else
		{
			// _frame[i].isblack &= C_b;
		}

		if ( _frame[i].isblack & C_b )
			i = i;

		if ( _frame[i].isblack & C_r )
			i = i;

		if ( _frame[i].brightness > 0 )
		{
			_histogram.bright[_frame[i].brightness]++;

			if ( _frame[i].brightness < min_brightness_found )
				min_brightness_found = _frame[i].brightness;

			_histogram.uniform[(_frame[i].uniform / UNIFORMSCALE < 255 ? _frame[i].uniform / UNIFORMSCALE : 255)]++;
		}

		if ( _frame[i].volume >= 0 )
		{
			_histogram.volume[(_frame[i].volume/volumeScale < 255 ? _frame[i].volume/volumeScale : 255)]++;
		}

		framenum_real = i;

		if ( _frame[i].range.x.max == 0 )
		{
			if ( i == 1 )
			{
				int w(0);

				if ( _frame[i].ar_ratio < 0.5 )
				{
					if ( _frame[i].range.y.max + _frame[i].range.y.min < 600 )
						w = 720;
					else if ( _frame[i].range.y.max + _frame[i].range.y.min < 800 )
						w = 1200;
					else
						w = 1920;
				}
				else
					w = (int) ((_frame[i].range.y.max + _frame[i].range.y.min) * _frame[i].ar_ratio );

				setVideoWidth(w);
				setWidth(w);
			}

			_frame[i].range.x.max = _videoWidth - 10;
			_frame[i].range.x.min = 10;
		}

		if ( i == 1 )
		{
#if 0
			if ( _frame[i].maxX == 0 )
				videowidth = width = (int) ((_frame[i].maxY - _frame[i].minY) * _frame[i].ar_ratio );
#endif
			initProcessArInfo(_frame[i].range);
		}
		else
		{
			processArInfo(_frame[i].range);
		}

		_frame[i].ar_ratio = last_ar_ratio;

		if ( i == 22494 )
			i = i;

		if ( _frame[i].isblack == 1 )
			i = i;

		if ( (commDetectMethod & DET::RESOLUTION_CHANGE) )
		{
			/* not reliable!!!!!!!!!!!!!!!!!!!!!
			_frame[i].isblack &= ~C_r;
			videowidth = width = _frame[i].minX + _frame[i].maxX;
			height = _frame[i].minY + _frame[i].maxY;

			if ((old_width != 0 && abs(width-old_width) > 50)
					|| (old_height != 0 && abs(height - old_height) > 50))
			{
				_frame[i].isblack |= C_r;
			}

			old_width = width;
			old_height = height;
			*/
		}
		else
			_frame[i].isblack &= ~C_r;


		if ( commDetectMethod & DET::BLACK_FRAME )
		{
               // if (_frame[i].brightness <= _maxAvgBrightness && (_nonUniformity == 0 || _frame[i].uniform < _nonUniformity)/* && _frame[i].volume < max_volume */ && !(_frame[i].isblack & C_b))
               // _frame[i].isblack |= C_b;
               if ( (_frame[i].isblack & C_b) && _frame[i].brightness > _maxAvgBrightness )
                    _frame[i].isblack &= ~C_b;

               if ( use_bright )
			   {
                    if ( _frame[i].hasBright > 0
							&& min_hasBright
								> _frame[i].hasBright * 720 * 480 / _videoWidth / _height)
						min_hasBright = _frame[i].hasBright * 720 * 480 / _videoWidth / _height;

                    if ( _frame[i].dimCount > 0
							&& min_dimCount
								> _frame[i].dimCount * 720 * 480 / _videoWidth / _height )
						min_dimCount = _frame[i].dimCount * 720 * 480 / _videoWidth / _height;

                    if ( _frame[i].brightness <= _maxAvgBrightness
							&& _frame[i].hasBright < _maxBright
							&& _frame[i].dimCount < (int)(.05 * _videoWidth * _height) )
                         _frame[i].isblack |= C_b;
               }

               _frame[i].isblack &= ~C_u;

               if ( ! (_frame[i].isblack & C_b)
						&& _nonUniformity > 0
						&& _frame[i].uniform < _nonUniformity
						&& _frame[i].brightness < 180 /*&& _frame[i].volume < max_volume*/ )
                    _frame[i].isblack |= C_u;
          } else
		  {
               _frame[i].isblack &= ~(C_u | C_b);
          }

          if ( _frame[i].isblack & C_s )
               _frame[i].isblack &= ~C_s;


          if (commDetectMethod & DET::SCENE_CHANGE
					&& !(_frame[i-1].isblack & C_b)
					&& !(_frame[i].isblack & C_b) )
		  {
               if ( _frame[i].brightness > 5
						&& abs(_frame[i].brightness - csBrightnessLast) > _brightnessJump )
                    _frame[i].isblack |= C_s;

               if ( _frame[i].brightness > 5 )
                    csBrightnessLast = _frame[i].brightness;

               if ( _frame[i].brightness > 5 && _frame[i].schange_percent < 15 )
                    _frame[i].isblack |= C_s;
          }


          if ( _frame[i].isblack & C_t )
               _frame[i].isblack &= ~C_t;

#if 1
          if ( (commDetectMethod & DET::CUTSCENE)
					&& (cutscene_nonzero_count > 0)
					&& (_frame[i].cutscenematch < CutScene::delta) )
               _frame[i].isblack |= C_t;
#else
          if ( (commDetectMethod & DET::CUTSCENE)
					&& (cutscene_nonzero_count > 0)
					&& (_frame[i].cutscenematch < cutscenedelta) )
               _frame[i].isblack |= C_t;
#endif

          if ( _frame[i].isblack & C_v )
               _frame[i].isblack &= ~C_v;

          if ( _frame[i].isblack )
		  {
               insertBlackFrame(i, (int)_frame[i].isblack);

#if 0
				j = i - volume_slip;

				if ( j < 0 )
					j = 0;

				k = i + volume_slip;

				if ( k > _frameCount )
					k = _frameCount;

				for ( x = j; x < k; ++x )
					if ( _frame[x].volume >= 0 )
						if ( black[Black_frameCount].volume > _frame[x].volume )
							black[Black_frameCount].volume = _frame[x].volume;

				// if ( black[Black_frameCount].volume < max_volume )
					// _frame[i].volume = 1;
#endif
          }

          if ( (_frame[i].schange_percent < 20) && (csBlack.back().frame != i) )
		  {
               if ( _frame[i].brightness < _frame[i - 1].brightness * 2 )
					_sceneChangeVec.push_back(SceneChange(i, _frame[i].schange_percent));
          }
		  else if ( _frame[i].schange_percent < schange_threshold )
		  {
               // Scene Change threshold: original = 91
				_sceneChangeVec.push_back(SceneChange(i, _frame[i].schange_percent));
          }

          if ( (commDetectMethod & DET::LOGO)
				&& ((i % (int)(fps * logoFreq)) == 0) )
		  {
               curLogoTest = (_frame[i].currentGoodEdge > logo_threshold);
               lastLogoTest = processLogoTest(i, curLogoTest, false);
               _frame[i].logo_present = lastLogoTest;
          }

          if ( lastLogoTest )
				++_framesWithLogo;

#if 0
			if ( live_tv && !_frame[i].isblack )
				BuildCommListAsYouGo();
#endif

     }

     framenum_real = _frameCount;
     framesprocessed = _frameCount;

     buildMasterCommList();

     if ( flagFiles.debugwindow.use )
	 {
#if 1
          skip_B_frames = 0;
#endif
          doProcessCc = false;
          i = 0;
#ifdef _WIN32
          printf("Close window when done\n");
          if ( ReviewResult() )
		  {
               LoadIniFile();
               goto again;
          }
#endif
          //		printf(" Press Enter to close debug window\n");
//		gets(HomeDir);
     }

     exit(0);
}

} // namespace CS

