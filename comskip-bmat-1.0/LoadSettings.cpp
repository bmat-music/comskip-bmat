#include <config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <argtable2.h>
#include "comskip.h"

#include <algorithm>
#include <string>

namespace {
char incomingCommandLine[MAX_PATH];
int subsample_video(0x1f);
int argument_count;
char **argument;
const char progname[] = "ComSkip";
#ifdef LIVE_TV_RETRIES
int live_tv_retries(4);
int dvrms_live_tv_retries(300);
#endif
bool useExistingLogoFile(true);

std::string DirName(std::string source)
{
	source.erase(std::find(source.rbegin()
							, source.rend()
							, DIRSEP).base()
					, source.end());

	if ( source.empty() )
		source += "." DIRSEPSTR;

	return source;
}

bool Exists(const std::string &s)
{
	struct stat st;
	return ! stat(s.c_str(), &st);
}

} // anonymous namespace

namespace CS {

void CommercialSkipper::loadSettings(int argc, char **argv)
{
#ifdef __unix__
	(void)nice(19);
#endif

	struct arg_lit *cl_playnice = arg_lit0("n", "playnice", "Slows detection down");
	struct arg_lit *cl_output_zp_cutlist = arg_lit0(NULL, "zpcut", "Outputs a ZoomPlayer cutlist");
	struct arg_lit *cl_output_zp_chapter = arg_lit0(NULL, "zpchapter", "Outputs a ZoomPlayer chapter file");
	struct arg_lit *cl_output_vredo = arg_lit0(NULL, "videoredo", "Outputs a VideoRedo cutlist");
	struct arg_lit *cl_output_csv = arg_lit0(NULL, "csvout", "Outputs a csv of the frame array");
	struct arg_lit *cl_output_training = arg_lit0(NULL, "quality", "Outputs a csv of false detection segments");
	struct arg_lit *cl_output_plist	= arg_lit0(NULL, "plist", "Outputs a mac-style plist for addition to an EyeTV archive as the 'markers' property");
	struct arg_int *cl_detectmethod = arg_intn("d", "detectmethod", NULL, 0, 1, "An integer sum of the detection methods to use");
	// struct arg_int *cl_pid = arg_intn("p", "pid", NULL, 0, 1, "The PID of the video in the TS");
	struct arg_str *cl_pid = arg_strn("p", "pid", NULL, 0, 1, "The PID of the video in the TS");
	struct arg_int *cl_dump = arg_intn("u", "dump", NULL, 0, 1, "Dump the cutscene at this frame number");
	struct arg_lit *cl_ts = arg_lit0("t", "ts", "The input file is a Transport Stream");
	struct arg_lit *cl_help = arg_lit0("h", "help", "Display syntax");
	struct arg_lit *cl_show = arg_lit0("s", "play", "Play the video");
	struct arg_lit *cl_timing = arg_lit0(NULL, "timing", "Dump the timing into a file");
	struct arg_lit *cl_debugwindow = arg_lit0("w", "debugwindow", "Show debug window");
	struct arg_lit *cl_quiet = arg_lit0("q", "quiet", "Not output logging to the console window");
	struct arg_lit *cl_demux = arg_lit0("m", "demux", "Demux the input into elementary streams");
	struct arg_int *cl_verbose = arg_intn("v", "verbose", NULL, 0, 1, "Verbose level");
	struct arg_file *cl_ini = arg_filen(NULL, "ini", NULL, 0, 1, "Ini file to use");
	struct arg_file *cl_logo = arg_filen(NULL, "logo", NULL, 0, 1, "Logo file to use");
	struct arg_file *cl_cut = arg_filen(NULL, "cut", NULL, 0, 1, "CutScene file to use");
	struct arg_file *cl_work = arg_filen(NULL, "output", NULL, 0, 1, "Folder to use for all output files");
	struct arg_int *cl_selftest = arg_intn(NULL, "selftest", NULL, 0, 1, "Execute a selftest");
	struct arg_file *in = arg_filen(NULL, NULL, NULL, 1, 1, "Input file");
	struct arg_file *out = arg_filen(NULL, NULL, NULL, 0, 1, "Output folder for cutlist");
	struct arg_end *end = arg_end(20);

	void *argtable[] = {
		cl_help
		, cl_debugwindow
		, cl_playnice
		, cl_output_zp_cutlist
		, cl_output_zp_chapter
		, cl_output_vredo
		, cl_output_csv
		, cl_output_training
		, cl_output_plist
		, cl_demux
		, cl_pid
		, cl_ts
		, cl_detectmethod
		, cl_verbose
		, cl_dump
		, cl_show
		, cl_timing
		, cl_quiet
		, cl_ini
		, cl_logo
		, cl_cut
		, cl_work
		, cl_selftest
		, in
		, out
		, end
	};

	// int exitcode = 0;
	incomingCommandLine[0] = 0;

	const char *MPEG_EXTENSIONS[] = { "mpg", "ts", "mkv", "tp", "dvr-ms", "wtv", "mp4" };
	const int MPEG_ARRAY_SIZE(sizeof(MPEG_EXTENSIONS) / sizeof(char *));

	if ( strchr(argv[0], ' ') )
		::sprintf(incomingCommandLine, "\"%s\"", argv[0]);
	else
		::sprintf(incomingCommandLine, "%s", argv[0]);

	for ( int i = 1; i < argc; ++i )
	{
		char str[MAX_PATH];

		::sprintf(str, "%s", incomingCommandLine);

		if ( strchr(argv[i], ' ') )
			::sprintf(incomingCommandLine, "%s \"%s\"", str, argv[i]);
		else
			::sprintf(incomingCommandLine, "%s %s", str, argv[i]);
	}

	::printf("The commandline used was:\n%s\n\n", incomingCommandLine);
	argument = (char**)malloc(sizeof(char *) * argc);
	argument_count = argc;

	for ( int i = 0; i < argc; ++i )
	{
		argument[i] = (char*)malloc(sizeof(char) * (strlen(argv[i]) + 1));
		::strcpy(argument[i], argv[i]);
	}

	if ( argc <= 1 )
	{
#ifdef COMSKIPGUI
		// flagFiles.debugwindow.use = true;
#endif

		if ( strstr(argv[0],"GUI") )
			flagFiles.debugwindow.use = true;

		if ( flagFiles.debugwindow.use )
		{
#if 0
			// This is a trick to ask for a input filename when no argument has been given.
			while ( mpegfilename[0] == 0 )
				ReviewResult();
			++argc;
			strcpy(argument[1], mpegfilename);
#endif
		}
	}

	// verify the argtable[] entries were allocated sucessfully
	if ( arg_nullcheck(argtable) != 0 )
	{
		// NULL entries were detected, some allocations must have failed
		Debug(0, "%s: insufficient memory\n", progname);
		exit(1);
	}

	int nerrors(arg_parse(argc, argv, argtable));

	if ( cl_help->count )
	{
		::printf("Usage:\n  comskip ");
		arg_print_syntaxv(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		::printf("\nDetection Methods\n");
		::printf("\t%3i - Black Frame\n", DET::BLACK_FRAME);
		::printf("\t%3i - Logo\n", DET::LOGO);
		::printf("\t%3i - Scene Change\n", DET::SCENE_CHANGE);
		::printf("\t%3i - Resolution Change\n", DET::RESOLUTION_CHANGE);
		::printf("\t%3i - Closed Captions\n", DET::CC);
		::printf("\t%3i - Aspect Ratio\n", DET::AR);
		::printf("\t%3i - Silence\n", DET::SILENCE);
		::printf("\t%3i - CutScenes\n", DET::CUTSCENE);
		::printf("\t255 - USE ALL AVAILABLE\n");
		exit(2);
	}

	if ( nerrors )
	{
		::printf("Usage:\n  comskip ");
		arg_print_syntaxv(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		::printf("\nDetection methods available:\n");
		::printf("\t%3i - Black Frame\n", DET::BLACK_FRAME);
		::printf("\t%3i - Logo\n", DET::LOGO);
		::printf("\t%3i - Scene Change\n", DET::SCENE_CHANGE);
		::printf("\t%3i - Resolution Change\n", DET::RESOLUTION_CHANGE);
		::printf("\t%3i - Closed Captions\n", DET::CC);
		::printf("\t%3i - Aspect Ratio\n", DET::AR);
		::printf("\t%3i - Silence\n", DET::SILENCE);
		::printf("\t%3i - CutScenes\n", DET::CUTSCENE);
		::printf("\t255 - USE ALL AVAILABLE\n");
		::printf("\nErrors:\n");
		arg_print_errors(stdout, end, "ComSkip");
		exit(2);
	}

	if ( ::strcmp(in->extension[0], ".csv")
		&& ::strcmp(in->extension[0], ".txt") )
	{
		_mpegFilename = in->filename[0];
		_stat(_mpegFilename.c_str(), &instat);

		_basename = _mpegFilename.substr(0, _mpegFilename.length() - (int)strlen(in->extension[0]));

		{
			size_t lastSep(_basename.find_last_of(DIRSEP));

			if ( lastSep == std::string::npos )
				lastSep = 0;

			_shortBasename = _basename.substr(lastSep, std::string::npos);

			_iniFilename = _basename.substr(0, lastSep) + "comskip.ini";
		}

		if ( std::string(in->extension[0]) == ".mkv" )
			_isMkv = true;
	}
	else if ( ! strcmp(in->extension[0], ".csv") )
	{
		_isLoadingCsv = true;

		::printf("Opening %s array file.\n", in->filename[0]);

		{
			FlagFile ff;

			ff.open(in->filename[0], "r", true);
		}

		std::string fn(in->filename[0]);

		_basename = fn.substr(0, fn.length() - (int)strlen(in->extension[0]));

		bool foundMpeg(false);

		for ( int i = 0; i < MPEG_ARRAY_SIZE; ++i )
		{
			_mpegFilename = _basename + "." + MPEG_EXTENSIONS[i];

			FlagFile ff;

			if ( ff.open(_mpegFilename, "rb") )
			{
				foundMpeg = true;

				if ( std::string(MPEG_EXTENSIONS[i]) == "mkv" )
					_isMkv = true;

				break;
			}
		}

		if ( ! foundMpeg )
			_mpegFilename.clear();

		{
			size_t lastSep(_basename.find_last_of(DIRSEP));

			if ( lastSep == std::string::npos )
				lastSep = 0;

			_shortBasename = _basename.substr(lastSep, std::string::npos);

			_iniFilename = _basename.substr(0, lastSep) + "comskip.ini";
		}

		if ( _mpegFilename.empty() )
			_mpegFilename = _basename + ".mpg";
	}
	else if ( ! strcmp(in->extension[0], ".txt") )
	{
		_isLoadingTxt = true;
		flagFiles._default.use = false;

		::printf("Opening %s for review\n", in->filename[0]);

		{
			FlagFile ff;

			ff.open(in->filename[0], "r", true);
		}

		std::string fn(in->filename[0]);

		_basename = fn.substr(0, fn.length() - (int)strlen(in->extension[0]));

		bool foundMpeg(false);

		for ( int i = 0; i < MPEG_ARRAY_SIZE; ++i )
		{
			_mpegFilename = _basename + "." + MPEG_EXTENSIONS[i];

			FlagFile ff;

			if ( ff.open(_mpegFilename, "rb") )
			{
				foundMpeg = true;

				if ( std::string(MPEG_EXTENSIONS[i]) == "mkv" )
					_isMkv = true;

				break;
			}
		}

		if ( ! foundMpeg )
			_mpegFilename.clear();

		{
			size_t lastSep(_basename.find_last_of(DIRSEP));

			if ( lastSep == std::string::npos )
				lastSep = 0;

			_shortBasename = _basename.substr(lastSep, std::string::npos);

			_iniFilename = _basename.substr(0, lastSep) + "comskip.ini";
		}

	}
	else
	{
		::printf("The input file was not a video file, "
				"comskip CSV or TXT file - %s.\n"
				, in->extension[0]);
		::exit(5);
	}

	if ( cl_ini->count )
	{
		::printf("Setting ini file to %s as per commandline\n", cl_ini->filename[0]);
		_iniFilename = cl_ini->filename[0];
	}

	flagFiles.ini.open(_iniFilename, "r");

	if ( cl_work->count )
	{
		_outputDirname = cl_work->filename[0];

		if ( _outputDirname[_outputDirname.length() - 1] == DIRSEP )
			_outputDirname.erase(_outputDirname.end() - 1);

		_workBasename = _outputDirname + DIRSEPSTR + _shortBasename;
		_outBasename = _workBasename;
	}
	else
	{
		_outputDirname.clear();
		_workBasename = _basename;
	}

	if ( out->count )
	{
		_outputDirname = out->filename[0];

		if ( _outputDirname[_outputDirname.length() - 1] == DIRSEP )
			_outputDirname.erase(_outputDirname.end() - 1);

		_outBasename = _outputDirname + DIRSEPSTR + _shortBasename;
	}
	else
	{
		_outputDirname.clear();
		_outBasename = _basename;
	}

	/*
	 * --output also sets the output file location
	 * if not specified as 2nd argument.
	 */

	if ( cl_work->count && ! out->count )
		_outBasename = _workBasename;

	_initLogoFilename = DirName(_workBasename) + "comskip.logo.txt";
	_logoFilename = _workBasename + ".logo.txt";
	_logFilename = _workBasename + ".log";

	std::string filename(_outBasename + ".txt");

	if ( ! strcmp(HomeDir, ".") )
	{
		if ( ! flagFiles.ini.f )
		{
			_iniFilename = "comskip.ini";
			flagFiles.ini.open(_iniFilename, "r");
		}

		_exeFilename = EXEFILENAME;
		_dictFilename = "comskip.dictionary";
	}
	else
	{
		std::string hd(HomeDir);
		hd += DIRSEPSTR;

		if ( ! flagFiles.ini.f )
		{
			_iniFilename = hd + "comskip.ini";
			flagFiles.ini.open(_iniFilename, "r");
		}

		_exeFilename = hd + EXEFILENAME;
		_dictFilename = hd + "comskip.dictionary";
	}

	if ( cl_cut->count )
	{
		::printf("Loading cutfile %s as per commandline\n"
				, cl_cut->filename[0]);
		loadCutScene(cl_cut->filename[0]);
	}

	if ( cl_logo->count )
	{
		_logoFilename = cl_logo->filename[0];
		::printf("Setting logo file to %s as per commandline\n"
				, _logoFilename.c_str());
	}

	// if ( ! _isLoadingTxt )
	loadIniFile();

	// live_tv = true;

	time_t ltime;

	time(&ltime);
	struct tm *now(localtime(&ltime));
	int mil_time((now->tm_hour * 100) + now->tm_min);

	if ( (_playNice.start > -1) && (_playNice.end > -1) )
	{
		if ( _playNice.start > _playNice.end )
		{
			if ( (mil_time >= _playNice.start) || (mil_time <= _playNice.end) )
				_playNice.use = true;
		}
		else
		{
			if ( (mil_time >= _playNice.start) && (mil_time <= _playNice.end) )
				_playNice.use = true;
		}
	}

	if ( cl_verbose->count )
	{
		_verbose = cl_verbose->ival[0];
		::printf("Setting verbose level to %i as per command line.\n", _verbose);
	}

	if ( cl_selftest->count )
	{
		_selfTest = cl_selftest->ival[0];
		::printf("Setting selftest to %i as per command line.\n", _selfTest);
	}

	if ( cl_debugwindow->count || _isLoadingTxt )
		flagFiles.debugwindow.use = true;

	if ( cl_timing->count )
		flagFiles.timing.use = true;

	if ( cl_show->count )
	{
		subsample_video = 0;
		flagFiles.debugwindow.use = true;
	}

	if ( cl_quiet->count )
		flagFiles.console.use = false;

#ifdef COMSKIPGUI
	// flagFiles.debugwindow.use = true;
#endif

	if ( strstr(argv[0],"GUI") )
		flagFiles.debugwindow.use = true;

	if ( cl_demux->count )
		flagFiles.demux.use = true;

	if ( ! _isLoadingTxt && ! useExistingLogoFile && cl_logo->count == 0 )
	{
		FlagFile ff;

		if ( ff.open(_logoFilename, "r") )
		{
			ff.close();
			::remove(_logoFilename.c_str());
		}
	}

	if ( cl_output_csv->count )
		flagFiles.framearray.use = true;

	if ( cl_output_training->count )
		flagFiles.training.use = true;

	if ( _verbose )
	{
		if ( _isLoadingTxt )
		{
			// Do nothing to the log file
			_verbose = 0;
		}
		else
		{
			FlagFile logoFile;
			logoFile.open(_logoFilename, "r");

			if ( _isLoadingCsv )
			{
				FlagFile ff;

				if ( ff.open(_logFilename, "w") )
				{
					ff.printf("################################################################\n");
					ff.printf("Generated using Comskip %s.%s\n", PACKAGE_VERSION, SUBVERSION);
					ff.printf("Loading comskip csv file - %s\n", in->filename[0]);
					ff.printf("Time at start of run:\n%s", ctime(&ltime));
					ff.printf("################################################################\n");
				}
			}
			else if ( logoFile.f )
			{
				FlagFile ff;

				if ( ff.open(_logFilename, "a+") )
				{
					ff.printf("################################################################\n");
					ff.printf("Starting second pass using %s\n", _logoFilename.c_str());
					ff.printf("Time at start of second run:\n%s", ctime(&ltime));
					ff.printf("################################################################\n");
				}
			}
			else
			{
				FlagFile ff;

				if ( ff.open(_logFilename, "w") )
				{
					ff.printf("################################################################\n");
					ff.printf("Generated using Comskip %s.%s\n", PACKAGE_VERSION, SUBVERSION);
					ff.printf("Time at start of run:\n%s", ctime(&ltime));
					ff.printf("################################################################\n");
				}
			}
		}
	}

	if ( cl_playnice->count )
	{
		_playNice.use = true;
		Debug(1, "ComSkip playing nice due as per command line.\n");
	}

	if ( cl_detectmethod->count )
	{
		commDetectMethod = cl_detectmethod->ival[0];
		::printf("Setting detection methods to %i as per command line.\n", commDetectMethod);
	}

	if ( cl_dump->count )
	{
		_cutSceneNo = cl_dump->ival[0];
		::printf("Setting dump frame number to %i as per command line.\n", _cutSceneNo);
	}

	if ( cl_ts->count )
	{
		demux_pid = 1;
		printf("Auto selecting the PID.\n");
	}

	if ( cl_pid->count )
	{
		// demux_pid = cl_pid->ival[0];
		sscanf(cl_pid->sval[0],"%x", &demux_pid);
		::printf("Selecting PID %x as per command line.\n", demux_pid);
	}

	Debug(9, "Mpeg:\t%s\nExe\t%s\nLogo:\t%s\nIni:\t%s\n"
			, _mpegFilename.c_str()
			, _exeFilename.c_str()
			, _logoFilename.c_str()
			, _iniFilename.c_str());
	Debug(1, "\nDetection Methods to be used:\n");

	int i(0);

	if ( commDetectMethod & DET::BLACK_FRAME )
		Debug(1, "\t%i) Black Frame\n", ++i);

	if ( commDetectMethod & DET::LOGO )
		Debug(1, "\t%i) Logo - Give up after %i seconds\n", ++i, giveUpOnLogoSearch);

	if ( commDetectMethod & DET::CUTSCENE )
	{
		// commDetectMethod &= ~SCENE_CHANGE;
	}

	if ( commDetectMethod & DET::SCENE_CHANGE )
		Debug(1, "\t%i) Scene Change\n", ++i);

	if ( commDetectMethod & DET::RESOLUTION_CHANGE )
		Debug(1, "\t%i) Resolution Change\n", ++i);

	if ( commDetectMethod & DET::CC )
	{
		doProcessCc = true;
		Debug(1, "\t%i) Closed Captions\n", ++i);
	}

	if ( commDetectMethod & DET::AR )
		Debug(1, "\t%i) Aspect Ratio\n", ++i);

	if ( commDetectMethod & DET::SILENCE )
		Debug(1, "\t%i) Silence\n", ++i);

	if ( commDetectMethod & DET::CUTSCENE )
		Debug(1, "\t%i) CutScenes\n", ++i);

	Debug(1, "\n");

	if ( _playNice.start || _playNice.end )
	{
		Debug(1, "\nComSkip throttles back from %.4i to %.4i.\nThe time is now %.4i "
				, _playNice.start, _playNice.end, mil_time);

		if ( _playNice.use )
			Debug(1, "so comskip is running slowly.\n");
		else
			Debug(1, "so it's full speed ahead!\n");
	}

	Debug(10, "\nSettings\n--------\n");
	Debug(10, "%s\n", _iniText.c_str());
	_outFilename = _outBasename + ".txt";

	if ( ! _isLoadingTxt )
	{
		if ( Exists(_logoFilename) )
			loadLogoMaskData(_logoFilename);
		else
			loadLogoMaskData(_initLogoFilename);
	}

	if ( cl_output_plist->count )
		flagFiles.plist_cutlist.use = true;

	if ( cl_output_zp_cutlist->count )
		flagFiles.zoomplayer_cutlist.use = true;

	if ( cl_output_zp_chapter->count )
		flagFiles.zoomplayer_chapter.use = true;

	if ( cl_output_vredo->count )
		flagFiles.videoredo.use = true;

	if ( cl_output_plist->count )
		flagFiles.plist_cutlist.use = true;

	if ( flagFiles._default.use && ! _isLoadingTxt && ! _isSecondPass )
	{
		flagFiles.out.open(_outFilename, "w", true);
		flagFiles._default.use = true;
		flagFiles.out.close();
	}

	// max_commercialbreak *= fps;
	// min_commercialbreak *= fps;
	// max_commercial_size *= fps;
	// min_commercial_size *= fps;

	if ( _isLoadingTxt )
	{
		_frameCount = inputReffer(".txt", true);

		if ( _frameCount < 0 )
		{
			::printf("Incompatible TXT file\n");
			exit(2);
		}

		flagFiles.framearray.use = false;
#ifdef _WIN32
		::printf("Close window or hit ESCAPE when done\n");
		flagFiles.debugwindow.use = true;
		ReviewResult();
#endif
		// flagFiles.in.f = 0;
	}

	if ( ! _isLoadingTxt && (flagFiles.srt.use || flagFiles.smi.use ) )
	{
#ifdef PROCESS_CC
		const char *CEW_argv[10];
		int i(0);
		CEW_argv[i++] = EXEFILENAME;

		if ( flagFiles.smi.use )
		{
			CEW_argv[i++] = "-sami";
			flagFiles.srt.use = true;
		}
		else
			CEW_argv[i++] = "-srt";

		CEW_argv[i++] = in->filename[0];
		CEW_init (i, CEW_argv);
#else
		if ( flagFiles.smi.use )
			flagFiles.srt.use = true;
#endif
	}

	if ( _isLoadingCsv )
	{
		flagFiles.framearray.use = false;
		processCsv(in->filename[0]);
		flagFiles.debugwindow.use = false;
	}

	// deallocate each non-null entry in argtable[]
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
}

void CommercialSkipper::findIniFile(void)
{
#ifndef _WIN32
#if 1
	_iniFilename = "/etc/comskip/comskip.ini";
	_dictFilename = "/etc/comskip/comskip.dictionary";
	_exeFilename = "/usr/bin/" EXEFILENAME;
#else
	char searchinifile[] = "comskip.ini";
	char searchexefile[] = EXEFILENAME;
	char searchdictfile[] = "comskip.dictionary";
	char envvar[] = "PATH";
#endif
#else
	char searchinifile[] = "comskip.ini";
	char searchexefile[] = EXEFILENAME;
	char searchdictfile[] = "comskip.dictionary";
	char envvar[] = "PATH";

	_searchenv(searchinifile, envvar, inifilename);

	if ( *inifilename != '\0' )
		Debug(1, "Path for %s: %s\n", searchinifile, inifilename);
	else
		Debug(1, "%s not found\n", searchinifile);

	_searchenv(searchdictfile, envvar, dictfilename);

	if ( *dictfilename != '\0' )
		Debug(1, "Path for %s: %s\n", searchdictfile, dictfilename);
	else
		Debug(1, "%s not found\n", searchdictfile);

	_searchenv(searchexefile, envvar, exefilename);

	if ( *exefilename != '\0' )
		Debug(1, "Path for %s: %s\n", searchexefile, exefilename);
	else
		Debug(1, "%s not found\n", searchexefile);
#endif
}

void CommercialSkipper::loadIniFile(void)
{
	char data[60000];
	const char *ts;
	double tmp;
	_iniText = "";

	if ( ! flagFiles.ini.f )
	{
		printf("No INI file found in current directory."
				"  Searching PATH...\n");

		findIniFile();

		if ( ! _iniFilename.empty() )
		{
			::printf("INI file found at %s\n", _iniFilename.c_str());
			flagFiles.ini.open(_iniFilename.c_str(), "r");
		}
		else
			::printf("No INI file found in PATH...\n");
	}

	size_t len(0);

	if ( flagFiles.ini.f )
	{
		::printf("Using %s for initiation values.\n", _iniFilename.c_str());
		len = fread(data, 1, 59999, flagFiles.ini.f);
		flagFiles.ini.close();
		data[len] = '\0';
		_iniText = "";
		_iniText += "[Main Settings]\n";
		_iniText += ";the sum of the values for which kind of frames comskip will consider as possible cutpoints: 1=uniform (black or any other color) frame, 2=logo, 4=scene change, 8=resolution change, 16=closed captions, 32=aspect ration, 64=silence, 255=all.\n";

		if ( (tmp = FindNumber(data, "detect_method="
				, (double)commDetectMethod)) > -1 )
			commDetectMethod = (int)tmp;

		_iniText += ";Set to 10 to show a lot of extra info, level 5 is also OK, set to 0 to disable\n";

		if ( (tmp = FindNumber(data, "verbose=", (double)_verbose)) > -1 )
			_verbose = (int)tmp;

		_iniText += ";Frame not black if any of the pixels of the frame has a brightness greater than this (scale 0 to 255)\n";

		if ( (tmp = FindNumber(data, "max_brightness="
				, (double)_maxBrightness)) > -1 )
			_maxBrightness = (int)tmp;

		if ( (tmp = FindNumber(data, "maxbright="
				, (double)_maxBright)) > -1u )
			_maxBright = (int)tmp;

		_iniText += ";Frame not pure black if a small number of the pixels of the frame has a brightness greater than this. To decide if the frame is truly black, comskip will also check average brightness (scale 0 to 255)\n";

		if ( (tmp = FindNumber(data, "test_brightness="
				, (double)_testBrightness)) > -1 )
			_testBrightness = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "max_avg_brightness="
				, (double)_maxAvgBrightness)) > -1 )
			_maxAvgBrightness = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "max_commercialbreak="
				, (double)_commBreakSize._break.max)) > -1 )
			_commBreakSize._break.max = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "min_commercialbreak="
				, (double)_commBreakSize._break.min)) > -1 )
			_commBreakSize._break.min = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "max_commercial_size="
				, (double)_commBreakSize._size.max)) > -1 )
			_commBreakSize._size.max = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "min_commercial_size="
				, (double)_commBreakSize._size.min)) > -1 )
			_commBreakSize._size.min = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "min_show_segment_length="
				, (double)min_show_segment_length)) > -1 )
			min_show_segment_length = (double)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "max_volume="
				, (double)_maxVolume)) > -1 )
			_maxVolume = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "max_silence="
				, (double)_maxSilence)) > -1 )
			_maxSilence = (int)tmp;

		_iniText += ";\n";

		if ( (tmp = FindNumber(data, "non_uniformity="
				, (double)_nonUniformity)) > -1 )
			_nonUniformity = (int)tmp;

		_iniText += "[Detailed Settings]\n";

		if ( (tmp = FindNumber(data, "min_silence="
				, (double)_minSilence)) > -1 )
			_minSilence = (int)tmp;

		if ( (tmp = FindNumber(data, "noise_level="
				, (double)_noiseLevel)) > -1 )
			_noiseLevel = (int)tmp;

		if ( (tmp = FindNumber(data, "brightness_jump="
				, (double)_brightnessJump)) > -1 )
			_brightnessJump = (int)tmp;

		if ( (tmp = FindNumber(data, "fps=", (double)fps)) > -1 )
			fps = tmp;

		if ( (tmp = FindNumber(data, "validate_silence="
				, (double)_validateSilence)) > -1 )
			_validateSilence = (bool)tmp;

		if ( (tmp = FindNumber(data, "validate_uniform="
				, (double)_validateUniform)) > -1 )
			_validateUniform = (bool)tmp;

		if ( (tmp = FindNumber(data, "validate_scenechange="
				, (double)_validateSceneChange)) > -1 )
			_validateSceneChange = (bool)tmp;

		if ( (tmp = FindNumber(data, "global_threshold="
				, (double)_globalThreshold)) > -1 )
			_globalThreshold = (double)tmp;

		if ( (tmp = FindNumber(data, "disable_heuristics="
				, (double)disable_heuristics)) > -1)
			disable_heuristics = (bool)tmp;

		_iniText += "[CPU Load Reduction]\n";

		if ( (tmp = FindNumber(data, "thread_count="
				, (double)_threadCount)) > -1 )
			_threadCount = (int)tmp;

		if ( (tmp = FindNumber(data, "play_nice_start="
				, (double)_playNice.start)) > -1 )
			_playNice.start = (int)tmp;

		if ( (tmp = FindNumber(data, "play_nice_end="
				, (double)_playNice.end)) > -1 )
			_playNice.end = (int)tmp;

		if ( (tmp = FindNumber(data, "play_nice_sleep="
				, (double)_playNice.sleep)) > -1 )
			_playNice.sleep = (int)tmp;

		_iniText += "[Input Correction]\n";

		if ( (tmp = FindNumber(data, "max_repair_size="
				, (double)max_repair_size)) > -1 )
			max_repair_size = (int)tmp;

		if ( (tmp = FindNumber(data, "ms_audio_delay="
				, (double)ms_audio_delay)) > -1 )
			ms_audio_delay = -(int)tmp;

		if ( (tmp = FindNumber(data, "volume_slip="
				, (double)volume_slip)) > -1 )
			volume_slip = (int)tmp;

		if ( (tmp = FindNumber(data, "variable_bitrate="
				, (double)variable_bitrate)) > -1 )
			variable_bitrate = (int)tmp;

		if ( (tmp = FindNumber(data, "lowres=", (double)_lowRes)) > -1 )
			_lowRes = (int)tmp;

#if 1
		if ( (tmp = FindNumber(data, "skip_b_frames="
				, (double)skip_B_frames)) > -1 )
			skip_B_frames = (int)tmp;
#if 0
		if ( skip_B_frames != 0 && max_repair_size == 0 )
			max_repair_size = 40;
#endif
#else

#ifdef _DEBUG
		if ( (tmp = FindNumber(data, "skip_b_frames="
				, (double)skip_B_frames)) > -1 )
			skip_B_frames = (int)tmp;

		if ( skip_B_frames != 0 && max_repair_size == 0 )
			max_repair_size = 40;
#endif
#endif

		_iniText += "[Aspect Ratio]\n";

		if ( (tmp = FindNumber(data, "ar_delta=", (double)ar_delta)) > -1 )
			ar_delta = (double)tmp;

		if ( (tmp = FindNumber(data, "cut_on_ar_change="
				, (double)cut_on_ar_change)) > -1 )
			cut_on_ar_change = (int)tmp;

		_iniText += "[Global Removes]\n";

		if ( (tmp = FindNumber(data, "padding=", (double) padding)) > -1 )
			padding = (int)tmp;

		if ( (tmp = FindNumber(data, "remove_before="
				, (double)remove_before)) > -1 )
			remove_before = (int)tmp;

		if ( (tmp = FindNumber(data, "remove_after="
				, (double)remove_after)) > -1 )
			remove_after = (int)tmp;

		if ( (tmp = FindNumber(data, "added_recording="
				, (double)added_recording)) > -1 )
			added_recording = (int)tmp;

		if ( (tmp = FindNumber(data, "delete_show_after_last_commercial="
				, (double)delete_show_after_last_commercial)) > -1 )
			delete_show_after_last_commercial = (int)tmp;

		if ( (tmp = FindNumber(data, "delete_show_before_first_commercial="
				, (double)delete_show_before_first_commercial)) > -1 )
			delete_show_before_first_commercial = (int)tmp;

		if ( (tmp = FindNumber(data, "delete_show_before_or_after_current="
				, (double)delete_show_before_or_after_current)) > -1 )
			delete_show_before_or_after_current = (int)tmp;

		if ( (tmp = FindNumber(data, "delete_block_after_commercial="
				, (double)delete_block_after_commercial)) > -1 )
			delete_block_after_commercial = (int)tmp;

		if ( (tmp = FindNumber(data, "min_commercial_break_at_start_or_end="
				, (double)min_commercial_break_at_start_or_end)) > -1 )
			min_commercial_break_at_start_or_end = (int)tmp;

		if ( (tmp = FindNumber(data, "always_keep_first_seconds="
				, (double)always_keep_first_seconds)) > -1 )
			always_keep_first_seconds = (int)tmp;

		if ( (tmp = FindNumber(data, "always_keep_last_seconds="
				, (double)always_keep_last_seconds)) > -1 )
			always_keep_last_seconds = (int)tmp;

		_iniText += "[USA Specific]\n";

		if ( (tmp = FindNumber(data, "intelligent_brightness="
				, (double)intelligent_brightness)) > -1 )
			intelligent_brightness = (bool) tmp;

		if ( (tmp = FindNumber(data, "black_percentile="
				, (double)black_percentile)) > -1 )
			black_percentile = (double)tmp;

		if ( (tmp = FindNumber(data, "uniform_percentile="
				, (double)uniform_percentile)) > -1 )
			uniform_percentile = (double)tmp;

		if ( (tmp = FindNumber(data, "score_percentile="
				, (double)score_percentile)) > -1 )
			score_percentile = (double)tmp;

		_iniText += "[Main Scoring]\n";

		if ( (tmp = FindNumber(data, "length_strict_modifier="
				, (double)length_strict_modifier)) > -1 )
			length_strict_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "length_nonstrict_modifier="
				, (double)length_nonstrict_modifier)) > -1 )
			length_nonstrict_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "combined_length_strict_modifier="
				, (double)combined_length_strict_modifier)) > -1 )
			combined_length_strict_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "combined_length_nonstrict_modifier="
				, (double)combined_length_nonstrict_modifier)) > -1 )
			combined_length_nonstrict_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "ar_wrong_modifier="
				, (double)ar_wrong_modifier)) > -1 )
			ar_wrong_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "excessive_length_modifier="
				, (double)excessive_length_modifier)) > -1 )
			excessive_length_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "dark_block_modifier="
				, (double)dark_block_modifier)) > -1 )
			dark_block_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "min_schange_modifier="
				, (double)min_schange_modifier)) > -1 )
			min_schange_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "max_schange_modifier="
				, (double)max_schange_modifier)) > -1 )
			max_schange_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "logo_present_modifier="
				, (double)logo_present_modifier)) > -1 )
			logo_present_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "punish_no_logo="
				, (double)punish_no_logo)) > -1 )
			punish_no_logo = (bool)tmp;

		_iniText += "[Detailed Scoring]\n";

		if ( (tmp = FindNumber(data, "punish=", (double)punish)) > -1 )
			punish = (int)tmp;

		if ( (tmp = FindNumber(data, "reward=", (double)reward)) > -1 )
			reward = (int)tmp;

		if ( (tmp = FindNumber(data, "punish_threshold="
				, (double)punish_threshold)) > -1 )
			punish_threshold = (double)tmp;

		if ( (tmp = FindNumber(data, "punish_modifier="
				, (double)punish_modifier)) > -1 )
			punish_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "reward_modifier="
				, (double)reward_modifier)) > -1 )
			reward_modifier = (double)tmp;

		_iniText += "[Logo Finding]\n";

		if ( (tmp = FindNumber(data, "border=", (double)border)) > -1 )
			border = (int)tmp;

		if ( (tmp = FindNumber(data, "give_up_logo_search="
				, (double)giveUpOnLogoSearch)) > -1 )
			giveUpOnLogoSearch = (int)tmp;

		if ( (tmp = FindNumber(data, "delay_logo_search="
				, (double)delay_logo_search)) > -1 )
			delay_logo_search = (int)tmp;

		if ( (tmp = FindNumber(data, "logo_max_percentage_of_screen="
				, (double)logo_max_percentage_of_screen)) > -1 )
			logo_max_percentage_of_screen = (double)tmp;

		if ( (tmp = FindNumber(data, "ticker_tape="
				, (double)ticker_tape)) > -1 )
			ticker_tape = (int)tmp;

		if ( (tmp = FindNumber(data, "ticker_tape_percentage="
				, (double)ticker_tape_percentage)) > -1 )
			ticker_tape_percentage = (int)tmp;

		if ( (tmp = FindNumber(data, "ignore_side="
				, (double)ignore_side)) > -1 )
			ignore_side = (int)tmp;

		if ( (tmp = FindNumber(data, "subtitles="
				, (double)subtitles)) > -1 )
			subtitles = (int)tmp;

		if ( (tmp = FindNumber(data, "logo_at_bottom="
				, (double)logo_at_bottom)) > -1 )
			logo_at_bottom = (int)tmp;

		if ( (tmp = FindNumber(data, "logo_threshold="
				, (double)logo_threshold)) > -1 )
			logo_threshold = (double)tmp;

		if ( (tmp = FindNumber(data, "logo_filter="
				, (double)logo_filter)) > -1 )
			logo_filter = (int)tmp;

		if ( (tmp = FindNumber(data, "aggressive_logo_rejection="
				, (double)_aggressiveLogoRejection)) > -1 )
			_aggressiveLogoRejection = (int)tmp;

		if ( (tmp = FindNumber(data, "edge_level_threshold="
				, (double)_edges.getThreshold())) > -1 )
			_edges.setThreshold((int)tmp);

		if ( (tmp = FindNumber(data, "edge_radius="
				, (double)_edges.getRadius())) > -1 )
			_edges.setRadius((int)tmp);

		if ( (tmp = FindNumber(data, "edge_weight="
				, (double)_edges.getWeight())) > -1 )
			_edges.setWeight((int)tmp);

		if ( (tmp = FindNumber(data, "edge_step="
				, (double)_edges.getStep())) > -1 )
			_edges.setStep((int)tmp);

		if ( _edges.getStep() < 1 )
			_edges.setStep(1);

		if ( (tmp = FindNumber(data, "num_logo_buffers="
				, (double)_logoBuffers.getBufferCount())) > -1 )
			_logoBuffers.resize((size_t)tmp);

		if ( (tmp = FindNumber(data, "use_existing_logo_file="
				, (double)useExistingLogoFile)) > -1 )
			useExistingLogoFile = (bool)tmp;

		if ( (tmp = FindNumber(data, "two_pass_logo="
				, (double)startOverAfterLogoInfoAvail)) > -1 )
			startOverAfterLogoInfoAvail = (bool)tmp;

		_iniText += "[Logo Interpretation]\n";

		if ( (tmp = FindNumber(data, "connect_blocks_with_logo="
				, (double)connect_blocks_with_logo)) > -1 )
			connect_blocks_with_logo = (int)tmp;

		if ( (tmp = FindNumber(data, "edl_mode="
				, (double)_edlMode)) > -1 )
			_edlMode = (int)tmp;

		if ( (tmp = FindNumber(data, "mkv_time_offset="
				, (double)_mkvTimeOffset)) > -1 )
			_mkvTimeOffset = (double)tmp;

		if ( (tmp = FindNumber(data, "logo_percentile="
				, (double)logo_percentile)) > -1 )
			logo_percentile = (double)tmp;

		if ( (tmp = FindNumber(data, "logo_fraction="
				, (double)logo_fraction)) > -1 )
			logo_fraction = (double)tmp;

		if ( (tmp = FindNumber(data, "shrink_logo="
				, (double)shrink_logo)) > -1 )
			shrink_logo = (double)tmp;

		if ( (tmp = FindNumber(data, "shrink_logo_tail="
				, (double)shrink_logo_tail)) > -1 )
			shrink_logo_tail = (int)tmp;

		if ( (tmp = FindNumber(data, "before_logo="
				, (double)before_logo)) > -1 )
			before_logo = (int)tmp;

		if ( (tmp = FindNumber(data, "after_logo="
				, (double)after_logo)) > -1 )
			after_logo = (int)tmp;

		if ( (tmp = FindNumber(data, "where_logo="
				, (double)where_logo)) > -1 )
			where_logo = (int)tmp;

		if ( (tmp = FindNumber(data, "min_black_frames_for_break="
				, (double)min_black_frames_for_break)) > -1 )
			min_black_frames_for_break = (unsigned int)tmp;

		_iniText += "[Closed Captioning]\n";

		if ( (tmp = FindNumber(data, "ccCheck=", (double)ccCheck)) > -1 )
			ccCheck = (bool)tmp;

		if ( (tmp = FindNumber(data, "cc_commercial_type_modifier="
				, (double)cc_commercial_type_modifier)) > -1 )
			cc_commercial_type_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "cc_wrong_type_modifier="
				, (double)cc_wrong_type_modifier)) > -1 )
			cc_wrong_type_modifier = (double)tmp;

		if ( (tmp = FindNumber(data, "cc_correct_type_modifier="
				, (double)cc_correct_type_modifier)) > -1 )
			cc_correct_type_modifier = (double)tmp;

		_iniText += "[Live TV]\n";

		if ( (tmp = FindNumber(data, "live_tv="
				, (double)live_tv)) > -1 )
			live_tv = (bool) tmp;

#ifdef LIVE_TV_RETRIES
		if ( (tmp = FindNumber(data, "live_tv_retries="
				, (double)live_tv_retries)) > -1 )
			live_tv_retries = (int)tmp;

		if ( (tmp = FindNumber(data, "dvrms_live_tv_retries="
				, (double)dvrms_live_tv_retries)) > -1 )
			dvrms_live_tv_retries = (int)tmp;
#endif

		if ( (tmp = FindNumber(data, "standoff="
				, (double)standoff)) > -1 )
			standoff = (int)tmp;

		if ( (tmp = FindNumber(data, "dvrmsstandoff="
				, (double)dvrmsstandoff)) > -1 )
			dvrmsstandoff = (int)tmp;

		// set_standoff(live_tv_retries, standoff, live_tv);

		if ( (tmp = FindNumber(data, "require_div5="
				, (double)require_div5)) > -1 )
			require_div5 = (bool)tmp;

		if ( (tmp = FindNumber(data, "div5_tolerance="
				, (double)div5_tolerance)) > -1 )
			div5_tolerance = tmp;

		if ( (tmp = FindNumber(data, "incommercial_frames="
				, (double)incommercial_frames)) > -1 )
			incommercial_frames = (int)tmp;

		_iniText += "[Output Control]\n";

		if ( (tmp = FindNumber(data, "output_default="
				, (double)flagFiles._default.use)) > -1 )
			flagFiles._default.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_chapters="
				, (double)flagFiles.chapters.use)) > -1 )
			flagFiles.chapters.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_plist_cutlist="
				, (double)flagFiles.plist_cutlist.use)) > -1 )
			flagFiles.plist_cutlist.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_zoomplayer_cutlist="
				, (double)flagFiles.zoomplayer_cutlist.use)) > -1 )
			flagFiles.zoomplayer_cutlist.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_zoomplayer_chapter="
				, (double)flagFiles.zoomplayer_chapter.use)) > -1 )
			flagFiles.zoomplayer_chapter.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_vcf="
				, (double)flagFiles.vcf.use)) > -1 )
			flagFiles.vcf.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_vdr="
				, (double)flagFiles.vdr.use)) > -1 )
			flagFiles.vdr.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_projectx="
				, (double)flagFiles.projectx.use)) > -1 )
			flagFiles.projectx.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_avisynth="
				, (double)flagFiles.avisynth.use)) > -1 )
			flagFiles.avisynth.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_videoredo="
				, (double)flagFiles.videoredo.use)) > -1 )
			flagFiles.videoredo.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "videoredo_offset="
				, (double)videoredo_offset)) != -1 )
			videoredo_offset = (int)tmp;

		if ( (tmp = FindNumber(data, "output_btv="
				, (double)flagFiles.btv.use)) > -1 )
			flagFiles.btv.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_edl="
				, (double)flagFiles.edl.use)) > -1 )
			flagFiles.edl.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "edl_offset="
				, (double)_edlOffset)) != -1 )
			_edlOffset = (int)tmp;

		if ( (tmp = FindNumber(data, "output_edlp="
				, (double)flagFiles.edlp.use)) > -1 )
			flagFiles.edlp.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_bsplayer="
				, (double)flagFiles.bsplayer.use)) > -1 )
			flagFiles.bsplayer.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_edlx="
				, (double)flagFiles.edlx.use)) > -1 )
			flagFiles.edlx.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_cuttermaran="
				, (double)flagFiles.cuttermaran.use)) > -1 )
			flagFiles.cuttermaran.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_mpeg2schnitt="
				, (double)flagFiles.mpeg2schnitt.use)) > -1 )
			flagFiles.mpeg2schnitt.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_womble="
				, (double)flagFiles.womble.use)) > -1 )
			flagFiles.womble.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_mls="
				, (double)flagFiles.mls.use)) > -1 )
			flagFiles.mls.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_mpgtx="
				, (double)flagFiles.mpgtx.use)) > -1 )
			flagFiles.mpgtx.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_dvrmstb="
				, (double)flagFiles.dvrmstb.use)) > -1 )
			flagFiles.dvrmstb.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_dvrcut="
				, (double)flagFiles.dvrcut.use)) > -1 )
			flagFiles.dvrcut.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_ipodchap="
				, (double)flagFiles.ipodchap.use)) > -1 )
			flagFiles.ipodchap.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_framearray="
				, (double)flagFiles.framearray.use)) > -1 )
			flagFiles.framearray.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_debugwindow="
				, (double)flagFiles.debugwindow.use)) > -1 )
			flagFiles.debugwindow.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_tuning="
				, (double)flagFiles.tuning.use)) > -1 )
			flagFiles.tuning.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_training="
				, (double)flagFiles.training.use)) > -1 )
			flagFiles.training.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_false="
				, (double)flagFiles._false.use)) > -1 )
			flagFiles._false.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_aspect="
				, (double)flagFiles.aspect.use)) > -1 )
			flagFiles.aspect.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_demux="
				, (double)flagFiles.demux.use)) > -1 )
			flagFiles.demux.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_data="
				, (double)flagFiles.data.use)) > -1 )
			flagFiles.data.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_srt="
				, (double)flagFiles.srt.use)) > -1 )
			flagFiles.srt.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_smi="
				, (double)flagFiles.smi.use)) > -1 )
			flagFiles.smi.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "output_timing="
				, (double)flagFiles.timing.use)) > -1 )
			flagFiles.timing.use = (bool)tmp;

		if ( (tmp = FindNumber(data, "delete_logo_file="
				, (double)deleteLogoFile)) > -1 )
			deleteLogoFile = (int)tmp;

		if ( (tmp = FindNumber(data, "cutscene_frame="
				, (double)_cutSceneNo)) > -1 )
			_cutSceneNo = (int)tmp;

		if ( (ts = FindString(data, "cutscene_dumpfile=", "")) != 0 )
			_cutSceneFile[0] = ts;

		if ( (tmp = FindNumber(data, "cutscene_threshold="
				, (double)CutScene::delta)) > -1 )
			CutScene::delta = (int)tmp;

		for ( int i = 1; i <= 8; ++i )
		{
			char buf[32];
			::snprintf(buf, sizeof(buf), "cutscenefile%d=", i);

			if ( (ts = FindString(data, buf, "")) != 0 )
				_cutSceneFile[i] = ts;

			if ( _cutSceneFile[i] != "" )
				loadCutScene(_cutSceneFile[i]);
		}

		if ( (ts = FindString(data, "windowtitle=", windowtitle)) != 0 )
			strcpy(windowtitle,ts);

		if ( (ts = FindString(data, "cuttermaran_options="
				, cuttermaran_options)) != 0 )
			::strcpy(cuttermaran_options, ts);

		if ( (ts = FindString(data, "mpeg2schnitt_options="
				, mpeg2schnitt_options)) != 0 )
			::strcpy(mpeg2schnitt_options, ts);

		if ( (ts = FindString(data, "avisynth_options="
				, avisynth_options)) != 0 )
			::strcpy(avisynth_options, ts);

		if ( (ts = FindString(data, "dvrcut_options="
				, dvrcut_options)) != 0 )
			::strcpy(dvrcut_options, ts);

		_iniText += "[Sage Workarounds]\n";

		if ( (tmp = FindNumber(data, "sage_framenumber_bug="
				, (double)sage_framenumber_bug)) > -1 )
			sage_framenumber_bug = (bool)tmp;

		if ( (tmp = FindNumber(data, "sage_minute_bug="
				, (double)sage_minute_bug)) > -1 )
			sage_minute_bug = (bool)tmp;

		if ( (tmp = FindNumber(data, "enable_mencoder_pts="
				, (double)enable_mencoder_pts)) > -1 )
			enable_mencoder_pts = (bool)tmp;
	}
	else
	{
		::printf("No INI file found anywhere!!!!\n");
	}

	if ( live_tv )
		flagFiles.incommercial.use = true;

	if ( added_recording > 0 && giveUpOnLogoSearch < added_recording * 60 )
		giveUpOnLogoSearch += added_recording * 60;
}

void CommercialSkipper::loadCutScene(const std::string &fname)
{
	const char *filename(fname.c_str());
	FILE *f(fopen(filename, "rb"));

	if ( f )
	{
		CutScene cs;

		::fread(&cs.brightness, sizeof(int), 1, f);
		int c(::fread(cs.data, sizeof(uint8_t), MAXCSLENGTH, f));

		if ( c > 0 )
		{
			Debug(7, "Loaded %i bytes from cutfile \"%s\"\n", c, filename);
			cs.length = c;
#if 0
			int b(0);

			for ( int j = 0; j < c; ++j )
				b += cs.data[j];

			cs.brightness = b / c;
#endif
			_cutScenes.push_back(cs);
		}

		fclose(f);
	}
}

void CommercialSkipper::loadLogoMaskData(const std::string &filename)
{
	const char *logofilename(filename.c_str());
	FILE *f(::fopen(logofilename, "r"));
	char data[2000];

	if ( f )
	{
		Debug(1, "Using %s for logo data.\n", logofilename);
		size_t len(fread(data, 1, 1999, f));
		fclose(f);
		data[len] = '\0';
		double tmp;

		if ( (tmp = FindNumber(data, "picWidth="
				, (double)_width)) > -1 )
			_videoWidth = _width = (int)tmp;

		if ( (tmp = FindNumber(data, "picHeight="
				, (double)_height)) > -1 )
			_height = (int)tmp;

		if ( (tmp = FindNumber(data, "logoMinX="
				, (double) _logoRange.x.min) ) > -1 )
			_logoRange.x.min = (int)tmp;

		if ( (tmp = FindNumber(data, "logoMaxX="
				, (double) _logoRange.x.max)) > -1 )
			_logoRange.x.max = (int)tmp;

		if ( (tmp = FindNumber(data, "logoMinY="
				, (double) _logoRange.y.min)) > -1 )
			_logoRange.y.min = (int)tmp;

		if ( (tmp = FindNumber(data, "logoMaxY="
				, (double) _logoRange.y.max)) > -1 )
			_logoRange.y.max = (int)tmp;
	}
	else
	{
		Debug(0, "Could not find the logo file.\n");
		_logoInfoAvailable = false;
		return;
	}

	f = ::fopen(logofilename, "r");

#if 0
	_edgeMask.cHor = malloc(_width * _height * sizeof(uint8_t));

	if ( ! _edgeMask.cHor )
	{
		Debug(0, "Could not allocate memory for horizontal edgemask\n");
		exit(8);
	}

	_edgeMask.cVer = malloc(_width * _height * sizeof(uint8_t));

	if ( ! _edgeMask.cVer )
	{
		Debug(0, "Could not allocate memory for vertical edgemask\n");
		exit(9);
	}

	memset(_edgeMask.cHor, 0, _width * _height);
	memset(_edgeMask.cVer, 0, _width * _height);
#endif

	char temp;

	do
	{
		temp = getc(f);
	}
	while ( (temp != '\200') && !feof(f) );

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
	{
		for ( int x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
		{
			temp = getc(f);

			// If a carriage return was retrieved, get the next character
			if ( temp == '\n' )
				temp = getc(f);

			_edgeMask.getCHor().set(x, y, temp == '|');
		}
	}

	::fclose(f);

	f = ::fopen(logofilename, "r");

	do
	{
		temp = getc(f);
	}
	while ( (temp != '\201') && !feof(f) );

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
	{
		for ( int x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
		{
			temp = getc(f);

			// If a carrage return was retrieved, get the next character
			if ( temp == '\n' )
				temp = getc(f);

			_edgeMask.getCVer().set(x, y, temp == '-');
		}
	}

	::fclose(f);

	f = ::fopen(logofilename, "r");

	do
	{
		temp = getc(f);
	}
	while ( (temp != '\202') && !feof(f) );

	if ( ! feof(f) )
	{
		for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
		{
			for ( int x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
			{
				temp = getc(f);

				// If a carrage return was retrieved, get the next character
				if ( temp == '\n' )
					temp = getc(f);

				switch ( temp )
				{
				case ' ':
					_edgeMask.getCHor().set(x, y, false);
					_edgeMask.getCVer().set(x, y, false);
					break;

				case '-':
					_edgeMask.getCHor().set(x, y, false);
					_edgeMask.getCVer().set(x, y, true);
					break;

				case '|':
					_edgeMask.getCHor().set(x, y, true);
					_edgeMask.getCVer().set(x, y, false);
					break;

				case '+':
					_edgeMask.getCHor().set(x, y, true);
					_edgeMask.getCVer().set(x, y, true);
					break;
				}
			}
		}
	}

	::fclose(f);
	_logoInfoAvailable = true;
	// prevent continuous searching for logo when a logo file is specified
	startOverAfterLogoInfoAvail = true;
	secondLogoSearch = true;
	initScanLines();
	initHasLogo();
	_isSecondPass = true;

	if ( ! _isLoadingCsv )
	{
		// dumpEdgeMask(_edgeMask.cHor, HORIZ);
		// dumpEdgeMask(_edgeMask.cVer, VERT);
		dumpEdgeMasks();
	}

	::memset(data, 0, sizeof(data));

#ifdef _WIN32
	_flushall();
#endif

	if ( flagFiles._default.use )
	{
		FILE *txt_file(fopen(_outFilename.c_str(), "r"));
#ifdef _WIN32
		if ( ! txt_file )
		{
			Sleep(50L);
			txt_file = fopen(_outFilename.c_str(), "r");
		}
#endif
		if ( ! txt_file )
		{
			Debug(0, "ERROR reading from %s\n", _outFilename.c_str());
			_isSecondPass = false;
			return;
		}

		if ( fseek(txt_file, 0L, SEEK_SET) )
			Debug(0, "ERROR SEEKING\n");

		while ( fgets(data, 1999, txt_file) )
		{
			if ( strstr(data, "FILE PROCESSING COMPLETE") )
			{
				lastFrame = 0;
				break;
			}

			char *p(strchr(data, '\t'));

			if ( p )
			{
				int n(strtol(++p, 0, 10));

				if ( n > lastFrame )
					lastFrame = n;
			}
		}

		fclose(txt_file);
	}

	Debug(10, "The last frame found in %s was %i\n", _outFilename.c_str(), lastFrame);
}

double CommercialSkipper::FindNumber(const char *str1, const char *str2, double v)
{
	if ( ! str1 )
		return -1;

	char tmp[256];
	double res(-1);
	bool negative(false);

	if ( (str1 = strstr(str1, str2)) )
	{
		str1 += strlen(str2);

		while ( isspace(*str1) )
			++str1;

		if ( *str1 == '-' )
		{
			++str1;
			negative = true;
		}

		res = (negative) ? -atof(str1) : atof(str1);

		::snprintf(tmp, sizeof(tmp), "%s%0f\n", str2, res);
	}
	else
	{
		::snprintf(tmp, sizeof(tmp), "%s%0f\n", str2, v);
	}

	int i(strlen(tmp));

	while ( i >= 2 && tmp[i-2] == '0' )
	{
		tmp[i - 1] = 0;
		tmp[(i--) - 2] = '\n';
	}

	if ( i >= 2 && tmp[i-2] == '.' )
	{
		tmp[i - 1] = 0;
		tmp[(i--) - 2] = '\n';
	}

	_iniText += tmp;
	return res;
}

const char *CommercialSkipper::FindString(const char *str1, const char *str2, const char *v)
{
	if ( ! str1 )
		return 0;

	static char foundText[1024];
	char tmp[255];
	bool found(false);

	if ( (str1 = strstr(str1, str2)) )
	{
		str1 += strlen(str2);

		while ( isspace(*str1) )
			++str1;

		if ( *str1 == '"' )
		{
			char *tt = foundText;
			++str1;

			while ( *str1 != '"' && *str1 != 0 && *str1 != 10 )
			{
				if ( *str1 != '\\' || *(str1+1) != 'n' )
					*tt++ = *str1++;
				else
				{
					*tt++ = '\n';
					str1 += 2;
				}
			}

			*tt++ = 0;
			v = foundText;
			found = true;
			::snprintf(tmp, sizeof(tmp), "%s\"%s\"\n", str2, foundText);
			// strcat(_iniText, tmp);
			// return foundText;
		}
	}

	char *t(tmp);

	while ( *str2 )
		*t++ = *str2++;

	*t++ = '"';

	while ( *v )
	{
		switch( *v )
		{
		case '"':
		case '\\':
			*(t++) = '\\';
			*t++ = *v++;
			break;
		case '\n':
			*t++ = '\\';
			*t++ = 'n';
			v++;
			break;
		case '\t':
			*t++ = '\\';
			*t++ = 't';
			v++;
			break;
		default:
			*(t++) = *v++;
		}
	}

	*t++ = '"';
	*t++ = '\n';
	*t++ = 0;
	// snprintf(tmp, sizeof(tmp), "%s\"%s\"\n", str2, v);
	_iniText += tmp;

	return ( found ) ?  foundText : 0;
}

void CommercialSkipper::dumpEdgeMasks(void)
{
	std::vector<char> outbuf(_width + 1);

	{
		int x;

		for ( x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
			outbuf[x - _logoRange.x.min] = '0'+ (x % 10);

		outbuf[x - _logoRange.x.min] = 0;
		Debug(1, "%s\n", outbuf.data());
	}

	for ( int y = _logoRange.y.min; y <= _logoRange.y.max; ++y )
	{
		Debug(1, "%3d: ", y);

		int x;

		for ( x = _logoRange.x.min; x <= _logoRange.x.max; ++x )
		{
			if ( _edgeMask.getConstCHor().get(x, y) )
			{
				outbuf[x - _logoRange.x.min]
						= ( _edgeMask.getConstCVer().get(x, y) )
							? '+'
							: '|';
			}
			else
			{
				outbuf[x - _logoRange.x.min]
						=  ( _edgeMask.getConstCVer().get(x, y) )
							? '-'
							: ' ';
			}
		}

		outbuf[x - _logoRange.x.min] = 0;
		Debug(1, "%s\n", outbuf.data());
	}
}

} // namespace CS

