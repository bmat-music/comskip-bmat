#pragma once

#include "CsTypes.h"
#include "CsOptions.h"
#include "CsExceptions.h"

namespace CS {

class Edges
{
public:
	Edges()
		: _threshold(5)
		, _radius(2)
		, _weight(10)
		, _step(1)
	{}
	Edges(const Edges &o)
		: _threshold(o._threshold)
		, _radius(o._radius)
		, _weight(o._weight)
		, _step(o._step)
	{}

	template<class T>
	bool testHor0(const T &f, int x, int y, int width) const
	{
		const int yOff(y * width);
		const int xMinus(x - _radius);
		const int xPlus(x + _radius);

#if USE_RANGE_CHECKING
		checkBounds(x, y);
		checkBounds(xMinus, y);
		checkBounds(xPlus, y);
#endif

		return (std::abs(f[yOff + xMinus] - f[yOff + x] ) >= _threshold)
			|| (std::abs(f[yOff + xPlus] - f[yOff + x] ) >= _threshold);
	}

	bool testHor0(const U8Vec &f, int x, int y, int width) const
	{
		return testHor0<U8Vec>(f, x, y, width);
	}

	bool testHor0(const uint8_t *f, int x, int y, int width) const
	{
		return testHor0<const uint8_t *>(f, x, y, width);
	}

	template<class T>
	bool testVer0(const T &f, int x, int y, int width) const
	{
		const int yMinus(y - _radius);
		const int yPlus(y + _radius);

#if USE_RANGE_CHECKING
		checkBounds(x, y);
		checkBounds(x, yMinus);
		checkBounds(x, yPlus);
#endif

		return (std::abs(f[yMinus * width + x] - f[y * width + x]) >= _threshold)
			|| (std::abs(f[yPlus * width + x] - f[y * width + x]) >= _threshold);
	}

	bool testVer0(const U8Vec &f, int x, int y, int width) const
	{
		return testVer0<U8Vec>(f, x, y, width);
	}

	bool testVer0(const uint8_t *f, int x, int y, int width) const
	{
		return testVer0<const uint8_t *>(f, x, y, width);
	}

	template<class T>
	bool testHor1(const T &f, int x, int y, int width) const
	{
		const int yOff(y * width);
		const int xMinus(x - _radius);
		const int xPlus(x + _radius);

#if USE_RANGE_CHECKING
		checkBounds(x, y);
		checkBounds(xMinus, y);
		checkBounds(xPlus, y);
#endif

		return std::abs(f[yOff + xMinus] - f[yOff + xPlus] ) >= _threshold;
	}

	bool testHor1(const U8Vec &f, int x, int y, int width) const
	{
		return testHor1<U8Vec>(f, x, y, width);
	}

	bool testHor1(const uint8_t *f, int x, int y, int width) const
	{
		return testHor1<const uint8_t *>(f, x, y, width);
	}

	template<class T>
	bool testVer1(const T &f, int x, int y, int width) const
	{
		const int yMinus(y - _radius);
		const int yPlus(y + _radius);

#if USE_RANGE_CHECKING
		checkBounds(x, y);
		checkBounds(x, yMinus);
		checkBounds(x, yPlus);
#endif
		return std::abs(f[yMinus * width + x] - f[yPlus * width + x]) >= _threshold;
	}

	bool testVer1(const U8Vec &f, int x, int y, int width) const
	{
		return testVer1<U8Vec>(f, x, y, width);
	}

	bool testVer1(const uint8_t *f, int x, int y, int width) const
	{
		return testVer1<const uint8_t *>(f, x, y, width);
	}

	template<class T>
	bool testHor2(const T &f, int x, int y, int width) const
	{
		const int yOff(y * width);
		const int xMinus(x - _radius);
		const int xMinusMinus(xMinus - 1);
		const int xMinusPlus(xMinus + 1);
		const int xPlus(x + _radius);
		const int xPlusMinus(xPlus - 1);
		const int xPlusPlus(xPlus + 1);

#if USE_RANGE_CHECKING
		checkBounds(x, y);
		checkBounds(xMinusMinus, y);
		checkBounds(xPlusPlus, y);
#endif

		return std::abs( (f[yOff + xMinusMinus]
					+ f[yOff + xMinus]
					+ f[yOff + xMinusPlus])
				- (f[yOff + xPlusMinus]
					+ f[yOff + xPlus]
					+ f[yOff + xPlusPlus]))
				/ 3 >= _threshold;
	}

	bool testHor2(const U8Vec &f, int x, int y, int width) const
	{
		return testHor2<U8Vec>(f, x, y, width);
	}

	bool testHor2(const uint8_t *f, int x, int y, int width) const
	{
		return testHor2<const uint8_t *>(f, x, y, width);
	}

	template<class T>
	bool testVer2(const T &f, int x, int y, int width) const
	{
		const int yMinus(y - _radius);
		const int yMinusMinus(yMinus - 1);
		const int yMinusPlus(yMinus + 1);
		const int yPlus(y + _radius);
		const int yPlusMinus(yPlus - 1);
		const int yPlusPlus(yPlus + 1);

#if USE_RANGE_CHECKING
		checkBounds(x, y);
		checkBounds(x, yMinusMinus);
		checkBounds(x, yPlusPlus);
#endif

		return std::abs((f[yMinusMinus * width + x]
					+ f[yMinus * width + x]
					+ f[yMinusPlus * width + x])
				- (f[yPlusMinus * width + x]
					+ f[yPlus * width + x]
					+ f[yPlusPlus * width + x]))
				/3 >= _threshold;
	}

	bool testVer2(const U8Vec &f, int x, int y, int width) const
	{
		return testVer2<U8Vec>(f, x, y, width);
	}

	bool testVer2(const uint8_t *f, int x, int y, int width) const
	{
		return testVer2<const uint8_t *>(f, x, y, width);
	}

	template<class T>
	bool testHor3(const T &f, int x, int y, int width) const
	{
		const int yMinus(y - _radius);
		const int yPlus(y + _radius);
		const int xMinus(x - _radius);
		const int xPlus(x + _radius);

#if USE_RANGE_CHECKING
		checkBounds(xMinus, yMinus);
		checkBounds(xPlus, yPlus);
#endif

		return std::abs((f[yMinus * width + xMinus]
						- f[yMinus * width + xPlus]
						+ f[y * width + xMinus]
						- f[y * width + xPlus]
						+ f[yPlus * width + xMinus]
						- f[yPlus * width + xPlus]))
					>= _threshold;

	}

	bool testHor3(const U8Vec &f, int x, int y, int width) const
	{
		return testHor3<U8Vec>(f, x, y, width);
	}

	bool testHor3(const uint8_t *f, int x, int y, int width) const
	{
		return testHor3<const uint8_t *>(f, x, y, width);
	}

	template<class T>
	bool testVer3(const T &f, int x, int y, int width) const
	{
		const int yMinus(y - _radius);
		const int yPlus(y + _radius);
		const int xMinus(x - _radius);
		const int xPlus(x + _radius);

#if USE_RANGE_CHECKING
		checkBounds(xMinus, yMinus);
		checkBounds(xPlus, yPlus);
#endif

		return std::abs(f[yMinus * width + xMinus]
						- f[yPlus * width + xMinus]
						+ f[yMinus * width + x]
						- f[yPlus * width + x]
						+ f[yMinus * width + xPlus]
						- f[yPlus * width + xPlus])
					>= _threshold;
	}

	bool testVer3(const U8Vec &f, int x, int y, int width) const
	{
		return testVer3<U8Vec>(f, x, y, width);
	}

	bool testVer3(const uint8_t *f, int x, int y, int width) const
	{
		return testVer3<const uint8_t *>(f, x, y, width);
	}

	int getThreshold(void) const { return _threshold; }
	void setThreshold(int value) { _threshold = value; }
	int getRadius(void) const { return _radius; }
	void setRadius(int value) { _radius = value; }
	int getWeight(void) const { return _weight; }
	void setWeight(int value) { _weight = value; }
	int getStep(void) const { return _step; }
	void setStep(int value) { _step = value; }

private:
	void checkBounds(int x, int y) const;

	int _threshold;
	int _radius;
	int _weight;
	int _step;
};

} // namespace CS

