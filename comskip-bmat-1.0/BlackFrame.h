#pragma once
#include <stdint.h>
#include <vector>

namespace CS {

class BlackFrame
{
public:
#if USE_NEW_CPP_CONSTRUCTORS
	BlackFrame()
		: BlackFrame(0, 0, 0, 0, 0)
		{}
	BlackFrame(const BlackFrame &o)
		: BlackFrame(o.frame, o.cause, o.brightness, o.uniform, o.volume)
		{}
	BlackFrame(int f, int c, int b, int u, int v)
		: frame(f)
		  , cause(c)
		  , brightness(b)
		  , uniform(u)
		  , volume(v)
		{}
#else
	BlackFrame()
		{ init(0, 0, 0, 0, 0); }
	BlackFrame(const BlackFrame &o)
		{ init(o.frame, o.cause, o.brightness, o.uniform, o.volume); }
	BlackFrame(int f, int c, int b, int u, int v)
		{ init(f, c, b, u, v); }

	void init(int f, int c, int b, int u, int v)
	{
		frame = f;
		cause = c;
		brightness = b;
		uniform = u;
		volume = v;
	}
#endif

	~BlackFrame() {}

	friend bool operator < (const BlackFrame &f1, const BlackFrame &f2);
	friend bool operator > (const BlackFrame &f1, const BlackFrame &f2);
	friend bool operator == (const BlackFrame &f1, const BlackFrame &f2);

	int frame;
	int cause;
	int brightness;
	int uniform;
	int volume;
};

typedef std::vector<BlackFrame> BlackVec;

} // namespace CS

