#pragma once

#include <string>
#include <stdexcept>

namespace CS {

class FileException : public std::runtime_error
{
public:
	FileException(const std::string &msg) : std::runtime_error(msg) {}
	FileException(const FileException &o) : std::runtime_error(o) {}
	virtual ~FileException() throw() {}

	const char *what() const throw()
	{
		return std::runtime_error::what();
	}
};

class FormatException : public std::runtime_error
{
public:
	FormatException(const std::string &msg) : std::runtime_error(msg) {}
	FormatException(const FormatException &o) : std::runtime_error(o) {}
	virtual ~FormatException() throw() {}

	const char *what() const throw()
	{
		return std::runtime_error::what();
	}
};

} // namespace CS

