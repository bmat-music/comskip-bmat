#ifndef AVFORMAT_INTTYPES_H
#define AVFORMAT_INTTYPES_H

#ifdef __unix__
#include <stdint.h>
#else
typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
typedef signed __int64 int64_t;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned __int64 uint64_t;
#endif


//#define EINVAL  -1  /**< unknown error */
//#define EIO		-2     /**< I/O error */
//#define EDOM 	-3    /**< Number syntax expected in filename. */
//#define ENOENT	-4
//#define ENOMEM 	-5  /**< not enough memory */
//#define EILSEQ 	-6  /**< unknown format */
//#define ENOSYS 	-7
//#define EPIPE	-8

#endif
