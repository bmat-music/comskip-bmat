#include "CsEdges.h"
#include "CsExceptions.h"

namespace CS {

void Edges::checkBounds(int x, int y) const
{
	if ( y < 0 )
		throw std::out_of_range("y is less than zero");
	else if ( y >= 1080 )
		throw std::out_of_range("y is greater than or equal to 1080");

	if ( x < 0 )
		throw std::out_of_range("x is less than zero");
	else if ( x >= 1920 )
		throw std::out_of_range("x is greater than or equal to 1920");
}

} // namespace CS

