#pragma once

#include "CsTypes.h"
#include "CsOptions.h"
#include "CsEdges.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>

#ifdef __unix__
#if ! defined(MAX_PATH)
#define MAX_PATH 4096
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <unistd.h>
#define _read read
#define _write write
#define _close close
#define _cprintf printf
#define _getcwd getcwd
#define _stat stat
#endif
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include "BlackFrame.h"
#include "CcPacket.h"
#include "CsExceptions.h"

#define MAXCSLENGTH (400*300)
#define MAXCUTSCENES 8
#define MAX_ASPECT_RATIOS 1000
#define AR_TREND 0.8

#ifdef _WIN32
#define EXEFILENAME "comskip.exe"
#define DIRSEP '\\'
#define DIRSEPSTR "\\"
#define CS_TMPPATH "v:\\"
#else
#define EXEFILENAME "comskip"
#define DIRSEP '/'
#define DIRSEPSTR "/"
#define CS_TMPPATH "/tmp/"
#endif

namespace CS {

#define		C_c			(1<<1)
#define		C_l			(1<<0)
#define		C_s			(1<<2)
#define		C_a			(1<<5)
#define		C_u			(1<<3)
#define		C_b			(1<<4)
#define		C_r			((int)1<<29)

#define		C_STRICT	(1<<6)
#define		C_NONSTRICT	(1<<7)
#define		C_COMBINED	(1<<8)
#define		C_LOGO		(1<<9)
#define		C_EXCEEDS	(1<<10)
#define		C_AR		(1<<11)
#define		C_SC		(1<<12)
#define		C_H1		(1<<13)
#define		C_H2		(1<<14)
#define		C_H3		(1<<15)
#define		C_H4		((int)1<<16)
#define		C_H5		((int)1<<17)
#define		C_H6		((int)1<<22)
#define		C_v			((int)1<<18)
#define		C_BRIGHT	((int)1<<19)
#define		C_NOTBRIGHT	((int)1<<20)
#define		C_DIM		((int)1<<21)
#define		C_AB		((int)1<<23)
#define		C_AU		((int)1<<24)
#define		C_AL		((int)1<<25)
#define		C_AS		((int)1<<26)
#define		C_AC		((int)1<<26)
#define		C_F			((int)1<<27)
#define		C_t			((int)1<<28)
#define		C_H7		((int)1<<29)

#define C_CUTMASK	(C_c | C_l | C_s | C_a | C_u | C_b | C_t | C_r)
#define CUTCAUSE(c) ( c & C_CUTMASK)
#define UNIFORMSCALE 100

std::string secondsToString(double seconds);
std::string secondsToString(int seconds);

// Define detection methods
class DET
{
public:
	static const int BLACK_FRAME;
	static const int LOGO;
	static const int SCENE_CHANGE;
	static const int RESOLUTION_CHANGE;
	static const int CC;
	static const int AR;
	static const int SILENCE;
	static const int CUTSCENE;
};

class MinMax
{
public:
	MinMax() : min(0), max(0) {}
	MinMax(int min, int max) : min(min), max(max) {}
	MinMax(const MinMax &o) : min(o.min), max(o.max) {}
	void set(int min, int max) { this->min = min; this->max = max; }
	int getDiff(void) const { return max - min; }
	bool isInRangeInclusive(int n) const { return n >= min && n <= max; }
	int min;
	int max;
};

class MinMaxBright
{
public:
	MinMaxBright() : min(255), max(0) {}
	MinMaxBright(const MinMaxBright &o) : min(o.min), max(o.max) {}

	void set(uint8_t n)
	{
		if ( n < min )
			min = n;

		if ( n > max )
			max = n;
	}

	uint8_t min;
	uint8_t max;
};

class MinMaxDouble
{
public:
    MinMaxDouble() : min(0.0), max(0.0) {}
    MinMaxDouble(double min, double max)
		: min(min), max(max) {}
    MinMaxDouble(const MinMaxDouble &o)
		: min(o.min), max(o.max) {}
    double min;
    double max;
};

class CommercialBreakSize
{
public:
	CommercialBreakSize()
		: _break(20, 600) // min/max in seconds
		, _size(4, 120)
	{
	}

	MinMaxDouble _break;
	MinMaxDouble _size;
};

class Rectangle
{
public:
	Rectangle() : x(), y() {}
	Rectangle(const Rectangle &o) : x(o.x), y(o.y) {}
	int getWidth(void) const { return x.getDiff(); }
	int getHeight(void) const { return y.getDiff(); }

	std::string getString(void) const
	{
		char buf[64];

		::snprintf(buf, sizeof(buf)
				, "%3d,%3d - %3d,%3d"
				, x.min, y.min, x.max, y.max);

		return buf;
	}

	MinMax x;
	MinMax y;
};

class CutScene
{
public:
	CutScene() : brightness(0), length(0) {}
	CutScene(const CutScene &o)
		: brightness(o.brightness)
		, length(o.length)
	{
		::memcpy(data, o.data, sizeof(data));
	}

	uint8_t data[MAXCSLENGTH];
	int brightness;
	int length;
	static int delta;
};

typedef std::vector<CutScene> CutScenes;

class LogoBlockInfo
{
public:
	int start;
	int end;
};

class PlayNice
{
public:
	PlayNice()
		: use(false)
		, start(-1)
		, end(-1)
		, sleep(10)
	{}

	bool use;
	int start;
	int end;
	int sleep;
};

class FlagFile
{
public:
	FlagFile(): use(false), f(0) {}
	FlagFile(bool use): use(use), f(0) {}
	~FlagFile();

	FILE *open(const std::string &filename, const char *options
			, bool throwOnError = false) throw(FileException)
	{
		return this->open(filename.c_str(), options, throwOnError);
	}

	FILE *open(const char *filename, const char *options
			, bool throwOnError = false) throw(FileException);
	void printf(const char *fmt, ...) __attribute__ ((__format__ (__printf__, 2, 3)));

	void close(void);

	bool use;
	FILE *f;
};

class FlagFiles
{
public:
	FlagFiles()
		: _default(true)
		, in(false)
		, out(false)
		, ini(false)
		, console(true)
		, timing(false)
		, debugwindow(false)
		, framearray(false)
		, plist_cutlist(false)
		, zoomplayer_cutlist(false)
		, zoomplayer_chapter(false)
		, videoredo(false)
		, ipodchap(false)
		, edl(false)
		, edlp(false)
		, bsplayer(false)
		, edlx(false)
		, btv(false)
		, cuttermaran(false)
		, mpeg2schnitt(false)
		, demux(false)
		, womble(false)
		, mls(false)
		, mpgtx(false)
		, dvrcut(false)
		, dvrmstb(false)
		, vdr(false)
		, vcf(false)
		, projectx(false)
		, avisynth(false)
		, incommercial(false)
		, chapters(false)
		, tuning(false)
		, _false(false)
		, aspect(false)
		, log(false)
		, bcf(false)
		, data(false)
		, training(false)
		, dumpData(false)
		, dumpAudio(false)
		, dumpVideo(false)
		, srt(false)
		, smi(false)
		{}

	FlagFile _default;
	FlagFile in;
	FlagFile out;
	FlagFile ini;
	FlagFile console;
	FlagFile timing;
	FlagFile debugwindow;
	FlagFile framearray;
	FlagFile plist_cutlist;
	FlagFile zoomplayer_cutlist;
	FlagFile zoomplayer_chapter;
	FlagFile videoredo;
	FlagFile ipodchap;
	FlagFile edl;
	FlagFile edlp;
	FlagFile bsplayer;
	FlagFile edlx;
	FlagFile btv;
	FlagFile cuttermaran;
	FlagFile mpeg2schnitt;
	FlagFile demux;
	FlagFile womble;
	FlagFile mls;
	FlagFile mpgtx;
	FlagFile dvrcut;
	FlagFile dvrmstb;
	FlagFile vdr;
	FlagFile vcf;
	FlagFile projectx;
	FlagFile avisynth;
	FlagFile incommercial;
	FlagFile chapters;
	FlagFile tuning;
	FlagFile _false;
	FlagFile aspect;
	FlagFile log;
	FlagFile bcf;
	FlagFile data;
	FlagFile training;
	FlagFile dumpData;
	FlagFile dumpAudio;
	FlagFile dumpVideo;
	FlagFile srt;
	FlagFile smi;
};

extern int commDetectMethod;
extern bool disable_heuristics;
extern int giveUpOnLogoSearch;

extern int lastFrame;
extern int lastFrameCommCalculated;

extern std::string cutSceneFile[];
extern std::string ini_text;

/* implemented in comskip.cpp */
extern char HomeDir[256];
extern bool doProcessCc;

#ifdef __x86_64__
typedef uint64_t BoolMatrixType;
#else
typedef uint32_t BoolMatrixType;
#endif

class BoolMatrix
{
// may throw std::out_of_range
public:
	BoolMatrix()
		: _maxWidth(0)
		, _maxHeight(0)
		, _vecSize(0)
		, _totalSize(0)
		, _matrix()
	{
	}

	BoolMatrix(int maxWidth, int maxHeight)
		: _maxWidth(maxWidth)
		, _maxHeight(maxHeight)
		, _vecSize(0)
		, _totalSize(0)
		, _matrix()
	{
		_vecSize = _maxWidth / (sizeof(BoolMatrixType) * 8);

		if ( (_maxWidth % (sizeof(BoolMatrixType) * 8)) )
			++_vecSize;

		_totalSize = _vecSize * _maxHeight;
		_matrix.resize(_totalSize);
	}

	BoolMatrix(const BoolMatrix &o)
		: _maxWidth(o._maxWidth)
		, _maxHeight(o._maxHeight)
		, _vecSize(0)
		, _totalSize(o._totalSize)
		, _matrix(o._matrix)
	{
	}

	~BoolMatrix()
	{
	}

	void resize(size_t width, size_t height)
	{
		_maxWidth = width;
		_maxHeight = height;

		_vecSize = _maxWidth / (sizeof(BoolMatrixType) * 8);

		if ( (_maxWidth % (sizeof(BoolMatrixType) * 8)) )
			++_vecSize;

		_totalSize = _vecSize * _maxHeight;
		_matrix.resize(_totalSize);
	}

	void initRange(Rectangle range)
	{
		reset();

		for ( int y = range.y.min; y < range.y.max; ++y )
			for ( int x = range.x.min; x < range.x.max ; ++x )
				set(x, y, true);
	}

	void reset(bool value = false)
	{
		BoolMatrixType n(0);

		if ( value )
			n = ~n;

		for ( size_t i = 0; i < _totalSize; ++i )
			_matrix[i] = n;
	}

	void set(int x, int y, bool value)
	{
#if USE_RANGE_CHECKING
		if ( x < 0 || x >= _maxWidth )
			throw std::out_of_range((x < 0) ? "x is less than zero" : "x exceeds max width" );
		if ( y < 0 || y >= _maxHeight )
			throw std::out_of_range((y < 0) ? "y is less than zero" : "y exceeds max height" );
#endif

		const BoolMatrixType yOff(y * _vecSize);
		const BoolMatrixType off(x / (sizeof(BoolMatrixType) * 8));
		const BoolMatrixType rem(x % (sizeof(BoolMatrixType) * 8));
		BoolMatrixType &v(_matrix[yOff + off]);
		const BoolMatrixType ONE(1); // ensure that we have enough bits in shift

		if ( value )
			v |= (ONE << rem);
		else
			v &= ~(ONE << rem);
	}

	bool get(int x, int y) const
	{
#if USE_RANGE_CHECKING
		if ( x < 0 || x >= _maxWidth )
			throw std::out_of_range((x < 0) ? "x is less than zero" : "x exceeds max width" );
		if ( y < 0 || y >= (int)_maxHeight )
			throw std::out_of_range((y < 0) ? "y is less than zero" : "y exceeds max height" );
#endif

		const BoolMatrixType yOff(y * _vecSize);
		const BoolMatrixType ONE(1); // ensure that we have enough bits in shift

		return _matrix[yOff + (x / (sizeof(BoolMatrixType) * 8))]
				& (ONE << (x % (sizeof(BoolMatrixType) * 8)));
	}

private:
	int _maxWidth;
	int _maxHeight;
	size_t _vecSize;
	size_t _totalSize;
	std::vector<BoolMatrixType> _matrix;
};

class HorVerType
{
public:
	HorVerType() : _hor(0), _ver(0) {}
	HorVerType(const HorVerType &o) : _hor(o._hor), _ver(o._ver) {}
	void reset(void) { _hor = 0; _ver = 0; }
	void setHor(uint8_t value) { _hor = value; }
	void setVer(uint8_t value) { _ver = value; }
	void incHor(void) { ++_hor; }
	void incVer(void) { ++_ver; }
	uint8_t getHor(void) const { return _hor; }
	uint8_t getVer(void) const { return _ver; }
private:
	uint8_t _hor;
	uint8_t _ver;
};

class HorVerMatrix
{
// may throw std::out_of_range
public:
	HorVerMatrix()
		: _maxWidth(0)
		, _maxHeight(0)
		, _totalSize(0)
		, _matrix()
	{
	}

	HorVerMatrix(int maxWidth, int maxHeight)
		: _maxWidth(maxWidth)
		, _maxHeight(maxHeight)
		, _totalSize(maxWidth*maxHeight)
		, _matrix(_totalSize)
	{
	}

	HorVerMatrix(const HorVerMatrix &o)
		: _maxWidth(o._maxWidth)
		, _maxHeight(o._maxHeight)
		, _totalSize(o._totalSize)
		, _matrix(o._matrix)
	{
	}

	~HorVerMatrix()
	{
	}

	void resize(size_t width, size_t height)
	{
		_maxWidth = width;
		_maxHeight = height;
		_totalSize = width * height;
		_matrix.resize(_totalSize);
	}

	void reset(void)
	{
		for ( size_t i = 0; i < _totalSize; ++i )
			_matrix[i].reset();
	}

	HorVerType &get(int x, int y)
	{
#if ! USE_RANGE_CHECKING
		return _matrix[y * _maxWidth + x];
#else
		if ( x >= 0 && x < _maxWidth && y >= 0 && y < _maxHeight )
			return _matrix[y * _maxWidth + x];

		if ( x < 0 || x >= _maxWidth )
			throw std::out_of_range((x < 0) ? "x is less than zero" : "x exceeds max width" );

		throw std::out_of_range((y < 0) ? "y is less than zero" : "y exceeds max height" );

		return _matrix[0];
#endif
	}

	const HorVerType &getConst(int x, int y) const
	{
#if ! USE_RANGE_CHECKING
		return _matrix[y * _maxWidth + x];
#else
		if ( x >= 0 && x < _maxWidth && y >= 0 && y < _maxHeight )
			return _matrix[y * _maxWidth + x];

		if ( x < 0 || x >= _maxWidth )
			throw std::out_of_range((x < 0) ? "x is less than zero" : "x exceeds max width" );

		throw std::out_of_range((y < 0) ? "y is less than zero" : "y exceeds max height" );

		return _matrix[0];
#endif
	}

private:
	int _maxWidth;
	int _maxHeight;
	size_t _totalSize;
	std::vector<HorVerType> _matrix;
};

class EdgeMask
{
public:
	EdgeMask()
		: _tHor()
		, _tVer()
		, _cHor()
		, _cVer()
	{
	}

#if 0
	EdgeMask(int maxWidth, int maxHeight)
		: _tHor(maxWidth, maxHeight)
		, _tVer(maxWidth, maxHeight)
		, _cHor(maxWidth, maxHeight)
		, _cVer(maxWidth, maxHeight)
	{
	}
#endif

	EdgeMask(const EdgeMask &o)
		: _tHor(o._tHor)
		, _tVer(o._tVer)
		, _cHor(o._cHor)
		, _cVer(o._cVer)
	{
	}

	void resize(int width, int height)
	{
		_tHor.resize(width, height);
		_tVer.resize(width, height);
		_cHor.resize(width, height);
		_cVer.resize(width, height);
	}

	const BoolMatrix &getConstTHor(void) const { return _tHor; }
	const BoolMatrix &getConstTVer(void) const { return _tVer; }
	const BoolMatrix &getConstCHor(void) const { return _tHor; }
	const BoolMatrix &getConstCVer(void) const { return _tVer; }
	BoolMatrix &getTHor(void) { return _tHor; }
	BoolMatrix &getTVer(void) { return _tVer; }
	BoolMatrix &getCHor(void) { return _tHor; }
	BoolMatrix &getCVer(void) { return _tVer; }
private:
	BoolMatrix _tHor;
	BoolMatrix _tVer;
	BoolMatrix _cHor;
	BoolMatrix _cVer;
};

class LogoBuffer
{
public:
	LogoBuffer()
		: _frameNum(0)
		, _maxWidth(0)
		, _maxHeight(0)
		, _buf()
#if MULTI_EDGE_BUFFER
		, _horEdge()
		, _verEdge()
#endif
	{}

	LogoBuffer(size_t maxWidth, size_t maxHeight)
		: _frameNum(0)
		, _maxWidth(maxWidth)
		, _maxHeight(maxHeight)
		, _buf(maxWidth * maxHeight)
#if MULTI_EDGE_BUFFER
		, _horEdge(maxWidth, maxHeight)
		, _verEdge(maxWidth, maxHeight)
#endif
	{}

	LogoBuffer(const LogoBuffer &o)
		: _frameNum(o._frameNum)
		, _maxWidth(o._maxWidth)
		, _maxHeight(o._maxHeight)
		, _buf(o._buf)
#if MULTI_EDGE_BUFFER
		, _horEdge(o._horEdge)
		, _verEdge(o._verEdge)
#endif
	{}

	void resize(size_t width, size_t height)
	{
		_maxWidth = width;
		_maxHeight = height;
		_buf.resize(_maxWidth * _maxHeight);
#if MULTI_EDGE_BUFFER
		_horEdge.resize(_maxWidth, _maxHeight);
		_verEdge.resize(_maxWidth, _maxHeight);
#endif
	}
	int getFrameNumber(void) const { return _frameNum; }
	void setFrameNumber(int value) { _frameNum = value; }
	U8Vec &getBuffer(void) { return _buf; }
	const U8Vec &getConstBuffer(void) const { return _buf; }

private:
	int _frameNum;
	size_t _maxWidth;
	size_t _maxHeight;
	U8Vec _buf;

#if MULTI_EDGE_BUFFER
public:
	BoolMatrix &getHorEdge(void) { return _horEdge; }
	BoolMatrix &getVerEdge(void) { return _verEdge; }
	const BoolMatrix &getConstHorEdge(void) const { return _horEdge; }
	const BoolMatrix &getConstVerEdge(void) const { return _verEdge; }
private:
	BoolMatrix _horEdge;
	BoolMatrix _verEdge;
#endif

};

class LogoBuffers
{
public:
	LogoBuffers(size_t maxBuffers = 50)
		: _maxBuf(maxBuffers)
		, _maxWidth(0)
		, _maxHeight(0)
		, _newestIndex(-1)
		, _oldestIndex(0)
		, _isFull(false)
		, _logoBuf(maxBuffers)
	{
	}

#if 0
	LogoBuffers(size_t maxBuffers, size_t maxWidth, size_t maxHeight)
		: _maxBuf(maxBuffers)
		, _maxWidth(maxWidth)
		, _maxHeight(maxHeight)
		, _newestIndex(-1)
		, _oldestIndex(0)
		, _isFull(false)
		, _logoBuf(maxBuffers, LogoBuffer(_maxWidth, _maxHeight))
	{
	}
#endif

	LogoBuffers(const LogoBuffers &o)
		: _maxBuf(o._maxBuf)
		, _maxWidth(o._maxWidth)
		, _maxHeight(o._maxHeight)
		, _newestIndex(o._newestIndex)
		, _oldestIndex(o._oldestIndex)
		, _isFull(o._isFull)
		, _logoBuf(o._logoBuf)
	{
	}

	void resize(size_t numBuffers)
	{
		_maxBuf = numBuffers;
		_logoBuf.resize(_maxBuf, LogoBuffer(_maxWidth, _maxHeight));
	}

	void resize(size_t width, size_t height)
	{
		_maxWidth = width;
		_maxHeight = height;
                for (int i = 0; i < _logoBuf.size(); i++)
                    _logoBuf[i].resize(_maxWidth, _maxHeight);
		//for ( auto &o: _logoBuf )
		//	o.resize(_maxWidth, _maxHeight);
	}

	size_t getBufferCount(void) const { return _maxBuf; }
	size_t getMaxBufSize(void) const { return _maxWidth * _maxHeight; }
	bool getIsFull(void) const { return _isFull; }
	void setIsFull(bool value) { _isFull = value; }

	void checkIsFull(void)
	{
		if ( _isFull )
			return;

		if ( _newestIndex == _maxBuf - 1 )
			_isFull = true;
	}

	const LogoBuffer &getConstLogoBuffer(int index) const
	{
		return _logoBuf[index];
	}

	LogoBuffer &getLogoBuffer(int index)
	{
		return _logoBuf[index];
	}

	const U8Vec &getConstBuffer(int index) const
	{
		return _logoBuf[index].getConstBuffer();
	}

	U8Vec &getNewestBuffer(void)
	{
		const int index(_newestIndex >= 0 ? _newestIndex : 0);
		return _logoBuf[index].getBuffer();
	}

	const U8Vec &getConstNewestBuffer(void) const
	{
		const int index(_newestIndex >= 0 ? _newestIndex : 0);
		return _logoBuf[index].getConstBuffer();
	}

	void setNewestIndex(int value) { _newestIndex = value; }
	void setOldestIndex(int value) { _oldestIndex = value; }
	int getNewestIndex(void) const { return _newestIndex; }
	int getOldestIndex(void) const { return _oldestIndex; }

	void rotate(void)
	{
		++_newestIndex;

		if ( _newestIndex == _maxBuf )
			_newestIndex = 0;
	}

	int getFrameNumber(int index) const
	{
		return _logoBuf[index].getFrameNumber();
	}

	int getNewestFrameNumber(void) const
	{
		return _logoBuf[_newestIndex].getFrameNumber();
	}

	void setNewestFrameNumber(int value)
	{
		_logoBuf[_newestIndex].setFrameNumber(value);
	}

	int getOldestFrameNumber(void) const
	{
		return _logoBuf[_oldestIndex].getFrameNumber();
	}

private:
	int _maxBuf;
	int _maxWidth;
	int _maxHeight;
	int _newestIndex;
	int _oldestIndex;
	bool _isFull;
	std::vector<LogoBuffer> _logoBuf;
};

extern double fps;

extern bool live_tv;
extern bool sage_framenumber_bug;
extern bool sage_minute_bug;
extern bool cut_on_ar_change;
extern bool startOverAfterLogoInfoAvail;
extern bool secondLogoSearch;

//extern uint8_t *videoFramePointer;
extern int lastFrameWasSceneChange;
extern int skip_B_frames;
extern int standoff;
extern int max_repair_size;
extern int variable_bitrate;
extern int min_brightness_found;
extern int non_uniformity;
extern double min_show_segment_length;

extern int border;
extern int ticker_tape;
extern int ticker_tape_percentage;
extern int ignore_side;
extern int added_recording;
extern int volume_slip;

extern bool delete_show_after_last_commercial;
extern bool delete_show_before_first_commercial;
extern int delete_show_before_or_after_current;

extern char cuttermaran_options[];
extern char mpeg2schnitt_options[];
extern char avisynth_options[];
extern char dvrcut_options[];

extern bool ccCheck;
extern double cc_commercial_type_modifier;
extern double cc_wrong_type_modifier;
extern double cc_correct_type_modifier;

extern uint8_t ccData[];
extern int ccDataLen;

extern double black_percentile;
extern double uniform_percentile;
extern double score_percentile;
extern double logo_percentile;
extern double ar_delta;

class Histogram
{
public:
	Histogram()
		: uniform(256)
		, bright(256)
		, black(256)
		, volume(256)
		, silence(256)
		, logo(256)
	{
		clear(uniform);
		clear(bright);
		clear(black);
		clear(volume);
		clear(silence);
		clear(logo);
	}

	void clear(std::vector<int> &v)
	{
            for (int i = 0; i < v.size(); i++)
                v[i] = 0;
		//for ( auto &o : v )
		//	o = 0;
	}

	std::vector<int> uniform;
	std::vector<int> bright;
	std::vector<int> black;
	std::vector<int> volume;
	std::vector<int> silence;
	std::vector<int> logo;
};

extern int padding;
extern int remove_before;
extern int remove_after;

extern int delete_block_after_commercial;
extern int min_commercial_break_at_start_or_end;
extern int always_keep_first_seconds;
extern int always_keep_last_seconds;
extern bool intelligent_brightness;
extern double length_strict_modifier;
extern double length_nonstrict_modifier;
extern double combined_length_strict_modifier;
extern double combined_length_nonstrict_modifier;
extern double ar_wrong_modifier;
extern double excessive_length_modifier;
extern double dark_block_modifier;
extern double min_schange_modifier;
extern double max_schange_modifier;
extern double logo_present_modifier;
extern double punish_modifier;
extern double punish_threshold;
extern double reward_modifier;

extern bool punish_no_logo;
extern int punish;
extern int reward;

// If no logo is identified after x seconds into the show - give up.
extern int delay_logo_search;
extern double logo_max_percentage_of_screen;
extern int subtitles;
extern int logo_at_bottom;

extern double logo_threshold;
extern int logo_filter;
extern bool connect_blocks_with_logo;
extern double logo_fraction;
extern double shrink_logo;
extern int shrink_logo_tail;
extern int before_logo;
extern int after_logo;
extern int where_logo;

extern unsigned int min_black_frames_for_break;
extern int dvrmsstandoff;
// set to true to only mark breaks divisible by 5 as a commercial.
extern bool require_div5;
extern double div5_tolerance;
extern int incommercial_frames;
extern int videoredo_offset;
extern bool deleteLogoFile;
extern char windowtitle[];
extern bool enable_mencoder_pts;

extern int csBrightnessLast;

extern int volumeScale;
extern int framenum_real;
extern double last_ar_ratio;
extern int min_hasBright;
extern int min_dimCount;
extern const int schange_threshold;
extern double logoFreq; // times fps between logo checks
extern int frames_with_logo;
extern int framesprocessed;

extern int selected_audio_pid;
extern int selected_subtitle_pid;
extern int selected_video_pid;
extern int key;

#ifdef __unix__
#ifdef __linux__
static const char osname[] = "linux";
#else
static const char osname[] = "unix";
#endif
#else
extern char osname[];
#endif
extern int demux_asf;
extern int demux_pid;
extern int64_t goppos;
extern int64_t packpos;
extern int64_t headerpos;
extern struct stat instat;

extern int ms_audio_delay;
extern int framenum;
extern bool is_h264;
#ifdef PROCESS_CC
extern bool reorderCC;
#endif
#ifdef _WIN32
extern bool soft_seeking;
#endif
extern int xPos;
extern int yPos;
extern int lMouseDown;
extern int framenum_infer;
extern int soft_seeking;

#define AR_UNDEF 0.0

class Frame
{
public:
	Frame()
		: brightness(0)
		, schange_percent(100)
		, range()
		, uniform(0)
		, volume(0)
		, currentGoodEdge(0.0)
		, ar_ratio(AR_UNDEF)
		, logo_present(false)
		, commercial(false)
		, isblack(false)
		, goppos(0)
		, pts(0.0)
		, hasBright(0)
		, dimCount(0)
		, cutscenematch(0)
		, logo_filter(0.0)
		, xds(0)
#ifdef FRAME_WITH_HISTOGRAM
		, histogram(256, 0)
#endif
	{
	}

	Frame(const Frame &o)
		: brightness(o.brightness)
		, schange_percent(o.schange_percent)
		, range(o.range)
		, uniform(o.uniform)
		, volume(o.volume)
		, currentGoodEdge(o.currentGoodEdge)
		, ar_ratio(o.ar_ratio)
		, logo_present(o.logo_present)
		, commercial(o.commercial)
		, isblack(o.isblack)
		, goppos(o.goppos)
		, pts(o.pts)
		, hasBright(o.hasBright)
		, dimCount(o.dimCount)
		, cutscenematch(o.cutscenematch)
		, logo_filter(o.logo_filter)
		, xds(o.xds)
#ifdef FRAME_WITH_HISTOGRAM
		, histogram(o.histogram)
#endif
	{
	}

	int	brightness;
	int	schange_percent;
	Rectangle range;
	int uniform;
	int volume;
	double currentGoodEdge;
	double ar_ratio;
	bool logo_present;
	bool commercial;
	bool isblack;
	int64_t goppos;
	double pts;
	int hasBright;
	int dimCount;
	int cutscenematch;
	double logo_filter;
	int xds;
#ifdef FRAME_WITH_HISTOGRAM
	std::vector<int> histogram;
#endif
	//static int count;
	//static int max;
};

class ArBlockInfo
{
public:
	std::string getString(void) const
	{
		char buf[256];

		::snprintf(buf, sizeof(buf)
				, "Start: %6i\tEnd: %6i\tAR_R: %.2f\t"
					"Size[%4dx%4d] Bounds[%s]"
				, start
				, end
				, ar_ratio
				, width
				, height
				, range.getString().c_str());
		return buf;
	}

	int start;
	int end;
	double ar_ratio;
	int volume;
	int width;
	int height;
	Rectangle range;
};

class ArHistogram
{
public:
	int frames;
	double ar_ratio;
};

// Define CC types
#define CC_NONE			0
#define CC_ROLLUP		1
#define CC_POPON		2
#define CC_PAINTON		3
#define CC_COMMERCIAL	4


class CcBlock
{
public:
	CcBlock()
		: _startFrame(-1)
		, _endFrame(-1)
		, _type(CC_NONE)
	{}
	CcBlock(const CcBlock &o)
		: _startFrame(o._startFrame)
		, _endFrame(o._endFrame)
		, _type(o._type)
	{}

	int32_t _startFrame;
	int32_t _endFrame;
	int32_t _type;
};

class CcText
{
public:
#if 1
	CcText()
		: _startFrame(-1)
		, _endFrame(-1)
		, _text()
	{
	}

	CcText(const CcText &o)
		: _startFrame(o._startFrame)
		, _endFrame(o._endFrame)
		, _text(o._text)
	{
	}

	bool contains(std::string phrase) const
	{
		std::string text(_text);

		std::transform(text.begin(), text.end()
						, text.begin(), ::tolower);
		std::transform(phrase.begin(), phrase.end()
						, phrase.begin(), ::tolower);

		return text.find(phrase) != std::string::npos;
	}

	int _startFrame;
	int _endFrame;
	std::string _text;
#else
	CcText()
		: _startFrame(-1)
		, _endFrame(-1)
		, _textLen(0)
	{
		_text[0] = '\0';
	}

	CcText(const CcText &o)
		: _startFrame(o._startFrame)
		, _endFrame(o._endFrame)
		, _textLen(o._testLen)
	{
		::strncpy(_text, o._text, sizeof(_text));
	}

	bool contains(std::string phrase) const
	{
#warning need to implement this ignoring case
		return false;
	}

	int _startFrame;
	int _endFrame;
	int _textLen;
	uint8_t _text[256];
#endif
};

extern ArHistogram ar_histogram[MAX_ASPECT_RATIOS];

extern ArBlockInfo *ar_block;
extern int ar_block_count;
extern int max_ar_block_count;
extern void InitializeArBlockArray(int i);

void InitProcessLogoTest(void);

typedef std::pair<int, int> SceneChange;

class CommercialSkipper;

class FramesPerSecond
{
public:
	FramesPerSecond(CommercialSkipper *pcs)
		: pcs(pcs)
		, frameCounter(0)
		, tvBeg()
		, tvStart()
		, totalElapsed(0)
		, lastCount(0)
	{}

	void print(bool final);

private:

	CommercialSkipper *pcs;
	uint32_t frameCounter;
	struct timeval tvBeg;
	struct timeval tvStart;
	int totalElapsed;
	int lastCount;
};

// this is temporary, there is no reason to have a hard-coded max
#define MAX_SAVED_VOLUMES 10000

class Volumes
{
public:
	Volumes() : _map() {}

	void set(int f, int v)
	{
		_map[f] = v;
	}

	int get (int f)
	{
		try
		{
			return _map.at(f);
		}
		catch ( std::out_of_range &e )
		{
		}

		return -1;
	}

private:
	std::map<int,int> _map;
};

class CommercialSkipper
{
public:
	CommercialSkipper()
		: flagFiles()
		, _videoFramePointer(0)
		, _frameCount(0)
		, _frameIncrement(122880)
		, _frame(_frameIncrement)
		, _cc()
		, _lastCc()
		, _br()
		, _arRange()
		, _useFrameArray(true)
		, csBlack()
		, _commBreakSize()
		, _isLoadingCsv(false)
		, _isLoadingTxt(false)
		, _iniText()
		, _cutSceneFile()
		, _logoInfoAvailable(false)
		, _logoRange()
		, _tLogoRange()
		, _logoBlockCount(0)
		, _logoTrendCounter(0)
		, _framesWithLogo(0)
		, _lastLogoTest(false)
		, _curLogoTest(false)
		, _logoBlock(1024)
		, _verbose(0)
		, _isSecondPass(false)
		, _outputDirname()
		, _mpegFilename()
		, _basename()
		, _shortBasename()
		, _exeFilename()
		, _iniFilename()
		, _dictFilename()
		, _initLogoFilename()
		, _logoFilename()
		, _logFilename()
		, _workBasename()
		, _outBasename()
		, _outFilename()
		, _isMkv(false)
		, _videoWidth(0)
		, _width(0)
		, _height(0)
		, _lowRes(0)
		, _selfTest(0)
		, _threadCount(2)
		, _sceneChangeVec()
		, _cutScenes()
		, _cutSceneNo(0)
		, _playNice()
		, _histogram()
		, _mkvTimeOffset(0.0)
		, _edlOffset(0)
		, _framesPerSecond(this)
		, _edgeCount()
		, _edgeMask()
		, _hasLogo()
		, _edlMode(0) // 0=cut, 3=commercial break
		, _globalThreshold(1.05)
		, _validateSilence(true)
		, _validateUniform(true)
		, _validateSceneChange(true)
		, _maxBrightness(60)
		, _maxBright(1)
		, _testBrightness(40)
		, _maxAvgBrightness(19)
		, _brightnessJump(200)
		, _maxVolume(500)
		, _maxSilence(100)
		, _minSilence(12)
		, _noiseLevel(5)
		, _nonUniformity(500)
		, _aggressiveLogoRejection(0)
		, _logoBuffers(50)
		, _ccBlockCount(0)
		, _ccBlock(512)
		, _ccTextCount(0)
		, _ccText(512)
		, _lastCcType(CC_NONE)
		, _currentCcType(CC_NONE)
		, _mostCcType(CC_NONE)
		, _isCcOnScreen(false)
		, _isCcInMemory(false)
		, _edges()
		, _graph()
		, _bfRange()
		, _volumes()
	{
		initHasLogo();
	}

	~CommercialSkipper()
	{
	}

	void loadSettings(int argc, char **argv);
	int detectCommercials(int frameIndex, double pts);
	bool buildMasterCommList(void);
	bool getTimingUse(void) { return flagFiles.timing.use; }
	void setTimingUse(bool tf) { flagFiles.timing.use = tf; }
	bool getDebugWindowUse(void) { return flagFiles.debugwindow.use; }
	void setDebugWindowUse(bool tf) { flagFiles.debugwindow.use = tf; }
	bool setVideoFramePointer(uint8_t *p) { _videoFramePointer = p; return p; }
	uint8_t *getVideoFramePointer(void) { return _videoFramePointer; }
	void setFrameVolume(int f, int volume);
	void dumpAudio(const char *start, const char *end);
	void dumpVideo(const char *start, const char *end);
	void dumpData(const char *start, int length);
	void Debug(int level, const char *fmt, ...) __attribute__ ((__format__ (__printf__, 3, 4)));
	bool reviewResult(void);
	double getFps(void);
	void setFps(unsigned int fp);
	int getVerbose(void) const { return _verbose; }
	const std::string &getBasename(void) const { return _basename; }
	const std::string &getMpegFilename(void) const
	{
		return _mpegFilename;
	}
	int getVideoWidth(void) const { return _videoWidth; }
	void setVideoWidth(int w) { _videoWidth = w; }
	int getWidth(void) const { return _width; }

	bool checkVideoSize(int h, int w, int vw) const
	{
		return h == _height
				&& w == _width
				&& vw == _videoWidth;
	}

	void setVideoSize(int h, int w, int vw)
	{
		if ( h != _height || w != _width )
		{
			_edgeCount.resize(w, h);
			_edgeMask.resize(w, h);
			_hasLogo.resize(w, h);
			_logoBuffers.resize(w, h);

			if ( commDetectMethod & DET::LOGO )
				_br.resize(w * h);

			_graph.resize(w * h * 3);
			_bfRange.resize(h);
		}

		_height = h;
		_width = w;
		_videoWidth = vw;
	}

	void setWidth(int w) { _width = w; }
	int getHeight(void) const { return _height; }
	void setHeight(int h) { _height = h; }
	int getSelfTest(void) const { return _selfTest; }
	int getLowRes(void) const { return _lowRes; }
	int getThreadCount(void) const { return _threadCount; }
	FramesPerSecond &getFramesPerSecond(void) { return _framesPerSecond; }
private:
	void initComSkip(void);

	void initFrame(int i)
	{
		int size(_frame.size());

		// allow audio to run 1024 frames ahead of video
		if ( _frameCount + 1024 < size )
		{
			if ( i > 0 )
				_frame[i].xds = _frame[i-1].xds;

			return;
		}

		size += _frameIncrement;

		Debug(9, "Resizing frame array to accomodate %i frames.\n", size);

		_frame.resize(size);
		_frame[i].xds = _frame[i-1].xds;
	}

	void ensureLogoBlockSize(int i)
	{
		const int size(_logoBlock.size());

		if ( i < size && _logoBlockCount < size )
			return;

		int increase(0);

		if ( i >= size )
			increase = std::max(48, size - i);
		else if ( _logoBlockCount >= size )
			increase = 48;

		_logoBlock.resize(size + increase);
	}

	void initCcBlock(int i)
	{
		int size(_ccBlock.size());

		if ( i < size && _ccBlockCount < size )
			return;

		int increase(0);

		if ( i >= size )
			increase = std::max(128, size - i);
		else if ( _ccBlockCount >= size )
			increase = 128;

		_ccBlock.resize(size + increase);
	}

	void initCcText(int i)
	{
		int size(_ccText.size());

		if ( i < size && _ccTextCount < size )
			return;

		int increase(0);

		if ( i >= size )
			increase = std::max(128, size - i);
		else if ( _ccTextCount >= size )
			increase = 128;

		_ccText.resize(size + increase);
	}

	void initScanLines(void);
	void initHasLogo(void);
	void loadIniFile(void);
	void loadLogoMaskData(const std::string &filename);
	void processCsv(std::string csvFilename);
	void loadCutScene(const std::string &fname);
	void recordCutScene(int frameCount, int brightness);
	double FindNumber(const char *str1
		, const char *str2
		, double v);
	const char *FindString(const char *str1
		, const char *str2
		, const char *v);
	void dumpEdgeMasks(void);
	void dumpEdgeMask(uint8_t *buffer, int direction);
	void findLogoThreshold(void);
	bool searchForLogoEdges(void);

	void insertBlackFrame(int b, int u, int v, int fid, int c);
	void insertBlackFrame(const Frame &f, int fid, int c);
	void insertBlackFrame(int fid, int c);
	double validateBlackFrames(int reason, double ratio, int remove);
	void recalc(void);
	void outputFrameArray(bool screenOnly);
	bool buildBlocks(bool recalc);
	void printLogoFrameGroups(void);
	void weighBlocks(void);
	void printCcBlocks(void);
	bool outputBlocks(void);
	void outputAspect(void);
	void outputTraining(void);
	bool outputCleanMpg(void);
	void outputStrict(double len, double delta, double tol);
	void outputFrame(int frame_number);
	void outputHistogram(int *histogram, int scale
		, const char *title, bool truncate);
	void outputLogoHistogram(int buckets);
	void outputBrightHistogram(void);
	void outputUniformHistogram(void);
	void cleanLogoBlocks(void);
	void outputCommercialBlock(int i, int prev
		, int start, int end, bool last);
	void buildCommercial(void);
	bool checkSceneHasChanged(void);
	void addXds(uint8_t hi, uint8_t lo);
	void initXdsBlock(void);
	void addXdsBlock(void);
	void addNewCcBlock(int current_frame, int type);
	int determineCcTypeForBlock(int start, int end);
	int inputReffer(const char *extension, bool setfps);
	void initProcessLogoTest(void);
	bool processLogoTest(int realFrameNum, bool curLogoTest, int close);
	void initProcessArInfo(Rectangle);
	void processArInfo(Rectangle);
	void openOutputFiles(void);
	void closeDump(void);
	bool isStandardCommercialLength(double length
					, double tolerance
					, bool strict);
	int findBlackThreshold(double percentile);
	int findUniformThreshold(double percentile);
	double findScoreThreshold(double percentile);
	void buildCommListAsYouGo(void);
	void fillArHistogram(bool refill);
	void resetLogoBuffers(void);
	void saveLogoMaskData(void);
	void initLogoBuffers(void);
	void initializeBlockArray(int i);
	void initializeLogoBlockArray(int i);
	void initializeArBlockArray(int i);
	void outputCcBlock(int i);
	bool processCcDict(void);
	void saveVolume (int f, int v);
	int retrieveVolume(int f);

	double calculateLogoFraction(int start, int end);
	bool checkFramesForLogo(int start, int end);
	bool checkFrameForLogo(int i);
	double checkStationLogoEdge(const uint8_t *frame);
	double checkStationLogoEdge0(const uint8_t *frame);
	double checkStationLogoEdge1(const uint8_t *frame);
	double checkStationLogoEdge2(const uint8_t *frame);
	double checkStationLogoEdge3(const uint8_t *frame);
	double checkStationLogoEdge4(const uint8_t *frame);
	int matchCutScene(const CutScene &cs);
	void edgeDetect(void);
	double doubleCheckStationLogoEdge(const U8Vec &frame);
	double doubleCheckStationLogoEdge0(const U8Vec &frame);
	double doubleCheckStationLogoEdge1(const U8Vec &frame);
	double doubleCheckStationLogoEdge2(const U8Vec &frame);
	double doubleCheckStationLogoEdge3(const U8Vec &frame);
	double doubleCheckStationLogoEdge4(const U8Vec &frame);
	void fillLogoBuffer(void);
	void findIniFile(void);
	int clearEdgeMaskArea(BoolMatrix &temp, const BoolMatrix &test);
	void setEdgeMaskArea(const BoolMatrix &temp);
	int countEdgePixels(void);
	int countSceneChanges(int StartFrame, int EndFrame);

	FlagFiles flagFiles;
	uint8_t *_videoFramePointer;
	int _frameCount;
	const int _frameIncrement;
	std::vector<Frame> _frame;

	void addCc(int i);
	CcPacket _cc;
	CcPacket _lastCc;

	std::vector<MinMaxBright> _br;
	Rectangle _arRange; // for aspect ratio calculations

	bool _useFrameArray;
	BlackVec csBlack;
	CommercialBreakSize _commBreakSize;
	bool _isLoadingCsv;
	bool _isLoadingTxt;
	std::string _iniText;
	std::string _cutSceneFile[9];
	bool _logoInfoAvailable;
	Rectangle _logoRange;
	Rectangle _tLogoRange;
	int _logoBlockCount;
	int _logoTrendCounter;
	int _framesWithLogo;
	bool _lastLogoTest;
	bool _curLogoTest;
	std::vector<LogoBlockInfo> _logoBlock;
	int _verbose;
	bool _isSecondPass;

	std::string _outputDirname;
	std::string _mpegFilename;
	std::string _basename;
	std::string _shortBasename;
	std::string _exeFilename;
	std::string _iniFilename;
	std::string _dictFilename;
	std::string _initLogoFilename;
	std::string _logoFilename;
	std::string _logFilename;
	std::string _workBasename;
	std::string _outBasename;
	std::string _outFilename;
	bool _isMkv;
	int _videoWidth;
	int _width;
	int _height;
	int _lowRes;
	int _selfTest;
	int _threadCount;
	std::vector<SceneChange> _sceneChangeVec;
	CutScenes _cutScenes;
	int _cutSceneNo;
	PlayNice _playNice;
	Histogram _histogram;
	double _mkvTimeOffset;
	int _edlOffset;
	FramesPerSecond _framesPerSecond;
	HorVerMatrix _edgeCount;
	EdgeMask _edgeMask;
	BoolMatrix _hasLogo;
	int _edlMode;
	double _globalThreshold;
	bool _validateSilence;
	bool _validateUniform;
	bool _validateSceneChange;

	/*
	 * frame not black if any pixels checked are greater
	 * than this (scale 0 to 255)
	 */
	int _maxBrightness;
	int _maxBright;
	/*
	 * frame not pure black if any pixels are greater than this,
	 * will check average
	 */
	int _testBrightness;
	/*
	 * maximum average brightness for a dim frame
	 * to be considered black (scale 0 to
	 */
	int _maxAvgBrightness;
	int _brightnessJump;
	int _maxVolume;
	int _maxSilence;
	int _minSilence;
	int _noiseLevel;
	int _nonUniformity;
	int _aggressiveLogoRejection;
	LogoBuffers _logoBuffers;
	int _ccBlockCount;
	std::vector<CcBlock> _ccBlock;
	int _ccTextCount;
	std::vector<CcText> _ccText;
	int _lastCcType;
	int _currentCcType;
	int _mostCcType;
	bool _isCcOnScreen;
	bool _isCcInMemory;
	Edges _edges;
	std::vector<uint8_t> _graph;
	/* Area to include for black frame detection, non logo area */
	std::vector<MinMax> _bfRange;
	Volumes _volumes;
};

/* implemented in mpeg2dec.cpp */
extern void decodeOnePicture(FILE *f, double pts, CommercialSkipper *);

} // CS namespace

#ifdef _WIN32
extern void gettimeofday (struct timeval *tp, void *dummy);
#endif

