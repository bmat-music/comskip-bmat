#pragma once

#ifndef COMSKIP
#define COMSKIP
#define SUBVERSION "000"
#endif

#define USE_NEW_CPP_CONSTRUCTORS 0
#define USE_RANGE_CHECKING 1
#define MULTI_EDGE_BUFFER 0

#ifdef __GNUC__
#include <features.h>
#if __GNUC_PREREQ(4,8)
#undef USE_NEW_CPP_CONSTRUCTORS
#define USE_NEW_CPP_CONSTRUCTORS 1
#else
#if __GNUC_PREREQ(4,7)
#ifdef __GNUC_PATCHLEVEL__
#if __GNUC_PATCHLEVEL__ >= 2
#undef USE_NEW_CPP_CONSTRUCTORS
#define USE_NEW_CPP_CONSTRUCTORS 1
#endif
#endif
#endif
#endif
#endif

