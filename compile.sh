mkdir -p /var/tmp/ComSkipWork
cp argtable2-13.tar.gz /var/tmp/ComSkipWork
cp ffmpeg-2.0.tar.bz2 /var/tmp/ComSkipWork
cp -r comskip-bmat-1.0 /var/tmp/ComSkipWork
cd /var/tmp/ComSkipWork
tar xf argtable2-13.tar.gz 
cd argtable2-13
./configure --prefix=/var/tmp/ComSkipWork/argtable2-install --disable-shared --enable-static
make -j8
make install
cd ..
tar xf ffmpeg-2.0.tar.bz2
cd ffmpeg-2.0
./configure --prefix=/var/tmp/ComSkipWork/ffmpeg-2.0-install
make -j8
make install
echo "Compiling comskip..."
cd ..
cd comskip-bmat-1.0
pwd
PKG_CONFIG_PATH=/var/tmp/ComSkipWork/ffmpeg-2.0-install/lib/pkgconfig:/var/tmp/ComSkipWork/argtable2-install/lib/pkgconfig ./configure --prefix=/usr
make -j8
echo ***************************************************************************
echo                     =================
echo .
echo You have compiled comskip at /var/tmp/ComSkipWork/comskip-bmat-1.0/comskip
echo .
echo                     =================
echo ***************************************************************************
#make install
